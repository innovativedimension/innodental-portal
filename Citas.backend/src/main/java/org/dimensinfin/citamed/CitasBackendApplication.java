//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.3
//  SITE:        backcitas.herokuapp.com
//  DESCRIPTION: CitasBackend. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import javax.persistence.EntityManager;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;
import org.dimensinfin.citamed.conf.FileSystemSBImplementation;
import org.dimensinfin.citamed.conf.GlobalSBConfigurationProvider;
import org.dimensinfin.citamed.conf.IConfigurationProvider;
import org.dimensinfin.citamed.conf.IFileSystem;
import org.dimensinfin.citamed.conf.MetricsComponent;
import org.dimensinfin.citamed.entities.Centro;
import org.dimensinfin.citamed.entities.Cita;
import org.dimensinfin.citamed.repository.CentroRepository;
import org.dimensinfin.citamed.security.ISessionManager;
import org.dimensinfin.citamed.security.SessionManager;
import org.dimensinfin.citamed.security.SessionManagerRedisson;
import org.dimensinfin.citamed.services.AppointmentCreationService;
import org.dimensinfin.citamed.services.SeedDataCreationService;

/**
 * This is the main class for the backend service. The Citas.backend application will store and manage doctor's appointments inside a
 * backend repository that is directly connected to this subsystem. The springboot microservice only stores some session information
 * inside a distributed Redis repository so an indeterminate number of instances can be deployed to increase the processing power.
 *
 * Current implementation covers the funtionality up to version 0.8.x.
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@EnableScheduling
@EnableJpaAuditing
@RestController
@SpringBootApplication
//@EnableApiStatistics
public class CitasBackendApplication {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("CitasBackendApplication");
	private static GlobalSBConfigurationProvider configurationProvider = null;
	private static String applicationFolder = getConfiguration().getResourceString("P.runtime.application.datafolder"
			, "./backend-aplication");
	private static IFileSystem fileSystemIsolation = null;
	public static ISessionManager sessionManager;
	public static RedissonClient redisson;

	public static IConfigurationProvider getConfiguration() {
		if ( null == configurationProvider ) {
			// Create a new configuration provider and read the properties.
			logger.info("-- [CitasBackendApplication.<static>]> Connecting the Configuration Manager...");
			configurationProvider = new GlobalSBConfigurationProvider("properties");
		}
		return configurationProvider;
	}


	// - S E S S I O N   M A N A G E M E N T
	public static ISessionManager getSessionManager() {
		return sessionManager;
	}

	public static void installFileSystem( final IFileSystem newfileSystem ) {
		fileSystemIsolation = newfileSystem;
	}

	protected static IFileSystem getFileSystem() {
		if ( null != fileSystemIsolation ) return fileSystemIsolation;
		else throw new RuntimeException("File System isolation layer is not installed.");
	}

	public static InputStream openResource4Input( final String filePath ) throws IOException {
		return getFileSystem().openResource4Input(filePath);
	}

	public static OutputStream openResource4Output( final String filePath ) throws IOException {
		return getFileSystem().openResource4Output(filePath);
	}

	// - M A I N   S E C T I O N
	public static void main( String[] args ) {
		logger.info(">> [CitasBackendApplication.main]");
		// Connect the file system to be able to read the assets and other application resources stored externally.
		logger.info("-- [CitasBackendApplication.main]> Connecting the File System to Global...");
		CitasBackendApplication.installFileSystem(new FileSystemSBImplementation("./backend.application")
		);

		// Connect the Configuration manager.
		logger.info("-- [CitasBackendApplication.main]> Connecting the Configuration Manager...");
		configurationProvider = new GlobalSBConfigurationProvider("properties");

		logger.info("-- [CitasBackendApplication.main]> Connecting the Session store...");
		if ( configurationProvider.getBooleanResource("R.runtime.session.redisson.active") ) {
			// Initialize Redisson redis session manager.
			String redisUriString = System.getenv("REDIS_URL");
			logger.info("-- [CitasBackendApplication.main]> Redis URL: {}", redisUriString);
			if ( redisUriString.equalsIgnoreCase("-USE-SESSION-") ) {
				// Read saved session data.
				sessionManager = new SessionManager();
				CitasBackendApplication.getSessionManager().readSessionData();
			} else {
				Config config = new Config();
				SingleServerConfig serverConfig = config.useSingleServer()
						.setAddress(redisUriString)
						.setConnectionPoolSize(5)
						.setConnectionMinimumIdleSize(2)
						.setTimeout(5000);
				// Authentication
				final String[] redisData = redisUriString.split(":");
				final String[] redisPass = redisData[2].split("@");

				if ( null != redisUriString ) {
					logger.info("-- [CitasBackendApplication.main]> Redis password: {}", redisPass[0]);
					serverConfig.setPassword(redisPass[0]);
				}
				redisson = Redisson.create(config);
				sessionManager = new SessionManagerRedisson();
			}
		} else {
			// Read saved session data.
			sessionManager = new SessionManager();
			CitasBackendApplication.getSessionManager().readSessionData();
		}

		logger.info("-- [CitasBackendApplication.main]> Starting application instance...");
		SpringApplication.run(CitasBackendApplication.class, args);
	}

	// - S E R I A L I Z E R S   S E C T I O N
	public static final ObjectMapper jsonMapper = new ObjectMapper();

	static {
		jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
		// Add our own serializers.
		SimpleModule neocomSerializerModule = new SimpleModule();
		neocomSerializerModule.addSerializer(AppointmentCreationService.AppointmentReport.class, new AppointmentReportSerializer());
		jsonMapper.registerModule(neocomSerializerModule);
	}

	public static class AppointmentReportSerializer extends JsonSerializer<AppointmentCreationService.AppointmentReport> {
		@Override
		public void serialize( final AppointmentCreationService.AppointmentReport value, final JsonGenerator jgen, final SerializerProvider provider )
				throws IOException, JsonProcessingException {
			jgen.writeStartObject();
			jgen.writeObjectField("reportdate", value.reportdate);
			jgen.writeNumberField("previousSlotCount", value.previousSlotCount);
			jgen.writeNumberField("newSlotCount", value.newSlotCount);
			jgen.writeNumberField("collisionCount", value.collisions.size());
			jgen.writeObjectField("collisions", value.collisions);
			jgen.writeStringField("comment", value.comment);
			jgen.writeEndObject();
		}
	}

//	public static class MinuteIntervalSerializer extends JsonSerializer<MinuteInterval> {
//		@Override
//		public void serialize(final MinuteInterval value, final JsonGenerator jgen, final SerializerProvider provider)
//				throws IOException, JsonProcessingException {
//			jgen.writeStartObject();
//			jgen.writeNumberField("startMinute", value.start());
//			jgen.writeNumberField("endMinute", value.end());
//			jgen.writeEndObject();
//		}
//	}

	public static class CitaSerializer extends JsonSerializer<Cita> {
		@Override
		public void serialize( final Cita value, final JsonGenerator jgen, final SerializerProvider provider )
				throws IOException, JsonProcessingException {
			jgen.writeStartObject();
			jgen.writeNumberField("id", value.getId());
			jgen.writeStringField("medicoIdentifier", value.getMedico().getId());
			jgen.writeObjectField("fecha", value.getFecha());
			jgen.writeNumberField("huecoId", value.getHuecoId());
			jgen.writeNumberField("huecoDuracion", value.getHuecoDuracion());
			jgen.writeStringField("estado", value.getEstado());
			jgen.writeStringField("pacienteLocalizador", value.getPacienteLocalizador());
			jgen.writeEndObject();
		}
	}

	// - C O M M A N D L I N E   R U N N E R S
	@Component
	@Transactional
	public static class CommandLineAppStartupRunner implements CommandLineRunner {
		@Autowired
		protected SeedDataCreationService creationService;
		@Autowired
		protected CentroRepository centroRepository;

		@Override
		public void run( String... args ) throws Exception {
			logger.info(">> [CitasBackendApplication.CommandLineAppStartupRunner]> Application started. Executing database check and " +
					"initialization");
			if ( CitasBackendApplication.getConfiguration()
					.getBooleanResource("R.runtime.database.initializeseeddata") ) {
				// Check the number of Centros already on the database.
				final List<Centro> centros = centroRepository.findAll();
				if ( centros.size() < 1 ) {
					// Do initial data loading.
					creationService.loadCentroSeedData();
					creationService.loadCredencialSeedData();
					creationService.loadMedicoSeedData();
					creationService.loadServiceSeedData();
				}
			}
			logger.info("<< [CitasBackendApplication.CommandLineAppStartupRunner]");
		}
	}

	// - G L O B A L   A C C E S S
	@Autowired
	protected static SeedDataCreationService creationService;

	public static SeedDataCreationService getService() {
		return creationService;
	}

	// - S C H E D U L E R S   S E C T I O N
	@Autowired
	@Qualifier("SeedData")
	protected SeedDataCreationService appointmentsService;

	/**
	 * Run the BLOQUEADA appointments every minute. The configuration says that the blocked appointments exceeding the configured delay
	 * should be removed by putting again on the database as LIBRE.
	 * Runs every 60 seconds.
	 * If is disabled by a configuration flag.
	 */
	@Scheduled(initialDelay = 60000, fixedDelay = 60000)
	private void runRemoveBlockedAppointments() {
		if ( CitasBackendApplication.getConfiguration()
				.getResourceString("R.runtime.scheduler.activate.blockedremoval").equalsIgnoreCase("true") ) {
			final Instant timer = Instant.now();
			try {
				appointmentsService.removeBlockedAppointments();
			} finally {
				logger.info("-- [CitasBackendApplication.runRemoveBlockedAppointments]> TIMING: {}",
						Duration.between(timer, Instant.now()) + "ms");
			}
		}
	}

//	@Scheduled(initialDelay = 60000, fixedDelay = 900000)
//	private void runCreateAdditionalMedico() {
//		if ( CitasBackendApplication.getConfiguration()
//				.getResourceString("R.runtime.scheduler.activate.newmedicogeneration").equalsIgnoreCase("true") ) {
//			final Instant timer = Instant.now();
//			try {
//				appointmentsService.createAdditionalMedico();
//			} finally {
//				logger.info("-- [CitasBackendApplication.runRemoveBlockedAppointments]> TIMING: {}",
//						Duration.between(timer, Instant.now()) + "ms");
//			}
//		}
//	}

	/**
	 * Write down session data. This has a configuration flag to be anabled and only has sense on local development.
	 * Timing is every 60 seconds.
	 */
	@Scheduled(initialDelay = 60000, fixedDelay = 60000)
	private void writeSessionData() {
		if ( CitasBackendApplication.getConfiguration().getBooleanResource("R.runtime.session.savedata", true) )
			CitasBackendApplication.getSessionManager().writeSessionData();
	}

	@Autowired
	private EntityManager entityManager;

	@Scheduled(initialDelay = 10000, fixedDelay = 60000)
	@Transactional
	private void collectAppointmentsCount() {
		if ( CitasBackendApplication.getConfiguration()
				.getBooleanResource("R.runtime.scheduler.activate.metrics.appointmentcounts", true) ) {
			if ( null != entityManager ) {
				// Count the different types of appontments and store them into metrics.
				List<Object[]> counters = entityManager.createQuery(
						"SELECT ct.estado, COUNT(ct.estado) FROM Citas ct GROUP BY ct.estado")
						.getResultList();

				counters.stream().forEach(( record ) -> {
					String name = (String) record[0];
					Long count = ((Long) record[1]).longValue();
					if ( name.equalsIgnoreCase("LIBRE") ) {
						logger.info("-- [CitasBackendApplication.collectAppointmentsCount]> Appointments LIBRE: {}", count);
						MetricsComponent.libreAppointments.set(count);
					}
					if ( name.equalsIgnoreCase("RESERVADA") ) {
						logger.info("-- [CitasBackendApplication.collectAppointmentsCount]> Appointments RESERVADA: {}", count);
						MetricsComponent.reservadaAppointments.set(count);
					}
					if ( name.equalsIgnoreCase("BLOQUEADA") ) {
						logger.info("-- [CitasBackendApplication.collectAppointmentsCount]> Appointments BLOQUEADA: {}", count);
						MetricsComponent.bloqueadaAppointments.set(count);
					}
					if ( name.equalsIgnoreCase("DESCANSO") ) {
						logger.info("-- [CitasBackendApplication.collectAppointmentsCount]> Appointments DESCANSO: {}", count);
						MetricsComponent.descansoAppointments.set(count);
					}
					if ( name.equalsIgnoreCase("VACACIONES") ) {
						logger.info("-- [CitasBackendApplication.collectAppointmentsCount]> Appointments VACACIONES: {}", count);
						MetricsComponent.vacacionesAppointments.set(count);
					}
				});
			}
		}
	}


	// - R E D I S S O N
//	@EnableRedissonHttpSession
//	public class RedissonConfig {
//
//		@Bean
//		public RedissonClient redisson() {
//			return Redisson.create();
//		}
//	}

	// - M E T R I C S
//	@Bean
//	MeterRegistryCustomizer<MeterRegistry> metricsCommonTagsApplication() {
//		return registry -> registry.config().commonTags("application", "Citas.backend");
//	}
//
//	@Bean
//	MeterRegistryCustomizer<MeterRegistry> metricsCommonTagsInstance() {
//		return registry -> registry.config().commonTags("instance", "01");
//	}
//
//	@Bean
//	MeterRegistryCustomizer<MeterRegistry> metricsCommonTagsVersion() {
//		return registry -> registry.config().commonTags("appVersion", "v0.8.2");
//	}
//
//	@Bean
//	MeterRegistryCustomizer<MeterRegistry> metricsCommonTagsBrand() {
//		return registry -> registry.config().commonTags("appBrand", "Demo");
//	}
}
// - UNUSED CODE ............................................................................................
//[01]
