//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.conf;

import java.io.IOException;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public abstract class GlobalConfigurationProvider implements IConfigurationProvider {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("GlobalConfigurationProvider");
	private static final String DEFAULT_PROPERTIES_FOLDER = "properties";

	// - F I E L D - S E C T I O N ............................................................................
	protected Properties globalConfigurationProperties = new Properties();
	private String configuredPropertiesFolder = DEFAULT_PROPERTIES_FOLDER;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public GlobalConfigurationProvider(final String propertiesFolder) {
		if ( null != propertiesFolder ) configuredPropertiesFolder = propertiesFolder;
		initialize();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getResourceString(final String key) {
		try {
			return globalConfigurationProperties.getProperty(key);
		} catch ( MissingResourceException mre ) {
			return '!' + key + '!';
		}
	}

	public String getResourceString(final String key, final String defaultValue) {
		try {
			return globalConfigurationProperties.getProperty(key, defaultValue);
		} catch ( MissingResourceException mre ) {
			return '!' + key + '!';
		}
	}

	public Long getNumberResource(final String key) {
		try {
			final String resource = globalConfigurationProperties.getProperty(key);
			// Parse the resource into a integer number.
			return Long.parseLong(resource);
		} catch ( MissingResourceException mre ) {
			return 0l;
		} catch ( NumberFormatException nfe ) {
			return 0l;
		}
	}

	public Long getNumberResource(final String key, final Long defaultValue) {
		try {
			final String resource = globalConfigurationProperties.getProperty(key, defaultValue.toString());
			// Parse the resource into a integer number.
			return Long.parseLong(resource);
		} catch ( MissingResourceException mre ) {
			return defaultValue;
		} catch ( NumberFormatException nfe ) {
			return defaultValue;
		}
	}

	public boolean getBooleanResource(final String key) {
		final String resource = globalConfigurationProperties.getProperty(key);
		if ( null == resource ) return false;
		if ( resource.equalsIgnoreCase("true") ) return true;
		if ( resource.equalsIgnoreCase("on") ) return true;
		return false;
	}

	public boolean getBooleanResource(final String key, final boolean defaultValue) {
		final String resource = globalConfigurationProperties.getProperty(key);
		if ( null == resource ) return defaultValue;
		if ( resource.equalsIgnoreCase("true") ) return true;
		if ( resource.equalsIgnoreCase("on") ) return true;
		return false;
	}

	/**
	 * Ths initialization method reads all the files located on a predefined folder under the src/main/resources path.
	 * All the files are expected to be Properties files and are read in alphabetical order and their contents added
	 * to the list of application properties. Read order will replace same ids with new data so the developer
	 * can use a naming convention to replace older values with new values without editing the older files.
	 */
	public GlobalConfigurationProvider initialize() {
		try {
			readAllProperties();
		} catch ( IOException ioe ) {
			logger.error("E [GlobalConfigurationProvider.initialize]> Unprocessed exception: {}", ioe.getMessage());
			ioe.printStackTrace();
		}
		return this;
	}

	public int contentCount() {
		return globalConfigurationProperties.size();
	}

	protected String getResourceLocation() {
		return configuredPropertiesFolder;
	}

	@Override
	public String toString() {
		return new StringBuffer("GlobalConfigurationProvider[")
				.append("Property count: ").append(contentCount()).append(" ")
				.append("]")
				.toString();
	}

	//--- P L A T F O R M   S P E C I F I C   S E C T I O N
	protected abstract void readAllProperties() throws IOException;

	protected abstract List<String> getResourceFiles(String path) throws IOException;
}

// - UNUSED CODE ............................................................................................
//[01]
