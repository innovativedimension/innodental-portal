//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.conf;

public interface IConfigurationProvider {
	public IConfigurationProvider initialize();

	public int contentCount();

	public String getResourceString(final String key);

	public String getResourceString(final String key, final String defaultValue);

	Long getNumberResource(final String key);

	Long getNumberResource(final String key, final Long defaultValue);

	boolean getBooleanResource(final String key);

	boolean getBooleanResource(final String key, final boolean defaultValue);
}
