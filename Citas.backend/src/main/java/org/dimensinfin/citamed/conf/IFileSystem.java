//  PROJECT:     NeoCom.DataManagement(NEOC.DTM)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2013-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Java 1.8 Library.
//  DESCRIPTION: NeoCom project library that comes from the old Models package but that includes much more
//               functionality than the model definitions for the Eve Online NeoCom application.
//               If now defines the pure java code for all the repositories, caches and managers that do
//               not have an specific Android implementation serving as a code base for generic platform
//               development. The architecture model has also changed to a better singleton/static
//               implementation that reduces dependencies and allows separate use of the modules. Still
//               there should be some initialization/configuration code to connect the new library to the
//               runtime implementation provided by the Application.
package org.dimensinfin.citamed.conf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

// - CLASS IMPLEMENTATION ...................................................................................
public interface IFileSystem {
	public InputStream openResource4Input( final String filePath ) throws IOException;

	public OutputStream openResource4Output( String filePath ) throws IOException;

	public InputStream openAsset4Input( final String filePath ) throws IOException;

	public String accessAsset4Path( final String filePath ) throws IOException;

	public String accessResource4Path( final String filePath );

//	public String accessAppStorage4Path( final String filePath );

//	public String accessAssetPath();
}
