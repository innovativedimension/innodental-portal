//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.conf;

import java.time.Duration;
import java.time.Instant;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.dimensinfin.citamed.CitasBackendApplication;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class LogInterceptor implements HandlerInterceptor {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static Logger logger = LoggerFactory.getLogger("LogInterceptor");

	// - F I E L D - S E C T I O N ............................................................................
	private String requestPath;
	private Instant timer;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean preHandle( final HttpServletRequest request, final HttpServletResponse response, final Object handler ) {
		requestPath = request.getRequestURI();
		timer = Instant.now();
		if ( CitasBackendApplication.getConfiguration().getBooleanResource("R.runtime.request.headers") ) {
			// Dump to logger almost all headers.
			final Enumeration<String> headerNames = request.getHeaderNames();
			while ( headerNames.hasMoreElements() ) {
				final String header = headerNames.nextElement();
				final String headerValue = request.getHeader(header);
				if ( header.equalsIgnoreCase("xapp-authentication") ) continue;
				logger.info("[REQUEST]> HeaderData: {}={}", header, headerValue);
			}
		} else logger.info("[REQUEST]> Configuration requests to skip headers.");
		return true;
	}

	@Override
	public void afterCompletion( final HttpServletRequest request, final HttpServletResponse response, final Object handler, final Exception ex ) {
		logger.info("[TIMING]> {}", Duration.between(this.timer, Instant.now()).toMillis() + "ms");
	}
}

// - UNUSED CODE ............................................................................................
//[01]
