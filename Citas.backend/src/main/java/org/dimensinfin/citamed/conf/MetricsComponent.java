//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.3
//  SITE:        backcitas.herokuapp.com
//  DESCRIPTION: CitasBackend. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.conf;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.MeterRegistry;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Component
public class MetricsComponent {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("MetricsComponent");
	public static MeterRegistry metricsRegistry;
	public static AtomicLong libreAppointments;
	public static AtomicLong reservadaAppointments;
	public static AtomicLong bloqueadaAppointments;
	public static AtomicLong descansoAppointments;
	public static AtomicLong vacacionesAppointments;

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public MetricsComponent( final MeterRegistry registry ) {
		this.metricsRegistry = registry;
		libreAppointments = this.metricsRegistry.gauge("appointments.libre", new AtomicLong(0));
		reservadaAppointments = this.metricsRegistry.gauge("appointments.reservada", new AtomicLong(0));
		bloqueadaAppointments = this.metricsRegistry.gauge("appointments.bloqueada", new AtomicLong(0));
		descansoAppointments = this.metricsRegistry.gauge("appointments.descanso", new AtomicLong(0));
		vacacionesAppointments = this.metricsRegistry.gauge("appointments.vacaciones", new AtomicLong(0));
	}

	// - M E T H O D - S E C T I O N ..........................................................................
}

// - UNUSED CODE ............................................................................................
//[01]
