//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.3
//  SITE:        backcitas.herokuapp.com
//  DESCRIPTION: CitasBackend. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.conf;

import java.time.Duration;
import java.time.Instant;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;

import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.Timer;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class MetricsInterceptor implements HandlerInterceptor {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("MetricsInterceptor");

	// - F I E L D - S E C T I O N ............................................................................
	private Instant timer = Instant.now();
	private String requestPath;
	protected Tags basetags;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public boolean preHandle( final HttpServletRequest request, final HttpServletResponse response, final Object handler ) {
		this.timer = Instant.now();
		requestPath = request.getRequestURI();
		this.basetags = Tags.empty()
				.and("entrypoint", requestPath);
		return true;
	}

	@Override
	public void afterCompletion( final HttpServletRequest request, final HttpServletResponse response, final Object handler, final Exception ex ) {
		final Timer elapsed = Timer
				.builder("entry.point.elapsed")
				.tags(basetags)
				.register(MetricsComponent.metricsRegistry);
		elapsed.record(Duration.between(this.timer, Instant.now()));
	}
}

// - UNUSED CODE ............................................................................................
//[01]
