//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.controller;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.dimensinfin.citamed.entities.Centro;
import org.dimensinfin.citamed.entities.Cita;
import org.dimensinfin.citamed.entities.Medico;
import org.dimensinfin.citamed.repository.CentroRepository;
import org.dimensinfin.citamed.repository.CitaRepository;
import org.dimensinfin.citamed.repository.MedicoRepository;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@RestController
@Component
@CrossOrigin
public class CentroController extends BaseController {
	// - S T A T I C - S E C T I O N ..........................................................................

	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	protected CentroRepository centroRepository;
	@Autowired
	protected MedicoRepository medicoRepository;
	@Autowired
	protected CitaRepository citaRepository;

	protected MeterRegistry metricsRegistry;
	protected Tags basetags;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
//	CentroController( MeterRegistry registry ) {
//		this.metricsRegistry = registry;
//		// Register the metrics for this controller.
//		this.basetags = Tags.empty()
//				.and("entrypoint", "/v1/centros")
//				.and("verb", "medicos");
////		registry.gauge("dictionary.size", basetags, this.words);
//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@GetMapping("/v1/centros")
	public Page<Centro> getCentros( Pageable pageable ) {
		return centroRepository.findAll(pageable);
	}

	/**
	 * Get the complete list of <b>Medicos</b> related to a <b>Centro</b>.
	 * The first appointment is not stored on the record. So when retrieving the data we sould access the appointment record to
	 * complete this information.
	 * @param centroId the unique identifier for the center of reference for the search.
	 * @return the list of <b>Medicos</b> that are adhered to this <b>Centro</b>.
	 */
	@GetMapping("/v1/centros/{centroId}/medicos")
	public ResponseEntity<List<Medico>> getMedicosByCentroId( HttpServletResponse response,
	                                                          @PathVariable Long centroId ) {
		return (ResponseEntity<List<Medico>>) centroRepository.findById(centroId)
				.map(centro -> {
					final List<Medico> medList = medicoRepository.findByCentroId(centroId);
					// Update the first appointment manually.
					for ( Medico med : medList ) {
						final Optional<Cita> citaOptional = citaRepository.findById(med.getFirstAppointmentId());
						try {
							final Cita cita = citaOptional.get();
							if ( null != cita )
								med.setFirstAppointment(cita);
						} catch ( NoSuchElementException nsee ) {
						}
					}
					return ResponseEntity.ok(medList);
				})
				.orElseGet(() -> {
					try {
						logger.info("[ERROR][CentroController.getMedicosByCentroId]> Centro {} no encontrado en repositorio."
								, centroId);
						response.getWriter().println("Centro " + centroId + " no encontrado en repositorio.");
						return new ResponseEntity<List<Medico>>(HttpStatus.NOT_FOUND);
					} catch ( IOException e ) {
						return new ResponseEntity<List<Medico>>(HttpStatus.NOT_FOUND);
					}
				});
//	});
	}

	/**
	 * Create a new <b>Medico</b> entity from the data received on the Body of the POST request.
	 * @param centroId The <b>Centro</b> to connect to this <b>Medico</b> instance. A <b>Medico</b> can have more than one instance,
	 *                 each of them connected to a different <b>Centro</b>
	 * @param medico   The new <b>Medico</b> entity fields.
	 * @return a new serialization of the <b>Medico</b> instance with the Id filled and all the other fields verified.
	 */
	@PostMapping("/v1/centros/{centroId}/medicos/new")
	public ResponseEntity<Medico> addMedico( @PathVariable Long centroId,
	                                         @Valid @RequestBody Medico medico ) {
		return centroRepository.findById(centroId)
				.map(centro -> {
					medico.setCentro(centro)
							.setId(Medico.generateNewId(centroId, medico.getReferencia()));
					return new ResponseEntity<Medico>(medicoRepository.save(medico), HttpStatus.OK);
				})
				.orElse(new ResponseEntity<Medico>(HttpStatus.BAD_REQUEST));
	}

	@PutMapping("/v1/centros/{centroId}/medicos/update")
	public ResponseEntity<Medico> upateMedico( @PathVariable Long centroId,
	                                           @Valid @RequestBody Medico medico ) {
		return medicoRepository.findById(medico.getId())
				.map(updatemedico -> {
					// WARNING: There are fields that once set on the creation are not editable
					updatemedico.setNombre(medico.getNombre())
							.setApellidos(medico.getApellidos())
							.setEspecialidad(medico.getEspecialidad())
							.setActivo(medico.isActivo())
							.setActosMedicos(medico.getActosMedicos());
					return new ResponseEntity<Medico>(medicoRepository.save(updatemedico), HttpStatus.OK);
				})
				.orElse(new ResponseEntity<Medico>(HttpStatus.BAD_REQUEST));
	}
}
// - UNUSED CODE ............................................................................................
