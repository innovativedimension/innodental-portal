//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.dimensinfin.citamed.entities.Cita;
import org.dimensinfin.citamed.entities.Medico;
import org.dimensinfin.citamed.exception.ResourceNotFoundException;
import org.dimensinfin.citamed.model.OpenAppointment;
import org.dimensinfin.citamed.model.Paciente;
import org.dimensinfin.citamed.repository.CentroRepository;
import org.dimensinfin.citamed.repository.CitaRepository;
import org.dimensinfin.citamed.repository.MedicoRepository;
import org.dimensinfin.citamed.services.ICreationService;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@RestController
@CrossOrigin
public class CitaController extends BaseController {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final int NUMBER_APPOINTMENTS = 10;
	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	protected ICreationService creationService;
	@Autowired
	protected CitaRepository citaRepository;
	@Autowired
	protected MedicoRepository medicoRepository;
	@Autowired
	protected CentroRepository centroRepository;

	// - M E T H O D - S E C T I O N ..........................................................................
	@GetMapping("/v1/medicos/{medicoId}/citas")
	public List<Cita> getCitasByMedicoId( @PathVariable String medicoId ) {
		return citaRepository.findByMedicoIdentifier(medicoId);
	}

	@GetMapping("/v1/medicos/{medicoId}/citas/byreferencia")
	public List<Cita> getCitasByReferencia( @PathVariable String medicoId ) {
		return citaRepository.findByMedicoByReferencia(medicoId);
	}

	//--- A D D I T I O N A L   A P P O I N T M E N T   E N T R Y - P O I N T S
	@GetMapping("/v1/medicos/{medicoId}/citas/bymonth/{year}/{month}")
	public List<Cita> getCitasByMonth( @PathVariable String medicoId,
	                                   @PathVariable Integer year,
	                                   @PathVariable Integer month ) {
		// TODO. Until the centers tab are ready we should return all the appointments aggregated.
		final List<Cita> citas = citaRepository.findByMedicoYearMonth(medicoId, year, month);
		if ( citas.size() < 1 )
			return citaRepository.findByYearMonth(year, month);
		else return citas;
	}

	@GetMapping("/v1/medicos/{medicoId}/citas/nextfree/{targetDateString}")
	public Page<Cita> getCitasNextFree( @PathVariable String medicoId,
	                                    @PathVariable String targetDateString ) {
		final List<Sort.Order> orderList = new ArrayList<Sort.Order>();
		orderList.add(Sort.Order.asc("fecha"));
		orderList.add(Sort.Order.asc("huecoId"));

		LocalDate targetdate = LocalDate.parse(targetDateString, DateTimeFormatter.BASIC_ISO_DATE);
		return citaRepository.findNextFree(medicoId, targetdate, PageRequest.of(0, NUMBER_APPOINTMENTS, Sort.by(orderList)));
	}

	//--- A P P O I N T M E N T   M A N A G E M E N T
	@GetMapping("/v1/citas/{citaId}/block/{pacienteDatos}")
	public Cita getBlockCita( @PathVariable Long citaId,
	                          @PathVariable String pacienteDatos ) {
		final Cita updatedCita = citaRepository.findById(citaId)
				.map(cita -> {
					cita.setEstado(Cita.ECitaStates.BLOQUEADA.name()).setPacienteLocalizador(pacienteDatos);
					final Cita result = citaRepository.save(cita);
					logger.info("-- [CitaController.getBlockCita]> Appointment {} new state: {}", citaId, result.getEstado());
					return result;
				})
				.orElseThrow(() -> new ResourceNotFoundException("Cita seleccionada no encontrada [" + citaId + "]."));
		final Optional<Medico> medico = medicoRepository.findById(updatedCita.getMedicoIdentifier());
		if ( medico.isPresent() ) creationService.updateMedicoReport(medico.get());
		return updatedCita;
	}

	@GetMapping("/v1/citas/{citaId}/cancel/{pacienteDatos}")
	public Cita getCancelCita( @PathVariable Long citaId,
	                           @PathVariable String pacienteDatos ) {
		return citaRepository.findById(citaId)
				.map(cita -> {
					cita.setEstado(Cita.ECitaStates.LIBRE.name()).setPacienteLocalizador(pacienteDatos);
					final Cita result = citaRepository.save(cita);
					logger.info("-- [CitaController.getCancelCita]> Appointment {} new state: {}", citaId, result.getEstado());
					return result;
				})
				.orElseThrow(() -> new ResourceNotFoundException("Cita seleccionada no encontrada [" + citaId + "]."));
	}

	@PostMapping("/v1/citas/{citaId}/reserve/{pacienteIdentificador}")
	public OpenAppointment getReserveCita( @PathVariable Long citaId,
	                                       @PathVariable String pacienteIdentificador,
	                                       @Valid @RequestBody Paciente pacienteData ) {
		final Cita updatedCita = citaRepository.findById(citaId)
				.map(cita -> {
					cita.setEstado(Cita.ECitaStates.RESERVADA.name())
							.setPacienteLocalizador(pacienteIdentificador)
							.setPacienteData(pacienteData);
					final Cita result = citaRepository.save(cita);
					logger.info("-- [CitaController.getReserveCita]> Appointment {} new state: {}", citaId, result.getEstado());
					logger.info("-- [CitaController.getReserveCita]> Appointment {} Patient: {}", citaId, result.getPacienteLocalizador());
					return result;
				})
				.orElseThrow(() -> new ResourceNotFoundException("Cita seleccionada no encontrada [" + citaId + "]."));
		final Optional<Medico> medico = medicoRepository.findById(updatedCita.getMedicoIdentifier());
		if ( medico.isPresent() ) creationService.updateMedicoReport(medico.get());
		return new OpenAppointment().setCita(updatedCita).setMedico(medico.get());
	}

	// - A P I V 2

	/**
	 * Deletes a set of appointments from the list of cita unique identifiers received. Only appointments on the LIBRE state are
	 * deleted. Any other state will leave the record without modifications.
	 * @param identifierList
	 * @return
	 */
	@PostMapping("/v2/citas/delete")
	public List<Cita> postDeleteCitav2( @Valid @RequestBody ArrayList<Long> identifierList ) {
		// Process the list of identifiers.
		final ArrayList<Cita> results = new ArrayList<>();
		String selectedIdentifier = "";
		for ( Long identifier : identifierList ) {
			try {
				logger.info("-- [CitaController.postDeleteCitav2]> Initialing delete for identifier: ", identifier);
				final Optional<Cita> processedAppointment = citaRepository.findById(identifier)
						.map(cita -> {
							// Check the appointment is on the correct state.
							if ( cita.getEstado().equals(Cita.ECitaStates.LIBRE.name()) ||
									cita.getEstado().equals(Cita.ECitaStates.VACACIONES.name()) ||
									cita.getEstado().equals(Cita.ECitaStates.DESCANSO.name()) ) {
								// Store on the result and delete it.
								results.add(cita);
								citaRepository.delete(cita);
								logger.info("-- [CitaController.postDeleteCitav2]> Appointment {} new state: {}", identifier, "DELETED");
							}
							return cita;
						});
				if ( processedAppointment.isPresent() )
					selectedIdentifier = processedAppointment.get().getMedicoIdentifier();
			} catch ( NoSuchElementException nsee ) {
				// Just continue with the processing of other appointments
				logger.info("-- [CitaController.postDeleteCitav2]> Appointment {} not found.", identifier);
			}
		}

		final Optional<Medico> medico = medicoRepository.findById(selectedIdentifier);
		if ( medico.isPresent() ) creationService.updateMedicoReport(medico.get());
		return results;
	}

	@PostMapping("/v2/citas/vacaciones")
	public List<Cita> postVacacionesCitav2( @Valid @RequestBody ArrayList<Long> identifierList ) {
		// Process the list of identifiers.
		final ArrayList<Cita> results = new ArrayList<>();
		String selectedIdentifier = "";
		for ( Long identifier : identifierList ) {
			final Cita updatedCita = citaRepository.findById(identifier)
					.map(cita -> {
						cita.setEstado(Cita.ECitaStates.VACACIONES.name());
						final Cita result = citaRepository.save(cita);
						logger.info("-- [CitaController.postVacacionesCitav2]> Appointment {} new state: {}", identifier, result.getEstado());
						return result;
					}).get();
//					.orElseThrow(() -> new ResourceNotFoundException("Cita seleccionada no encontrada [" + identifier + "]."));
			results.add(updatedCita);
			selectedIdentifier = updatedCita.getMedicoIdentifier();
		}
		final Optional<Medico> medico = medicoRepository.findById(selectedIdentifier);
		if ( medico.isPresent() ) creationService.updateMedicoReport(medico.get());
		return results;
	}

	@PostMapping("/v2/citas/block/{pacienteDatos}")
	public List<Cita> getBlockCitav2( @PathVariable String pacienteDatos,
	                                  @Valid @RequestBody ArrayList<Long> identifierList ) {
		// Process the list of identifiers.
		final ArrayList<Cita> results = new ArrayList<>();
		String selectedIdentifier = "";
		for ( Long identifier : identifierList ) {
			final Cita updatedCita = citaRepository.findById(identifier)
					.map(cita -> {
						cita.setEstado(Cita.ECitaStates.BLOQUEADA.name()).setPacienteLocalizador(pacienteDatos);
						final Cita result = citaRepository.save(cita);
						logger.info("-- [CitaController.getBlockCitav2]> Appointment {} new state: {}", identifier, result.getEstado());
						return result;
					})
					.orElseThrow(() -> new ResourceNotFoundException("Cita seleccionada no encontrada [" + identifier + "]."));
			results.add(updatedCita);
			selectedIdentifier = updatedCita.getMedicoIdentifier();
		}
		final Optional<Medico> medico = medicoRepository.findById(selectedIdentifier);
		if ( medico.isPresent() ) creationService.updateMedicoReport(medico.get());
		return results;
	}

	@PostMapping("/v2/citas/cancel/{pacienteDatos}")
	public List<Cita> getCancelCitav2( @PathVariable String pacienteDatos,
	                                   @Valid @RequestBody ArrayList<Long> identifierList ) {
		// Process the list of identifiers.
		final ArrayList<Cita> results = new ArrayList<>();
		String selectedIdentifier = "";
		for ( Long identifier : identifierList ) {
			final Cita updatedCita = citaRepository.findById(identifier)
					.map(cita -> {
						cita.setEstado(Cita.ECitaStates.LIBRE.name()).setPacienteLocalizador(pacienteDatos);
						final Cita result = citaRepository.save(cita);
						logger.info("-- [CitaController.getCancelCitav2]> Appointment {} new state: {}", identifier, result.getEstado());
						return result;
					})
					.orElseThrow(() -> new ResourceNotFoundException("Cita seleccionada no encontrada [" + identifier + "]."));
			results.add(updatedCita);
			selectedIdentifier = updatedCita.getMedicoIdentifier();
		}
		final Optional<Medico> medico = medicoRepository.findById(selectedIdentifier);
		if ( medico.isPresent() ) creationService.updateMedicoReport(medico.get());
		return results;
	}

	@PostMapping("/v2/citas/reserve/{pacienteIdentificador}")
	public List<OpenAppointment> getReserveCitav2( @PathVariable String pacienteIdentificador,
	                                               @Valid @RequestBody AppointmentReservationRequest reservationRequest ) {
		// Process the list of identifiers.
		final ArrayList<OpenAppointment> results = new ArrayList<>();
		String selectedIdentifier = "";
		for ( Long identifier : reservationRequest.getIdentifiers() ) {
			final Cita updatedCita = citaRepository.findById(identifier)
					.map(cita -> {
						cita.setEstado(Cita.ECitaStates.RESERVADA.name())
								.setPacienteLocalizador(reservationRequest.getPatientData().getIdentificador())
								.setPacienteData(reservationRequest.getPatientData());
						final Cita result = citaRepository.save(cita);
						logger.info("-- [CitaController.getReserveCitav2]> Appointment {} new state: {}", identifier, result.getEstado());
						logger.info("-- [CitaController.getReserveCitav2]> Appointment {} Patient: {}", identifier, result.getPacienteLocalizador());
						return result;
					})
					.orElseThrow(() -> new ResourceNotFoundException("Cita seleccionada no encontrada [" + identifier + "]."));
			selectedIdentifier = updatedCita.getMedicoIdentifier();
			final Optional<Medico> medico = medicoRepository.findById(selectedIdentifier);
			results.add(new OpenAppointment().setCita(updatedCita).setMedico(medico.get()));
		}
		final Optional<Medico> medico = medicoRepository.findById(selectedIdentifier);
		if ( medico.isPresent() ) creationService.updateMedicoReport(medico.get());
		return results;
	}

	public static class AppointmentReservationRequest {
		public Paciente patientData;
		public ArrayList<Long> identifiers;

		public Paciente getPatientData() {
			return patientData;
		}

		public AppointmentReservationRequest setPatientData( final Paciente patientData ) {
			this.patientData = patientData;
			return this;
		}

		public ArrayList<Long> getIdentifiers() {
			return identifiers;
		}

		public AppointmentReservationRequest setIdentifiers( final ArrayList<Long> identifiers ) {
			this.identifiers = identifiers;
			return this;
		}
	}
}
// - UNUSED CODE ............................................................................................
//[01]
