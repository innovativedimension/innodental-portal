//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.controller;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.joda.time.DateTime;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.dimensinfin.citamed.CitasBackendApplication;
import org.dimensinfin.citamed.entities.Credencial;
import org.dimensinfin.citamed.exception.BackendException;
import org.dimensinfin.citamed.repository.CredencialRepository;
import org.dimensinfin.citamed.security.AppSession;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@RestController
@CrossOrigin
public class LoginController extends BaseController {
	// - S T A T I C - S E C T I O N ..........................................................................

	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	protected CredencialRepository credencialRepository;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@GetMapping("/v1/credencial/validate")
	public ResponseEntity<Credencial> checkCredencial( @RequestHeader("xApp-Authorization") String authorization ) {
		// Get access to the basic authentication data.
		String[] values = new String[2];
		if ( authorization != null && authorization.toLowerCase().startsWith("basic") ) {
			// Authorization: Basic base64credentials
			String base64Credentials = authorization.substring("Basic".length()).trim();
			byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
			String credentials = new String(credDecoded, StandardCharsets.UTF_8);
			// credentials = username:password
			values = credentials.split(":", 2);
		}
		final String identifier = values[0];
		final String password = values[1];
		if ( CitasBackendApplication.getConfiguration().getBooleanResource("R.runtime.mode.development") )
			logger.info("-- [LoginController.checkCredencial]> Password: {}", password);

		return credencialRepository.findById(identifier)
				.map(credencial -> {
					// Check that an unencrypted password matches or not
					boolean match = BCrypt.checkpw(password, credencial.getPassword());
					if ( match ) {
						logger.info("-- [LoginController.checkCredencial]> Password matches.");
						// Create a new session and store the authorization token and the rest of the data.
						// Generate token.
						final String salt = BCrypt.gensalt(8);
						final String payload = DateTime.now().toString() + ":" + credencial.getNif();
						final String authorizationToken = BCrypt.hashpw(payload, salt);
						final AppSession session = new AppSession();
						session.setAuthorizationToken(authorizationToken)
								.setPayload(payload)
								.setAuthorizationPassword(salt)
								.setUserIdentifier(credencial.getNif())
								.setRole(credencial.getRole())
								.store();
						logger.info("-- [LoginController.checkCredencial]> Session id: {}", session.getId());
						credencial.setAuthorizationToken(authorizationToken);
						return new ResponseEntity<Credencial>(credencial, HttpStatus.OK);
					} else throw new BackendException("Password does not match with repository.");
				})
				.orElse(new ResponseEntity<Credencial>(HttpStatus.NOT_FOUND));
	}

	@GetMapping("/v1/credencial/password/validate")
	public ResponseEntity<Boolean> validatePassword( @RequestHeader HttpHeaders httpHeaders ) {
		// Get the authorization header and decode the password.
		final List<String> base64Payload = httpHeaders.getValuesAsList("xApp-Payload");
		if ( null != base64Payload ) {
			logger.info("-- [LoginController.validatePassword]> Payload found.");
			String payload = null;
			for ( String element : base64Payload ) {
				if ( null != element ) {
					byte[] decodedBytes = Base64.getDecoder().decode(element);
					payload = new String(decodedBytes);
				}
			}
			if ( null != payload ) {
				logger.info("-- [LoginController.validatePassword]> Payload valid. {}", payload);
				// Get the session from the headers and check that the passwords match.
				final List<String> headerValueList = httpHeaders.getValuesAsList("xApp-Authentication");
				if ( (null != headerValueList) && (headerValueList.size() > 0) ) {
					final String headerValue = headerValueList.get(0);
					logger.info("-- [LoginController.validatePassword]> Header valid. {}", headerValue);
					// Search for the local session and check it is still valid.
					AppSession session = CitasBackendApplication.getSessionManager().retrieve(headerValue);
					if ( null != session ) {
						logger.info("-- [LoginController.validatePassword]> Session found: {}", headerValue);
						final String identifier = session.getUserIdentifier();
						final Optional<Credencial> credentialOptional = credencialRepository.findById(identifier);
						try {
							final Credencial credencial = credentialOptional.get();
							boolean match = BCrypt.checkpw(payload, credencial.getPassword());
							return new ResponseEntity<Boolean>(match, HttpStatus.OK);
						} catch ( final RuntimeException rtex ) {
							return new ResponseEntity<Boolean>(false, HttpStatus.OK);
						}
					}
				}
			}
		}
		return new ResponseEntity<Boolean>(false, HttpStatus.OK);
	}
}
// - UNUSED CODE ............................................................................................
//[01]
