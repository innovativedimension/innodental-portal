//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.dimensinfin.citamed.entities.CitasTemplate;
import org.dimensinfin.citamed.entities.LimitesServicio;
import org.dimensinfin.citamed.model.Limitador;
import org.dimensinfin.citamed.repository.CentroRepository;
import org.dimensinfin.citamed.repository.CitaTemplateRepository;
import org.dimensinfin.citamed.repository.LimitesServicioRepository;
import org.dimensinfin.citamed.repository.MedicoRepository;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@RestController
@CrossOrigin
public class MedicoController extends BaseController {
	// - S T A T I C - S E C T I O N ..........................................................................

	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	protected MedicoRepository medicoRepository;
	@Autowired
	protected CentroRepository centroRepository;
	@Autowired
	protected CitaTemplateRepository citaTemplateRepository;
	@Autowired
	protected LimitesServicioRepository limitesServicioRepository;

	// - M E T H O D - S E C T I O N ..........................................................................
	@GetMapping("/v1/medicos/{identificador}/templates")
	public List<CitasTemplate> getTemplates( @PathVariable String identificador ) {
		return citaTemplateRepository.findAll4Servicio(identificador);
	}

	@PostMapping("/v1/medicos/{identificador}/templates/new")
	public ResponseEntity<CitasTemplate> newTemplate( HttpServletResponse response,
	                                                  @PathVariable String identificador,
	                                                  @Valid @RequestBody CitasTemplate newTemplate ) {
		return medicoRepository.findById(identificador)
				.map(servicio -> {
					newTemplate.setServicio(servicio);
					return ResponseEntity.ok(citaTemplateRepository.save(newTemplate));
				})
				.orElseGet(() -> {
					try {
						logger.info("[ERROR][MedicoController.newTemplate]> Médico/Servicio {} no encontrado. Template no creada."
								, identificador);
						response.getWriter().println("Médico/Servicio no encontrado. Template no creada.");
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					} catch ( IOException e ) {
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					}
				});
	}

	@PutMapping("/v1/medicos/{identificador}/templates/update")
	public ResponseEntity<CitasTemplate> updateTemplate( HttpServletResponse response,
	                                                     @PathVariable String identificador,
	                                                     @Valid @RequestBody CitasTemplate newTemplateData ) {
		// Search for the parent credential.
		return citaTemplateRepository.findById(newTemplateData.getId())
				.map(oldTemplate -> {
					// Update the new template with the old template data.
					newTemplateData.setServicio(oldTemplate.getServicio())
							.setCreatedAt(oldTemplate.getCreatedAt())
							.setUpdatedAt(oldTemplate.getUpdatedAt());
					return ResponseEntity.ok(citaTemplateRepository.save(newTemplateData));
				})
				.orElseGet(() -> {
					try {
						logger.info("[ERROR][MedicoController.newTemplate]> Plantilla {} no encontrada en el repositorio. Template no actalizada."
								, newTemplateData.getId());
						response.getWriter().println("Template " + newTemplateData.getId() + " no encontrada. Delete no realizado.");
						return new ResponseEntity<CitasTemplate>(HttpStatus.NOT_FOUND);
					} catch ( IOException e ) {
						return new ResponseEntity<CitasTemplate>(HttpStatus.NOT_FOUND);
					}
				});
	}

	@DeleteMapping("/v1/medicos/{identificador}/templates/delete/{idTemplate}")
	public ResponseEntity<Long> deleteTemplate( HttpServletResponse response,
	                                            @PathVariable String identificador,
	                                            @PathVariable Long idTemplate ) {
		// Search for the parent credential.
		return citaTemplateRepository.findById(idTemplate)
				.map(oldTemplate -> {
					// Delete the template from the respository.
					citaTemplateRepository.delete(oldTemplate);
					return ResponseEntity.ok(idTemplate);
				})
				.orElseGet(() -> {
					try {
						logger.info("[ERROR][MedicoController.newTemplate]> Plantilla {} no encontrada en el respositorio. Template no eliminada."
								, idTemplate);
						response.getWriter().println("Template " + idTemplate + " no encontrada. Delete no realizado.");
						return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
					} catch ( IOException e ) {
						return new ResponseEntity<Long>(HttpStatus.NOT_FOUND);
					}
				});
	}

	@GetMapping("/v1/medicos/{identificador}/limites")
	public ResponseEntity<ArrayList<Limitador>> getLimites( @PathVariable String identificador ) {
		final List<LimitesServicio> limitsList = limitesServicioRepository.findByIdServicio(identificador);
		if ( limitsList.size() > 0 )
			return new ResponseEntity<ArrayList<Limitador>>(limitsList.get(0).getLimites(), HttpStatus.OK);
		else
			return new ResponseEntity<ArrayList<Limitador>>(HttpStatus.NOT_FOUND);
	}

	@PostMapping("/v1/medicos/{identificador}/limites")
	public ResponseEntity<ArrayList<Limitador>> setLimites( HttpServletResponse response,
	                                                        @PathVariable String identificador,
	                                                        @Valid @RequestBody ArrayList<Limitador> newLimits ) {
		final List<LimitesServicio> limitList = limitesServicioRepository.findByIdServicio(identificador);
		if ( limitList.size() > 0 ) {
			final LimitesServicio target = limitList.get(0);
			target.setLimites(newLimits);
			limitesServicioRepository.save(target);
			return new ResponseEntity<ArrayList<Limitador>>(target.getLimites(), HttpStatus.OK);
		} else {
			return medicoRepository.findById(identificador)
					.map(servicio -> {
						final LimitesServicio limit4Service = new LimitesServicio()
								.setLimites(newLimits)
								.setServicio(servicio);
						limitesServicioRepository.save(limit4Service);
						return new ResponseEntity<ArrayList<Limitador>>(limit4Service.getLimites(), HttpStatus.OK);
					})
					.orElseGet(() -> {
						try {
							logger.info("[ERROR][MedicoController.setLimites]> Médico/Servicio {} no encontrado. Limites no creados."
									, identificador);
							response.getWriter().println("Médico/Servicio " + identificador + " no encontrado. Limites no creados.");
							return new ResponseEntity<>(HttpStatus.NOT_FOUND);
						} catch ( IOException e ) {
							return new ResponseEntity<>(HttpStatus.NOT_FOUND);
						}
					});
		}
	}
}
// - UNUSED CODE ............................................................................................
//[01]
