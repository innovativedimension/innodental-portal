//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.dimensinfin.citamed.entities.Cita;
import org.dimensinfin.citamed.entities.Medico;
import org.dimensinfin.citamed.model.OpenAppointment;
import org.dimensinfin.citamed.repository.CitaRepository;
import org.dimensinfin.citamed.repository.MedicoRepository;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@RestController
@CrossOrigin
public class PacienteController extends BaseController {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("PacienteController");

	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	protected CitaRepository citaRepository;
	@Autowired
	protected MedicoRepository medicoRepository;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................

	//--- P A T I E N T   M A N A G E M E N T
	@GetMapping("/v1/paciente/{identificadorPaciente}/citas")
	public List<OpenAppointment> getCitas4Paciente( @PathVariable String identificadorPaciente ) {
		logger.info(">>>>>>>>>>>>>>>>>>>>NEW REQUEST: /v1/paciente/{}/citas",
				identificadorPaciente);
//		final Chrono timer = new Chrono();
		final ArrayList<OpenAppointment> appointments = new ArrayList<>();
		try {
			final List<Cita> citas = citaRepository.findCitasByPatient(identificadorPaciente);
			for ( final Cita c : citas ) {
				final Optional<Medico> medico = medicoRepository.findById(c.getMedicoIdentifier());
				appointments.add(new OpenAppointment().setCita(c).setMedico(medico.get()));
			}
			return appointments;
		} finally {
//			logger.info("<< [PacienteController.getCitas4Paciente]> TIMING: {}", timer.printElapsed(Chrono.ChronoOptions.SHOWMILLIS));
		}
	}
}

// - UNUSED CODE ............................................................................................
//[01]
