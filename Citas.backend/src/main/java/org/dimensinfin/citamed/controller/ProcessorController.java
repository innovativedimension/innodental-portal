//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.controller;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.dimensinfin.citamed.CitasBackendApplication;
import org.dimensinfin.citamed.entities.Centro;
import org.dimensinfin.citamed.entities.Cita;
import org.dimensinfin.citamed.entities.CitasTemplate;
import org.dimensinfin.citamed.entities.Medico;
import org.dimensinfin.citamed.exception.JsonExceptionInstance;
import org.dimensinfin.citamed.exception.ResourceNotFoundException;
import org.dimensinfin.citamed.model.FormularioCreacionCitas;
import org.dimensinfin.citamed.repository.CentroRepository;
import org.dimensinfin.citamed.repository.CitaRepository;
import org.dimensinfin.citamed.repository.MedicoRepository;
import org.dimensinfin.citamed.services.AppointmentCreationService;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@RestController
@CrossOrigin
public class ProcessorController extends BaseController {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("ProcessorController");

	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	protected AppointmentCreationService creationService;
	@Autowired
	protected MedicoRepository medicoRepository;
	@Autowired
	protected CentroRepository centroRepository;
	@Autowired
	protected CitaRepository citaRepository;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@PostMapping("/v1/processor/creacitas/{consultaReference}")
	public List<AppointmentCreationService.AppointmentReport> processorCreaCitas( @PathVariable String consultaReference,
	                                                                              @Valid @RequestBody FormularioCreacionCitas formulario ) {
		logger.info(">>>>>>>>>>>>>>>>>>>>NEW REQUEST: /v1/processor/creacitas/{}", consultaReference);
		final List<AppointmentCreationService.AppointmentReport> creationReport = new ArrayList<>();
		try {
			// Validate the data received.
			// Check that the credential is found on the repository.
			final Optional<Medico> data = creationService.findById(consultaReference);
			if ( !data.isPresent() ) {
				logger.info("-- [ProcessorController.processorCreaCitas]> Excepcion: {}",
						"Medico not found with id " + consultaReference);
				throw new ResourceNotFoundException("Medico not found with id " + consultaReference);
			}
			// Store the medico into a variable.
			final Medico medico = data.get();
			logger.info("-- [ProcessorController.processorCreaCitas]> Medico: {}",
					CitasBackendApplication.jsonMapper.writeValueAsString(medico));
			// Process the template to create new appointment slots.
			if ( null == formulario ) {
				logger.info("-- [ProcessorController.processorCreaCitas]> Excepcion: {}",
						"FormularioCreacionCitas invalido. No existe formulario");
				throw new ResourceNotFoundException("FormularioCreacionCitas invalido. No existe formulario");
			} else {
				logger.info(">> [ProcessorController.processorCreaCitas]> Formulario: {}",
						CitasBackendApplication.jsonMapper.writeValueAsString(formulario));
				// Check required fields.
				if ( null == formulario.getFechaInicio() ) {
					logger.info("-- [ProcessorController.processorCreaCitas]> Excepcion: {}",
							"FormularioCreacionCitas invalido. No se ha definido fecha de inicio.");
					throw new ResourceNotFoundException("FormularioCreacionCitas invalido. No se ha definido fecha de inicio.");
				}
				if ( null == formulario.getFechaFin() ) {
					logger.info("-- [ProcessorController.processorCreaCitas]> Excepcion: {}",
							"FormularioCreacionCitas invalido. No se ha definido fecha de fin.");
					throw new ResourceNotFoundException("FormularioCreacionCitas invalido. No se ha definido fecha de fin.");
				}
				// Do the iteration from the start point up to the last hole only for the weeks days requested.
				logger.info("-- [ProcessorController.processorCreaCitas]> Starting date processing.");
				int generationLimit = 0;
				for ( LocalDate date = formulario.getFechaInicio();
				      date.isBefore(formulario.getFechaFin().plus(1, ChronoUnit.DAYS));
				      date = date.plusDays(1) ) {
					logger.info("-- [ProcessorController.processorCreaCitas]> Processing date: {}", date);
					// If the flag to delete appointments is set clear previous 'LIBRE' records.
					if ( formulario.isClearData() ) {
						logger.info("-- [ProcessorController.processorCreaCitas]> ClearData is active. Deleting appointments.");
						creationService.clearUnused4Date(medico, date);
					}
					// Create the slots for a single day. This follows the IntervalTree algorithm.
					creationReport.add(creationService.createDayAppointments(medico, date, formulario));

					// Keep a counter to limit the number of generated days to a configured number.
					generationLimit++;
					if ( generationLimit > CitasBackendApplication.getConfiguration()
							.getNumberResource("P.runtime.appointments.limitofdates") )
						break;
				}
			}
		} catch ( JsonProcessingException jspe ) {
			logger.info("-- [ProcessorController.processorCreaCitas]> JsonProcessingException: {}", jspe.getMessage());
			throw new ResourceNotFoundException(new JsonExceptionInstance(jspe).toJson());
		}
		return creationReport;
	}

	@PostMapping("/v2/processor/creacitas/{identifier}/date/{targetDateBasicIso}")
	public ResponseEntity<List<AppointmentCreationService.AppointmentReport>> processorCreaCitas( HttpServletResponse response,
	                                                                                              @PathVariable String identifier,
	                                                                                              @PathVariable String targetDateBasicIso,
	                                                                                              @Valid @RequestBody CitasTemplate template ) {
		if ( null == template ) {
			logger.info("-- [ProcessorController.processorCreaCitas]> Excepcion: {}",
					"Creacion de citas cancelado. No existe plantilla.");
			try {
				response.getWriter().println("Creacion de citas cancelado. No existe plantilla.");
				return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
			} catch ( IOException e ) {
				return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
			}
		} else return medicoRepository.findById(identifier)
				.map(medico -> {
					final List<AppointmentCreationService.AppointmentReport> creationReport = new ArrayList<>();
					// Validate the date.
					final LocalDate processingDate = LocalDate.parse(targetDateBasicIso, DateTimeFormatter.BASIC_ISO_DATE);
					logger.info("-- [ProcessorController.processorCreaCitas]> Starting date processing.");
					logger.info("-- [ProcessorController.processorCreaCitas]> Processing date: {}", processingDate);
					// Clear not reserved slots before creating new ones. Templates supersede other appointments.
					creationService.clearUnused4Date(medico, processingDate);
					// Create the slots for a single day. This follows the IntervalTree algorithm.
					creationReport.add(creationService.createDayAppointments(medico, processingDate, template));
					return ResponseEntity.ok(creationReport);
				})
				.orElseGet(() -> {
					try {
						response.getWriter().println("Médico/Servicio no encontrado. Identificador: " + identifier);
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					} catch ( IOException e ) {
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					}
				});
	}

	@GetMapping("/v1/processor/extract/{startscn}")
	public String extractAppointmentsFromSCN( HttpServletResponse response,
	                                          @PathVariable Long startscn ) {
// Create the specific serializer
		final ObjectMapper jsonMapper = new ObjectMapper();
		jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);
		// Add our own serializers.
		SimpleModule extractorSerializerModule = new SimpleModule();
		extractorSerializerModule.addSerializer(CitaExtract.class, new CitaExtractSerializer());
		extractorSerializerModule.addSerializer(Medico.class, new MedicoSerializer());
		jsonMapper.registerModule(extractorSerializerModule);

		final List<CitaExtract> extraction = new ArrayList<>();
		citaRepository.extractCitasBySCN(startscn).forEach(( cita ) -> {
			// Extract the rest of the daa and convert the structures into a single autonomous element
			// suitable for extraction.
			centroRepository.findById(Long.parseLong(cita.getCentroIdentifier()))
					.map(centro -> {
						medicoRepository.findById(cita.getMedicoIdentifier())
								.map(medico -> {
									// Inject the data into a new renderable Cita.
									CitaExtract extract = new CitaExtract(cita)
											.setCentro(centro);
									extract.setMedico(medico);
									extraction.add(extract);
									return cita;
								})
								.orElseGet(() -> {
									logger.info("[ERROR][ProcessorController.extractAppointmentsFromSCN]> Medico {} no encontrado en repositorio."
											, cita.getMedicoIdentifier());
									return cita;
								});
						return cita;
					})
					.orElseGet(() -> {
						logger.info("[ERROR][ProcessorController.extractAppointmentsFromSCN]> Centro {} no encontrado en repositorio."
								, cita.getCentroIdentifier());
						return cita;
					});
		});
		try {
			return jsonMapper.writeValueAsString(extraction);
		} catch ( JsonProcessingException e ) {
			e.printStackTrace();
			return "";
		}
	}

	public static class CitaExtract extends Cita {
		protected Centro centro;

		public CitaExtract( final Cita _cita ) {
			super();
			this.id = _cita.getId();
			this.referencia = _cita.getReferencia();
			this.fecha = _cita.getFecha();
			this.huecoId = _cita.getHuecoId();
			this.huecoDuracion = _cita.getHuecoDuracion();
			this.estado = _cita.getEstado();
			this.pacienteLocalizador = _cita.getPacienteLocalizador();
			this.pacienteData = _cita.getPacienteData();
			this.tipo = _cita.getTipo();
			this.zonaHoraria = _cita.getZonaHoraria();
			this.scn = _cita.getScn();
		}

		public CitaExtract setCentro( final Centro _centro ) {
			this.centro = _centro;
			return this;
		}

		public long getSCN() {
			return this.scn;
		}
	}

	public static class CitaExtractSerializer extends JsonSerializer<CitaExtract> {
		@Override
		public void serialize( final CitaExtract value, final JsonGenerator jgen, final SerializerProvider provider )
				throws IOException, JsonProcessingException {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

			jgen.writeStartObject();
			jgen.writeStringField("jsonClass", value.getJsonClass());
			jgen.writeNumberField("id", value.getId());
			jgen.writeNumberField("scn", value.getSCN());
			jgen.writeStringField("referencia", value.getReferencia());
			jgen.writeStringField("fecha", value.getFecha().format(formatter));
			jgen.writeNumberField("huecoId", value.getHuecoId());
			jgen.writeNumberField("huecoDuracion", value.getHuecoDuracion());
			jgen.writeStringField("estado", value.getEstado());
			jgen.writeStringField("pacienteLocalizador", value.getPacienteLocalizador());
			jgen.writeObjectField("pacienteData", value.getPacienteData());
			jgen.writeStringField("tipo", value.getTipo());
			jgen.writeStringField("zonaHoraria", value.getZonaHoraria());
			jgen.writeObjectField("centro", value.centro);
			jgen.writeObjectField("medico", value.getMedico());
			jgen.writeEndObject();
		}
	}

	public static class MedicoSerializer extends JsonSerializer<Medico> {
		@Override
		public void serialize( final Medico value, final JsonGenerator jgen, final SerializerProvider provider )
				throws IOException, JsonProcessingException {
			jgen.writeStartObject();
			jgen.writeStringField("jsonClass", value.getJsonClass());
			jgen.writeStringField("id", value.getId());
			jgen.writeStringField("referencia", value.getReferencia());
			jgen.writeStringField("tratamiento", value.getTratamiento());
			jgen.writeStringField("nombre", value.getNombre());
			jgen.writeStringField("apellidos", value.getApellidos());
			jgen.writeStringField("especialidad", value.getEspecialidad());
			jgen.writeBooleanField("activo", value.isActivo());
			jgen.writeObjectField("actosMedicos", value.getActosMedicos());
			jgen.writeEndObject();
		}
	}
}

// - UNUSED CODE ............................................................................................
//[01]
