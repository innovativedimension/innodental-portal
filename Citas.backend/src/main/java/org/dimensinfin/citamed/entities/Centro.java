//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.dimensinfin.citamed.model.AuditModel;

/**
 * A <b>Centro</b> represents the basic data for a corporation able to serve medical services. The single function on this
 * application is to allow the aggregation of <b>Medicos</b> under corporations to filter in or out them from the core appointment
 * search.
 *
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Entity
@Table(name = "Centros")
public class Centro extends AuditModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long serialVersionUID = -2973746770428914334L;
//	private static Logger logger = LoggerFactory.getLogger("Centro");

	// - F I E L D - S E C T I O N ............................................................................
	@Id
	@GeneratedValue(generator = "centro_generator")
	@SequenceGenerator(
			name = "centro_generator",
			sequenceName = "centros_sequence",
			initialValue = 100001
	)
	private long id;
	@Column(name = "nombre")
	@NotBlank
	@Size(min = 3, max = 100)
	private String nombre;
	@Column(name = "logotipo")
	private String logotipo;
	@Column(name = "localidad")
	private String localidad;
	@Column(name = "direccion")
	private String direccion;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public long getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getLogotipo() {
		return logotipo;
	}

	public String getLocalidad() {
		return localidad;
	}

	public String getDireccion() {
		return direccion;
	}

	public Centro setNombre(final String nombre) {
		this.nombre = nombre;
		return this;
	}

	public Centro setLogotipo(final String logotipo) {
		this.logotipo = logotipo;
		return this;
	}

	public Centro setLocalidad(final String localidad) {
		this.localidad = localidad;
		return this;
	}

	public Centro setDireccion(final String direccion) {
		this.direccion = direccion;
		return this;
	}

	// --- O V E R R I D E D   M E T H O D S
	@Override
	public String toString() {
		return String.format("Centro [ id:%d nombre:'%s' ]",
				this.id,
				this.nombre
		);
	}
}


// - UNUSED CODE ............................................................................................
//[01]
