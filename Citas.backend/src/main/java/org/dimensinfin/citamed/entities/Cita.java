//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.dimensinfin.citamed.model.AuditModel;
import org.dimensinfin.citamed.model.Paciente;

/**
 * This entity should store the properties for a period of time where there can be a medical act or a patient appointment with a
 * doctor or a service. The <b>Cita</b> can have some states representing if the period of time or slot is assigned to a patient or
 * in any other state that can affect the list of appointment to be shown to a service requester.
 *
 * When a <b>Cita</b> is reserved by a patient it should have a patient data record pointer so the doctor or service will be able to
 * see and relate that information to the service (patient registration, appointment confirmation, cancelation or change report).
 * Patient data is a set of fields that are under the RGPD legislation. This set of data is still under development so the use of a
 * different table will allow isolation from repository evolution. The other option is to store this data as a JSON serialized
 * object that can also be converted to its Java POJO counterpart.
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Entity(name = "Citas")
@Table(name = "Citas")
public class Cita extends AuditModel {
	public enum ECitaStates {
		LIBRE, DESCANSO, BLOQUEADA, RESERVADA, CONFIRMADA, VACACIONES, ANULADA;
	}

	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long serialVersionUID = 7126978339744587231L;

	// - F I E L D - S E C T I O N ............................................................................
	@Id
	@GeneratedValue(generator = "cita_generator")
	@SequenceGenerator(
			name = "cita_generator",
			sequenceName = "cita_sequence",
			initialValue = 400001
	)
	protected long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "medico_id")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	protected Medico medico;

	@NotBlank
	@Size(min = 3, max = 40)
	@Column(name = "referencia")
	protected String referencia;
	@Column(name = "fecha")
	protected LocalDate fecha;
	@Column(name = "huecoId")
	protected int huecoId;
	@Column(name = "huecoDuracion")
	protected int huecoDuracion;
	@NotBlank
	@Size(min = 3, max = 30)
	@Column(name = "estado")
	protected String estado;
	@Size(max = 30)
	@Column(name = "pacienteLocalizador")
	protected String pacienteLocalizador = ""; // This should store the patient DNI, NIF or passport number as a unique identifier.
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	protected Paciente pacienteData; // Stores the serialized Paciente data. This removes a table to store the detailed data.
	@NotBlank
	@Size(min = 3, max = 30)
	@Column(name = "tipo")
	protected String tipo = "-NORMAL-";
	@NotBlank
	@Size(min = 3, max = 20)
	@Column(name = "zonahoraria")
	protected String zonaHoraria = "MAÑANA";
	@Column(name = "scn")
	protected long scn = 0;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- O V E R R I D E D   M E T H O D S
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("Cita [");
		buffer.append(id).append(" ");
		if ( null != medico ) buffer.append("MedicoId: ").append(medico.getId()).append(" ");
//		buffer.append("Ref: ").append(referencia).append(" ");
		buffer.append("+").append(fecha).append("+").append(huecoId).append("-").append(huecoDuracion).append('\n');
		buffer.append(estado).append("-").append(pacienteLocalizador).append(" ");
		buffer.append("horario:").append(zonaHoraria);
		buffer.append("]");
		return buffer.toString();
	}

	// --- G E T T E R S   &   S E T T E R S
	public long getId() {
		return id;
	}

	public String getReferencia() {
		return referencia;
	}

	public Medico getMedico() {
		return medico;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public int getHuecoId() {
		return huecoId;
	}

	public int getHuecoDuracion() {
		return huecoDuracion;
	}

	public String getEstado() {
		return estado;
	}

	public String getPacienteLocalizador() {
		return pacienteLocalizador;
	}

	public Paciente getPacienteData() {
		return pacienteData;
	}

	public String getTipo() {
		return tipo;
	}

	public String getZonaHoraria() {
		return zonaHoraria;
	}

	public Cita setReferencia( final String referencia ) {
		this.referencia = referencia;
		return this;
	}

	public Cita setFecha( final LocalDate fecha ) {
		this.fecha = fecha;
		return this;
	}

	public Cita setHuecoId( final int huecoId ) {
		this.huecoId = huecoId;
		return this;
	}

	public Cita setHuecoDuracion( final int huecoDuracion ) {
		this.huecoDuracion = huecoDuracion;
		return this;
	}

	public Cita setEstado( final String estado ) {
		this.estado = estado;
		return this;
	}

	public Cita setPacienteLocalizador( final String pacienteLocalizador ) {
		this.pacienteLocalizador = pacienteLocalizador;
		return this;
	}

	public Cita setPacienteData( final Paciente pacienteData ) {
		this.pacienteData = pacienteData;
		return this;
	}

	public Cita setMedico( final Medico medico ) {
		this.medico = medico;
		return this;
	}

	public Cita setTipo( final String tipo ) {
		this.tipo = tipo;
		return this;
	}

	public Cita setZonaHoraria( final String zonaHoraria ) {
		this.zonaHoraria = zonaHoraria;
		return this;
	}

	// --- V I R T U A L   G E T T E R S   &   S E T T E R S
	public String getHuecoIdentifier() {
		return String.format("%04d", this.huecoId);
	}

	public Cita setHuecoIdentifier( final String dummy ) {
		return this;
	}

	public String getMedicoIdentifier() {
		if ( null == medico ) return "MID:<id>:<referencia>";
		return this.getMedico().getId();
	}

	public Cita setMedicoIdentifier( final String dummy ) {
		return this;
	}

	public String getCentroIdentifier() {
		if ( null == medico ) return "-NOT-DEFINED-";
		final String[] parts = this.getMedico().getId().split(":");
		if ( parts.length > 1 ) return parts[1];
		else return "-NOT-FOUND-";
	}

	public Cita setCentroIdentifier( final String dummy ) {
		return this;
	}

	public String getCentroName() {
		if ( null == medico ) return "-NOT-DEFINED-";
		if ( null != this.medico ) {
			final Centro centro = this.medico.getCentro();
			if ( null != centro ) {
				return centro.getNombre();
			} else return "-NOT-FOUND-";
		} else return "-NOT-FOUND-";
	}

	public Cita setCentroName( final String dummy ) {
		return this;
	}

	public String getMedicoReference() {
		if ( null == this.medico ) return "-NOT-DEFINED-";
		final String[] parts = this.getMedico().getId().split(":");
		if ( parts.length > 2 ) return parts[2];
		else return "-NOT-FOUND-";
	}

	public Cita setMedicoReference( final String dummy ) {
		return this;
	}

	public String getMedicoNombre() {
		if ( null == this.medico ) return "-NOMBRE-NO-DISPONIBLE-";
		else return this.medico.getApellidos() + ", " + this.medico.getNombre();
	}

	public Cita setMedicoNombre( final String dummy ) {
		return this;
	}

	public String getEspecialidad() {
		if ( null != this.medico ) return this.medico.getEspecialidad();
		else return "-ESPECIALIDAD-";
	}

	public Cita setEspecialidad( final String dummy ) {
		return this;
	}

	public long getScn() {
		return scn;
	}

	public Cita setScn( final long scn ) {
		this.scn = scn;
		return this;
	}
}
// - UNUSED CODE ............................................................................................
//[01]
