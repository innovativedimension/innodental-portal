//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.dimensinfin.citamed.model.AuditModel;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Entity(name = "CitasTemplates")
@Table(name = "citastemplates")
public class CitasTemplate extends AuditModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long serialVersionUID = 9043305423577521576L;

	// - F I E L D - S E C T I O N ............................................................................
	@Id
	@GeneratedValue(generator = "citatemplate_generator")
	@SequenceGenerator(
			name = "citatemplate_generator",
			sequenceName = "citatemplate_sequence",
			initialValue = 200001
	)
	private long id;
	@ManyToOne
	@JoinColumn(name = "idservicio")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Medico servicio;
	@NotBlank
	@Size(min = 3, max = 100)
	@Column(name = "nombre")
	public String nombre;
	@Column(name = "tmhoraInicio")
	public String tmhoraInicio;
	@Column(name = "tthoraInicio")
	public String tthoraInicio;
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private ArrayList<Cita> morningList = new ArrayList<Cita>();
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private ArrayList<Cita> eveningList = new ArrayList<Cita>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- D E L E G A T E D   M E T H O D S
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("CitasTemplate [");
		if ( null != servicio ) buffer.append("CTT:").append(servicio.getId()).append(":").append(id).append(" ");
		buffer.append(nombre).append("[").append(morningList.size()).append("/").append(eveningList.size()).append("] ")
				.append("]");
		return buffer.toString();
	}

	// --- G E T T E R S   &   S E T T E R S

	public Long getId() {
		return id;
	}

	public Medico getServicio() {
		return servicio;
	}

	public String getNombre() {
		return nombre;
	}

	public String getTmhoraInicio() {
		return tmhoraInicio;
	}

	public String getTthoraInicio() {
		return tthoraInicio;
	}

	public ArrayList<Cita> getMorningList() {
		return morningList;
	}

	public ArrayList<Cita> getEveningList() {
		return eveningList;
	}

	public CitasTemplate setId(final Long id) {
		this.id = id;
		return this;
	}

	public CitasTemplate setServicio(final Medico servicio) {
		this.servicio = servicio;
		return this;
	}

	public CitasTemplate setNombre(final String nombre) {
		this.nombre = nombre;
		return this;
	}

	public CitasTemplate setTmhoraInicio(final String tmhoraInicio) {
		this.tmhoraInicio = tmhoraInicio;
		return this;
	}

	public CitasTemplate setTthoraInicio(final String tthoraInicio) {
		this.tthoraInicio = tthoraInicio;
		return this;
	}

	public CitasTemplate setMorningList(final ArrayList<Cita> morningList) {
		this.morningList = morningList;
		return this;
	}

	public CitasTemplate setEveningList(final ArrayList<Cita> eveningList) {
		this.eveningList = eveningList;
		return this;
	}
}

// - UNUSED CODE ............................................................................................
//[01]
