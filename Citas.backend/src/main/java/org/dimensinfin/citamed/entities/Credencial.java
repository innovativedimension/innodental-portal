//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.mindrot.jbcrypt.BCrypt;
import org.dimensinfin.citamed.model.AuditModel;
import org.dimensinfin.citamed.model.ServiceAuthorized;

/**
 * One <b>Credencial</b> is the login card and identification information for a service able to have appointments. There are some
 * different types of <b>Credenciales</b>. One of them matches the <b>Medico</b> concept while another is the database
 * representation for a medical service machine such as a radio machine or a tac.
 * Both representations will be able to define and have the property of <b>Citas</b>.
 * A <b>Credencial</b> is related
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Entity(name = "Credenciales")
@Table(name = "Credenciales")
public class Credencial extends AuditModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static String crypt( final String password, final String salt ) {
		return BCrypt.hashpw(password, salt);
	}

	// - F I E L D - S E C T I O N ............................................................................
	@Id
	@NotBlank
	@Size(min = 3, max = 40)
	@Column(name = "nif")
	private String nif;
	@NotBlank
	@JsonIgnore
	@Column(name = "password")
	private String password;
	@NotBlank
	@JsonIgnore
	@Column(name = "salt")
	private String salt;
	@NotBlank
	@Size(min = 3, max = 100)
	@Column(name = "nombre")
	private String nombre;
	@Size(max = 180)
	@Column(name = "apellidos")
	private String apellidos;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "centro_id")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Centro centro;
	@Column(name = "enabled")
	private boolean enabled = true;
	@NotBlank
	@Size(min = 4, max = 20)
	@Column(name = "role")
	private String role = "USUARIO";

	/**
	 * Stores the serialized Authorization data. This removes a table to store the detailed data that is only used on the frontend.
	 */
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private ArrayList<ServiceAuthorized> serviciosAutorizados = new ArrayList<ServiceAuthorized>();
	@Column(name = "allservices")
	private boolean allservices = false;

	transient String authorizationToken;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- G E T T E R S   &   S E T T E R S
	public String getNif() {
		return nif;
	}

	public String getPassword() {
		return password;
	}

	public String getSalt() {
		return salt;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public Centro getCentro() {
		return centro;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public String getRole() {
		return role;
	}

	public ArrayList<ServiceAuthorized> getServiciosAutorizados() {
		return serviciosAutorizados;
	}

	public boolean isAllservices() {
		return allservices;
	}

	public String getAuthorizationToken() {
		return authorizationToken;
	}

	public Credencial setNif( final String nif ) {
		this.nif = nif;
		return this;
	}

	public Credencial setPassword( final String password ) {
		this.password = password;
		return this;
	}

	public Credencial setSalt( final String salt ) {
		this.salt = salt;
		return this;
	}

	public Credencial setNombre( final String nombre ) {
		this.nombre = nombre;
		return this;
	}

	public Credencial setApellidos( final String apellidos ) {
		this.apellidos = apellidos;
		return this;
	}

	public Credencial setCentro( final Centro centro ) {
		this.centro = centro;
		return this;
	}

	public Credencial setEnabled( final boolean enabled ) {
		this.enabled = enabled;
		return this;
	}

	public Credencial setRole( final String role ) {
		this.role = role;
		return this;
	}

	public Credencial setServiciosAutorizados( final ArrayList<ServiceAuthorized> serviciosAutorizados ) {
		this.serviciosAutorizados = serviciosAutorizados;
		return this;
	}

	public Credencial addServicioAutorizado( final ServiceAuthorized _servicio ) {
		if ( null != _servicio ) this.serviciosAutorizados.add(_servicio);
		return this;
	}

	public Credencial setAllservices( final boolean allservices ) {
		this.allservices = allservices;
		return this;
	}

	public Credencial setAuthorizationToken( final String authorizationToken ) {
		this.authorizationToken = authorizationToken;
		return this;
	}

	// --- D E L E G A T E D   M E T H O D S
	@Override
	public String toString() {
		return new StringBuffer("Credencial [")
				.append(nif).append(" ")
				.append("]")
				.toString();
	}
}

// - UNUSED CODE ............................................................................................
//[01]
