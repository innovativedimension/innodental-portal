//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.dimensinfin.citamed.model.AuditModel;
import org.dimensinfin.citamed.model.Limitador;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Entity(name = "LimitesServicio")
@Table(name = "LimitesServicio")
public class LimitesServicio extends AuditModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long serialVersionUID = 908137654551832565L;

	// - F I E L D - S E C T I O N ............................................................................
	@Id
	@GeneratedValue(generator = "limitesservicio_generator")
	@SequenceGenerator(
			name = "limitesservicio_generator",
			sequenceName = "limitesservicio_sequence",
			initialValue = 300001
	)
	private long id;
	@ManyToOne
	@JoinColumn(name = "idservicio")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Medico servicio;
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private ArrayList<Limitador> limites = new ArrayList<Limitador>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- D E L E G A T E D   M E T H O D S
//	@Override
//	public String toString() {
//		StringBuffer buffer = new StringBuffer("CitasTemplate [");
//		if ( null != servicio ) buffer.append("CTT:").append(servicio.getId()).append(":").append(id).append(" ");
//		buffer.append(nombre).append("[").append(morningList.size()).append("/").append(eveningList.size()).append("] ")
//				.append("]");
//		return buffer.toString();
//	}

	// --- G E T T E R S   &   S E T T E R S

	public long getId() {
		return id;
	}

	public Medico getServicio() {
		return servicio;
	}

	public ArrayList<Limitador> getLimites() {
		return limites;
	}

	public LimitesServicio setServicio(final Medico servicio) {
		this.servicio = servicio;
		return this;
	}

	public LimitesServicio setLimites(final ArrayList<Limitador> limites) {
		this.limites = limites;
		return this;
	}
}

// - UNUSED CODE ............................................................................................
//[01]
