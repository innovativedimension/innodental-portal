//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.dimensinfin.citamed.model.ActoMedico;
import org.dimensinfin.citamed.model.AuditModel;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Entity
@Table(name = "Medicos")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Medico extends AuditModel {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long serialVersionUID = 8827857735721877274L;
//	private static Logger logger = LoggerFactory.getLogger("Medico");

	public static String generateNewId( final Long centroIdentifier, final String referencia ) {
		return new StringBuilder("MID:").append(centroIdentifier).append(":").append(referencia).toString();
	}

	// - F I E L D - S E C T I O N ............................................................................
	@Id
	private String id;
	@ManyToOne
	@JoinColumn(name = "centro_id")
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Centro centro;
	@NotBlank
	@Size(min = 3, max = 40)
	@Column(name = "referencia")
	private String referencia;
	@NotBlank
	@Size(min = 3, max = 10)
	@Column(name = "tratamiento")
	private String tratamiento = "Dr.";
	@NotBlank
	@Size(min = 3, max = 50)
	@Column(name = "nombre")
	private String nombre;
	@Size(max = 100)
	@Column(name = "apellidos")
	private String apellidos;
	@NotBlank
	@Size(min = 3, max = 50)
	@Column(name = "especialidad")
	private String especialidad;
	@Column(name = "activo")
	private boolean activo = true;
	@Column(name = "appointmentsCount")
	private int appointmentsCount = 0;
	@Column(name = "freeAppointmentsCount")
	private int freeAppointmentsCount = 0;
	@JoinColumn(name = "firstappointment_id")
	private long firstAppointmentId = -1;
	@Transient
	public Cita firstAppointment = null;
	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private ArrayList<ActoMedico> actosMedicos = new ArrayList<ActoMedico>();

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- G E T T E R S   &   S E T T E R S
	public String getId() {
		return id;
	}

	public String getReferencia() {
		return referencia;
	}

	public String getTratamiento() {
		return tratamiento;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public Centro getCentro() {
		return centro;
	}

//	public String getLocalidad() {
//		return localidad;
//	}
//
//	public String getDireccion() {
//		return direccion;
//	}
//
//	public String getHorario() {
//		return horario;
//	}
//
//	public String getTelefono() {
//		return telefono;
//	}

	public int getFreeAppointmentsCount() {
		return this.freeAppointmentsCount;
	}

	public Cita getFirstAppointment() {
		return this.firstAppointment;
	}

	public long getFirstAppointmentId() {
		return this.firstAppointmentId;
	}

	public Medico setId( final String id ) {
		this.id = id;
		return this;
	}

//	public Medico setCredencial(final Credencial credencial) {
//		this.credencial = credencial;
//		return this;
//	}

	public Medico setReferencia( final String referencia ) {
		this.referencia = referencia;
		return this;
	}

	public Medico setTratamiento( final String tratamiento ) {
		this.tratamiento = tratamiento;
		return this;
	}

	public Medico setNombre( final String nombre ) {
		this.nombre = nombre;
		return this;
	}

	public Medico setApellidos( final String apellidos ) {
		this.apellidos = apellidos;
		return this;
	}

	public Medico setEspecialidad( final String especialidad ) {
		this.especialidad = especialidad;
		return this;
	}

	public Medico setCentro( final Centro centro ) {
		this.centro = centro;
		return this;
	}

//	public Medico setLocalidad(final String localidad) {
//		this.localidad = localidad;
//		return this;
//	}
//
//	public Medico setDireccion(final String direccion) {
//		this.direccion = direccion;
//		return this;
//	}
//
//	public Medico setHorario(final String horario) {
//		this.horario = horario;
//		return this;
//	}
//
//	public Medico setTelefono(final String telefono) {
//		this.telefono = telefono;
//		return this;
//	}

	public Medico setFreeAppointmentsCount( final int freeAppointmentCount ) {
		this.freeAppointmentsCount = freeAppointmentCount;
		return this;
	}

	public Medico setFirstAppointment( final Cita firstAppointment ) {
		this.firstAppointment = firstAppointment;
		return this;
	}

	public Medico setFirstAppointmentId( final long firstAppointment ) {
		this.firstAppointmentId = firstAppointment;
		return this;
	}

	public boolean isActivo() {
		return activo;
	}

	public Medico setActivo( final boolean activo ) {
		this.activo = activo;
		return this;
	}

	public int getAppointmentsCount() {
		return appointmentsCount;
	}

	public Medico setAppointmentsCount( final int appointmentsCount ) {
		this.appointmentsCount = appointmentsCount;
		return this;
	}

	public ArrayList<ActoMedico> getActosMedicos() {
		return actosMedicos;
	}

	public Medico setActosMedicos( final ArrayList<ActoMedico> actosMedicos ) {
		this.actosMedicos = actosMedicos;
		return this;
	}

	// --- O V E R R I D E D   M E T H O D S
	@Override
	public String toString() {
		return String.format("Medico [ id:%s nombre:'%s' esp:%s]",
				this.id,
				this.nombre + " " + this.apellidos,
				this.especialidad
		);
	}
}
// - UNUSED CODE ............................................................................................
//[01]
