//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.model;

import java.io.Serializable;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class ActoMedico implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
//	private static Logger logger = LoggerFactory.getLogger("ActoMedico");

	// - F I E L D - S E C T I O N ............................................................................
	public String etiqueta = "M";
	public String nombre;
	public int tiempoRequerido = 15;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
//	public ActoMedico() {
//	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- G E T T E R S   &   S E T T E R S
	public String getEtiqueta() {
		return etiqueta;
	}

	public ActoMedico setEtiqueta( final String etiqueta ) {
		this.etiqueta = etiqueta;
		return this;
	}

	public String getNombre() {
		return nombre;
	}

	public ActoMedico setNombre( final String nombre ) {
		this.nombre = nombre;
		return this;
	}

	public int getTiempoRequerido() {
		return tiempoRequerido;
	}

	public ActoMedico setTiempoRequerido( final int tiempoRequerido ) {
		this.tiempoRequerido = tiempoRequerido;
		return this;
	}

	// --- D E L E G A T E D   M E T H O D S
	@Override
	public String toString() {
		return new StringBuffer("ActoMedico [")
				.append("[").append(this.etiqueta).append("] ").append(nombre).append(" ")
				.append("]")
				.toString();
	}
}

// - UNUSED CODE ............................................................................................
//[01]
