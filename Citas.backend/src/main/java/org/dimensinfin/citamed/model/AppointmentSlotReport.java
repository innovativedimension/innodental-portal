//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.model;

import org.dimensinfin.citamed.entities.Cita;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class AppointmentSlotReport extends Node{
	// - S T A T I C - S E C T I O N ..........................................................................
//	private static Logger logger = LoggerFactory.getLogger("AppointmentReport");

	// - F I E L D - S E C T I O N ............................................................................
	private int freeAppointmentCount = 0;
	private Cita firstAppointment;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AppointmentSlotReport() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- G E T T E R S   &   S E T T E R S

	public int getFreeAppointmentCount() {
		return freeAppointmentCount;
	}

	public Cita getFirstAppointment() {
		return firstAppointment;
	}

	public AppointmentSlotReport setFreeAppointmentCount(final int freeAppointmentCount) {
		this.freeAppointmentCount = freeAppointmentCount;
		return this;
	}

	public AppointmentSlotReport setFirstAppointment(final Cita firstAppointment) {
		this.firstAppointment = firstAppointment;
		return this;
	}

	// --- D E L E G A T E D   M E T H O D S
//	@Override
//	public String toString() {
//		return new StringBuffer("AppointmentSlotReport [")
//				.append("field:").append().append(" ")
//				.append("]")
//				.append("->").append(super.toString())
//				.toString();
//	}
}

// - UNUSED CODE ............................................................................................
//[01]
