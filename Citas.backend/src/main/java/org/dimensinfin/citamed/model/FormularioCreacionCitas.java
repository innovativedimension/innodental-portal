//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.model;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class FormularioCreacionCitas extends Node {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("FormularioCreacionCitas");

	// - F I E L D - S E C T I O N ............................................................................
	public LocalDate fechaInicio;
	public LocalDate fechaFin;
	public boolean lunes = false;
	public boolean martes = false;
	public boolean miercoles = false;
	public boolean jueves = false;
	public boolean viernes = false;
	public boolean sabado = false;
	public int duracion = 30;
	public String horario = "MAÑANA";
	public LocalTime horaInicio = LocalTime.of(9, 0);
	public LocalTime horaFin = LocalTime.of(13, 0);
	public boolean clearData = false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public FormularioCreacionCitas() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public boolean isDayOfWeekActive(final DayOfWeek day) {
		if ( day.getValue() == 1 )
			if ( lunes )
				return true;
		if ( day.getValue() == 2 )
			if ( martes )
				return true;
		if ( day.getValue() == 3 )
			if ( miercoles )
				return true;
		if ( day.getValue() == 4 )
			if ( jueves )
				return true;
		if ( day.getValue() == 5 )
			if ( viernes )
				return true;
		if ( day.getValue() == 6 )
			if ( sabado )
				return true;
		return false;
	}

	// --- G E T T E R S   &   S E T T E R S
	public LocalDate getFechaInicio() {
		return fechaInicio;
	}

	public LocalDate getFechaFin() {
		return fechaFin;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public int getDuracion() {
		return duracion;
	}

	public String getHorario() {
		return horario;
	}

	public boolean isClearData() {
		return clearData;
	}

	public FormularioCreacionCitas setFechaInicio(final LocalDate fechaInicio) {
		this.fechaInicio = fechaInicio;
		return this;
	}

	public FormularioCreacionCitas setFechaInicio(final String fechaInicio) {
		// Convert the string to the date.
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		this.fechaInicio = LocalDate.parse(fechaInicio, dtf);
		return this;
	}

	public FormularioCreacionCitas setFechaFin(final LocalDate fechaFin) {
		this.fechaFin = fechaFin;
		return this;
	}

	public FormularioCreacionCitas setFechaFin(final String fechaInicio) {
		// Convert the string to the date.
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		this.fechaFin = LocalDate.parse(fechaInicio, dtf);
		return this;
	}

	public FormularioCreacionCitas setLunes(final boolean lunes) {
		this.lunes = lunes;
		return this;
	}

	public FormularioCreacionCitas setMartes(final boolean martes) {
		this.martes = martes;
		return this;
	}

	public FormularioCreacionCitas setMiercoles(final boolean miercoles) {
		this.miercoles = miercoles;
		return this;
	}

	public FormularioCreacionCitas setJueves(final boolean jueves) {
		this.jueves = jueves;
		return this;
	}

	public FormularioCreacionCitas setViernes(final boolean viernes) {
		this.viernes = viernes;
		return this;
	}

	public FormularioCreacionCitas setSabado(final boolean sabado) {
		this.sabado = sabado;
		return this;
	}

	public FormularioCreacionCitas setDuracion(final int duracion) {
		this.duracion = duracion;
		return this;
	}

	public FormularioCreacionCitas setHorario(final String horario) {
		this.horario = horario;
		return this;
	}

	public FormularioCreacionCitas setHoraInicio(final String horaInicio) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		this.horaInicio = LocalTime.parse(horaInicio);
		return this;
	}

	public FormularioCreacionCitas setHoraFin(final String horaFin) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
		this.horaFin = LocalTime.parse(horaFin, dtf);
		return this;
	}

	public FormularioCreacionCitas setClearData(final boolean clearData) {
		this.clearData = clearData;
		return this;
	}

	// --- O V E R R I D E D   M E T H O D S
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer("FormularioCreacionCitas [");
		if ( null != this.fechaInicio ) buffer.append("fechaInicio:").append(this.fechaInicio).append(" ");
		buffer.append("]")
				.append("->").append(super.toString());
		return buffer.toString();
	}
}

// - UNUSED CODE ............................................................................................
//[01]
