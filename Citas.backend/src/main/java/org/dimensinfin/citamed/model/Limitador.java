//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.model;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class Limitador extends Node {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long serialVersionUID = 7908385671110214620L;

	// - F I E L D - S E C T I O N ............................................................................
	private String tipo = "-BLOCKING-";
	private String icon;
	private String titulo;
	private String descripcion;
	private int valorLimite = 10;
	private String valorReferencia;
	private String periodo = "-DAY-";
	private boolean activeState = false;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Limitador() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- G E T T E R S   &   S E T T E R S

	public String getTipo() {
		return tipo;
	}

	public String getIcon() {
		return icon;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public int getValorLimite() {
		return valorLimite;
	}

	public String getValorReferencia() {
		return valorReferencia;
	}

	public String getPeriodo() {
		return periodo;
	}

	public boolean isActiveState() {
		return activeState;
	}

	public Limitador setTipo(final String tipo) {
		this.tipo = tipo;
		return this;
	}

	public Limitador setIcon(final String icon) {
		this.icon = icon;
		return this;
	}

	public Limitador setTitulo(final String titulo) {
		this.titulo = titulo;
		return this;
	}

	public Limitador setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
		return this;
	}

	public Limitador setValorLimite(final int valorLimite) {
		this.valorLimite = valorLimite;
		return this;
	}

	public Limitador setValorReferencia(final String valorReferencia) {
		this.valorReferencia = valorReferencia;
		return this;
	}

	public Limitador setPeriodo(final String periodo) {
		this.periodo = periodo;
		return this;
	}

	public Limitador setActiveState(final boolean activeState) {
		this.activeState = activeState;
		return this;
	}

	// --- D E L E G A T E D   M E T H O D S
//	@Override
//	public String toString() {
//		return new StringBuffer("Limitador [")
//				.append("field:").append().append(" ")
//				.append("]")
//				.append("->").append(super.toString())
//				.toString();
//	}
}

// - UNUSED CODE ............................................................................................
//[01]
