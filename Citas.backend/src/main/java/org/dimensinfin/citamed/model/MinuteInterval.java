//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.model;

import org.dimensinfin.citamed.entities.Cita;

import datastructures.Interval;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class MinuteInterval implements Interval {
	// - S T A T I C - S E C T I O N ..........................................................................

	// - F I E L D - S E C T I O N ............................................................................
	private int startMinute = 0;
	private int endMinute = 0;
	private Cita payload;

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public String toString() {
		return "start: " + startMinute + " end: " + endMinute;
	}

	//--- I N T E R V A L   I N T E R F A C E
	@Override
	public int start() {
		return startMinute;
	}

	@Override
	public int end() {
		return endMinute;
	}

	//--- G E T T E R S   &   S E T T E R S

	public int getStartMinute() {
		return startMinute;
	}

	public int getEndMinute() {
		return endMinute;
	}

	public Cita getPayload() {
		return payload;
	}

	public MinuteInterval setPayload(final Cita payload) {
		this.payload = payload;
		return this;
	}

	public MinuteInterval setStart(final int startPoint) {
		this.startMinute = startPoint;
		return this;
	}

	public MinuteInterval setEnd(final int endPoint) {
		this.endMinute = endPoint;
		return this;
	}

	//--- C O M P A R A B L E   I N T E R F A C E
	@Override
	public boolean equals(Object other) {
		// No need for null check. The instanceof operator returns false if (other == null).
		if ( !(other instanceof MinuteInterval) ) {
			return false;
		}

		return startMinute == ((MinuteInterval) other).startMinute && endMinute == ((MinuteInterval) other).endMinute;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + startMinute;
		result = 31 * result + endMinute;
		return result;
	}
}
// - UNUSED CODE ............................................................................................
//[01]
