//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.model;

import org.dimensinfin.citamed.entities.Cita;
import org.dimensinfin.citamed.entities.Medico;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class OpenAppointment extends Node {
	// - S T A T I C - S E C T I O N ..........................................................................
//	private static Logger logger = LoggerFactory.getLogger("OpenAppointment");

	// - F I E L D - S E C T I O N ............................................................................
	private Cita cita;
	private Medico medico;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public OpenAppointment() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public Cita getCita() {
		return cita;
	}

	public Medico getMedico() {
		return medico;
	}

	public OpenAppointment setCita(final Cita cita) {
		this.cita = cita;
		return this;
	}

	public OpenAppointment setMedico(final Medico medico) {
		this.medico = medico;
		return this;
	}
}

// - UNUSED CODE ............................................................................................
//[01]
