//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.model;

import java.io.Serializable;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class Paciente implements Serializable {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static final long serialVersionUID = 499865936591044756L;

	// - F I E L D - S E C T I O N ............................................................................
	public String identificador;
	public String tefContacto;
	public String nombre;
	public String apellidos;
	public String email;
	public boolean perteneceAseguradora;
	public String aseguradora;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public Paciente() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public String getIdentificador() {
		return identificador;
	}

	public String getTefContacto() {
		return tefContacto;
	}

	public String getNombre() {
		return nombre;
	}

	public String getEmail() {
		return email;
	}

	public boolean isPerteneceAseguradora() {
		return perteneceAseguradora;
	}

	public String getAseguradora() {
		return aseguradora;
	}

	public Paciente setIdentificador( final String identificador ) {
		this.identificador = identificador;
		return this;
	}

	public Paciente setTefContacto( final String tefContacto ) {
		this.tefContacto = tefContacto;
		return this;
	}

	public Paciente setNombre( final String nombre ) {
		this.nombre = nombre;
		return this;
	}

	public Paciente setEmail( final String email ) {
		this.email = email;
		return this;
	}

	public Paciente setPerteneceAseguradora( final boolean perteneceAseguradora ) {
		this.perteneceAseguradora = perteneceAseguradora;
		return this;
	}

	public Paciente setAseguradora( final String aseguradora ) {
		this.aseguradora = aseguradora;
		return this;
	}

	public String getApellidos() {
		return apellidos;
	}

	public Paciente setApellidos( final String apellidos ) {
		this.apellidos = apellidos;
		return this;
	}
}

// - UNUSED CODE ............................................................................................
//[01]
