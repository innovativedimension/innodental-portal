//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.model;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class ServiceAuthorized extends Authorization {
	// - S T A T I C - S E C T I O N ..........................................................................
//	private static Logger logger = LoggerFactory.getLogger("ServiceAuthorized");

	// - F I E L D - S E C T I O N ............................................................................
	private String serviceIdentifier;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ServiceAuthorized() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	// --- G E T T E R S   &   S E T T E R S
	public String getServiceIdentifier() {
		return serviceIdentifier;
	}

	public ServiceAuthorized setServiceIdentifier( final String serviceIdentifier ) {
		this.serviceIdentifier = serviceIdentifier;
		return this;
	}

	// --- D E L E G A T E D   M E T H O D S
//	@Override
//	public String toString() {
//		return new StringBuffer("ServiceAuthorized [")
//				.append("field:").append().append(" ")
//				.append("]")
//				.append("->").append(super.toString())
//				.toString();
//	}
}

// - UNUSED CODE ............................................................................................
//[01]
