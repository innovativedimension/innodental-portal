//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.dimensinfin.citamed.entities.Cita;

public interface CitaRepository extends JpaRepository<Cita, Long> {
	@Query("SELECT ct FROM Citas ct WHERE medico_id=:medid ORDER BY fecha, huecoId")
	List<Cita> findByMedicoIdentifier( @Param("medid") String medicoId );

	@Query("SELECT ct FROM Citas ct WHERE referencia=:medid ORDER BY fecha, huecoId")
	List<Cita> findByMedicoByReferencia( @Param("medid") String medicoId );

	@Query("SELECT ct FROM Citas ct WHERE medico_id=:medid AND YEAR(ct.fecha) = :year AND MONTH(ct.fecha) = :month ORDER BY fecha, " +
			"huecoId")
	List<Cita> findByMedicoYearMonth( @Param("medid") String medicoId, @Param("year") Integer year, @Param("month") Integer
			month );

	@Query("SELECT ct FROM Citas ct WHERE YEAR(ct.fecha) = :year AND MONTH(ct.fecha) = :month ORDER BY fecha, " +
			"huecoId")
	List<Cita> findByYearMonth( @Param("year") Integer year, @Param("month") Integer month );

	@Query("SELECT ct FROM Citas ct WHERE medico_id=:medid AND estado='LIBRE' AND fecha > :limitDate ORDER BY fecha, huecoId")
	Page<Cita> findNextFree( @Param("medid") String medicoId, @Param("limitDate") LocalDate limitDate, Pageable pageable );

	@Query("SELECT ct FROM Citas ct WHERE medico_id=:medid AND fecha = :targetDate ORDER BY fecha, huecoId")
	List<Cita> findAppointments4Date( @Param("medid") String medicoId, @Param("targetDate") LocalDate targetDate );

	@Query("SELECT ct FROM Citas ct WHERE referencia=:ref AND fecha = :targetDate ORDER BY fecha, huecoId")
	List<Cita> findAppointments4Referencia( @Param("ref") String referencia, @Param("targetDate") LocalDate targetDate );

	@Query("SELECT ct FROM Citas ct WHERE estado='BLOQUEADA' ORDER BY fecha, huecoId")
	List<Cita> findBlockedAppointments();

	@Query("SELECT ct FROM Citas ct WHERE referencia=:ref AND estado='LIBRE' ORDER BY fecha, huecoId")
	List<Cita> countFreeAppointments4Referencia( @Param("ref") String referencia );

	@Query("SELECT ct FROM Citas ct WHERE referencia=:ref ORDER BY fecha, huecoId")
	List<Cita> countAppointments4Referencia( @Param("ref") String referencia );

	@Query("SELECT ct FROM Citas ct WHERE pacienteLocalizador=:patid ORDER BY fecha, huecoId ASC")
	List<Cita> findCitasByPatient( @Param("patid") String identificador );

	// - M E T R I C S
//	@Query("SELECT ct FROM Citas ct WHERE referencia=:ref ORDER BY fecha, huecoId")
//	List<Cita> countAllLIBREAppointments(  );
	// - E X T R A C T I O N
	@Query("SELECT ct FROM Citas ct WHERE scn >= :start ORDER BY scn ASC")
	List<Cita> extractCitasBySCN( @Param("start") Long start );
}
