//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.security;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.dimensinfin.citamed.CitasBackendApplication;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class AppSession implements Serializable{
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("AppSession");

	// - F I E L D - S E C T I O N ............................................................................
	private static final long serialVersionUID = 4860200580420516458L;
	private String authorizationToken;
	private String payload;
	private String authorizationPassword;
	private String userIdentifier;
	private String role;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public AppSession() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void store() {
		CitasBackendApplication.getSessionManager().store(this);
	}

	// --- G E T T E R S   &   S E T T E R S
	public String getId() {
		return this.authorizationToken;
	}

	public String getAuthorizationToken() {
		return authorizationToken;
	}

	public AppSession setAuthorizationToken( final String authorizationToken ) {
		this.authorizationToken = authorizationToken;
		return this;
	}

	public String getPayload() {
		return payload;
	}

	public AppSession setPayload( final String payload ) {
		this.payload = payload;
		return this;
	}

	public String getAuthorizationPassword() {
		return authorizationPassword;
	}

	public AppSession setAuthorizationPassword( final String authorizationPassword ) {
		this.authorizationPassword = authorizationPassword;
		return this;
	}

	public String getUserIdentifier() {
		return userIdentifier;
	}

	public AppSession setUserIdentifier( final String userIdentifier ) {
		this.userIdentifier = userIdentifier;
		return this;
	}

	public String getRole() {
		return role;
	}

	public AppSession setRole( final String role ) {
		this.role = role;
		return this;
	}
}

// - UNUSED CODE ............................................................................................
//[01]
