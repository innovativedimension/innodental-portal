//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.security;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.dimensinfin.citamed.CitasBackendApplication;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class SessionManager implements ISessionManager {
	// - S T A T I C - S E C T I O N ..........................................................................
	public static Logger logger = LoggerFactory.getLogger("SessionManager");

	// - F I E L D - S E C T I O N ............................................................................
	public HashMap<String, AppSession> sessionStore = new HashMap();

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public void store( final AppSession _session ) {
		sessionStore.put(_session.getId(), _session);
	}

	public AppSession retrieve( final String _id ) {
		return sessionStore.get(_id);
	}

	public void readSessionData() {
		logger.info(">> [SessionManager.readSessionData]");
		final String cacheFileName = CitasBackendApplication.getConfiguration().getResourceString("R.cache.directorypath")
				+ CitasBackendApplication.getConfiguration().getResourceString("R.runtime.session.savedata.filename");
		logger.info("-- [SessionManager.readSessionData]> Opening cache file: {}", cacheFileName);
		try {
			final BufferedInputStream buffer = new BufferedInputStream(
					CitasBackendApplication.openResource4Input(cacheFileName)
			);
			final ObjectInputStream input = new ObjectInputStream(buffer);
			try {
				synchronized (sessionStore) {
					sessionStore = (HashMap<String, AppSession>) input.readObject();
					logger.info("-- [SessionManager.readSessionData]> Restored Session Data: " + sessionStore.size()
							+ " entries.");
				}
			} finally {
				input.close();
				buffer.close();
			}
		} catch ( final ClassNotFoundException ex ) {
			logger.warn("W> [SessionManager.readSessionData]> ClassNotFoundException."); //$NON-NLS-1$
		} catch ( final FileNotFoundException fnfe ) {
			logger.warn("W> [SessionManager.readSessionData]> FileNotFoundException."); //$NON-NLS-1$
		} catch ( final IOException ex ) {
			logger.warn("W> [SessionManager.readSessionData]> IOException. {}", ex.getMessage()); //$NON-NLS-1$
		} catch ( final RuntimeException rex ) {
			rex.printStackTrace();
		} finally {
			logger.info("<< [SessionManager.readSessionData]");
		}
	}

	public void writeSessionData() {
		logger.info(">> [SessionManager.writeSessionData]");
		final String cacheFileName = CitasBackendApplication.getConfiguration().getResourceString("R.cache.directorypath")
				+ CitasBackendApplication.getConfiguration().getResourceString("R.runtime.session.savedata.filename");
		logger.info("-- [SessionManager.writeSessionData]> Opening cache file: {}", cacheFileName);
		//		File modelStoreFile = new File(cacheFileName);
		try {
			final BufferedOutputStream buffer = new BufferedOutputStream(
					CitasBackendApplication.openResource4Output(cacheFileName)
			);
			final ObjectOutput output = new ObjectOutputStream(buffer);
			try {
				synchronized (sessionStore) {
					output.writeObject(sessionStore);
					logger.info(
							"-- [SessionManager.writeSessionData]> Wrote Session Data: " + sessionStore.size() + " entries.");
				}
			} finally {
				output.flush();
				output.close();
				buffer.close();
			}
		} catch ( final FileNotFoundException fnfe ) {
			logger.warn("W> [SessionManager.writeSessionData]> FileNotFoundException."); //$NON-NLS-1$
		} catch ( final IOException ex ) {
			logger.warn("W> [SessionManager.writeSessionData]> IOException."); //$NON-NLS-1$
		} finally {
			logger.info("<< [SessionManager.writeSessionData]");
		}
	}
}

// - UNUSED CODE ............................................................................................
//[01]
