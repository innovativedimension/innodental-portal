//  PROJECT:     CitaMed.backend (CMBK.SB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.security;

import org.redisson.api.RMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.dimensinfin.citamed.CitasBackendApplication;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class SessionManagerRedisson implements ISessionManager {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("SessionManagerRedisson");

	// - F I E L D - S E C T I O N ............................................................................
	public RMap<String, AppSession> sessionStore = CitasBackendApplication.redisson.getMap("Citas.backend");

	// - C O N S T R U C T O R - S E C T I O N ................................................................

	// - M E T H O D - S E C T I O N ..........................................................................
	public void store( final AppSession _session ) {
		sessionStore.put(_session.getId(), _session);
	}

	public AppSession retrieve( final String _id ) {
		return sessionStore.get(_id);
	}

	@Override
	public void readSessionData() {

	}

	@Override
	public void writeSessionData() {
		logger.info(">> [SessionManagerRedisson.writeSessionData]> Recorded sessions: {}",
				this.sessionStore.size());
	}
}

// - UNUSED CODE ............................................................................................
//[01]
