//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.services;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.dimensinfin.citamed.CitasBackendApplication;
import org.dimensinfin.citamed.entities.Cita;
import org.dimensinfin.citamed.entities.CitasTemplate;
import org.dimensinfin.citamed.entities.Credencial;
import org.dimensinfin.citamed.entities.Medico;
import org.dimensinfin.citamed.model.FormularioCreacionCitas;
import org.dimensinfin.citamed.model.MinuteInterval;
import org.dimensinfin.citamed.model.Node;
import org.dimensinfin.citamed.repository.CentroRepository;
import org.dimensinfin.citamed.repository.CitaRepository;
import org.dimensinfin.citamed.repository.CredencialRepository;
import org.dimensinfin.citamed.repository.MedicoRepository;

import datastructures.IntervalTree;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Service
@Qualifier("AppointmentCreation")
public class AppointmentCreationService implements ICreationService {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("AppointmentCreationService");

	private static final String DELETE_UNUSED_APPOINTMENTS4DATE = "DELETE FROM Citas WHERE estado = 'LIBRE' AND medico_id=:medid AND " +
			"fecha =" +
			" " +
			":targetDate";

	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	private EntityManager entityManager;
	@Autowired
	protected CredencialRepository credencialRepository;
	@Autowired
	protected CentroRepository centroRepository;
	@Autowired
	protected MedicoRepository medicoRepository;
	@Autowired
	protected CitaRepository citaRepository;

	// - M E T H O D - S E C T I O N ..........................................................................
	public Optional<Medico> findById( String consultaReference ) {
		return medicoRepository.findById(consultaReference);
	}

	public boolean checkUserIsEnabled( final String userIdentifier ) {
		if ( null == userIdentifier ) return false;
		final Optional<Credencial> user = credencialRepository.findById(userIdentifier);
		return user.get().isEnabled();
	}

	/**
	 * This method will use the Interval Tree algorithm to detect collisions from the already created slots to the new slots to be
	 * added. In case there is a collision then the slot is not created and the old slot and its data is kept.
	 * The process starts creating a tree from the already data stored on the database. After this point the algorithm will start
	 * creating new slots if they do not collide with current used slots.
	 * @return the number of slots created successfully.
	 */
	@Transactional
	public AppointmentReport createDayAppointments( final Medico medico, final LocalDate currentDate, final FormularioCreacionCitas
			formulario ) {
		logger.info(">> [AppointmentCreationService.createDayAppointments]");
		final AppointmentReport report = new AppointmentReport(currentDate);

		// First test if this date is in the past. If so we can register the report indicating the cause to not generate appointments.
		if ( currentDate.atStartOfDay().isBefore(LocalDate.now().atStartOfDay()) )
			return report.setComment("Citas no generadas. Fecha en el pasado");
		try {
			// Read all records for current date and initialize the interval tree.
			final List<Cita> intervals = citaRepository.findAppointments4Referencia(medico.getReferencia(), currentDate);
			IntervalTree<MinuteInterval> tree = new IntervalTree<MinuteInterval>();
			if ( intervals.size() > 0 ) {
				for ( Cita currentCita : intervals ) {
					// Get the start point form the slot identifier.
					final int startTime = currentCita.getHuecoId();
					final int hour = Math.toIntExact(currentCita.getHuecoId() / 100);
					final int minute = currentCita.getHuecoId() - hour * 100;
					final LocalTime endTime = LocalTime.of(hour, minute).plusMinutes(currentCita.getHuecoDuracion());
					// Convert to hour.
					tree.insert(new MinuteInterval()
							.setStart(currentCita.getHuecoId())
							.setEnd(endTime.getHour() * 100 + endTime.getMinute())
							.setPayload(currentCita));
				}
			}
			report.prevSlotCount(intervals.size());

			// Start processing the data in a loop starting at the request start time.
			int slotCount = 0;
			// Now check if the day of week matches.
			final DayOfWeek weekDay = currentDate.getDayOfWeek();
			if ( null == formulario.getHoraInicio() ) return report;
			if ( null == formulario.getHoraFin() ) return report;
			if ( formulario.isDayOfWeekActive(weekDay) ) {
				// The day is active. Register the slots starting by the start time.
				for ( LocalTime time = formulario.getHoraInicio();
				      time.isBefore(formulario.getHoraFin());
				      time = time.plusMinutes(formulario.getDuracion()) ) {
					logger.info("-- [AppointmentCreationService.createDayAppointments]> Time: {}", time);
					final LocalTime endTime = time.plusMinutes(formulario.duracion);
					Cita cita = new Cita()
							.setFecha(currentDate)
							.setReferencia(medico.getReferencia())
							.setEstado(Cita.ECitaStates.LIBRE.name())
							.setHuecoId(time.getHour() * 100 + time.getMinute())
							.setHuecoDuracion(formulario.duracion)
							.setMedico(medico)
							.setZonaHoraria(formulario.getHorario());
					entityManager.persist(cita);
//					citaRepository.save(cita);

					// Store the new Cita instance on the interval. After all interval creation just delete overlappers and then create Citas.
					final MinuteInterval newInterval = new MinuteInterval()
							.setStart(time.getHour() * 100 + time.getMinute())
							.setEnd(endTime.getHour() * 100 + endTime.getMinute())
							.setPayload(cita);

					// Check if this new interval overlaps any other at the tree.
					// Remove overlappers if found. Remove the new one being added.
					tree.overlappers(newInterval)
							.forEachRemaining(( interval ) -> {
								// Cancel the new appointment creation and report it as a collision.
								report.registerCollision(newInterval, interval);
								citaRepository.delete(cita);
							});
					logger.info("-- [AppointmentCreationService.createDayAppointments]> Creating slot: {}",
							"(" + "--" + ")" + currentDate + "-" + (time.getHour() * 100 + time.getMinute()));
					slotCount++;
				}
				// Update the medico first appointment.
				this.updateMedicoReport(medico);
			}
			report.newSlotCount(slotCount);
			return report;
		} catch ( RuntimeException rte ) {
			rte.printStackTrace();
		} finally {
			logger.info("<< [AppointmentCreationService.createDayAppointments]");
		}
		return report;
	}

	/**
	 * This method will use a appointment templete to create the appointments instead the periodic creation loop.
	 * @param medico
	 * @param currentDate
	 * @param template
	 * @return
	 */
	@Transactional
	public AppointmentReport createDayAppointments( final Medico medico, final LocalDate currentDate, final CitasTemplate
			template ) {
		logger.info(">> [AppointmentCreationService.createDayAppointments]");
		final AppointmentReport report = new AppointmentReport(currentDate);

		// First test if this date is in the past. If so we can register the report indicating the cause to not generate appointments.
		if ( currentDate.atStartOfDay().isBefore(LocalDate.now().atStartOfDay()) )
			return report.setComment("Citas no generadas. Fecha en el pasado");
		try {
			// Read all records for current date and initialize the interval tree.
			final List<Cita> intervals = citaRepository.findAppointments4Referencia(medico.getReferencia(), currentDate);
			IntervalTree<MinuteInterval> tree = new IntervalTree<MinuteInterval>();
			if ( intervals.size() > 0 ) {
				for ( Cita currentCita : intervals ) {
					// Get the start point form the slot identifier.
					final int startTime = currentCita.getHuecoId();
					final int hour = Math.toIntExact(currentCita.getHuecoId() / 100);
					final int minute = currentCita.getHuecoId() - hour * 100;
					final LocalTime endTime = LocalTime.of(hour, minute).plusMinutes(currentCita.getHuecoDuracion());
					// Convert to hour.
					tree.insert(new MinuteInterval()
							.setStart(currentCita.getHuecoId())
							.setEnd(endTime.getHour() * 100 + endTime.getMinute())
							.setPayload(currentCita));
				}
			}
			report.prevSlotCount(intervals.size());

			// Start processing the template slots.
			int slotCount = 0;
			for ( Cita templateCita : template.getMorningList() ) {
				// Convert the hueco identifier to a time.
				final int hour = templateCita.getHuecoId() / 100;
				final LocalTime time = LocalTime.of(templateCita.getHuecoId() / 100,
						templateCita.getHuecoId() - hour * 100);

				logger.info("-- [AppointmentCreationService.createDayAppointments]> Time: {}", time);
				final LocalTime endTime = time.plusMinutes(templateCita.getHuecoDuracion());
				Cita cita = new Cita()
						.setFecha(currentDate)
						.setReferencia(medico.getReferencia())
						.setHuecoId(time.getHour() * 100 + time.getMinute())
						.setHuecoDuracion(templateCita.getHuecoDuracion())
						.setEstado(templateCita.getEstado())
						.setTipo(templateCita.getTipo())
						.setMedico(medico)
						.setZonaHoraria("MAÑANA");
				entityManager.persist(cita);
				// Store the new Cita instance on the interval. After all interval creation just delete overlappers and then create Citas.
				final MinuteInterval newInterval = new MinuteInterval()
						.setStart(time.getHour() * 100 + time.getMinute())
						.setEnd(endTime.getHour() * 100 + endTime.getMinute())
						.setPayload(cita);

				// Check if this new interval overlaps any other at the tree.
				// Remove overlappers if found. Remove the new one being added.
				tree.overlappers(newInterval)
						.forEachRemaining(( interval ) -> {
							// Cancel the new appointment creation and report it as a collision.
							report.registerCollision(newInterval, interval);
							citaRepository.delete(cita);
						});
				logger.info("-- [AppointmentCreationService.createDayAppointments]> Creating slot: {}",
						"(" + "--" + ")" + currentDate + "-" + (time.getHour() * 100 + time.getMinute()));
				slotCount++;
			}
			for ( Cita templateCita : template.getEveningList() ) {
				// Convert the hueco identifier to a time.
				final int hour = templateCita.getHuecoId() / 100;
				final LocalTime time = LocalTime.of(templateCita.getHuecoId() / 100,
						templateCita.getHuecoId() - hour * 100);

				logger.info("-- [AppointmentCreationService.createDayAppointments]> Time: {}", time);
				final LocalTime endTime = time.plusMinutes(templateCita.getHuecoDuracion());
				Cita cita = new Cita()
						.setFecha(currentDate)
						.setReferencia(medico.getReferencia())
						.setHuecoId(time.getHour() * 100 + time.getMinute())
						.setHuecoDuracion(templateCita.getHuecoDuracion())
						.setEstado(templateCita.getEstado())
						.setTipo(templateCita.getTipo())
						.setMedico(medico)
						.setZonaHoraria("TARDE");
				entityManager.persist(cita);
				// Store the new Cita instanc e on the interval. After all interval creation just delete overlappers and then create Citas.
				final MinuteInterval newInterval = new MinuteInterval()
						.setStart(time.getHour() * 100 + time.getMinute())
						.setEnd(endTime.getHour() * 100 + endTime.getMinute())
						.setPayload(cita);

				// Check if this new interval overlaps any other at the tree.
				// Remove overlappers if found. Remove the new one being added.
				tree.overlappers(newInterval)
						.forEachRemaining(( interval ) -> {
							// Cancel the new appointment creation and report it as a collision.
							report.registerCollision(newInterval, interval);
							citaRepository.delete(cita);
						});
				logger.info("-- [AppointmentCreationService.createDayAppointments]> Creating slot: {}",
						"(" + "--" + ")" + currentDate + "-" + (time.getHour() * 100 + time.getMinute()));
				slotCount++;
			}

			// Update the medico first appointment.
			this.updateMedicoReport(medico);

			report.newSlotCount(slotCount);
			report.setComment("Citas creadas con template: " + template.getNombre());
			return report;
		} catch ( RuntimeException rte ) {
			rte.printStackTrace();
		} finally {
			logger.info("<< [AppointmentCreationService.createDayAppointments]");
		}
		return report;
	}

	public void updateMedicoReport( final Medico medico ) {
		// Generate the appointment report for this Medico.
		final List<Cita> slotsList = citaRepository.countAppointments4Referencia(medico.getReferencia());
		final List<Cita> freeSlotsList = citaRepository.countFreeAppointments4Referencia(medico.getReferencia());
		if ( null != freeSlotsList ) {
			final int slots = slotsList.size();
			final int freeSlots = freeSlotsList.size();
			if ( slots > 0 ) {
				if ( freeSlots > 0 ) {
					final Cita newCita = freeSlotsList.get(0);
					medico.setFirstAppointment(newCita);
					medico.setFirstAppointmentId(newCita.getId());
				}
				medico.setAppointmentsCount(slots);
				medico.setFreeAppointmentsCount(freeSlots);
			} else medico.setFreeAppointmentsCount(freeSlots);
		} else medico.setFreeAppointmentsCount(freeSlotsList.size());
		medicoRepository.save(medico);
	}

	@Transactional
	public Integer clearUnused4Date( final Medico medico, final LocalDate currentDate ) {
		logger.info(">> [AppointmentCreationService.clearUnused4Date]> Medico: {} - Date: {}"
				, medico, currentDate);
		// Remove the blocks hold at the Medico because the first available apointment.
		return entityManager.createQuery(DELETE_UNUSED_APPOINTMENTS4DATE)
				.setParameter("medid", medico.getId())
				.setParameter("targetDate", currentDate)
				.executeUpdate();
	}

//	/**
//	 * Create the list of Centros to be used on demos. We have restarted the list and we recommence with a single item.
//	 */
//	@Transactional
//	public void loadCentroSeedData() {
//		logger.info(">> [AppointmentCreationService.loadCentroSeedData]");
//
//		// Create the Centros.
////		this.centro = new Centro()
////				.setNombre("Particulares");
////		centroRepository.save(this.centro);
////		this.centro = new Centro()
////				.setNombre("Clínica Mompía")
////				.setLogotipo("https://www.clinicamompia.com/imagenes/logo.png")
////				.setLocalidad("Santander")
////				.setDireccion("Paseo de Pereda 15. Planta 6.");
////		centroRepository.save(this.centro);
////		this.centroIgualatorio = new Centro()
////				.setNombre("Grupo Empresarial Igualatorio Cantabria")
////				.setLogotipo("https://igualatoriocantabria.es/es/wp-content/uploads/2017/11/logo_igualatorio377.png");
////		centroRepository.save(this.centroIgualatorio);
//
//		// New Centros for the demo of services. v.0.7.0
//		this.centroHospitalManzana = new Centro()
//				.setNombre("Hospital de la Manzana")
//				.setLocalidad("Madrid")
//				.setDireccion("Calle Trinidad Grund 14");
//		centroRepository.save(this.centroHospitalManzana);
////		this.centroHospitalB = new Centro()
////				.setNombre("Hospital Málaga B")
////				.setLocalidad("Málaga")
////				.setDireccion("Avenida de Andalucía 84");
////		centroRepository.save(this.centroHospitalB);
////		this.centroRadiologico = new Centro()
////				.setNombre("Centro Radiológico Central")
////				.setLocalidad("Málaga")
////				.setDireccion("Avenida de Andalucía 84");
////		centroRepository.save(this.centroRadiologico);
//
//
//		logger.info("<< [AppointmentCreationService.loadCentroSeedData]");
//	}
//
//	public void loadMedicoSeedData() {
//		logger.info(">> [AppointmentCreationService.loadMedicoSeedData]");
//		// List of forms to use on the creation process.
//		final FormularioCreacionCitas oneWeek9to1260Minutes = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(9, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setDuracion(60)
//				.setHoraInicio("09:00")
//				.setHoraFin("12:00")
//				.setHorario("MAÑANA")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeek9to1230Minutes = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(9, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setDuracion(30)
//				.setHoraInicio("09:00")
//				.setHoraFin("12:00")
//				.setHorario("MAÑANA")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeekAllDays9to1230Minutes = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMartes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(30)
//				.setHoraInicio("09:00")
//				.setHoraFin("12:00")
//				.setHorario("MAÑANA")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeek14to1830Minutes = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(10, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setDuracion(30)
//				.setHoraInicio("14:00")
//				.setHoraFin("18:00")
//				.setHorario("TARDE")
//				.setClearData(false);
//		final FormularioCreacionCitas nextWeek9to1260Minutes = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(7, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(7 + 7, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setDuracion(60)
//				.setHoraInicio("09:00")
//				.setHoraFin("12:00")
//				.setHorario("MAÑANA")
//				.setClearData(false);
//
//		// Add the service providers.
//		final String salt = BCrypt.gensalt(8);
//		Credencial cred = new Credencial()
//				.setNif("942 314 112")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Dr.")
//				.setNombre("Luis")
//				.setApellidos("Fernández Pellón")
//				.setCentro(centroIgualatorio);
//		entityManager.persist(cred);
//		Medico medico = new Medico()
//				.setCentro(centroIgualatorio)
//				.setCredencial(cred)
//				.setReferencia("942 314 112")
//				.setId(Medico.generateNewId(centroIgualatorio.getId(), cred.getNif()))
//				.setTratamiento("Dr.")
//				.setNombre("Luis")
//				.setApellidos("Fernández Pellón")
//				.setEspecialidad("Alergología")
//				.setLocalidad("SANTANDER")
//				.setDireccion("CL. Isabel II, Nº 22, 2º IZ  Santander - 39002")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 112");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeekAllDays9to1230Minutes, medico);
//
//		cred = new Credencial()
//				.setNif("942 222 886")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Dra.")
//				.setNombre("Alicia")
//				.setApellidos("Terán Alonso")
//				.setCentro(centroIgualatorio);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroIgualatorio)
//				.setCredencial(cred)
//				.setReferencia("942 222 886")
//				.setId(Medico.generateNewId(centroIgualatorio.getId(), cred.getNif()))
//				.setTratamiento("Dra.")
//				.setNombre("Alicia")
//				.setApellidos("Terán Alonso")
//				.setEspecialidad("Análisis clínicos")
//				.setLocalidad("SANTANDER")
//				.setDireccion("CL. Amós de Escalante, Nº 4, 3º D  Santander - 39002")
//				.setHorario("De lunes a viernes de 9 a 14 y de 16 a 19.")
//				.setTelefono("942 222 886");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(nextWeek9to1260Minutes, medico);
//
//		cred = new Credencial()
//				.setNif("942 806 235")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Dr.")
//				.setNombre("Enrique")
//				.setApellidos("Mazo Polanco")
//				.setCentro(centroIgualatorio);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroIgualatorio)
//				.setCredencial(cred)
//				.setReferencia("942 806 235")
//				.setId(Medico.generateNewId(centroIgualatorio.getId(), cred.getNif()))
//				.setTratamiento("Dr.")
//				.setNombre("Enrique")
//				.setApellidos("Mazo Polanco")
//				.setEspecialidad("Análisis clínicos")
//				.setLocalidad("TORRELAVEGA")
//				.setDireccion("CL. Julián Ceballos, Nº 14, 2º A  Torrelavega - 39300")
//				.setHorario("De lunes a viernes de 9 a 12 y de 16:30 a 19. Sábados de 9:30 a 12.")
//				.setTelefono("942 806 235");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeek14to1830Minutes, medico);
//
//		cred = new Credencial()
//				.setNif("942 360 888")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Dr.")
//				.setNombre("Felipe")
//				.setApellidos("Gómez-Ullate Vergara")
//				.setCentro(centroIgualatorio);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centro)
//				.setCredencial(cred)
//				.setReferencia("942 360 888")
//				.setId(Medico.generateNewId(centro.getId(), cred.getNif()))
//				.setTratamiento("Dr.")
//				.setNombre("Felipe")
//				.setApellidos("Gómez-Ullate Vergara")
//				.setEspecialidad("Cardiología");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeek9to1260Minutes, medico);
////
////		medico = new Medico()
////				.setCentro(centroIgualatorio)
////				.setNif("942 228 743")
////				.setId(Medico.generateNewId(centroIgualatorio.getId(), "942 228 743"))
////				.setTratamiento("Dra.")
////				.setNombre("María Luisa")
////				.setApellidos("Mostaza Martínez")
////				.setEspecialidad("Pediatría")
////				.setLocalidad("SANTANDER")
////				.setDireccion("CL. Rualasal, Nº 23, 1º CT  Santander - 39001")
////				.setHorario("Solicitar hora.")
////				.setTelefono("Teléfono: 942 228 743");
////		entityManager.persist(medico);
////		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
////		// Create some appointments and fill them up.
////		this.createSeedAppointments(oneWeek9to1230Minutes, medico);
////		medico = new Medico()
////				.setCentro(centroIgualatorio)
////				.setNif("942 373 605")
////				.setId(Medico.generateNewId(centroIgualatorio.getId(), "942 373 605"))
////				.setTratamiento("Dr.")
////				.setNombre("Francisco Javier")
////				.setApellidos("Gómez-Ullate Vergara")
////				.setEspecialidad("Pediatría")
////				.setLocalidad("SANTANDER")
////				.setDireccion("CL. Castelar, Nº 21, 2º A  Santander - 39004")
////				.setHorario("Solicitar hora.")
////				.setTelefono("Teléfono: 942 373 605");
////		entityManager.persist(medico);
////		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
////		// Create some appointments and fill them up.
////		this.createSeedAppointments(oneWeek9to1230Minutes, medico);
////		medico = new Medico()
////				.setCentro(centroIgualatorio)
////				.setNif("942 212 472")
////				.setId(Medico.generateNewId(centroIgualatorio.getId(), "942 212 472"))
////				.setTratamiento("Dr.")
////				.setNombre("José")
////				.setApellidos("Alonso Palacio")
////				.setEspecialidad("Pediatría")
////				.setLocalidad("SANTANDER")
////				.setDireccion("CL. Castelar, Nº 43, 8º IZ  Santander - 39004")
////				.setHorario("Solicitar hora.")
////				.setTelefono("Teléfono: 942 212 472");
////		entityManager.persist(medico);
////		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
////		// Create some appointments and fill them up.
////		this.createSeedAppointments(oneWeek9to1230Minutes, medico);
////
////		medico = new Medico()
////				.setCentro(centroIgualatorio)
////				.setNif("639 311 123")
////				.setId(Medico.generateNewId(centroIgualatorio.getId(), "639 311 123"))
////				.setTratamiento("Dr.")
////				.setNombre("Miguel")
////				.setApellidos("Carbajo Carbajo")
////				.setEspecialidad("Cirugía Torácica")
////				.setLocalidad("SANTANDER")
////				.setDireccion("AV. Cantabria, Nº 50, 4º A  Santander - 39012")
////				.setHorario("Solicitar hora.")
////				.setTelefono("Teléfono: 639 311 123   Teléfono 2: 942 393 047");
////		entityManager.persist(medico);
////		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
////		medico = new Medico()
////				.setCentro(centroIgualatorio)
////				.setNif("942 835 208")
////				.setId(Medico.generateNewId(centroIgualatorio.getId(), "942 835 208"))
////				.setTratamiento("Dr.")
////				.setNombre("Miguel")
////				.setApellidos("Carbajo Carbajo")
////				.setEspecialidad("Cirugía Torácica")
////				.setLocalidad("TORRELAVEGA")
////				.setDireccion("CL. Hermilio Alcalde del Río, Nº 7  Torrelavega - 39300")
////				.setHorario("Solicitar hora.")
////				.setTelefono("Teléfono: 942 835 208");
////		entityManager.persist(medico);
////		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
////		// Create some appointments and fill them up.
////		this.createSeedAppointments(oneWeek9to1230Minutes, medico);
//
//		logger.info("<< [AppointmentCreationService.loadMedicoSeedData]");
//	}
//
//	public void loadServiceSeedData() {
//		logger.info(">> [AppointmentCreationService.loadServiceSeedData]");
//		// List of forms to use on the creation process.
//		final FormularioCreacionCitas oneWeek30MinutesMañana = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(30)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeek30MinutesTarde = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(30)
//				.setHoraInicio("16:00")
//				.setHoraFin("20:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeek30MinutesSabado = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setSabado(true)
//				.setDuracion(30)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//
//		final FormularioCreacionCitas oneWeek45MinutesMañana = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(45)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeek45MinutesTarde = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(45)
//				.setHoraInicio("16:00")
//				.setHoraFin("20:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeek45MinutesSabado = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setSabado(true)
//				.setDuracion(45)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//
//		final FormularioCreacionCitas oneWeekEcoMinutesMañana = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(15)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeekEcoMinutesTarde = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(15)
//				.setHoraInicio("16:00")
//				.setHoraFin("20:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeekEcoMinutesSabado = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setSabado(true)
//				.setDuracion(15)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//
//		final FormularioCreacionCitas oneWeekRayosMinutesMañana = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(30)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeekRayosMinutesTarde = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setLunes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(30)
//				.setHoraInicio("16:00")
//				.setHoraFin("20:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneWeekRayosMinutesSabado = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now().plus(2, ChronoUnit.DAYS))
//				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
//				.setSabado(true)
//				.setDuracion(30)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//
//		// Add the service providers.
//		final String salt = BCrypt.gensalt(8);
//		Credencial cred = new Credencial()
//				.setNif("Radiología P1")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Radiología")
//				.setApellidos("Planta 1")
//				.setCentro(centroHospitalA);
//		entityManager.persist(cred);
//		Medico medico = new Medico()
//				.setCentro(centroHospitalA)
//				.setCredencial(cred)
//				.setReferencia("Radiología P1")
//				.setId(Medico.generateNewId(centroHospitalA.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Radiología")
//				.setApellidos("Planta 1")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeekRayosMinutesMañana, medico);
//		this.createSeedAppointments(oneWeekRayosMinutesTarde, medico);
//		this.createSeedAppointments(oneWeekRayosMinutesSabado, medico);
//		cred = new Credencial()
//				.setNif("Ecografia P1")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Ecografía")
//				.setApellidos("Planta 1")
//				.setCentro(centroHospitalA);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroHospitalA)
//				.setCredencial(cred)
//				.setReferencia("Radiología P1")
//				.setId(Medico.generateNewId(centroHospitalA.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Ecografía")
//				.setApellidos("Planta 1")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeekEcoMinutesMañana, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesTarde, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesSabado, medico);
//		cred = new Credencial()
//				.setNif("Densi P1")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Densiometría")
//				.setApellidos("Planta 1")
//				.setCentro(centroHospitalA);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroHospitalA)
//				.setCredencial(cred)
//				.setReferencia("Radiología P1")
//				.setId(Medico.generateNewId(centroHospitalA.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Densiometría")
//				.setApellidos("Planta 1")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeekEcoMinutesMañana, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesTarde, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesSabado, medico);
//
//		cred = new Credencial()
//				.setNif("TAC P2")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Tomografía")
//				.setApellidos("Planta 2")
//				.setCentro(centroHospitalA);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroHospitalA)
//				.setCredencial(cred)
//				.setReferencia("Radiología P1")
//				.setId(Medico.generateNewId(centroHospitalA.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Tomografía")
//				.setApellidos("Planta 2")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeekEcoMinutesMañana, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesTarde, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesSabado, medico);
//		cred = new Credencial()
//				.setNif("RX P2")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Rayos X")
//				.setApellidos("Planta 2")
//				.setCentro(centroHospitalA);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroHospitalA)
//				.setCredencial(cred)
//				.setReferencia("Radiología P1")
//				.setId(Medico.generateNewId(centroHospitalA.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Rayos X")
//				.setApellidos("Planta 2")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeekEcoMinutesMañana, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesTarde, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesSabado, medico);
//
//		//--- C E N T R O   R A D I O L O G I C O
//		cred = new Credencial()
//				.setNif("Rayos X")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Rayos X")
//				.setApellidos("Planta 1")
//				.setCentro(centroRadiologico);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroRadiologico)
//				.setCredencial(cred)
//				.setReferencia("Rayos X")
//				.setId(Medico.generateNewId(centroRadiologico.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Rayos X")
//				.setApellidos("Planta 1")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeekEcoMinutesMañana, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesTarde, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesSabado, medico);
//		cred = new Credencial()
//				.setNif("Ecografo")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Ecografo")
//				.setApellidos("Planta 1")
//				.setCentro(centroRadiologico);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroRadiologico)
//				.setCredencial(cred)
//				.setReferencia("Ecografo")
//				.setId(Medico.generateNewId(centroRadiologico.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Ecografo")
//				.setApellidos("Planta 1")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeekEcoMinutesMañana, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesTarde, medico);
//		this.createSeedAppointments(oneWeekEcoMinutesSabado, medico);
//		cred = new Credencial()
//				.setNif("Mamógrafo")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Ecografo")
//				.setApellidos("Planta 1")
//				.setCentro(centroRadiologico);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroRadiologico)
//				.setCredencial(cred)
//				.setReferencia("Mamógrafo")
//				.setId(Medico.generateNewId(centroRadiologico.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Mamógrafo")
//				.setApellidos("Planta 2")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeek30MinutesMañana, medico);
//		this.createSeedAppointments(oneWeek30MinutesTarde, medico);
//		this.createSeedAppointments(oneWeek30MinutesSabado, medico);
//		cred = new Credencial()
//				.setNif("TAC")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("TAC")
//				.setApellidos("Planta 1")
//				.setCentro(centroRadiologico);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroRadiologico)
//				.setCredencial(cred)
//				.setReferencia("TAC")
//				.setId(Medico.generateNewId(centroRadiologico.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("TAC")
//				.setApellidos("Planta 2")
//				.setEspecialidad("Radiologia")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeek30MinutesMañana, medico);
//		this.createSeedAppointments(oneWeek30MinutesTarde, medico);
//		this.createSeedAppointments(oneWeek30MinutesSabado, medico);
//		cred = new Credencial()
//				.setNif("Resonancia")
//				.setPassword(Credencial.crypt("1234", salt))
//				.setSalt(salt)
////				.setTratamiento("Servicio")
//				.setNombre("Resonancia")
//				.setApellidos("Planta 2")
//				.setCentro(centroRadiologico);
//		entityManager.persist(cred);
//		medico = new Medico()
//				.setCentro(centroRadiologico)
//				.setCredencial(cred)
//				.setReferencia("Resonancia")
//				.setId(Medico.generateNewId(centroRadiologico.getId(), cred.getNif()))
//				.setTratamiento("Servicio")
//				.setNombre("Resonancia")
//				.setApellidos("Planta 2")
//				.setEspecialidad("Radiología")
//				.setLocalidad("Málaga")
//				.setDireccion("Calle Trinidad Grund 14")
//				.setHorario("Solicitar hora.")
//				.setTelefono("942 314 314");
//		entityManager.persist(medico);
//		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
//		// Create some appointments and fill them up.
//		this.createSeedAppointments(oneWeek45MinutesMañana, medico);
//		this.createSeedAppointments(oneWeek45MinutesTarde, medico);
//		this.createSeedAppointments(oneWeek45MinutesSabado, medico);
//
//		logger.info("<< [AppointmentCreationService.loadServiceSeedData]");
//	}

//	public void createSeedAppointments(final FormularioCreacionCitas form, final Medico medico) {
//		final List<AppointmentCreationService.AppointmentReport> creationReport = new ArrayList<>();
//		for ( LocalDate date = form.getFechaInicio();
//		      date.isBefore(form.getFechaFin().plus(1, ChronoUnit.DAYS));
//		      date = date.plusDays(1) ) {
//			logger.info("-- [AppointmentCreationService.createSeedAppointments]> Processing date: {}", date);
//			// If the flag to delete appointments is set clear previous 'LIBRE' records.
//			if ( form.isClearData() ) {
//				logger.info("-- [AppointmentCreationService.createSeedAppointments]> ClearData is active. Deleting appointments.");
//				this.clearUnused4Date(medico, date);
//			}
//			// Create the slots for a single day. This follows the IntervalTree algorithm.
//			creationReport.add(this.createDayAppointments(medico, date, form));
//		}
////		logger.info("-- [AppointmentCreationService.createSeedAppointments]> creationReport: {}", creationReport.toString());
//	}

	@Transactional
	public void removeBlockedAppointments() {
		logger.info(">> [AppointmentCreationService.removeBlockedAppointments]");
		// Get the valid duration from configuration.
		final long validDuration = CitasBackendApplication.getConfiguration().getNumberResource("P.runtime.database.blockedlivetime", 120l);

		final List<Cita> appointments = citaRepository.findBlockedAppointments();
		// Process appointments one by one and check the latest update time.
		for ( final Cita appointment : appointments ) {
			final Instant latestUpdate = appointment.getUpdatedAt().toInstant();
			final Duration elapsed = Duration.between(latestUpdate, Instant.now());
			if ( elapsed.getSeconds() > validDuration ) {
				// Cancel the block appointment.
				logger.info("-- [AppointmentCreationService.removeBlockedAppointments]> Clearing appointment blocked: {}-{}",
						appointment.getId(),
						appointment.getHuecoIdentifier());
				appointment.setEstado(Cita.ECitaStates.LIBRE.name());
				citaRepository.save(appointment);
			}
		}
		logger.info("<< [AppointmentCreationService.removeBlockedAppointments]");
	}

//	@Transactional
//	public void createAdditionalMedico() {
//		logger.info(">> [AppointmentCreationService.createAdditionalMedico]");
//		// List of forms to use on the creation process.
//		final FormularioCreacionCitas threeMonth9to1410Minutes = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now())
//				.setFechaFin(LocalDate.now().plus(1, ChronoUnit.MONTHS))
//				.setLunes(true)
//				.setMartes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(10)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//		final FormularioCreacionCitas oneMonth9to1410Minutes = new FormularioCreacionCitas()
//				.setFechaInicio(LocalDate.now())
//				.setFechaFin(LocalDate.now().plus(1, ChronoUnit.MONTHS))
//				.setLunes(true)
//				.setMartes(true)
//				.setMiercoles(true)
//				.setJueves(true)
//				.setViernes(true)
//				.setDuracion(15)
//				.setHoraInicio("09:00")
//				.setHoraFin("14:00")
//				.setClearData(false);
//
//		// Locate the Centro to be used to create the Medico.
//		final List<Centro> centros = centroRepository.findAll();
//		Centro centroTarget = null;
//		for ( final Centro centro : centros ) {
//			if ( centro.getNombre().equalsIgnoreCase("Centro Radiológico Central") )
//				centroTarget = centro;
//		}
//		if ( null != centroTarget ) {
//			// Add the service providers.
//			String newReferencia = "CR-" + Instant.now().toEpochMilli();
//			randomGenerator = new Random();
//			int index = randomGenerator.nextInt(especialities.size());
//			final String especialidad = especialities.get(index);
//
//			// Create first the Credencial and then the Medico.
//			final String salt = BCrypt.gensalt(8);
//			Credencial cred = new Credencial()
//					.setNif(newReferencia)
//					.setPassword(Credencial.crypt("1234", salt))
//					.setSalt(salt)
////					.setTratamiento("Dr.")
//					.setNombre(newReferencia)
//					.setApellidos("Pruebas Carga")
//					.setCentro(centroTarget);
//			entityManager.persist(cred);
//
//			Medico medico = new Medico()
//					.setCentro(centroTarget)
//					.setCredencial(cred)
//					.setReferencia(newReferencia)
//					.setId(Medico.generateNewId(centroTarget.getId(), newReferencia))
//					.setTratamiento("Dr.")
//					.setNombre(newReferencia)
//					.setApellidos("Pruebas Carga")
//					.setEspecialidad(especialidad)
//					.setLocalidad("SANTANDER")
//					.setDireccion("CL. Isabel II, Nº 22, 2º IZ  Santander - 39002")
//					.setHorario("Solicitar hora.")
//					.setTelefono("942 314 112");
//			entityManager.persist(medico);
//			logger.info("-- [AppointmentCreationService.createAdditionalMedico]> Creating Medico: {}", medico.getId());
//			// Create some appointments and fill them up.
//			this.createSeedAppointments(threeMonth9to1410Minutes, medico);
//			logger.info("<< [AppointmentCreationService.createAdditionalMedico]");
//		}
//	}

	public static class AppointmentReport extends Node {
		public LocalDate reportdate;
		public int previousSlotCount = 0;
		public int newSlotCount = 0;
		public String comment = "Dia no laborable para el especialista. No hay citas.";
		public List<MinuteInterval> collisions = new ArrayList();

		// - C O N S T R U C T O R - S E C T I O N ................................................................
		public AppointmentReport( final LocalDate currentDate ) {
			this.reportdate = currentDate;
		}

		// - M E T H O D - S E C T I O N ..........................................................................
		public AppointmentReport prevSlotCount( final int size ) {
			this.previousSlotCount = size;
			// Clear the default message because there are appointments for this date.
			this.comment = "";
			return this;
		}

		public AppointmentReport newSlotCount( final int slotCount ) {
			this.newSlotCount = slotCount;
			// Clear the default message because there are appointments for this date.
			this.comment = "";
			return this;
		}

		public AppointmentReport registerCollision( final MinuteInterval newInterval, final MinuteInterval oldInterval ) {
			// Replace the old interval start-end into the new MinuteInterval instance.
			oldInterval.setStart(newInterval.start());
			oldInterval.setEnd(newInterval.end());
			this.collisions.add(oldInterval);
			this.comment = "Revise la lista de colisiones. Algunas citas no se han creado.";
			return this;
		}

		// --- G E T T E R S   &   S E T T E R S
		public AppointmentReport setComment( final String comment ) {
			this.comment = comment;
			return this;
		}
	}
}

// - UNUSED CODE ............................................................................................
//[01]
