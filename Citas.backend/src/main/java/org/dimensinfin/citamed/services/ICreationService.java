//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.services;

import java.time.LocalDate;
import java.util.Optional;

import org.dimensinfin.citamed.model.FormularioCreacionCitas;
import org.dimensinfin.citamed.entities.Medico;

public interface ICreationService {
	Optional<Medico> findById(final String consultaReference);

	AppointmentCreationService.AppointmentReport createDayAppointments(final Medico medico, final LocalDate currentDate, final FormularioCreacionCitas formulario);

	Integer clearUnused4Date(final Medico medico, final LocalDate currentDate);

	void removeBlockedAppointments();

	public void updateMedicoReport(final Medico medico);

//	void loadCentroSeedData();
//
//	void loadMedicoSeedData();
//
//	void loadServiceSeedData();
//
//	void createSeedAppointments(final FormularioCreacionCitas form, final Medico medico);


//	void createAdditionalMedico();

}
