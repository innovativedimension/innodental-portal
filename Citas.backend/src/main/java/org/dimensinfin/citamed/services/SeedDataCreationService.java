//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.services;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.persistence.EntityManager;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.dimensinfin.citamed.entities.Centro;
import org.dimensinfin.citamed.entities.Credencial;
import org.dimensinfin.citamed.entities.Medico;
import org.dimensinfin.citamed.model.FormularioCreacionCitas;
import org.dimensinfin.citamed.model.ServiceAuthorized;
import org.dimensinfin.citamed.repository.CentroRepository;
import org.dimensinfin.citamed.repository.CitaRepository;
import org.dimensinfin.citamed.repository.CredencialRepository;
import org.dimensinfin.citamed.repository.MedicoRepository;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@Service
@Qualifier("SeedData")
@Primary
public class SeedDataCreationService extends AppointmentCreationService {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("SeedDataCreationService");

	//	private static final String DELETE_UNUSED_APPOINTMENTS4DATE = "DELETE FROM Citas WHERE estado = 'LIBRE' AND medico_id=:medid AND " +
//			"fecha =" +
//			" " +
//			":targetDate";
	private static ArrayList<String> especialities = new ArrayList<>();

	static {
		especialities.add("Alergología");
//		especialities.add("Análisis clínicos");
		especialities.add("Anatomía patológica");
		especialities.add("Anestesia y reanimación");
		especialities.add("Aparato digestivo");
		especialities.add("Cardiología");
		especialities.add("Cirugía cardiovascular");
		especialities.add("Cirugía torácica");
		especialities.add("Dermatología");
//		especialities.add("Ecografía");
		especialities.add("Endocrinología y nutrición");
		especialities.add("Enfermería");
		especialities.add("Estomatología (odontología)");
		especialities.add("Medicina familiar");
		especialities.add("Nefrología");
		especialities.add("Obstetricia y ginecología");
		especialities.add("Oncología médica");
		especialities.add("Oncología radioterápica");
		especialities.add("Otorrinolaringología");
		especialities.add("Pediatría");
		especialities.add("Podología");
		especialities.add("Psicología y psicoterapia");
		especialities.add("Psiquiatría");
	}

	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	private EntityManager entityManager;
	@Autowired
	protected CredencialRepository credencialRepository;
	@Autowired
	protected CentroRepository centroRepository;
	@Autowired
	protected MedicoRepository medicoRepository;
	@Autowired
	protected CitaRepository citaRepository;

	protected Centro centroHospitalManzana;
	private Random randomGenerator;

	// - M E T H O D - S E C T I O N ..........................................................................

	/**
	 * Create the list of Centros to be used on demos. We have restarted the list and we recommence with a single item.
	 */
	@Transactional
	public void loadCentroSeedData() {
		logger.info(">> [AppointmentCreationService.loadCentroSeedData]");

		// Create the Centros.
		// New Centros for the demo of services. v.0.7.0
		this.centroHospitalManzana = new Centro()
				.setNombre("Hospital de la Manzana")
				.setLocalidad("Madrid")
				.setDireccion("Calle Trinidad Grund 14");
		entityManager.persist(this.centroHospitalManzana);

		logger.info("<< [AppointmentCreationService.loadCentroSeedData]");
	}

	@Transactional
	public void loadCredencialSeedData() {
		logger.info(">> [AppointmentCreationService.loadCredencialSeedData]");
		final String salt = BCrypt.gensalt(8);
		Credencial cred = new Credencial()
				.setNif("85456123Z")
				.setPassword(Credencial.crypt("1234", salt))
				.setSalt(salt)
				.setNombre("Luis")
				.setApellidos("López Delgado")
				.setCentro(this.centroHospitalManzana)
				.setRole("ADMIN")
				.setAllservices(true);
		entityManager.persist(cred);
		cred = new Credencial()
				.setNif("81456123J")
				.setPassword(Credencial.crypt("1234", salt))
				.setSalt(salt)
				.setNombre("Anjana")
				.setApellidos("Salceda Quintana")
				.setCentro(this.centroHospitalManzana)
				.setRole("RESPONSABLE")
				.setAllservices(false)
				.addServicioAutorizado(new ServiceAuthorized().setServiceIdentifier("MID:100001:CIE:942 314 110"))
				.addServicioAutorizado(new ServiceAuthorized().setServiceIdentifier("MID:100001:CIE:942 314 111"));
		entityManager.persist(cred);
		cred = new Credencial()
				.setNif("80456123F")
				.setPassword(Credencial.crypt("1234", salt))
				.setSalt(salt)
				.setNombre("Carmen")
				.setApellidos("Crespo García")
				.setCentro(this.centroHospitalManzana)
				.setRole("CALLCENTER")
				.setAllservices(false);
		entityManager.persist(cred);
		cred = new Credencial()
				.setNif("82456123L")
				.setPassword(Credencial.crypt("1234", salt))
				.setSalt(salt)
				.setNombre("Javier")
				.setApellidos("Fernández Pellón")
				.setCentro(this.centroHospitalManzana)
				.setRole("USUARIO")
				.setAllservices(false);
		entityManager.persist(cred);
		logger.info("<< [AppointmentCreationService.loadCredencialSeedData]");
	}

	@Transactional
	public void loadMedicoSeedData() {
		logger.info(">> [AppointmentCreationService.loadMedicoSeedData]");
		// List of forms to use on the creation process.
		final FormularioCreacionCitas oneWeek9to1260Minutes = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(16, ChronoUnit.DAYS))
				.setLunes(true)
				.setMartes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(60)
				.setHoraInicio("09:00")
				.setHoraFin("12:00")
				.setHorario("MAÑANA")
				.setClearData(false);
		final FormularioCreacionCitas oneWeek9to1230Minutes = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(16, ChronoUnit.DAYS))
				.setLunes(true)
				.setMartes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setSabado(true)
				.setDuracion(30)
				.setHoraInicio("09:00")
				.setHoraFin("12:00")
				.setHorario("MAÑANA")
				.setClearData(false);
		final FormularioCreacionCitas oneWeekAllDays9to1230Minutes = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(16, ChronoUnit.DAYS))
				.setLunes(true)
				.setMartes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(30)
				.setHoraInicio("09:00")
				.setHoraFin("12:00")
				.setHorario("MAÑANA")
				.setClearData(false);
		final FormularioCreacionCitas oneWeek14to1830Minutes = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(16, ChronoUnit.DAYS))
				.setLunes(true)
				.setMartes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setSabado(true)
				.setDuracion(30)
				.setHoraInicio("14:00")
				.setHoraFin("18:00")
				.setHorario("TARDE")
				.setClearData(false);
		final FormularioCreacionCitas nextWeek9to1260Minutes = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(20, ChronoUnit.DAYS))
				.setLunes(true)
				.setMartes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(60)
				.setHoraInicio("09:00")
				.setHoraFin("12:00")
				.setHorario("MAÑANA")
				.setClearData(false);

		// Add the service providers.
//		EntityTransaction userTransaction = entityManager.getTransaction();

//		userTransaction.begin();
		Medico medico = new Medico()
				.setCentro(this.centroHospitalManzana)
				.setReferencia("CIE:942 314 110")
				.setId(Medico.generateNewId(this.centroHospitalManzana.getId(), "CIE:942 314 110"))
				.setTratamiento("Dr.")
				.setNombre("Luis")
				.setApellidos("Ortiz Gómez")
				.setEspecialidad("Traumatología y Ortopedia");
		entityManager.persist(medico);
//		userTransaction.commit();
		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
		// Create some appointments and fill them up.
//		userTransaction.begin();
		this.createSeedAppointments(oneWeek9to1230Minutes, medico);
		this.createSeedAppointments(oneWeek14to1830Minutes, medico);
//		userTransaction.commit();

		medico = new Medico()
				.setCentro(this.centroHospitalManzana)
				.setReferencia("CIE:942 314 111")
				.setId(Medico.generateNewId(this.centroHospitalManzana.getId(), "CIE:942 314 111"))
				.setTratamiento("Dr.")
				.setNombre("Javier")
				.setApellidos("Villalba Aramburu")
				.setEspecialidad("Traumatología y Ortopedia");
		entityManager.persist(medico);
		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
		// Create some appointments and fill them up.
		this.createSeedAppointments(oneWeek9to1230Minutes, medico);
		this.createSeedAppointments(oneWeek14to1830Minutes, medico);

		medico = new Medico()
				.setCentro(this.centroHospitalManzana)
				.setReferencia("CIE:942 314 112")
				.setId(Medico.generateNewId(this.centroHospitalManzana.getId(), "CIE:942 314 112"))
				.setTratamiento("Dr.")
				.setNombre("Marta")
				.setApellidos("Lastra Olano")
				.setEspecialidad("Pediatría");
		entityManager.persist(medico);
		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
		// Create some appointments and fill them up.
		this.createSeedAppointments(oneWeek9to1230Minutes, medico);
		this.createSeedAppointments(oneWeek14to1830Minutes, medico);

		medico = new Medico()
				.setCentro(this.centroHospitalManzana)
				.setReferencia("CIE:942 314 113")
				.setId(Medico.generateNewId(this.centroHospitalManzana.getId(), "CIE:942 314 113"))
				.setTratamiento("Dr.")
				.setNombre("Agata")
				.setApellidos("Pérez Ochoa")
				.setEspecialidad("Oncología Médica");
		entityManager.persist(medico);
		logger.info("-- [AppointmentCreationService.loadMedicoSeedData]> Creating Medico: {}", medico.getId());
		// Create some appointments and fill them up.
		this.createSeedAppointments(oneWeek9to1230Minutes, medico);
		this.createSeedAppointments(oneWeek14to1830Minutes, medico);

		logger.info("<< [AppointmentCreationService.loadMedicoSeedData]");
	}

	public void loadServiceSeedData() {
		logger.info(">> [AppointmentCreationService.loadServiceSeedData]");
		// List of forms to use on the creation process.
		final FormularioCreacionCitas oneWeekEco15MinutesMañana = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
				.setLunes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(15)
				.setHoraInicio("09:00")
				.setHoraFin("14:00")
				.setHorario("MAÑANA")
				.setClearData(false);
		final FormularioCreacionCitas oneWeekEco15MinutesTarde = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
				.setLunes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(15)
				.setHoraInicio("16:00")
				.setHoraFin("20:00")
				.setHorario("TARDE")
				.setClearData(false);
		final FormularioCreacionCitas oneWeekEco15MinutesSabado = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
				.setSabado(true)
				.setDuracion(15)
				.setHoraInicio("10:00")
				.setHoraFin("12:00")
				.setHorario("MAÑANA")
				.setClearData(false);

		final FormularioCreacionCitas oneWeekReso30MinutesMañana = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
				.setLunes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(30)
				.setHoraInicio("09:00")
				.setHoraFin("14:00")
				.setHorario("MAÑANA")
				.setClearData(false);
		final FormularioCreacionCitas oneWeekReso30MinutesTarde = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
				.setLunes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(30)
				.setHoraInicio("16:00")
				.setHoraFin("20:00")
				.setHorario("TARDE")
				.setClearData(false);
		final FormularioCreacionCitas oneWeekReso30MinutesSabado = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
				.setSabado(true)
				.setDuracion(30)
				.setHoraInicio("10:00")
				.setHoraFin("12:00")
				.setHorario("MAÑANA")
				.setClearData(false);

		final FormularioCreacionCitas oneWeekRayos5MinutesMañana = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
				.setLunes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(5)
				.setHoraInicio("09:00")
				.setHoraFin("14:00")
				.setHorario("MAÑANA")
				.setClearData(false);
		final FormularioCreacionCitas oneWeekRayos5MinutesTarde = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now().plus(0, ChronoUnit.DAYS))
				.setFechaFin(LocalDate.now().plus(12, ChronoUnit.DAYS))
				.setLunes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(5)
				.setHoraInicio("16:00")
				.setHoraFin("20:00")
				.setHorario("TARDE")
				.setClearData(false);

		// Add the service providers.
		Medico medico = new Medico()
				.setCentro(this.centroHospitalManzana)
				.setReferencia("SRVC:RESO P1")
				.setId(Medico.generateNewId(this.centroHospitalManzana.getId(), "SRVC:RESO P1"))
				.setTratamiento("Servicio")
				.setNombre("Resonancia")
				.setApellidos("Planta 1")
				.setEspecialidad("Radiología");
		entityManager.persist(medico);
		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
		// Create some appointments and fill them up.
		this.createSeedAppointments(oneWeekReso30MinutesMañana, medico);
		this.createSeedAppointments(oneWeekReso30MinutesTarde, medico);
		this.createSeedAppointments(oneWeekReso30MinutesSabado, medico);

		medico = new Medico()
				.setCentro(this.centroHospitalManzana)
				.setReferencia("SRVC:ECO A")
				.setId(Medico.generateNewId(this.centroHospitalManzana.getId(), "SRVC:ECO A"))
				.setTratamiento("Servicio")
				.setNombre("Ecografo")
				.setApellidos("Maq A")
				.setEspecialidad("Ecografía");
		entityManager.persist(medico);
		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
		// Create some appointments and fill them up.
		this.createSeedAppointments(oneWeekEco15MinutesMañana, medico);
		this.createSeedAppointments(oneWeekEco15MinutesTarde, medico);
		this.createSeedAppointments(oneWeekEco15MinutesSabado, medico);

		medico = new Medico()
				.setCentro(this.centroHospitalManzana)
				.setReferencia("SRVC:ECO B")
				.setId(Medico.generateNewId(this.centroHospitalManzana.getId(), "SRVC:ECO B"))
				.setTratamiento("Servicio")
				.setNombre("Ecografo")
				.setApellidos("Maq B")
				.setEspecialidad("Ecografía");
		entityManager.persist(medico);
		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
		// Create some appointments and fill them up.
		this.createSeedAppointments(oneWeekEco15MinutesMañana, medico);
		this.createSeedAppointments(oneWeekEco15MinutesTarde, medico);
		this.createSeedAppointments(oneWeekEco15MinutesSabado, medico);

		medico = new Medico()
				.setCentro(this.centroHospitalManzana)
				.setReferencia("SRVC:RX")
				.setId(Medico.generateNewId(this.centroHospitalManzana.getId(), "SRVC:RX"))
				.setTratamiento("Servicio")
				.setNombre("Rayos X")
				.setApellidos("")
				.setEspecialidad("Radiología");
		entityManager.persist(medico);
		logger.info("-- [AppointmentCreationService.loadServiceSeedData]> Creating Medico: {}", medico.getId());
		// Create some appointments and fill them up.
		this.createSeedAppointments(oneWeekRayos5MinutesMañana, medico);
		this.createSeedAppointments(oneWeekRayos5MinutesTarde, medico);

		logger.info("<< [AppointmentCreationService.loadServiceSeedData]");
	}

	public void createSeedAppointments(final FormularioCreacionCitas form, final Medico medico) {
		final List<SeedDataCreationService.AppointmentReport> creationReport = new ArrayList<>();
		for ( LocalDate date = form.getFechaInicio();
		      date.isBefore(form.getFechaFin().plus(1, ChronoUnit.DAYS));
		      date = date.plusDays(1) ) {
			logger.info("-- [AppointmentCreationService.createSeedAppointments]> Processing date: {}", date);
			// If the flag to delete appointments is set clear previous 'LIBRE' records.
			if ( form.isClearData() ) {
				logger.info("-- [AppointmentCreationService.createSeedAppointments]> ClearData is active. Deleting appointments.");
				this.clearUnused4Date(medico, date);
			}
			// Create the slots for a single day. This follows the IntervalTree algorithm.
			creationReport.add(this.createDayAppointments(medico, date, form));
		}
//		logger.info("-- [AppointmentCreationService.createSeedAppointments]> creationReport: {}", creationReport.toString());
	}

//	@Transactional
//	public void removeBlockedAppointments() {
//		logger.info(">> [AppointmentCreationService.removeBlockedAppointments]");
//		// Get the valid duration from configuration.
//		final long validDuration = CitasBackendApplication.getConfiguration().getNumberResource("P.runtime.database.blockedlivetime", 120l);
//
//		final List<Cita> appointments = citaRepository.findBlockedAppointments();
//		// Process appointments one by one and check the latest update time.
//		for ( final Cita appointment : appointments ) {
//			final Instant latestUpdate = appointment.getUpdatedAt().toInstant();
//			final Duration elapsed = Duration.between(latestUpdate, Instant.now());
//			if ( elapsed.getSeconds() > validDuration ) {
//				// Cancel the block appointment.
//				logger.info("-- [AppointmentCreationService.removeBlockedAppointments]> Clearing appointment blocked: {}-{}",
//						appointment.getId(),
//						appointment.getHuecoIdentifier());
//				appointment.setEstado(Cita.ECitaStates.LIBRE.name());
//				citaRepository.save(appointment);
//			}
//		}
//		logger.info("<< [AppointmentCreationService.removeBlockedAppointments]");
//	}

	@Transactional
	public void createAdditionalMedico() {
		logger.info(">> [AppointmentCreationService.createAdditionalMedico]");
		// List of forms to use on the creation process.
		final FormularioCreacionCitas threeMonth9to1410Minutes = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now())
				.setFechaFin(LocalDate.now().plus(1, ChronoUnit.MONTHS))
				.setLunes(true)
				.setMartes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(10)
				.setHoraInicio("09:00")
				.setHoraFin("14:00")
				.setHorario("MAÑANA")
				.setClearData(false);
		final FormularioCreacionCitas oneMonth9to1410Minutes = new FormularioCreacionCitas()
				.setFechaInicio(LocalDate.now())
				.setFechaFin(LocalDate.now().plus(1, ChronoUnit.MONTHS))
				.setLunes(true)
				.setMartes(true)
				.setMiercoles(true)
				.setJueves(true)
				.setViernes(true)
				.setDuracion(15)
				.setHoraInicio("09:00")
				.setHoraFin("14:00")
				.setHorario("TARDE")
				.setClearData(false);

		// Locate the Centro to be used to create the Medico.
		final List<Centro> centros = centroRepository.findAll();
		Centro centroTarget = null;
		for ( final Centro centro : centros ) {
			if ( centro.getNombre().equalsIgnoreCase("Hospital de la Manzana") )
				centroTarget = centro;
		}
		if ( null != centroTarget ) {
			// Add the service providers.
			String newReferencia = "CR-" + Instant.now().toEpochMilli();
			randomGenerator = new Random();
			int index = randomGenerator.nextInt(especialities.size());
			final String especialidad = especialities.get(index);

			// Create first the Credencial and then the Medico.
			final String salt = BCrypt.gensalt(8);
			Credencial cred = new Credencial()
					.setNif(newReferencia)
					.setPassword(Credencial.crypt("1234", salt))
					.setSalt(salt)
//					.setTratamiento("Dr.")
					.setNombre(newReferencia)
					.setApellidos("Pruebas Carga")
					.setCentro(centroTarget);
			entityManager.persist(cred);

			Medico medico = new Medico()
					.setCentro(centroTarget)
//					.setCredencial(cred)
					.setReferencia(newReferencia)
					.setId(Medico.generateNewId(centroTarget.getId(), newReferencia))
					.setTratamiento("Dr.")
					.setNombre(newReferencia)
					.setApellidos("Pruebas Carga")
					.setEspecialidad(especialidad);
//					.setLocalidad("SANTANDER")
//					.setDireccion("CL. Isabel II, Nº 22, 2º IZ  Santander - 39002")
//					.setHorario("Solicitar hora.")
//					.setTelefono("942 314 112");
			entityManager.persist(medico);
			logger.info("-- [AppointmentCreationService.createAdditionalMedico]> Creating Medico: {}", medico.getId());
			// Create some appointments and fill them up.
			this.createSeedAppointments(threeMonth9to1410Minutes, medico);
			logger.info("<< [AppointmentCreationService.createAdditionalMedico]");
		}
	}
}

// - UNUSED CODE ............................................................................................
//[01]
