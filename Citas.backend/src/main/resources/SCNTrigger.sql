-- TRIGGER TO KEEP TRACK OF THE SCN FOR APPOINTMENTS
CREATE SEQUENCE IF NOT EXISTS public.scn_sequence
    START WITH 700001
    INCREMENT BY 1
    MINVALUE 700001
    NO MAXVALUE
    CACHE 1
    NO CYCLE;

CREATE OR REPLACE FUNCTION increment_scn()
    RETURNS trigger AS
		$BODY$
		BEGIN
		    new.scn:= nextval(public.scn_sequence);
		    RETURN new;
		END;
		$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER scn_appointments_insert
    BEFORE INSERT
    ON citamed.citas 
    FOR EACH ROW
    EXECUTE PROCEDURE increment_scn ();
    
CREATE TRIGGER scn_appointments_update
    BEFORE UPDATE
    ON citamed.citas 
    FOR EACH ROW
    EXECUTE PROCEDURE increment_scn ();
    
