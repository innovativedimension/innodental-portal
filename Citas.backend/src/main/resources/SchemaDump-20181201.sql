--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-1.pgdg14.04+1)
-- Dumped by pg_dump version 10.5 (Ubuntu 10.5-2.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE dno62h3bh6tpp;
--
-- Name: dno62h3bh6tpp; Type: DATABASE; Schema: -; Owner: mucbeqpwwbmyli
--

CREATE DATABASE dno62h3bh6tpp WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE dno62h3bh6tpp OWNER TO mucbeqpwwbmyli;

\connect dno62h3bh6tpp

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:

--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: increment_scn(); Type: FUNCTION; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE FUNCTION public.increment_scn() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    new.scn:= nextval('public.scn_sequence');
                    RETURN new;
                END;
                $$;


ALTER FUNCTION public.increment_scn() OWNER TO mucbeqpwwbmyli;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: centros; Type: TABLE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE TABLE public.centros (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    direccion character varying(255),
    localidad character varying(255),
    logotipo character varying(255),
    nombre character varying(100)
);


ALTER TABLE public.centros OWNER TO mucbeqpwwbmyli;

--
-- Name: centros_sequence; Type: SEQUENCE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE SEQUENCE public.centros_sequence
    START WITH 100001
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.centros_sequence OWNER TO mucbeqpwwbmyli;

--
-- Name: cita_sequence; Type: SEQUENCE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE SEQUENCE public.cita_sequence
    START WITH 400001
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cita_sequence OWNER TO mucbeqpwwbmyli;

--
-- Name: citas; Type: TABLE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE TABLE public.citas (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    estado character varying(30),
    fecha date,
    hueco_duracion integer,
    hueco_id integer,
    paciente_data jsonb,
    paciente_localizador character varying(30),
    referencia character varying(40),
    tipo character varying(30),
    zonahoraria character varying(20),
    medico_id character varying(255),
    scn bigint NOT NULL
);


ALTER TABLE public.citas OWNER TO mucbeqpwwbmyli;

--
-- Name: citastemplates; Type: TABLE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE TABLE public.citastemplates (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    evening_list jsonb,
    morning_list jsonb,
    nombre character varying(100),
    tmhora_inicio character varying(255),
    tthora_inicio character varying(255),
    idservicio character varying(255)
);


ALTER TABLE public.citastemplates OWNER TO mucbeqpwwbmyli;

--
-- Name: citatemplate_sequence; Type: SEQUENCE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE SEQUENCE public.citatemplate_sequence
    START WITH 200001
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.citatemplate_sequence OWNER TO mucbeqpwwbmyli;

--
-- Name: credenciales; Type: TABLE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE TABLE public.credenciales (
    nif character varying(40) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    allservices boolean,
    apellidos character varying(180),
    enabled boolean,
    nombre character varying(100),
    password character varying(255),
    role character varying(20),
    salt character varying(255),
    servicios_autorizados jsonb,
    centro_id bigint
);


ALTER TABLE public.credenciales OWNER TO mucbeqpwwbmyli;

--
-- Name: limites_servicio; Type: TABLE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE TABLE public.limites_servicio (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    limites jsonb,
    idservicio character varying(255)
);


ALTER TABLE public.limites_servicio OWNER TO mucbeqpwwbmyli;

--
-- Name: limitesservicio_sequence; Type: SEQUENCE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE SEQUENCE public.limitesservicio_sequence
    START WITH 300001
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.limitesservicio_sequence OWNER TO mucbeqpwwbmyli;

--
-- Name: medicos; Type: TABLE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE TABLE public.medicos (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    activo boolean,
    actos_medicos jsonb,
    apellidos character varying(100),
    appointments_count integer,
    especialidad character varying(50),
    first_appointment_id bigint NOT NULL,
    free_appointments_count integer,
    nombre character varying(50),
    referencia character varying(40),
    tratamiento character varying(10),
    centro_id bigint
);


ALTER TABLE public.medicos OWNER TO mucbeqpwwbmyli;

--
-- Name: scn_sequence; Type: SEQUENCE; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE SEQUENCE public.scn_sequence
    START WITH 700001
    INCREMENT BY 1
    MINVALUE 700001
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scn_sequence OWNER TO mucbeqpwwbmyli;

--
-- Name: centros centros_pkey; Type: CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.centros
    ADD CONSTRAINT centros_pkey PRIMARY KEY (id);


--
-- Name: citas citas_pkey; Type: CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.citas
    ADD CONSTRAINT citas_pkey PRIMARY KEY (id);


--
-- Name: citastemplates citastemplates_pkey; Type: CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.citastemplates
    ADD CONSTRAINT citastemplates_pkey PRIMARY KEY (id);


--
-- Name: credenciales credenciales_pkey; Type: CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.credenciales
    ADD CONSTRAINT credenciales_pkey PRIMARY KEY (nif);


--
-- Name: limites_servicio limites_servicio_pkey; Type: CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.limites_servicio
    ADD CONSTRAINT limites_servicio_pkey PRIMARY KEY (id);


--
-- Name: medicos medicos_pkey; Type: CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.medicos
    ADD CONSTRAINT medicos_pkey PRIMARY KEY (id);


--
-- Name: citas scn_appointments_insert; Type: TRIGGER; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE TRIGGER scn_appointments_insert BEFORE INSERT ON public.citas FOR EACH ROW EXECUTE PROCEDURE public.increment_scn();


--
-- Name: citas scn_appointments_update; Type: TRIGGER; Schema: public; Owner: mucbeqpwwbmyli
--

CREATE TRIGGER scn_appointments_update BEFORE UPDATE ON public.citas FOR EACH ROW EXECUTE PROCEDURE public.increment_scn();


--
-- Name: citas fkbfl57ey8hx1a3ubtrpy8x9ll; Type: FK CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.citas
    ADD CONSTRAINT fkbfl57ey8hx1a3ubtrpy8x9ll FOREIGN KEY (medico_id) REFERENCES public.medicos(id) ON DELETE CASCADE;


--
-- Name: limites_servicio fkflmhr5xig3rm9am2jielonmmb; Type: FK CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.limites_servicio
    ADD CONSTRAINT fkflmhr5xig3rm9am2jielonmmb FOREIGN KEY (idservicio) REFERENCES public.medicos(id) ON DELETE CASCADE;


--
-- Name: citastemplates fkg49joc7q9ube2esw81u3vuaty; Type: FK CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.citastemplates
    ADD CONSTRAINT fkg49joc7q9ube2esw81u3vuaty FOREIGN KEY (idservicio) REFERENCES public.medicos(id) ON DELETE CASCADE;


--
-- Name: credenciales fkmfg7aeihomyivqvy4l9vj3eua; Type: FK CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.credenciales
    ADD CONSTRAINT fkmfg7aeihomyivqvy4l9vj3eua FOREIGN KEY (centro_id) REFERENCES public.centros(id) ON DELETE CASCADE;


--
-- Name: medicos fknr3baadstfay6e88l1j170a6e; Type: FK CONSTRAINT; Schema: public; Owner: mucbeqpwwbmyli
--

ALTER TABLE ONLY public.medicos
    ADD CONSTRAINT fknr3baadstfay6e88l1j170a6e FOREIGN KEY (centro_id) REFERENCES public.centros(id) ON DELETE CASCADE;


--
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO mucbeqpwwbmyli;


--
-- PostgreSQL database dump complete
--