﻿-- ALTER TABLE public.citas
-- ADD COLUMN scn bigint;

-- UPDATE public.citas
-- SET scn = 700001;

-- ALTER TABLE public.citas
-- ALTER COLUMN scn SET NOT NULL;

-- COMMIT;


CREATE SEQUENCE IF NOT EXISTS public.scn_sequence
    START WITH 700001
    INCREMENT BY 1
    MINVALUE 700001
    NO MAXVALUE
    CACHE 1
    NO CYCLE;

CREATE OR REPLACE FUNCTION public.increment_scn()
    RETURNS trigger AS
		$BODY$
		BEGIN
		    new.scn:= nextval('public.scn_sequence');
		    RETURN new;
		END;
		$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER scn_appointments_insert
    BEFORE INSERT
    ON public.citas 
    FOR EACH ROW
    EXECUTE PROCEDURE increment_scn ();
    
CREATE TRIGGER scn_appointments_update
    BEFORE UPDATE
    ON public.citas 
    FOR EACH ROW
    EXECUTE PROCEDURE increment_scn ();