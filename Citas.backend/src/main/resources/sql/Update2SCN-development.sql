﻿-- CREATE SEQUENCE AND TRIGGER TO CONTROL THE SCN
CREATE SEQUENCE IF NOT EXISTS citamed.scn_sequence
    START WITH 700001
    INCREMENT BY 1
    MINVALUE 700001
    NO MAXVALUE
    CACHE 1
    NO CYCLE;

CREATE OR REPLACE FUNCTION citamed.increment_scn()
    RETURNS trigger AS
		$BODY$
		BEGIN
		    new.scn:= nextval('citamed.scn_sequence');
		    RETURN new;
		END;
		$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS scn_appointments_insert ON citamed.citas;
CREATE TRIGGER scn_appointments_insert
    BEFORE INSERT
    ON citamed.citas 
    FOR EACH ROW
    EXECUTE PROCEDURE citamed.increment_scn ();
    
DROP TRIGGER IF EXISTS scn_appointments_update ON citamed.citas;
CREATE TRIGGER scn_appointments_update
    BEFORE UPDATE
    ON citamed.citas 
    FOR EACH ROW
    EXECUTE PROCEDURE citamed.increment_scn ();