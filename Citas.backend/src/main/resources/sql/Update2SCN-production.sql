﻿-- UPDATE ANY VALUE ON THE TABLE TO THE MINIMUM VALUE
UPDATE public.citas
SET scn = 700001
WHERE scn < 700000;
COMMIT;


CREATE SEQUENCE IF NOT EXISTS public.scn_sequence
    START WITH 700001
    INCREMENT BY 1
    MINVALUE 700001
    NO MAXVALUE
    CACHE 1
    NO CYCLE;

CREATE OR REPLACE FUNCTION public.increment_scn()
    RETURNS trigger AS
		$BODY$
		BEGIN
		    new.scn:= nextval('public.scn_sequence');
		    RETURN new;
		END;
		$BODY$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS public.scn_appointments_insert ON public.citas;
CREATE TRIGGER public.scn_appointments_insert
    BEFORE INSERT
    ON public.citas 
    FOR EACH ROW
    EXECUTE PROCEDURE public.increment_scn ();
    
DROP TRIGGER IF EXISTS public.scn_appointments_update ON public.citas;
CREATE TRIGGER public.scn_appointments_update
    BEFORE UPDATE
    ON public.citas 
    FOR EACH ROW
    EXECUTE PROCEDURE public.increment_scn ();