//  PROJECT:     CitaMed.backend (CITM.SBBK)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: SpringBoot 2.0.
//  DESCRIPTION: CitaMed. Sistema S1. Aplicacion SpringBoot adaptada para Heroku y diseñada con arquitectura
//               JPA como sistema de persistencia. Se configura para utilizar una base de datos PostgreSQL
//               y dotado de un modelo de acceso RESTful para poder exportar el api y el modelo para acceso
//               desde aplicaciones dispares, como Angular 6 o Android.
package org.dimensinfin.citamed.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.dimensinfin.citamed.services.ICreationService;
import org.dimensinfin.citamed.entities.Centro;
import org.dimensinfin.citamed.entities.Cita;
import org.dimensinfin.citamed.model.FormularioCreacionCitas;
import org.dimensinfin.citamed.entities.Medico;
import org.dimensinfin.citamed.model.MinuteInterval;
import org.dimensinfin.citamed.repository.CentroRepository;
import org.dimensinfin.citamed.repository.CitaRepository;
import org.dimensinfin.citamed.repository.MedicoRepository;
import org.dimensinfin.citamed.services.AppointmentCreationService;

import datastructures.IntervalTree;

/**
 * @author Adam Antinoo
 */
// - CLASS IMPLEMENTATION ...................................................................................
@RunWith(SpringRunner.class)
@DataJpaTest
//@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProcessorControllerTestUnit {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("ProcessorControllerTestUnit");

	// - F I E L D - S E C T I O N ............................................................................
	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	protected AppointmentCreationService creationService;
	@Autowired
	private CentroRepository centroRepository;
	@Autowired
	private MedicoRepository medicoRepository;
	@Autowired
	private CitaRepository citaRepository;

	private IntervalTree<MinuteInterval> testTree = new IntervalTree<MinuteInterval>();
	private Centro centro;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	@TestConfiguration
	static class AppointmentCreationServiceTestContextConfiguration {

		@Bean
		public ICreationService creationService() {
			return new AppointmentCreationService();
		}
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void testBefore01Initialize() {
		logger.info(">> [ProcessorControllerTestUnit.testBefore01Initialize]");
		// Create the test centro.
		centro = new Centro()
				.setNombre("Centro de Pruebas");
		entityManager.persist(centro);

		// Create a test Medico and a set of appointments.
		Medico medico = new Medico()
				.setCentro(centro)
				.setReferencia("942 314 112")
				.setId(Medico.generateNewId(centro.getId(), "-PRUEBAS-"))
				.setTratamiento("Dr.")
				.setNombre("-Nombre-")
				.setApellidos("-Apellidos-")
				.setEspecialidad("Alergología");
//				.setLocalidad("SANTANDER")
//				.setDireccion("-Direccion-")
//				.setHorario("Solicitar hora.")
//				.setTelefono("-Telefono-");
		entityManager.persist(medico);

		// Create some appointments to cover some time.
		LocalDate testdate = LocalDate.now();
		Cita cita = new Cita()
				.setFecha(testdate)
				.setEstado(Cita.ECitaStates.LIBRE.name())
				.setHuecoId(1110)
				.setHuecoDuracion(40)
				.setMedico(medico);
		entityManager.persist(cita);

		// Flush all instances.
		entityManager.flush();

		logger.info("<< [ProcessorControllerTestUnit.testBefore01Initialize]");
	}

	@Test
	public void test01InsertAppointmentNoCollision() {
		logger.info(">> [ProcessorControllerTestUnit.test01InsertAppointmentNoCollision]");
		testBefore01Initialize();

		// Create the test data to simulate the day insertion and create the new slots.
		final Optional<Medico> data = medicoRepository.findById(Medico.generateNewId(centro.getId(), "-PRUEBAS-"));
		// Store the medico into a variable.
		final Medico medico = data.get();
		Assert.assertNotNull("> The Medico test data is not present.", medico);
		LocalDate testdate = LocalDate.now();
		final FormularioCreacionCitas formulario = new FormularioCreacionCitas();
		formulario.fechaInicio = LocalDate.now();
		formulario.fechaFin = LocalDate.now();
		formulario.lunes = true;
		formulario.martes = true;
		formulario.miercoles = true;
		formulario.jueves = true;
		formulario.viernes = true;
		formulario.sabado = true;
		formulario.duracion = 15;
		formulario.horaInicio = LocalTime.of(11, 0);
		formulario.horaFin = LocalTime.of(12, 0);
		creationService.createDayAppointments(medico, testdate, formulario);

		// Check the end result for the appointments.
		final List<Cita> citas = citaRepository.findAppointments4Date(medico.getId(), testdate);
		logger.info("<< [ProcessorControllerTestUnit.test01InsertAppointmentNoCollision]");
	}
}
// - UNUSED CODE ............................................................................................
//[01]
