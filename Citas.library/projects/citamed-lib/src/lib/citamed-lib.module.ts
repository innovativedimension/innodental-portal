//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders } from '@angular/core';
//--- BROWSER & ANIMATIONS
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { FormsModule } from '@angular/forms';
//--- HTTP CLIENT
// import { HttpClientModule } from '@angular/common/http';
//--- TOAST NOTIFICATIONS
// import { SimpleNotificationsModule } from 'angular2-notifications';
// import { NotificationsService } from 'angular2-notifications';
//--- ENVIRONMENT
// import { IEnvironmentConfig } from './interfaces/core/IEnvironmentConfig.interface';
// import { ENVIRONMENT_CONFIG } from './environment.config';
// import { AppConfigDevelopment } from './environment.config';
// import { AppConfigProduction } from './environment.config';
//--- IONIC
// import { IonicApp } from 'ionic-angular';
import { IonicModule } from 'ionic-angular';
// // import { IonicErrorHandler } from 'ionic-angular';
// import { CacheModule } from 'ionic-cache';
// //--- IONIC PLUGINS
// import { IonicStorageModule } from '@ionic/storage';
//--- COMPONENTS
// import { NodeComponent } from './components/node.component';
// import { ExpandableNodeComponent } from './components/expandablenode.component';
// import { CentroComponent } from './components/centro/centro.component';
// import { Cita4TemplateComponent } from './components/cita4-template/cita4-template.component';
// import { PanelCoreComponent } from './core/panel-core/panel-core.component';
// import { CalendarCoreComponent } from './core/calendar-core/calendar-core.component';
//--- RENDERS
// import { RenderComponent } from './renders/render/render.component';
// import { EspecialidadComponent } from './renders/especialidad/especialidad.component';
// // import { MedicoComponent } from './renders/medico/medico.component';
// import { ActoMedicoComponent } from './renders/acto-medico/acto-medico.component';
// import { CitaComponent } from './renders/cita/cita.component';
// import { CitaAsignadaComponent } from './renders/cita-asignada/cita-asignada.component';

@NgModule({
  imports: [
    //--- BROWSER & ANIMATIONS
    // BrowserModule,
    // BrowserAnimationsModule,
    // FormsModule,
    //--- HTTP CLIENT
    // HttpClientModule,
    //--- TOAST NOTIFICATIONS
    // SimpleNotificationsModule.forRoot(),
    //--- IONIC
    IonicModule,
    // IonicStorageModule.forRoot(),
    // CacheModule.forRoot()
  ],
  declarations: [
    //--- COMPONENTS
    // NodeComponent,
    // ExpandableNodeComponent,
    // CentroComponent,
    // PanelCoreComponent,
    // Cita4TemplateComponent,
    // //--- RENDERS
    // RenderComponent,
    // EspecialidadComponent,
    // // MedicoComponent,
    // ActoMedicoComponent,
    // CitaComponent,
    // CitaAsignadaComponent,
    // CalendarCoreComponent,
  ],
  exports: [
    //--- COMPONENTS
    // NodeComponent,
    // ExpandableNodeComponent,
    // CentroComponent,
    // Cita4TemplateComponent,
    // //--- RENDERS
    // RenderComponent,
    // EspecialidadComponent,
    // ActoMedicoComponent,
    // CitaComponent,
    // CitaAsignadaComponent,
  ],
  providers: [
  ]
})

export class CitaMedLibModule {
  // /**
  //  * Receive the configuration created when importing the library on other modules.
  //  * @param  environment the list of values used on the environment. The validation is defined on an Interface
  //  * @return             the module definition using the passed by configuration.
  //  */
  // static forRoot(environment: IEnvironmentConfig): ModuleWithProviders {
  //   console.log("><[CitaMedLibModule.forRoot]> environment:" + JSON.stringify(environment));
  //   return {
  //     ngModule: CitaMedLibModule,
  //     providers: [
  //       { provide: ENVIRONMENT_CONFIG, useValue: environment }
  //     ],
  //   };
  // }
}
