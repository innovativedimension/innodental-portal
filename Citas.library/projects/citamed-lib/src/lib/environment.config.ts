//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- CORE
import { InjectionToken } from "@angular/core";
//--- ENVIRONMENT
// import { environment } from '../../../../src/environments/environment';

export let ENVIRONMENT_CONFIG = new InjectionToken("library.config");

// export interface IEnvironmentService {
//   production: boolean,
//   development: boolean,
//   mockStatus: boolean,
//   showexceptions: boolean,
//   name: string,
//   version: string,
//   servicePort: number,
//   serverName: string
// }
//
// export const AppConfigDevelopment: IEnvironmentService = {
//   production: false,
//   development: true,
//   mockStatus: true,
//   showexceptions: true,
//   name: "dashCitaMed",
//   version: "0.3.0 dev",
//   servicePort: 9000,
//   serverName: ""
// };
//
// export const AppConfigProduction = {
//   production: true,
//   development: false,
//   mockStatus: false,
//   showexceptions: false,
//   name: "dashCitaMed",
//   version: "0.3.0",
//   servicePort: 9000,
//   serverName: "https://backcitamed.herokuapp.com"
// };
