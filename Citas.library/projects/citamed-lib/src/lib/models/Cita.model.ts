//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- CALENDAR
import { differenceInMilliseconds } from 'date-fns';
import { addMilliseconds } from 'date-fns';
//--- MODELS
import { Node } from './core/Node.model';
// import differenceInMilliseconds from 'date-fns/difference_in_milliseconds'

export class Cita extends Node {
  public id: number = -1;
  public referencia: string;
  public fecha: string;
  public fechaDate: Date = new Date();
  public huecoId: number;
  public huecoDuracion: number;
  public estado: string = 'LIBRE';
  public pacienteLocalizador: string;
  public pacienteData: any;
  public tipo: string = "-NORMAL-";
  public zonaHoraria: string = "MAÑANA";
  //--- VIRTUAL FIELDS
  public huecoIdentifier: string;
  public medicoIdentifier: string;
  public centroIdentifier: string;
  public centroName: string;
  public medicoReference: string;
  public medicoNombre: string;

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Cita";

    // Transform dependency objects
    if (null != this.fecha) {
      let dateFields = this.fecha.split("-");
      this.fechaDate = new Date(parseInt(dateFields[0]), parseInt(dateFields[1]) - 1, parseInt(dateFields[2]),
        this.getHour(), this.getMinutes(), 0);
    }
  }
  /**
   * Calculates the disitance in minutes to the target appointment from the nearest appointment edge times. This is done by substracting the four appointments boundaries from the two appointments.
   * Those calculations should be perfomred on the sexagesimal metric system or appointments arounf the hour will not be detected properly.
   * @param  _targetAppointment the target to where to calculate the distance
   * @return                    the distance in seconds.
   */
  public distance(_targetAppointment: Cita): number {
    let d1 = Math.abs(differenceInMilliseconds(this.fechaDate,
      _targetAppointment.fechaDate));
    let d2 = Math.abs(differenceInMilliseconds(this.fechaDate,
      addMilliseconds(_targetAppointment.fechaDate, _targetAppointment.huecoDuracion * 60 * 1000)));
    let d3 = Math.abs(differenceInMilliseconds(addMilliseconds(this.fechaDate, this.huecoDuracion * 60 * 1000), _targetAppointment.fechaDate));
    let d4 = Math.abs(differenceInMilliseconds(addMilliseconds(this.fechaDate, this.huecoDuracion * 60 * 1000),
      addMilliseconds(_targetAppointment.fechaDate, _targetAppointment.huecoDuracion * 60 * 1000)));
    return Math.min(d1, d2, d3, d4) / 1000;
  }
  //--- G E T T E R S   &   S E T T E R S
  public getId(): number {
    return this.id;
  }
  public getHour(): number {
    return Math.round(this.huecoId / 100);
  }
  public getMinutes(): number {
    return this.huecoId - Math.floor(this.huecoId / 100) * 100;
  }
  public getEndHour(): number {
    let h = this.getHour();
    let m = this.getMinutes();
    let newm = m + this.huecoDuracion;
    h = h + Math.floor(newm / 60);
    m = newm % 60;
    return h;
  }
  public getEndMinutes(): number {
    let h = this.getHour();
    let m = this.getMinutes();
    let newm = m + this.huecoDuracion;
    h = h + Math.floor(newm / 60);
    m = newm % 60;
    return m;
  }
  public getDuracion(): number {
    return this.huecoDuracion;
  }
  public getFecha(): Date {
    if (null != this.fecha)
      return this.fechaDate;
    else return new Date();
  }
  public getFechaString(): string {
    return this.fecha;
  }
  public patientIsValid(): boolean {
    if (this.isEmptyString(this.pacienteLocalizador)) return false;
    else return true;
  }
  public getPatientData(): string {
    if (this.isEmptyString(this.pacienteLocalizador)) return "-LIBRE-";
    else return "-" + this.pacienteLocalizador + "-";
  }
  public getTipo(): string {
    return this.tipo;
  }
  public setId(_newid: number): Cita {
    this.id = _newid;
    return this;
  }
  public setTipo(_tipo: string): Cita {
    this.tipo = _tipo;
    return this;
  }
  public setHuecoId(_hueco: number): Cita {
    this.huecoId = _hueco;
    return this;
  }
  public setHuecoDuracion(_duracion: number): Cita {
    this.huecoDuracion = _duracion;
    return this;
  }
  public isFree(): boolean {
    if (this.estado == 'LIBRE') return true;
    else return false;
  }
  public isReserved(): boolean {
    if (this.estado == 'RESERVADA') return true;
    else return false;
  }
}
