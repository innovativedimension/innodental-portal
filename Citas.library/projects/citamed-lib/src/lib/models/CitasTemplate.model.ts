//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Endless Dimensions, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- MODELS
import { Node } from './core/Node.model';
import { Cita } from './Cita.model';
import { MinuteInterval } from './MinuteInterval.model';
import { TemplateBuildingBlock } from './TemplateBuildingBlock.model';

// - C O N S T A N T S
const DEFAULT_MORNING_START_TIME = "08:00";
const DEFAULT_EVENING_START_TIME = "15:00";

export class CitasTemplate extends Node {
  public id: number = -1;
  public nombre: string = "";
  public tmhoraInicio: string;
  public tthoraInicio: string;

  private morningList: Cita[] = [];
  private eveningList: Cita[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "CitasTemplate";

    // Transform non native fields.
    if (this.isNonEmptyArray(this.morningList)) {
      let listTransform = [];
      for (let ct of this.morningList) {
        listTransform.push(new Cita(ct));
      }
      this.morningList = listTransform;
    }
    if (this.isNonEmptyArray(this.eveningList)) {
      let listTransform = [];
      for (let ct of this.eveningList) {
        listTransform.push(new Cita(ct));
      }
      this.eveningList = listTransform;
    }
  }

  //--- G E T T E R S   &   S E T T E R S
  public getMorningList(): Cita[] {
    return this.morningList;
  }
  public getEveningList(): Cita[] {
    return this.eveningList;
  }
  public addMorning(_block: TemplateBuildingBlock): void {
    let startDate = this.calculateStartTimePoint(this.tmhoraInicio, DEFAULT_MORNING_START_TIME, this.morningList);
    let newcita = new Cita()
      .setId(this.morningList.length + 1)
      .setHuecoId(startDate.getHours() * 100 + startDate.getMinutes())
      .setHuecoDuracion(_block.duracion)
      .setTipo(_block.tipo);
    this.morningList.push(newcita);

    // Detection changes on the initial times is complex. Reposition all slots.
    this.recalculateAllSlots();
  }
  public removeMorning(_slot: Cita): void {
    let slotList = [];
    for (let cita of this.morningList) {
      if (cita.getId() === _slot.getId()) continue;
      slotList.push(cita);
    }
    this.morningList = slotList;
    // Detection changes on the initial times is complex. Reposition all slots.
    this.recalculateAllSlots();
  }
  public removeEvening(_slot: Cita): void {
    let slotList = [];
    for (let cita of this.eveningList) {
      if (cita.getId() === _slot.getId()) continue;
      slotList.push(cita);
    }
    this.eveningList = slotList;
    // Detection changes on the initial times is complex. Reposition all slots.
    this.recalculateAllSlots();
  }
  public addEvening(_block: TemplateBuildingBlock): void {
    let startDate = this.calculateStartTimePoint(this.tthoraInicio, DEFAULT_EVENING_START_TIME, this.eveningList);
    let newcita = new Cita()
      .setId(this.eveningList.length + 1)
      .setHuecoId(startDate.getHours() * 100 + startDate.getMinutes())
      .setHuecoDuracion(_block.duracion)
      .setTipo(_block.tipo);
    this.eveningList.push(newcita);

    // Detection changes on the initial times is complex. Reposition all slots.
    this.recalculateAllSlots();
  }
  public addMorningOnTop(_slot: Cita, _block: TemplateBuildingBlock): void {
    // This time we should annd the new slot after the '_slot' item on the list and reorder the list.
    let recalculate = false;
    let currentMorning = this.morningList;
    this.morningList = [];
    for (let cita of currentMorning) {
      if (_slot.getId() === cita.getId()) {
        this.morningList.push(cita);
        // Now add the new slot.
        this.addMorning(_block);
        // And continue adding the rest of the old slots but recalculating.
        recalculate = true;
      } else {
        if (recalculate) this.addMorning(new TemplateBuildingBlock().setDuracion(cita.getDuracion()));
        else this.morningList.push(cita);
      }
    }

    // Detection changes on the initial times is complex. Reposition all slots.
    this.recalculateAllSlots();
  }
  public addEveningOnTop(_slot: Cita, _block: TemplateBuildingBlock): void {
    // This time we should annd the new slot after the '_slot' item on the list and reorder the list.
    let recalculate = false;
    let currentEvening = this.eveningList;
    this.eveningList = [];
    for (let cita of currentEvening) {
      if (_slot.getId() === cita.getId()) {
        this.eveningList.push(cita);
        // Now add the new slot.
        this.addEvening(_block);
        // And continue adding the rest of the old slots but recalculating.
        recalculate = true;
      } else {
        if (recalculate) this.addEvening(new TemplateBuildingBlock().setDuracion(cita.getDuracion()));
        else this.eveningList.push(cita);
      }
    }

    // Detection changes on the initial times is complex. Reposition all slots.
    this.recalculateAllSlots();
  }

  //--- P R I V A T E   S E C T I O N
  protected calculateStartTimePoint(_startTime: string, _defaultInitialTime: string, _list: Cita[]): Date {
    // Calculate the start time for the slot depending on the morning start time and the slots already on the list.
    if (null == _startTime) _startTime = _defaultInitialTime;
    let startHour = _startTime.split(':')[0];
    let startMinute = _startTime.split(':')[1];
    let startDate = new Date();
    startDate.setHours(Number(startHour));
    startDate.setMinutes(Number(startMinute));
    startDate.setSeconds(0);
    for (let cita of _list) {
      startDate = this.dateAdd(startDate, 'minute', cita.huecoDuracion);
    }
    return startDate;
  }
  private recalculateAllSlots(): void {
    // Check for the un initialized value.
    if (null == this.tmhoraInicio) this.tmhoraInicio = DEFAULT_MORNING_START_TIME;
    let timesetup = this.convert2Date(this.tmhoraInicio);
    for (let cita of this.morningList) {
      cita.setHuecoId(timesetup.getHours() * 100 + timesetup.getMinutes());
      timesetup = this.dateAdd(timesetup, 'minute', cita.getDuracion());
    }
    // Check for the un initialized value.
    if (null == this.tthoraInicio) this.tthoraInicio = DEFAULT_EVENING_START_TIME;
    timesetup = this.convert2Date(this.tthoraInicio);
    for (let cita of this.eveningList) {
      cita.setHuecoId(timesetup.getHours() * 100 + timesetup.getMinutes());
      timesetup = this.dateAdd(timesetup, 'minute', cita.getDuracion());
    }
  }
  private convert2Date(_baseTime: string): Date {
    let startHour = _baseTime.split(':')[0];
    let startMinute = _baseTime.split(':')[1];
    let startDate = new Date();
    startDate.setHours(Number(startHour));
    startDate.setMinutes(Number(startMinute));
    startDate.setSeconds(0);
    return startDate;
  }
}
