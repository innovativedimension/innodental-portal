//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- INTERFACES
import { INode } from '../interfaces/core/INode.interface';
import { IExpandable } from '../interfaces/core/IExpandable.interface';
import { IAppointmentEnabled } from '../interfaces/IAppointmentEnabled.interface';
//--- MODELS
import { Node } from './core/Node.model';
import { Medico } from './Medico.model';

export class Especialidad extends Node implements IExpandable, IAppointmentEnabled {
  public nombre: string;
  public _medicos: Medico[] = [];

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Especialidad";
  }

  //--- I A P P O I N T M E N T E N A B L E D   I N T E R F A C E
  /**
   * Return the list of the unique identifiers for all the Medicos on the list. Yhis way we can retrieve the appointments for all the instances on a single pass.
   */
  public getIdentifier(): string[] {
    let result: string[] = [];
    for (let med of this._medicos) {
      result.push(med.getId());
    }
    return result;
  }
  public getEspecialidad(): string {
    return this.nombre;
  }

  //--- I C O L L A B O R A T I O N   I N T E R F A C E
  public collaborate2View(): INode[] {
    let collab: INode[] = [];
    collab.push(this);
    if (this.isExpanded())
      collab = collab.concat(this.collaborateChildren(this._medicos));
    return collab;
  }
  public collaborateChildren(_children: INode[]): INode[] {
    let childCollab = [];
    for (let node of this._medicos) {
      let partialcollab = node.collaborate2View();
      for (let partialnode of partialcollab) {
        childCollab.push(partialnode);
      }
    }
    return childCollab;
  }

  //--- I E X P A N D A B L E   I N T E R F A C E
  public isEmpty(): boolean {
    if (this._medicos.length > 0) return false;
    return true;
  }
  public getContentSize(): number {
    return this._medicos.length;
  }

  //--- G E T T E R S   &   S E T T E R S
  public setNombre(newnombre: string): Especialidad {
    this.nombre = newnombre;
    return this;
  }
  public addMedico(newmedico: Medico): number {
    this._medicos.push(newmedico);
    return this._medicos.length;
  }
  public getMedicos(): Medico[] {
    return this._medicos;
  }
}
