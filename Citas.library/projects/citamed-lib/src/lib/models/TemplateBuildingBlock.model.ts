//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Endless Dimensions, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- MODELS
import { Node } from './core/Node.model';
import { Cita } from './Cita.model';
import { MinuteInterval } from './MinuteInterval.model';

export class TemplateBuildingBlock extends Node {
  public nombre: string = "";
  public duracion: number = 15;
  public tipo: string = "-NORMAL-";

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "TemplateBuildingBlock";

    // Transform non native fields.
  }
  //--- G E T T E R S   &   S E T T E R S
  public setDuracion(_newduration: number): TemplateBuildingBlock {
    this.duracion = _newduration;
    return this;
  }
}
