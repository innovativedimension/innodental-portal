//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- INTERFACES
import { INode } from '../../interfaces/core/INode.interface';
import { ICollaboration } from '../../interfaces/core/ICollaboration.interface';
import { IExpandable } from '../../interfaces/core/IExpandable.interface';

export class Node implements IExpandable {
  public jsonClass: string = "Node";
  public expanded: boolean = false;
  public selected: boolean = false;
  public renderWhenEmpty: boolean = true;
  private expandableActivated: boolean = false;

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    Object.assign(this, values);
    this.jsonClass = "Node";
  }

  //--- I C O L L A B O R A T I O N   I N T E R F A C E
  /**
   * This is the special case for the Node that even being expandable it will not show any internal data because it does not ahve any field to store children.
   * @param  _variant [description]
   * @return          [description]
   */
  public collaborate2View(_variant?: string): INode[] {
    let collab: INode[] = [];
    collab.push(this);
    return collab;
  }

  //--- I N O D E   I N T E R F A C E
  public getJsonClass(): string {
    return this.jsonClass;
  }

  //--- I E X P A N D A B L E   I N T E R F A C E
  public activateExpandable(): void {
    this.expandableActivated = true;
  }
  public disableExpandable(): void {
    this.expandableActivated = false;
  }
  public isExpanded(): boolean {
    return this.expanded;
  }
  public collapse(): boolean {
    this.expanded = false;
    return this.expanded;
  }
  public expand(): boolean {
    this.expanded = true;
    return this.expanded;
  }
  public toggleExpanded() {
    this.expanded = !this.expanded;
  }
  public getContentsSize(): number {
    return 0;
  }

  //--- I S E L E C T A B L E   I N T E R F A C E
  public toggleSelected(): boolean {
    this.selected = !this.selected;
    return this.selected;
  }
  public isSelected(): boolean {
    if (this.selected) return true;
    else return false;
  }
  public select(): void {
    this.selected = true;
  }
  public unselect(): void {
    this.selected = false;
  }

  //--- E Q U A L   I N T E R F A C E
  public isEquivalent(b: any) {
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(this);
    var bProps = Object.getOwnPropertyNames(b);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length != bProps.length) {
      return false;
    }

    for (var i = 0; i < aProps.length; i++) {
      var propName = aProps[i];

      // If values of same property are not equal,
      // objects are not equivalent
      if (this[propName] !== b[propName]) {
        return false;
      }
    }
    // If we made it this far, objects
    // are considered equivalent
    return true;
  }

  //--- G L O B A L   F U N C T I O N S
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }
  public isEmptyString(str: string): boolean {
    let empty = str && str.length > 0; // Or any other logic, removing whitespace, etc.
    return !empty;
  }
  public isNonEmptyArray(target: any[]): boolean {
    if (null == target) return false;
    if (target.length < 1) return false;
    return true;
  }
  /**
   * Adds time to a date. Modelled after MySQL DATE_ADD function.
   * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
   * https://stackoverflow.com/a/1214753/18511
   *
   * @param date  Date to start with
   * @param interval  One of: year, quarter, month, week, day, hour, minute, second
   * @param units  Number of units of the given interval to add.
   */
  protected dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }
}
