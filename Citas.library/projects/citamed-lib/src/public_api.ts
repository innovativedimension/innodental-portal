//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
/*
 * Public API Surface of citamed-lib
 */
export * from './lib/citamed-lib.module';
//--- COMPONENTS
// export * from './lib/components/node.component';
// export * from './lib/components/expandablenode.component';
// export * from './lib/components/centro/centro.component';
// export * from './lib/components/cita4-template/cita4-template.component';
//--- RENDERS
// export * from './lib/renders/render/render.component';
// export * from './lib/renders/especialidad/especialidad.component';
// // export * from './lib/renders/medico/medico.component';
// export * from './lib/renders/acto-medico/acto-medico.component';
// export * from './lib/renders/cita/cita.component';
// export * from './lib/renders/cita-asignada/cita-asignada.component';
//--- INTERFACES
export * from './lib/interfaces/IAppointmentEnabled.interface';
export * from './lib/interfaces/core/ICollaboration.interface';
export * from './lib/interfaces/core/IContainerController.interface';
export * from './lib/interfaces/core/IExpandable.interface';
export * from './lib/interfaces/core/INode.interface';
export * from './lib/interfaces/core/ISelectable.interface';
// export * from './lib/interfaces/core/IViewer.interface';
export * from './lib/interfaces/core/IDataSource.interface';
// export * from './lib/interfaces/EPack.enumerated';
//--- MODELS
export * from './lib/models/core/Node.model';
export * from './lib/models/Centro.model';
export * from './lib/models/Medico.model';
export * from './lib/models/Cita.model';
export * from './lib/models/Credencial.model';
export * from './lib/models/Especialidad.model';
export * from './lib/models/Limitador.model';
export * from './lib/models/MinuteInterval.model';
export * from './lib/models/CitasTemplate.model';
export * from './lib/models/TemplateBuildingBlock.model';
export * from './lib/models/ActoMedico.model';
export * from './lib/models/ActoMedicoCentro.model';
export * from './lib/models/Payload.model';
