import { Component } from '@angular/core';

@Component({
  selector: 'citamed-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'baseCitaMed';
}
