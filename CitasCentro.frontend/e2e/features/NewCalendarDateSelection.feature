Feature: New calendar date selecion
  As a developer testing the new Calendar
  I need to do a click on a calendar cell
  So that I can see that this precise date was selected

  Scenario: click tomorrow cell
    Given I am on the newui page
    When I click on the 31 cell
    Then I should get an output for the 31 date