//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// -- PROTRACTOR
import { browser } from 'protractor';
import { element } from 'protractor';
import { ElementFinder } from 'protractor';
import { by } from 'protractor';
// --- E2E PAGES
import { LoginPageE2E } from '../login-page/login-page.po';
import { DefinicionCalendarioPageE2E } from './definicion-calendario-page.po';

describe('PAGE Gestion Calendario', () => {
  // var log4jsGen = require("../../../log4jsGenerator");
  // log4jsGen.getLogger().info("Testing Log4js");


  let loginPage: LoginPageE2E;
  let dashboardPage: DefinicionCalendarioPageE2E;
  let page: DefinicionCalendarioPageE2E;

  beforeEach(() => {
    loginPage = new LoginPageE2E();
    dashboardPage = new DefinicionCalendarioPageE2E();
    // Move to the login page and start login into the application as administrator.
    loginPage.navigateToLogin();
    // browser.ignoreSynchronization = false;
    // browser.driver.sleep(10000);
    // browser.ignoreSynchronization = true;
    browser.waitForAngular();
    // Locate the login fields.
    let identificador = loginPage.locateIdentificadorField();
    let password = loginPage.locatePasswordField();
    expect(identificador).toBeDefined();
    expect(password).toBeDefined();
    // Enter the login data and pass to the dashboard.
    identificador.sendKeys("85456123Z");
    password.sendKeys("1234");
    let entrarButton = loginPage.locateEntrarButton();
    expect(entrarButton).toBeDefined();
    entrarButton.click();
    browser.ignoreSynchronization = false;
    browser.driver.sleep(2000);
    browser.ignoreSynchronization = true;

    // On the dashboard page locate the Definicion Calendario link.
    dashboardPage.locateDefinicionCalendarioLink()
      .then((definicionCalendarioLink) => {
        if (null != definicionCalendarioLink) definicionCalendarioLink.click();
        browser.ignoreSynchronization = false;
        browser.driver.sleep(10000);
        browser.ignoreSynchronization = true;
      });
  });
  it('search for dashboard labels', () => {
    expect(true).toBeTruthy();

    // On the dashboard page locate the Definicion Calendario link.
    dashboardPage.locateDefinicionCalendarioLink()
      .then((definicionCalendarioLink) => {
        console.log(">>[locateDefinicionCalendarioLink]> definicionCalendarioLink: " + definicionCalendarioLink);
        if (null != definicionCalendarioLink) definicionCalendarioLink.click();
        browser.ignoreSynchronization = false;
        browser.driver.sleep(10000);
        browser.ignoreSynchronization = true;
      })

  });

});

  // - V A C A T I O N   D E L E T E   F L O W
  // - Open the Definición Calendario Page.
  // it('locate the label that identifies there a service active', () => {
  //   console.log('E2E[page/DefinicionCalendarioPageE2E]> locate the label that identifies there a service active');
  //   // page.navigateToDefinicionCalendario();
  //   let serviceActiveLabel = page.checkIfServiceActive();
  //   if (null != serviceActiveLabel)
  //     expect(serviceActiveLabel).toContain('Seleccionado', 'already selected service should show the "Servicio Seleccionado" label');
  // });

  // - Create a new template with 4 30 minutes elements.
  // - Save the template.
  // - Apply the template to a whole week.
  // - Select a single day and convert that day to vacancy. The selected date is a Saturday.
  // - Select the converted date to get the list of appointments.
  // - Select the first appointment and delete it.


  // - L O G I N   P A G E   L I T E R A L S
  // it('should display application name and version', () => {
  //   expect(page.getApplicationName()).toEqual('Citas Centro');
  //   expect(page.getApplicationBrand()).toEqual('Interface Centro');
  //   expect(page.getApplicationVersion()).toEqual('0.8.2 dev');
  // });
  // // - M O D U L E S   P A N E L   L I T E R A L S
  // it('modules panel should be titled "Lista de Módulos"', () => {
  //   expect(page.getTagText('#modules-panel-header')).toEqual('Lista de Módulos');
  // });
  // // - L O G I N   P A N E L   L I T E R A L S
  // it('login panel should be titled "Panel de Identificación"', () => {
  //   expect(page.getTagText('#login-panel-header')).toEqual('Panel de Identificación');
  // });
  // - L O G I N   P A N E L   F O R M
  // it('login panel should validate NIF', () => {
  //   let identificador = element(by.css('#identificador'));
  //   let password = element(by.css('#password'));
  //   expect(identificador).toBeDefined();
  //   expect(password).toBeDefined();

  //   // Check the alert values while editing the NIF field.
  //   identificador.sendKeys("13739");
  //   // console.log('[LoginPageE2E]> form alerts: ' + JSON.stringify(JSON.stringify(element.all(by.css('.alert-danger div')))));
  //   let alertMessage1 = element.all(by.css('.alert-danger div')).get(0).getText();
  //   let alertMessage2 = element.all(by.css('.alert-danger div')).get(1).getText();
  //   // expect(alertMessage1).toEqual('NIF de autentificación debe tener al menos 8 caracteres.');
  //   // expect(alertMessage2).toEqual('Contraseña es un campo requerido.');

  //   // button.click();
  //   // expect(answer.getText()).toEqual("Chocolate!");

  //   // var answer = element(by.binding('answer'));
  //   // var button = element(by.className('question__button'));
  //   // expect(page.getTagText('#login-panel-header')).toEqual('Panel de Identificación');
  // });
// });