//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// -- PROTRACTOR
import { browser } from 'protractor';
import { element } from 'protractor';
import { ElementFinder } from 'protractor';
import { by } from 'protractor';

export class LoginPageE2E {
  navigateToLogin() {
    return browser.get('/#/login');
  }

  // - P A G E   E L E M E N T   L O C A T O R S
  public locateIdentificadorField() {
    return element(by.css('#identificador'));
  }
  public locatePasswordField() {
    return element(by.css('#password'));
  }
  public locateEntrarButton() {
    return element(by.css('#login-button'));
  }
}