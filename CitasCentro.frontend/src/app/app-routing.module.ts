//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
// --- CORE
import { NgModule } from '@angular/core';
// --- ROUTER
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
// --- GUARDS
import { AuthCommonGuard } from '@app/modules/authorization/auth-common.guard';
import { AuthManagerGuard } from '@app/modules/authorization/auth-manager.guard';
import { AuthAdminGuard } from '@app/modules/authorization/auth-admin.guard';

// --- PAGES
import { NotFoundPage } from '@app/pages/not-found-page/not-found-page.component';
import { LoginPageComponent } from '@app/modules/login/pages/login-page/login-page.component';
import { DashboardPageComponent } from '@app/modules/login/pages/dashboard-page/dashboard-page.component';
// --- CITACIONES - MODULE
import { CitacionesPageComponent } from './modules/citaciones/citaciones-page/citaciones-page.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  // --- LOGIN PAGES
  { path: 'login', component: LoginPageComponent },
  { path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthCommonGuard] },

  // - C E N T E R - A D M I N
  {
    path: 'admin',
    loadChildren: './modules/center-admin/center-admin.module#CenterAdminModule',
    canActivate: [AuthAdminGuard]
  },
  // - C I T A C I O N E S
  {
    path: 'citaciones',
    loadChildren: './modules/citaciones/citaciones.module#CitacionesModule'
  },
  // - G E S T I O N - S E R V I C I O
  {
    path: 'gestionservicio',
    loadChildren: './modules/gestion-servicio/gestion-servicio.module#GestionServicioModule',
    canActivate: [AuthManagerGuard]
  },
  // - V I S U A L - T E S T I N G
  {
    path: 'visualtests',
    loadChildren: './modules/visual-testing/visual-testing.module#VisualTestingModule'
  },
  // --- REDIRECT NOT FOUND PAGES
  { path: '**', component: NotFoundPage }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
