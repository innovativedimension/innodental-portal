// - CORE MODULES
import { NgModule } from '@angular/core';
// - BROWSER & ANIMATIONS
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BootstrapModalModule } from 'ng6-bootstrap-modal';
// - HTTP CLIENT
import { HttpClientModule } from '@angular/common/http';
// - NOTIFICATIONS
import { ToastrModule } from 'ng6-toastr-notifications';
// - DRAG & DROP
import { NgDragDropModule } from 'ng-drag-drop';
// - WEBSTORAGE
import { StorageServiceModule } from 'angular-webstorage-service';
// - CALENDAR
// import { CalendarModule } from 'angular-calendar';
// - IONIC
import { IonicModule } from 'ionic-angular';
// - ROUTING
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from './services/backend.service';
import { ModalService } from '@app/services/modal.service';
// - COMPONENTS-CORE
import { AppComponent } from './app.component';

// - APPLICATION MODULES
import { AuthorizationModule } from '@app/modules/authorization/authorization.module';
import { UIModule } from './modules/ui/ui.module';
import { LoginModule } from '@app/modules/login/login.module';
// - PAGES
import { NotFoundPage } from './pages/not-found-page/not-found-page.component';

// - COMPONENTS
// import { TimeSlotComponent } from './components/time-slot/time-slot.component';

import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeEs);

// - ERROR INTERCEPTION
import * as Rollbar from 'rollbar';
import { rollbarConfig } from '@app/rollbar-errorhandler.service';
import { RollbarService } from '@app/rollbar-errorhandler.service';
import { ErrorHandler } from '@angular/core';
import { RollbarErrorHandler } from '@app/rollbar-errorhandler.service';
export function rollbarFactory() {
  return new Rollbar(rollbarConfig);
}

import { IsolationService } from '@app/platform/isolation.service';
import { Angular6BackendService } from '@app/platform/angular6-backend.service';

@NgModule({
  imports: [
    // - BROWSER & ANIMATIONS
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    BootstrapModalModule,
    // - HTTP CLIENT
    HttpClientModule,
    // - NOTIFICATIONS
    ToastrModule.forRoot(),
    // - DRAG & DROP
    NgDragDropModule.forRoot(),
    // - WEBSTORAGE
    StorageServiceModule,
    // - IONIC
    IonicModule.forRoot(AppModule, {
      backButtonText: 'Retroceder',
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      tabsPlacement: 'bottom',
      pageTransition: 'ios-transition'
    }),
    // - APPLICATION MODULES
    AuthorizationModule,
    LoginModule,
    UIModule,
    // - ROUTING
    RouterModule,
    AppRoutingModule
  ],
  declarations: [
    // - COMPONENTS-CORE
    AppComponent,
    // - PAGES
    NotFoundPage
  ],
  providers: [
    // - SERVICES
    IsolationService,
    AppStoreService,
    BackendService,
    // { provide: BackendService, useClass: Angular6BackendService },
    // --- ERROR INTERCEPTION
    // { provide: ErrorHandler, useClass: RollbarErrorHandler },
    // { provide: RollbarService, useFactory: rollbarFactory },
    ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
