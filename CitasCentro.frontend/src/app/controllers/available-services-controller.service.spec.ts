// - CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// - TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { async } from '@angular/core/testing';
// - SERVICES
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
import { AppStoreService } from '@app/services/appstore.service';
// - COMPONENTS
import { AvailableServicesController } from '@app/controllers/available-services-controller.service';
// - MODELS
import { Medico } from '@models/Medico.model';
import { Perfil } from '@app/models/Perfil.model';
import { ServiceAuthorized } from '@app/models/ServiceAuthorized.model';

describe('CONTROLLER AvailableServicesControllerService [Module: CORE]', () => {
  let service: AvailableServicesController;
  let appStoreService: MockAppStoreService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
      ],
      providers: [
        AvailableServicesController,
        { provide: AppStoreService, useClass: MockAppStoreService }
      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    appStoreService = TestBed.get(AppStoreService);
    service = new AvailableServicesController(appStoreService as any);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[controller/AvailableServicesController]> should be created');
      expect(service).toBeDefined('component has not been created.');
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [accessAvailableServices]', () => {
    it('accessAvailableServices: get the list of authorised services for full user', async(() => {
      console.log('><[controller/AvailableServicesController]> accessAvailableServices: get the list of authorised services for this user');
      // Connect the subject listener to test the data.
      service.accessAvailableServices().subscribe((services) => {
        console.log('><[controller/AvailableServicesController]> Services: ' + services.length);
        // Validate the existence and the number of services.
        expect(services).toBeDefined('there should be a list of services');
        expect(services.length).toBe(9, 'the list of services should have 9 elements');

      });
      let credential = new Perfil();
      credential.allservices = true;
      appStoreService.storeCredential(credential);
      // Load the mock environmental data into the mock services
      appStoreService.downloadDoctors(appStoreService.directAccessMockResource('accessdoctors') as Medico[])
      expect(true).toBeTruthy();
    }));
    it('accessAvailableServices: get the list of authorised services for service list user', async(() => {
      console.log('><[controller/AvailableServicesController]> accessAvailableServices: get the list of authorised services for service list user');
      // Connect the subject listener to test the data.
      service.accessAvailableServices().subscribe((services) => {
        console.log('><[controller/AvailableServicesController]> Services: ' + services.length);
        // Validate the existence and the number of services.
        expect(services).toBeDefined('there should be a list of services');
        expect(services.length).toBe(2, 'the list of services should have 2 elements');

      });
      let credential = new Perfil();
      credential.allservices = false;
      let newservice = new ServiceAuthorized();
      newservice.authStatus = true;
      newservice.serviceIdentifier = 'MID:100001:SRVC:ECO A';
      credential.addAuthorisedService(newservice);
      newservice = new ServiceAuthorized();
      newservice.authStatus = true;
      newservice.serviceIdentifier = 'MID:100001:SRVC:ECO B';
      credential.addAuthorisedService(newservice);
      appStoreService.storeCredential(credential);
      // Load the mock environmental data into the mock services
      appStoreService.downloadDoctors(appStoreService.directAccessMockResource('accessdoctors') as Medico[])
      expect(true).toBeTruthy();
    }));
  });
});
