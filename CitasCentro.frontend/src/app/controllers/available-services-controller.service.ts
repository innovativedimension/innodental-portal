// - CORE
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
// - MODELS
import { ServiceAuthorized } from '@app/models/ServiceAuthorized.model';
import { Medico } from '@models/Medico.model';

@Injectable({
  providedIn: 'root'
})
export class AvailableServicesController {
  // - C O N S T R U C T O R
  constructor(protected appStoreService: AppStoreService) { }

  public accessAvailableServices(): Observable<Medico[]> {
    console.log(">>[AvailableServicesController.accessAvailableServices]");
    // Create a new observable that completes when the list of available services is processed.
    return Observable.create((observer) => {
      let services: Medico[] = [];
      this.appStoreService.accessDoctors()
        .subscribe((doctorList) => {
          // this.dataModelRoot = [];
          let authorized: ServiceAuthorized[] = this.appStoreService.accessCredential().serviciosAutorizados;
          for (let doctor of doctorList) {
            if (doctor.activo) {
              // Check if the profile has the right privileges to see the services.
              if (this.appStoreService.accessCredential().allservices)
                services.push(doctor);
              else {
                for (let service of authorized) {
                  if (service.serviceIdentifier === doctor.getId()) services.push(doctor);
                }
              }
            }
          }
          observer.next(services);
          observer.complete();
          console.log("<<[AvailableServicesController.accessAvailableServices]> Services: " + services.length);
        }, (error) => {
          // this.downloading = false;
          // The single error that can be processed at this point is a missing center. All others must be reported and end at the login page or the connections failure page.
          if (error.status == 404) {
            this.appStoreService.errorNotification("El centro solicitado en la credencial no existe. Pongase en contacto con el servicio técnico", "¡Atención!");
            // this.router.navigate(['login']);
          } else {
            this.appStoreService.processBackendError(error);
          }
          observer.next([]);
          observer.complete();
          console.log("EX[AvailableServicesController.accessAvailableServices]> Services: 0. Exception: " + error.message);
        });
    });
  }

}
