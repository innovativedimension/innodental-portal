//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - CORE
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Observable } from "rxjs/internal/Observable";
// - SERVICES
import { AppStoreService } from "@app/services/appstore.service";
import { BackendService } from "@app/services/backend.service";
// - INTERFACES
import { IAppointmentEnabled } from "@interfaces/IAppointmentEnabled.interface";
// - MODELS
import { Cita } from "@models/Cita.model";
import { Subject } from "rxjs/Subject";

@Injectable({
  providedIn: 'root'
})
export class CalendarAppointmentsControllerService {
  private _appointmentsSource = new Subject();
  private appointments = this._appointmentsSource.asObservable() as Observable<Cita[]>;

  // private appointments: Cita[] = [];
  // - C O N S T R U C T O R
  constructor(
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) { }

  // - B A C K E N D   A C C E S S
  public downloadAppointments4Target(_target: IAppointmentEnabled): void {
    console.log(">>[CalendarAppointmentsControllerService.downloadAppointments4Target]");
    if (null != _target) {
      // this.downloading = true;
      let serviceIdentifiers: string[] = _target.getServiceIdentifiers();
      let appointments = [];
      for (let identifier of serviceIdentifiers) {
        // Download the data about the appointments already reserved.
        this.backendService.backendCitas4MedicoById(identifier)
          .subscribe((citas) => {
            console.log("--[CalendarAppointmentsControllerService.downloadAppointments4Target.backendCitas4MedicoById]> count: "
              + citas.length);
            // Aggregate the data from multiples identifiers.
            this._appointmentsSource.next(appointments.concat(citas));
            // Store the processed appointements on the AppStore for easy access by other components.
            // console.log("--[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]> Storing: " + appointments.length + " appointments.");
            // this.appStoreService.storeAppointments(appointments);
            // this.downloading = false;
            console.log("<<[CalendarAppointmentsControllerService.downloadAppointments4Target]");
          }, (error) => {
            // Process any 401 exception that means the session is no longer valid.
            this.appStoreService.processBackendError(error);
          });
      }
    }
  }
  // - A P P O I N T M E N T S
  public accessAppointments(): Observable<Cita[]> {
    return this.appointments;
  }
}