// - CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// - MODULES IMPORTS
import { HttpClientModule } from '@angular/common/http';
// - TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { async } from '@angular/core/testing';
// - SERVICES
// import { StubService } from '../services/StubService.service';
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// - COMPONENTS
import { CalendarAppointmentsControllerService } from '@app/controllers/calendar-appointments-controller.service';
// - MODELS
import { Cita } from '@models/Cita.model';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';
import { Medico } from '@models/Medico.model';
// import { BackendServiceAppointmentsMock } from '../services/BackendServiceAppointments.mockup.service';
import { ActoMedico } from '@models/ActoMedico.model';
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';
import { BackendServiceAppointmentsMock } from '@app/testing/services/BackendServiceAppointments.mockup.service';

xdescribe('CONTROLLER CalendarAppointmentsControllerService [Module: CORE]', () => {
  let service: CalendarAppointmentsControllerService;
  let appStoreService: MockAppStoreService;
  let backendService: BackendServiceAppointmentsMock;

  beforeEach(async(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
      ],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: BackendService, useClass: BackendServiceAppointmentsMock }
      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    service = new CalendarAppointmentsControllerService(appStoreService as any, backendService as any);
    appStoreService = TestBed.get(AppStoreService);
    backendService = TestBed.get(BackendService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[core/CalendarAppointmentsControllerService]> should be created');
      expect(service).toBeDefined('component has not been created.');
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [downloadAppointments4Target]', () => {
    it('downloadAppointments4Target: download appointments for a Medico', () => {
      console.log('><[core/CalendarAppointmentsControllerService]> downloadAppointments4Target: download appointments for a single target');
      // Create the target to be used on the first test.
      let target = new Medico().setId('appointments-medico1');
      // Subscribe to the subject to receive the list of appointments and validate
      service.accessAppointments().subscribe((appointments) => {
        console.log('><[core/CalendarAppointmentsControllerService]> processing the list of appointments');
        expect(appointments).toBeDefined('the result should have contents');
        console.log('><[core/CalendarAppointmentsControllerService]> appointments.length: ' + appointments.length);
        expect(appointments.length).toBe(64, 'the number of mock appointments is 64');
      });
      service.downloadAppointments4Target(null);
      service.downloadAppointments4Target(target);
      expect(true).toBeTruthy();
    });
    it('downloadAppointments4Target: download appointments for a ActoMedico', () => {
      console.log('><[core/CalendarAppointmentsControllerService]> downloadAppointments4Target: download appointments for a single target');
      // Create the target to be used on the first test.
      let maquina = new Medico().setId('appointments-medico1');
      let target = new ActoMedicoCentro({
        maquina: maquina
      });
      // Subscribe to the subject to receive the list of appointments and validate
      service.accessAppointments().subscribe((appointments) => {
        console.log('><[core/CalendarAppointmentsControllerService]> processing the list of appointments');
        expect(appointments).toBeDefined('the result should have contents');
        console.log('><[core/CalendarAppointmentsControllerService]> appointments.length: ' + appointments.length);
        expect(appointments.length).toBe(64, 'the number of mock appointments is 64');
      });
      service.downloadAppointments4Target(null);
      service.downloadAppointments4Target(target);
      expect(true).toBeTruthy();
    });
  });
});