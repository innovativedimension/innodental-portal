//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { Subscription } from 'rxjs';
// --- ROUTER
import { Router } from '@angular/router';
// --- NOTIFICATIONS
//import { NotificationsService } from 'angular2-notifications';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// --- MODELS
import { Medico } from '@models/Medico.model';

@Injectable({
  providedIn: 'root'
})
export class MedicoControllerService {

  // - C O N S T R U C T O R
  constructor(
    protected router: Router,
    // //protected toasterService: NotificationsService,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) { }

  // - M E D I C O   U P D A T E R
  public updateMedico(_medico: Medico): Observable<boolean> {
    console.log(">>[MedicoControllerService.updateMedico]");
    // if (this.allFieldsValid(_medico)) {
    // Filter the data. In the case of Services be sure that the specility and the apellidos are clear.
    if (_medico.isService()) {
      _medico.apellidos = '';
      _medico.especialidad = 'Radiología';
    }
    // To create a new medico we should get access to the current Centro.
    let centro = this.appStoreService.accessCredential().getCentro();
    return this.backendService.backendUpdateConsultaMedico(centro, _medico)
      .pipe(map((savedService) => {
        // Clear the cache of services so next call should progress to a backend call.
        this.appStoreService.clearDoctors();
        console.log("<<[MedicoControllerService.updateMedico]");
        return true;
      }, (error) => {
        console.log("--[MedicoControllerService.updateMedico]> Error: " + error.message);
        // Process any 401 exception that means the session is no longer valid.
        if (error.status == 401) {
          this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
          this.router.navigate(['login']);
        }
        if (error.status == 0) {
          this.appStoreService.errorNotification("Error desconocido en la descarga de datos.", "¡Atención!");
          this.router.navigate(['login']);
        }
        return false;
      }));
    // }
    // console.log("<<[MedicoControllerService.updateMedico]");
  }
  public allFieldsValid(_medico: Medico): boolean {
    if (this.appStoreService.isEmptyString(_medico.tratamiento)) return false;
    else if (_medico.tratamiento == 'Service') return true;
    else if (this.appStoreService.isEmptyString(_medico.especialidad)) return false;
    return true;
  }
}
