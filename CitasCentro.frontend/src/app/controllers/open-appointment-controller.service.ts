//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
// --- ROUTER
import { Router } from '@angular/router';
// --- NOTIFICATIONS
//import { NotificationsService } from 'angular2-notifications';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// --- MODELS
import { OpenAppointment } from '@app/models/OpenAppointment.model';

@Injectable({
  providedIn: 'root'
})
export class OpenAppointmentControllerService {
  private _appointmentsSource = new Subject<OpenAppointment[]>();
  private appointments = this._appointmentsSource.asObservable();

  // - C O N S T R U C T O R
  constructor(
    protected router: Router,
    // protected notifier: NotificationsService,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) { }

  public downloadAppointments4Patient(_identifier: string): Observable<OpenAppointment[]> {
    console.log(">>[OpenAppointmentControllerService.downloadAppointments4Patient]"); this.backendService.backendAppointments4Patient(_identifier)
      .subscribe((openAppointments) => {
        // Read all the opena appointments and store them on the Subject.
        // Filtering is performed on the access call.
        let activeAppointments: OpenAppointment[] = [];
        for (let open of openAppointments) {
          // On the first approach use all the appointments
          console.log("--[OpenAppointmentControllerService.downloadAppointments4Patient.backendAppointments4Patient]> Appointment: " +
            open.getId() + ' state: ' + open.getEstado());
          activeAppointments.push(open);
        }
        // Store on the subject the new list of open appointments.
        this.storeOpenAppointments(activeAppointments);
      }, (error) => {
        // Process any 401 exception that means the session is no longer valid.
        if (error.status == 401)
          this.appStoreService.processBackendError(error);
        else {
          // Any other error should report the user of some network problem that needs previous solution.
          this.appStoreService.errorNotification("Se ha encontrado un error durante la descarga de datos. Consulte las acciones en el manual de usuario."
            , 'Error ' + error.status
            , {
              toastTimeout: 60000,
              dismiss: 'click',
              showCloseButton: true
            });
        }
        // After processing the error update the subject so the response arrives to the panel.
        this.storeOpenAppointments([]);
      });
    // While processing the download return the current value.
    console.log("<<[OpenAppointmentControllerService.downloadAppointments4Patient]");
    return this.accessOpenAppointments('ALL');
  }
  public accessOpenAppointments(_filter?: string): Observable<OpenAppointment[]> {
    if (null == _filter) return this.appointments;
    if (_filter == 'ALL') return this.appointments;
  }
  public accessAllOpenAppointments(): Observable<OpenAppointment[]> {
    return this.appointments;
  }
  public storeOpenAppointments(_newappointments: OpenAppointment[]): void {
    this._appointmentsSource.next(_newappointments);
  }
}
