//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Subscription } from 'rxjs';
// --- ROUTER
import { Router } from '@angular/router';
// --- NOTIFICATIONS
import { ToastrManager } from 'ng6-toastr-notifications';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// --- MODELS
import { Medico } from '@models/Medico.model';
import { CitasTemplate } from '@models/CitasTemplate.model';

@Injectable({
  providedIn: 'root'
})
export class TemplateControllerService {
  private _templateUnderControl: CitasTemplate;

  // - C O N S T R U C T O R
  constructor(
    protected router: Router,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) { }

  // - T E M P L A T E   A C C E S S
  public accessTemplate(): CitasTemplate {
    console.log(">>[TemplateControllerService.accessTemplate]");
    return this._templateUnderControl;
  }
  public storeTemplate(_newtemplate: CitasTemplate): void {
    console.log(">>[TemplateControllerService.storeTemplate]> _newtemplate.name: " + _newtemplate.nombre);
    this._templateUnderControl = _newtemplate;
    console.log(">>[TemplateControllerService.storeTemplate]");
  }

  // - T E M P L A T E   I N T E R A C T I O N
  public editTemplate(_target?: CitasTemplate): void {
    if (null != _target) {
      // Store the new teplate and operate on it.
      this.storeTemplate(_target);
      this.router.navigate(['/gestionservicio/constructorplantilla', this._templateUnderControl.id]);
    }
  }
  public deleteTemplate(_target?: CitasTemplate): Observable<number> {
    if (null != _target) this.storeTemplate(_target);
    console.log(">>[TemplateControllerService.deleteTemplate]> _target.name: " + this._templateUnderControl.nombre);
    // Return an Observable while waiting for the deletion.
    return Observable.create((observer) => {
      try {
        // Tell the backend to delete this template.
        this.backendService.backendDeleteTemplate(this._templateUnderControl.id,
          this.appStoreService.accessActiveService().getId())
          .subscribe((identifier) => {
            // Check we have deleted the right template.
            if (identifier === this._templateUnderControl.id) observer.next(identifier);
            else observer.next(-1);
            observer.complete();
          }), (error) => {
            console.log("--[TemplateControllerService.deleteTemplate]> Error: " + error.message);
            if (error.status == 404) {
              this.appStoreService.errorNotification(error.message, "Error HTTP 404. Not Found",
                { dismiss: 'click' });
            } else {
              this.appStoreService.errorNotification(error.message, "Error HTTP " + error.status,
                { dismiss: 'click' });
            }
            observer.next(-1);
            observer.complete();
          };
      } catch (unexpectedBackendException) {
        console.log("EX[TemplateControllerService.deleteTemplate]> Exception: " + unexpectedBackendException.message);
        this.appStoreService.errorNotification(unexpectedBackendException.message, null,
          { dismiss: 'click' });
        observer.next(-1);
        observer.complete();
      }
    });
  }
}
