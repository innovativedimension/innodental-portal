//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- INTERFACES
import { INode } from '@interfaces/core/INode.interface';
//--- MODELS
import { Node } from '@models/core/Node.model';
import { Cita } from '@models/Cita.model';

export class AppointmentGroup extends Node {
  public title: string;
  public appointments: Cita[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "AppointmentGroup";
  }

  //--- I C O L L A B O R A T I O N   I N T E R F A C E
  /**
   * If expended collaborate the list of appointments.
   * @param  _variant the optional variant for the container if this will affect the generation of the view collaboration.
   * @return          the list of nodes collaborated by this instance.
   */
  public collaborate2View(_variant?: string): INode[] {
    let collab: INode[] = [];
    collab.push(this);
    if (this.isExpanded()) {
      for (let node of this.appointments) {
        let partialcollab = node.collaborate2View(_variant);
        for (let partialnode of partialcollab) {
          collab.push(partialnode);
        }
      }
    }
    return collab;
  }

  //--- I E X P A N D A B L E   I N T E R F A C E
  public isExpandable(): boolean {
    return true;
  }
  //--- G E T T E R S   &   S E T T E R S
  public setTitle(_newtitle: string): AppointmentGroup {
    this.title = _newtitle;
    return this;
  }
  public addAppointment(_newappointment: Cita): void {
    this.appointments.push(_newappointment);
  }
}
