//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- MODELS
import { Node } from '@models/core/Node.model';
import { MinuteInterval } from '@models/MinuteInterval.model';

export class AppointmentReport extends Node {
  public reportdate: Date;
  public previousSlotCount: number = 0;
  public newSlotCount: number = 0;
  public collisions: MinuteInterval[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "AppointmentReport";

    // Transform non native fields.
    this.reportdate = new Date(this.reportdate);
    let collisionsTransform = [];
    if (this.isNonEmptyArray(this.collisions)) {
      for (let collision of this.collisions) {
        collisionsTransform.push(new MinuteInterval(collision));
      }
      this.collisions = collisionsTransform;
    }
  }
  //--- G E T T E R S   &   S E T T E R S
  public hasCollisions(): boolean {
    if (this.collisions.length > 0) return true;
    return false;
  }
}
