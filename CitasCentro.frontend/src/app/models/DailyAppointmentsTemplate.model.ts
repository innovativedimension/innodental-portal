//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- MODELS
import { Node } from '@models/core/Node.model';
import { Cita } from '@models/Cita.model';
// import { MinuteInterval } from '@models/Node.model';
// import { Limitador } from '@models/Node.model';

export class DailyAppointmentsTemplate extends Node {
  public nombre: string = "-TEMPLATE-";
  public slots: Cita[] = [];
  // public limitingRules: LimitingRule[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "DailyAppointmentsTemplate";

    // Transform non native fields.
    let slotsTransform = [];
    if (this.isNonEmptyArray(this.slots)) {
      for (let slot of this.slots) {
        slotsTransform.push(new Cita(slot));
      }
      this.slots = slotsTransform;
    }
  }
  //--- G E T T E R S   &   S E T T E R S
  public getSlots(): Cita[] {
    return this.slots;
  }
}
