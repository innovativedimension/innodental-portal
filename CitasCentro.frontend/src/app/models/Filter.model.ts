//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
export class Filter {
  public name: string;
  public description: String;
  public filterFunction: any;

  //--- G E T T E R S   &   S E T T E R S
  public setName(_newname: string): Filter {
    this.name = _newname;
    return this;
  }
  public setDescription(_newdescription: string): Filter {
    this.description = _newdescription;
    return this;
  }
  public setFilter(_function: any): Filter {
    this.filterFunction = _function;
    return this;
  }
}
