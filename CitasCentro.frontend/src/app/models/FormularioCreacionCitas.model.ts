//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
export class FormularioCreacionCitas {
  public jsonClass: string = "FormularioCreacionCitas";
  public localidad: string;
  public direccion: string;
  public horario: string;
  public telefono: string;
  public fechaInicio: Date;
  public fechaFin: Date;
  public lunes: boolean = false;
  public martes: boolean = false;
  public miercoles: boolean = false;
  public jueves: boolean = false;
  public viernes: boolean = false;
  public sabado: boolean = false;
  public clearData: boolean = false;
  public duracion: number = 30;
  public horaInicio: any;
  public horaFin: any;

  constructor(values: Object = {}) {
    Object.assign(this, values);
    this.jsonClass = "FormularioCreacionCitas";
  }
}
