//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- INTERFACES
// import { INode } from '../interfaces/core/INode.interface';
// import { IAppointmentEnabled } from '../interfaces/IAppointmentEnabled.interface';
// import { IExpandable } from '../interfaces/core/IExpandable.interface';
// //--- MODELS
// import { Node } from './core/Node.model';
// import { Cita } from './Cita.model';
// import { ActoMedico } from './ActoMedico.model';

export class NewMedico {
  public id: string = '';
  public referencia: string;
  public tratamiento: string;
  public nombre: string = "";
  public apellidos: string = "";
  public especialidad: string;
  public activo: boolean = true;

  //--- G E T T E R S   &   S E T T E R S
  public setReferencia(newreferencia: string): NewMedico {
    this.referencia = newreferencia;
    return this;
  }
  public setTratamiento(newtratamiento: string): NewMedico {
    this.tratamiento = newtratamiento;
    return this;
  }
  public setNombre(newnombre: string): NewMedico {
    this.nombre = newnombre;
    return this;
  }
  public setApellidos(newapellidos: string): NewMedico {
    this.apellidos = newapellidos;
    return this;
  }
  public setEspecialidad(newespecialidad: string): NewMedico {
    this.especialidad = newespecialidad;
    return this;
  }
}
