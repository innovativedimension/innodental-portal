//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
export class Pair {
  public open: number = 0;
  public reserved: number = 0;

  //--- G E T T E R S   &   S E T T E R S
  public addOpen(): number {
    return this.open++;
  }
  public addReserved(): number {
    this.open++;
    return this.reserved++;
  }
  public getOpen(): number {
    return this.open;
  }
  public getReserved(): number {
    return this.reserved;
  }
}
