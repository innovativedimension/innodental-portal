//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- INTERFACES
// import { INode } from '@models/Node.model';
//--- MODELS
import { Node } from '@models/core/Node.model';
import { OpenAppointment } from '@app/models/OpenAppointment.model';

export class PatientData extends Node {
  public tipoIdentificador: string = "NIF";
  public identificador: string;
  public tefContacto: string;
  public nombre: string;
  public apellidos: string;
  public email: string;
  public aseguradora: string = "Privado";

  // Additional local implementation fields
  public hasUsedService: boolean = false;
  public openAppointments: OpenAppointment[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "PatientData";
  }
  //--- G E T T E R S   &   S E T T E R S
  public getIdentificador(): string {
    return this.identificador;
  }
  public setIdentificador(_newidentificador: string): PatientData {
    this.identificador = _newidentificador;
    return this;
  }
  public isValid(): boolean {
    if (this.isEmptyString(this.identificador)) return false;
    if (this.isEmptyString(this.tefContacto)) return false;
    if (this.isEmptyString(this.nombre)) return false;
    if (this.isEmptyString(this.apellidos)) return false;
    if (this.isEmptyString(this.aseguradora)) return false;
    return true;
  }
  public setOpenAppointments(_appointments: OpenAppointment[]): PatientData {
    this.openAppointments = _appointments;
    return this;
  }
}
