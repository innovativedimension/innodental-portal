//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- INTERFACES
// import { INode } from '@models/Node.model';
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';
//--- MODELS
import { Node } from '@models/core/Node.model';
import { Cita } from '@models/Cita.model';
// import { Especialidad } from '@models/Node.model';
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';

export class Pendings extends Node {
  public node: IAppointmentEnabled;
  // public appointmentFulfilled: boolean = false;
  public appointments: Cita[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Pendings";
  }

  //--- G E T T E R S   &   S E T T E R S
  public getServiceIdentifiers(): string {
    return this.node.getEspecialidad();
  }
  public getNode(): IAppointmentEnabled {
    return this.node;
  }
  public getEspecialidad(): string {
    return this.node.getEspecialidad();
  }
  public getNodeClass(): string {
    if (null != this.node) return this.node.getJsonClass();
  }
  public setTarget(_target: any): Pendings {
    this.node = _target;
    return this;
  }
  public getAppointmentsCount(): number {
    return this.appointments.length;
  }
  /**
   * Assign and block an appointment to be associated with this patient and also see if it covers the time required by the Pending to be completed.
   * If the Pendings target is a Doctor then a single appintment will cover the required time. If otherwise it is a medical act then we should substract the times only.
   * This method should make the blocking of the appointment on the backend.
   * @param _citaSlot a new appointment slot with the start time, end time and duration. It has also a date.
   */
  public addAppointment(_citaSlot: Cita): void {
    // if (this.node.getJsonClass() === 'Especialidad') {
    //   // this.duration = _citaSlot.huecoDuracion;
    //   this.appointmentFulfilled = true;
    // }
    // if (this.node.getJsonClass() === 'ActoMedico') {
    //   // this.duration = _citaSlot.huecoDuracion;
    // }
    this.appointments.push(_citaSlot);
    // this.appointmentFulfilled = this.isCovered();
  }
  /**
   * Checks of the assigned appointments cover all the time required by the node.
   * @return true if the assigned appoitments cover the full time required. False otherwise
   */
  public isCovered(): boolean {
    if (this.node.getJsonClass() === 'Especialidad') {
      // Check if it has any appointment.
      if (this.appointments.length > 0) return true;
      else return false;
    }
    if (this.node.getJsonClass() === 'Medico') {
      // Check if it has any appointment.
      if (this.appointments.length > 0) return true;
      else return false;
    }
    if (this.node.getJsonClass() === 'ActoMedicoCentro') {
      let act = this.node as ActoMedicoCentro;
      let coverTime: number = act.tiempoRequerido;
      for (let app of this.appointments) {
        coverTime -= app.getDuracion();
      }
      if (coverTime > 0) return false;
      else return true;
    }
  }
}
