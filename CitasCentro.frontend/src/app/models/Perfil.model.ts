// - MODELS
import { Credencial } from '@models/Credencial.model';
import { ServiceAuthorized } from '@app/models/ServiceAuthorized.model';

/**
 * This entity will contain the validated credential data downloaded form the server. We are not going to download the password or any other information that could break security.
 * It will contain the code to locate the server side session and from that obtain any identification data required a  the backend.
 * One <b>Perfil</b> contains the reference to the authorizations profile to access some provileged menus and functionalities. It also has access to the list of services authorized to this credential.
 * 
 * @export
 * @class Perfil
 * @extends {Node}
 */
export class Perfil extends Credencial {
  public enabled: boolean = true;
  public role: string = "DESHABILITADO";
  public allservices: boolean = false;
  public serviciosAutorizados: ServiceAuthorized[] = [];
  public authorizationToken: string;

  // - C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Perfil";

    // Transform dependency objects
    if (null != this.serviciosAutorizados) {
      let serviceList = [];
      for (let service of this.serviciosAutorizados) {
        serviceList.push(new ServiceAuthorized(service));
      }
      this.serviciosAutorizados = serviceList;
    }
  }

  // - G E T T E R S   &   S E T T E R S
  public getLoginRole(): string {
    return this.role;
  }
  public getLoginLevel(): number {
    return this.convertRole2Level(this.getLoginRole());
  }
  public isValid(): boolean {
    if (this.isEmptyString(this.nif)) return false;
    if (this.isEmptyString(this.nombre)) return false;
    if (this.isEmptyString(this.authorizationToken)) return false;
    return true;
  }
  public getAuthorization(): string {
    return this.authorizationToken;
  }
  public addAuthorisedService(_service: ServiceAuthorized): number {
    if (null != _service)
      this.serviciosAutorizados.push(_service);
    return this.serviciosAutorizados.length;
  }

  protected convertRole2Level(_role: string): number {
    if (_role == 'ADMIN') return 999;
    if (_role == 'RESPONSABLE') return 300;
    if (_role == 'CALLCENTER') return 200;
    if (_role == 'USUARIO') return 100;
    return 0;
  }
}
