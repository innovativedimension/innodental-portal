//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- INTERFACES
// import { ETaskState } from 'app/interfaces/EPack.enumerated';

export class TaskFlag {
  private state: string = "RUNNING";

  //--- G E T T E R S   &   S E T T E R S
  // public getState(): ETaskState {
  //   return this.state;
  // }
  // public setState(newstate: ETaskState): TaskFlag {
  //   this.state = newstate;
  //   return this;
  // }
}
