//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
// import { throwError } from 'rxjs';
// import { BehaviorSubject } from 'rxjs';
// import { map } from 'rxjs/operators';
// import { catchError } from 'rxjs/operators';
// --- IONIC
// import { CacheService } from 'ionic-cache';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
// --- ROUTER
import { Router } from '@angular/router';
import { ActivatedRouteSnapshot } from '@angular/router';
import { RouterStateSnapshot } from '@angular/router';
import { CanActivate } from '@angular/router';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';

@Injectable({
  providedIn: 'root'
})
export class AuthAdminGuard implements CanActivate {
  constructor(
    @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
    protected router: Router,
    protected appStoreService: AppStoreService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    console.log(">>[AuthAdminGuard.canActivate]");
    // Check that the session has a valid token stored.
    let token = this.sessionStorage.get(environment.TOKEN_KEY);
    if (null == token) {
      console.log("--[AuthAdminGuard.canActivate]> Token invalid.");
      this.router.navigate(["login"]);
      return false;
    } else {
      // This is the point where we should check the destionation and see if it is allowed by the profile privileges.
      let credential = this.appStoreService.accessCredential();
      if (null == credential) {
        console.log("--[AuthAdminGuard.canActivate]> Credential not found.");
        this.router.navigate(["login"]);
        return false;
      } else {
        console.log("--[AuthAdminGuard.canActivate]> Credential role: " + credential.role);
        // Check if the credential has the right role.
        if (credential.role == 'ADMIN') return true;
        else return false;
      }
    }
  }
}
