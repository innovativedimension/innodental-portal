import { Component, OnInit } from '@angular/core';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// import { AppPanelComponent } from '@app/components/app-panel/app-panel.component';

@Component({
  selector: 'admin-dashboard-page',
  templateUrl: './admin-dashboard-page.component.html',
  styleUrls: ['./admin-dashboard-page.component.scss']
})
export class AdminDashboardPageComponent extends AppPanelComponent {
  public activatePage(_pageName : string):void {
    this.router.navigate([_pageName]);
  }
}
