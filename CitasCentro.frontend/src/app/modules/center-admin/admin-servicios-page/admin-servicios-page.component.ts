//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Inject } from '@angular/core';
import { OnInit } from '@angular/core';
import { Output } from '@angular/core';
import { ViewChild } from '@angular/core';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
// --- ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
// --- NOTIFICATIONS
//import { NotificationsService } from 'angular2-notifications';
import { ToastrManager } from 'ng6-toastr-notifications';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// import { AppModelStoreService } from 'app/services/app-model-store.service';
// import { BackendService } from 'app/services/backend.service';
// import { SubjectService } from 'app/services/subject.service';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { ServiceListPanelComponent } from '@app/modules/center-admin/panels/service-list-panel/service-list-panel.component';
// --- MODELS
import { Medico } from '@models/Medico.model';

@Component({
  selector: 'admin-servicios-page',
  templateUrl: './admin-servicios-page.component.html',
  styleUrls: ['./admin-servicios-page.component.scss']
})
export class AdminServiciosPageComponent extends AppPanelComponent {
  @ViewChild(ServiceListPanelComponent) private serviceList: ServiceListPanelComponent;

  public newService: boolean = false; // To signal to show the New Service panel.
  public editing: boolean = false; // Used to signal when w have to show the Edit Service panel.
  // public activateModal: boolean = false;
  public serviceActive: Medico;
  public targetService: Medico = new Medico();
  public validating: boolean = false; // Used to signal the processing of the validation on the backend.
  public showNewActoMedico: boolean = false; // Set tot TRUE to show the new Acto Medico Panel

  // //--- C O N S T R U C T O R
  // constructor(
  //   @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
  //   @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
  //   protected router: Router,
  //   protected activeRoute: ActivatedRoute,
  //   //protected toasterService: NotificationsService,
  //   //protected notifier: ToastrManager,
  //   protected appStoreService: AppStoreService,
  //   protected backendService: BackendService,
  //   private modalService: ModalService) {
  //   super(storage, sessionStorage, router, activeRoute, appStoreService, backendService);
  // }

  // - V I E W   I N T E R A C T I O N
  public onFinishedAction(_caller: AppPanelComponent): void {
    // If the caller is the new panel then reload the service list.
    this.serviceList.refresh();
    // Closed all other panels.
    this.newService = false;
    this.editing = false;
    this.showNewActoMedico = false;
  }

  // - F O R M   I N T E R A C T I O N
  public onServiceSelected(_service: Medico): void {
    this.newService = false;
    this.editing = true;
    this.serviceActive = _service;
  }
}
