//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// --- ROUTING
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
// --- TOAST NOTIFICATIONS
// //import { SimpleNotificationsModule } from 'angular2-notifications';
// //import { NotificationsService } from 'angular2-notifications';
// --- DRAG & DROP
import { NgDragDropModule } from 'ng-drag-drop';
// --- CALENDAR
// import { CalendarModule } from 'angular-calendar';
// --- IONIC
import { IonicModule } from 'ionic-angular';
// --- GUARDS
import { AuthAdminGuard } from '@app/modules/authorization/auth-admin.guard';

// --- APPLICATION LIBRARIES
import { UIModule } from '@app/modules/ui/ui.module';
import { SharedModule } from '@app/modules/shared/shared.module';

// --- PAGES
import { AdminDashboardPageComponent } from '@app/modules/center-admin/admin-dashboard-page/admin-dashboard-page.component';
import { ActoMedicoCreatePageComponent } from '@app/modules/center-admin/acto-medico-create-page/acto-medico-create-page.component';
import { AdminServiciosPageComponent } from '@app/modules/center-admin/admin-servicios-page/admin-servicios-page.component';
// --- PANELS
import { NewServicePanelComponent } from './panels/new-service-panel/new-service-panel.component';
import { ServiceListPanelComponent } from './panels/service-list-panel/service-list-panel.component';
import { EditServicePanelComponent } from './panels/edit-service-panel/edit-service-panel.component';
// import { ModalComponent } from '@app/modules/ui/modal.directive/modal.directive';
import { NewActoMedicoDoctorPanelComponent } from './panels/new-acto-medico-doctor-panel/new-acto-medico-doctor-panel.component';
import { HeaderPanelComponent } from '@app/modules/center-admin/panels/header-panel/header-panel.component';
import { ToolbarPanelComponent } from './panels/toolbar-panel/toolbar-panel.component';

// --- MODULE ROUTES
const routes: Routes = [
  { path: '', component: AdminDashboardPageComponent, canActivate: [AuthAdminGuard] },
  { path: 'servicios', component: AdminServiciosPageComponent , canActivate: [AuthAdminGuard]}
];

@NgModule({
  imports: [
    //--- CORE
    CommonModule,
    //--- ROUTING
    RouterModule.forChild(routes),
    //--- TOAST NOTIFICATIONS
    // SimpleNotificationsModule.forRoot(),
    //--- DRAG & DROP
    NgDragDropModule.forRoot(),
    //--- CALENDAR
    // CalendarModule.forRoot(),
    //--- IONIC
    IonicModule,
    //--- APPLICATION MODULES
    UIModule,
    SharedModule,
    // CitaMedLibModule
  ],
  declarations: [
    // --- PAGES
    AdminDashboardPageComponent,
    AdminServiciosPageComponent,
    ActoMedicoCreatePageComponent,
    // --- PANELS
    NewServicePanelComponent,
    ServiceListPanelComponent,
    EditServicePanelComponent,
    NewActoMedicoDoctorPanelComponent,
    HeaderPanelComponent,
    ToolbarPanelComponent
  ],
  exports: [
    RouterModule
  ]
})
export class CenterAdminModule { }
