//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// --- MODELS
import { Medico } from '@models/Medico.model';

@Component({
  selector: 'edit-service-panel',
  templateUrl: './edit-service-panel.component.html',
  styleUrls: ['./edit-service-panel.component.scss']
})
export class EditServicePanelComponent extends AppPanelComponent implements OnInit {
  @Input() service: Medico;
  @Output() finished = new EventEmitter<AppPanelComponent>();

  public tratamientos: string[] = [];
  public especialidades: string[] = [];
  private backup: Medico;

  // - L I F E C Y C L E
  ngOnInit() {
    // DEBUG: Remove form debug data.
    this.development=false;
    console.log(">> [EditServicePanelComponent.ngOnInit]");
    // Load the list of possible treatments.
    this.appStoreService.propertiesTratamientos()
      .subscribe((treatmentList) => {
        console.log("-- [EditServicePanelComponent.ngOnInit]> Obtained treatment list: " + treatmentList.length);
        this.tratamientos = treatmentList;
      });
    // Load the list of specilities.
    this.appStoreService.propertiesEspecialidades()
      .subscribe((specialityList) => {
        console.log("-- [EditServicePanelComponent.ngOnInit]> Obtained speciality list: " + specialityList.length);
        this.especialidades = specialityList;
      });
    // Make a backup copy of the service being edited.
    this.backup = new Medico(this.service);
    console.log("<< [EditServicePanelComponent.ngOnInit]");
    // this.development = false;
  }
  public isService(): boolean {
    if (this.appStoreService.isNonEmptyString(this.service.tratamiento))
      if (this.service.tratamiento == 'Servicio') return true;
    return false;
  }

  // - F O R M   I N T E R A C T I O N
  public credentialReset(): void {
    // this.service = new Medico();
  }
  public onSubmit(): void {
    console.log(">> [EditServicePanelComponent.onSubmit]");
    // Filter the data. In the case of Services be sure that the specility and the apellidos are clear.
    if (this.isService()) {
      this.service.apellidos = '';
      this.service.especialidad = 'Radiología';  // Speciality can never be empty.
    }
    // To update we should get access to the current Centro.
    let centro = this.appStoreService.accessCredential().getCentro();
    // Convert the frontend service to 
    this.backendService.backendUpdateConsultaMedico(centro, this.service)
      .subscribe((savedService) => {
        try{
        // Notify container that we have finished and we can close the panel.
        this.finished.emit(this);
        } catch (Exception){
          console.log("-- [EditServicePanelComponent.onSubmit]> Exception: " + Exception.message);
        }
      }), (error) => {
        console.log("-- [EditServicePanelComponent.onSubmit]> Error: " + error.message);
      };
    console.log("<< [EditServicePanelComponent.onSubmit]");
  }
  public cancelChanges(): void {
    this.service = new Medico(this.backup);
    this.finished.emit(this);
  }
  get diagnostic() { return JSON.stringify(this.service); }
}
