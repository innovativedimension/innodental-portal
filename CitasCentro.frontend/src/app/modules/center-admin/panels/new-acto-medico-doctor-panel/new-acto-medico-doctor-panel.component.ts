//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
// --- MODELS
import { Medico } from '@models/Medico.model';
import { ActoMedico } from '@models/ActoMedico.model';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'cc-new-acto-medico-doctor-panel',
  templateUrl: './new-acto-medico-doctor-panel.component.html',
  styleUrls: ['./new-acto-medico-doctor-panel.component.scss']
})
export class NewActoMedicoDoctorPanelComponent extends AppPanelComponent {
  @Input() activeService: Medico;
  @Output() finished = new EventEmitter<AppPanelComponent>();

  public actoMedico: ActoMedico = new ActoMedico();

  onSubmit() {
    console.log(">>[NewActoMedicoDoctorPanelComponent.onSubmit]");
    // Add the new medical act to the selected service.
    if (null != this.activeService) {
      this.activeService.addActoMedico(new ActoMedico(this.actoMedico));
    }
    // Now update the backend copy of the current service and notify the user.
    // To update we should get access to the current Centro.
    let centro = this.appStoreService.accessCredential().getCentro();
    // Convert the frontend service to 
    this.backendService.backendUpdateConsultaMedico(centro, this.activeService)
      .subscribe((savedService) => {
        // try {
        // Notify container that we have finished and we can close the panel.
        if (this.activeService.isService())
          this.appStoreService.successNotification("Datos del médico/servicio " + this.activeService.nombre + " actualizados correctamente.", "Actualizado");
        else this.appStoreService.successNotification("Datos del médico/servicio " + this.activeService.apellidos + " actualizados correctamente.", "Actualizado");
        // Clear the panel for new Actos Medicos.
        this.actoMedico = new ActoMedico();
        this.finished.emit(this);
        // } catch (Exception) {
        //   console.log("--[NewActoMedicoDoctorPanelComponent.onSubmit]> Exception: " + Exception.message);
        // }
      }), (error) => {
        // Process any 401 exception that means the session is no longer valid.
        if (error.status == 401) {
          this.appStoreService.errorNotification("¡Atención!", "La credencial ha expirado o no se encuentra.");
          this.router.navigate(['login']);
        }
        console.log("--[NewActoMedicoDoctorPanelComponent.onSubmit]> Error: " + error.message);
        this.appStoreService.errorNotification("¡Atención!", "Error actualizando Médico/Servicio. Mensaje: " + error.message);
      };
    console.log("<<[NewActoMedicoDoctorPanelComponent.onSubmit]");
  }
}
