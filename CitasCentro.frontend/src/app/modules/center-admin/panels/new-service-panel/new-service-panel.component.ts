//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// --- MODELS
import { Medico } from '@models/Medico.model';
import { NewMedico } from '@app/models/NewMedico.model';

@Component({
  selector: 'new-service-panel',
  templateUrl: './new-service-panel.component.html',
  styleUrls: ['./new-service-panel.component.scss']
})
export class NewServicePanelComponent extends AppPanelComponent implements OnInit {
  @Output() finished = new EventEmitter<AppPanelComponent>();

  public service: NewMedico = new NewMedico();
  public showDataFields: boolean = false; // Used to hide the data fields until the identifier is validated.

  public tratamientos: string[] = [];
  public especialidades: string[] = [];

  // - L I F E C Y C L E
  ngOnInit() {
    console.log(">> [NewServicePanelComponent.ngOnInit]");
    // Load the list of possible treatments.
    this.appStoreService.propertiesTratamientos()
      .subscribe((treatmentList) => {
        console.log("-- [NewServicePanelComponent.ngOnInit]> Obtained treatment list: " + treatmentList.length);
        this.tratamientos = treatmentList;
      });
    // Load the list of specilities.
    this.appStoreService.propertiesEspecialidades()
      .subscribe((specialityList) => {
        console.log("-- [NewServicePanelComponent.ngOnInit]> Obtained speciality list: " + specialityList.length);
        this.especialidades = specialityList;
      });
    console.log("<< [NewServicePanelComponent.ngOnInit]");
    // this.development = false;
  }
  public isService(): boolean {
    if (this.appStoreService.isNonEmptyString(this.service.tratamiento))
      if (this.service.tratamiento == 'Servicio') return true;
    return false;
  }

  // - F O R M   I N T E R A C T I O N
  public credentialReset(): void {
    this.service = new Medico();
  }
  public onSubmit(): void {
    console.log(">> [NewServicePanelComponent.onSubmit]");
    if (this.allFieldsValid()) {
      // Filter the data. In the case of Services be sure that the specility and the apellidos are clear.
      if (this.isService()) {
        this.service.apellidos = '';
        this.service.especialidad = 'Radiología';  // Speciality can never be empty.
      }
      // To create a new medico we should get access to the current Centro.
      let centro = this.appStoreService.accessCredential().getCentro();
      this.backendService.backendCreateConsultaMedico(centro, this.service)
        .subscribe((savedService) => {
          // Clear the cache of services so they should progress to a backend call.
          this.appStoreService.clearDoctors();
          this.finished.emit(this);
        }), (error) => {
          console.log("-- [NewServicePanelComponent.onSubmit]> Error: " + error.message);
        };
    }
    console.log("<< [NewServicePanelComponent.onSubmit]");
  }
  public allFieldsValid(): boolean {
    if (this.isEmptyString(this.service.tratamiento)) return false;
    else if (this.service.tratamiento == 'Service') return true;
    else if (this.isEmptyString(this.service.especialidad)) return false;
    return true;
  }
  /**
   * The user has exited the <b>reference</b> field. Check if the value is a valid NIF identifier. If the value is valid then we should check against the backend to check if there is another record with the same identifier on the same Centro. We should remember that doctor's instances can belong to more than a single Centro but that the unique identifier should be unique.
   * If the idntifier is free we can show the rest of the fields.
   */
  public onIdentificadorBlur(): void {
    console.log(">>[NewServicePanelComponent.onIdentificadorBlur]");
    if (this.service.referencia.length > 7) {
      // Check if a valid NIF.
      if (this.validateNIF(this.service.referencia)) {
        // Check if the identifier is free on the backend.
        let center = this.appStoreService.accessCredential().getCentro();
        // this.backendService.validateMedicoUniqueIdentifier(this.service.generateUniqueIdentifier(center))
        //   .subscribe((validUniqueIdentifier) => {
        this.showDataFields = true;
        // }, (error) => {
        //   console.log("--[NewServicePanelComponent.onIdentificadorBlur]> Error: " + error.message);
        //   this.toasterService.error('Excepción', 
        //   'Error procesando el nuevo idnetificador. Mensaje: ' + error.message);
        // });
      }
    }
    console.log("<<[NewServicePanelComponent.onIdentificadorBlur]");
  }
  /**
   * Funtion to validate NIF/CIF/NIE
   * @param  value the field value at least with 8 characters.
   * @return       boolean being true that the field is value.
   */
  public validateNIF(value: string): boolean {
    let validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
    let nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    let nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    let str = value.toString().toUpperCase();

    if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

    let nie = str
      .replace(/^[X]/, '0')
      .replace(/^[Y]/, '1')
      .replace(/^[Z]/, '2');

    let letter = str.substr(-1);
    let charIndex = parseInt(nie.substr(0, 8)) % 23;

    if (validChars.charAt(charIndex) === letter) return true;

    return false;
  }

  get diagnostic() { return JSON.stringify(this.service); }
  public isEmptyString(str: string): boolean {
    let empty = str && str.length > 0; // Or any other logic, removing whitespace, etc.
    return !empty;
  }
}
