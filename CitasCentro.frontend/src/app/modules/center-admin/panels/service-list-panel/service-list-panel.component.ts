//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { OnInit } from '@angular/core';
import { Output } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// --- MODELS
import { Medico } from '@models/Medico.model';

@Component({
  selector: 'service-list-panel',
  templateUrl: './service-list-panel.component.html',
  styleUrls: ['./service-list-panel.component.scss']
})
export class ServiceListPanelComponent extends AppPanelComponent implements OnInit {
  @Output() serviceSelected = new EventEmitter<Medico>();

  private _medicosSubscription : ISubscription;

  // - L I F E C Y C L E
  ngOnInit() {
    console.log(">> [ServiceListPanelComponent.ngOnInit]");
    this.downloading=true;
    // We sould expect that the data has beed started the download so we ony have to subscribe to the subjects.
    this.refresh();
  }

  // - F O R M   I N T E R A C T I O N
  public selectService(_service: Medico): void {
    this.serviceSelected.emit(_service);
  }

  // - I N T E R C O M M U N I C A T I O N S
  // - I M V C O N T R O L L E R   I N T E R F A C E
  public refresh(): void {
    // this.dataModelRoot = [];
    this._medicosSubscription = this.appStoreService.accessDoctors()
      .subscribe((doctorList) => {
        this.dataModelRoot = [];
        for (let doctor of doctorList) {
          this.dataModelRoot.push(doctor);
        }
        this.notifyDataChanged();
        this.downloading=false;
      }, (error) => {
        // Process any 401 exception that means the session is no longer valid.
        if (error.status == 401) {
          this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
          this.router.navigate(['login']);
        }
       if (error.status == 0) {
          this.appStoreService.errorNotification("Error desconocido en la descarga de datos.", "¡Atención!");
          this.router.navigate(['login']);
        }
      });
  }
}
