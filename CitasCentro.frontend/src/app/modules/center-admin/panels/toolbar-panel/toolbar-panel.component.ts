//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Inject } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { ViewChild } from '@angular/core';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
// --- ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
// --- NOTIFICATIONS
//import { NotificationsService } from 'angular2-notifications';
import { ToastrManager } from 'ng6-toastr-notifications';
// --- SERVICES
import { ModalService } from '@app/services/modal.service';
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { AdminServiciosPageComponent } from '@app/modules/center-admin/admin-servicios-page/admin-servicios-page.component';
// --- MODELS
import { Medico } from '@models/Medico.model';

@Component({
  selector: 'as-toolbar-panel',
  templateUrl: './toolbar-panel.component.html',
  styleUrls: ['./toolbar-panel.component.scss']
})
export class ToolbarPanelComponent extends AppPanelComponent {
  @Input() container: AdminServiciosPageComponent;
  @Input() editingService: boolean = false;
  public targetService: Medico = new Medico();
  private passwordValidacion: string; // Field where to store the form input filed value.
  public deactivating: boolean = false; // Used to signal the processing of the state change to the service.
  public validated: boolean = false; // TRUE means that the password has been validated succesfully.

  //--- C O N S T R U C T O R
  constructor(
    // @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    // @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    //protected toasterService: NotificationsService,
    //protected notifier: ToastrManager,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    private modalService: ModalService) {
    super(router, activeRoute, appStoreService, backendService);
  }

  // - V I E W   I N T E R A C T I O N
  public createNewService(): void {
    this.container.newService = true;
    this.container.editing = false;
  }
  public createNewMedicalAct(): void {
    // Open the new Medical Act panel
    this.container.showNewActoMedico = true;
  }
  // --- D R A G   &   D R O P
  public onDrop2DisableService(_event: any): void {
    console.log("><[AdminServiciosPageComponent.onDropService]");
    // Get the service to disable.
    this.targetService = _event.dragData;
    // this.activateModal = true;
    // Show the notification panel and get the authorization for verification of password and roles.
    this.modalService.open('deactivate-service');
  }

  // - M O D A L   I N T E R A C T I O N
  // public disableValidateCredential(): boolean {
  //   return this.validateNIF(this.passwordValidacion);
  // }
  public validateCredential(): void {
    // Go to the background to check for the password.
    this.backendService.validatePassword(this.passwordValidacion)
      .subscribe((validationResult) => {
        if (validationResult) this.validated = true;
      })
  }
  public disableService(): void {
    console.log(">>[AdminServiciosPageComponent.disableService]");
    console.log("--[AdminServiciosPageComponent.disableService]> Disabling service: " +
      this.targetService.nombre + ' ' + this.targetService.apellidos);
    this.deactivating = true;
    this.targetService.activo = false;
    // To update we should get access to the current Centro.
    let centro = this.appStoreService.accessCredential().getCentro();
    // Convert the frontend service to 
    this.backendService.backendUpdateConsultaMedico(centro, this.targetService)
      .subscribe((savedService) => {
        try {
          // Close the modal and fire a refresh.
          this.modalService.close('deactivate-service');
          // Update the service on the backend.
          // this.serviceList.refresh();
        } catch (Exception) {
          console.log("-- [EditServicePanelComponent.onSubmit]> Exception: " + Exception.message);
        }
      }), (error) => {
        console.log("-- [EditServicePanelComponent.onSubmit]> Error: " + error.message);
      };

    // Emit the event to refresh the service list adter this change.
    // TODO
    console.log("<<[AdminServiciosPageComponent.disableService]");
  }
  public cancelDisable(): void {
    this.modalService.close('deactivate-service');
  }
}
