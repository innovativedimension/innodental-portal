//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
import { BackendService } from '@app/services/backend.service';
import { StubService } from '@app/testing/services/StubService.service';
// --- COMPONENTS
import { CitacionesPageComponent } from '@app/modules/citaciones/citaciones-page/citaciones-page.component';
import { PatientData } from '@app/models/PatientData.model';
import { Pendings } from '@app/models/Pendings.model';
import { Medico } from '@models/Medico.model';
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';

// - S E R V I C E   S E L E C T   P A N E L - V 1
describe('PAGE CitacionesPageComponent [Module: CITACIONES]', () => {
  let component: CitacionesPageComponent;
  let fixture: ComponentFixture<CitacionesPageComponent>;
  let appStoreService: MockAppStoreService;
  // let openAppointmentController: MockOpenAppointmentControllerService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [CitacionesPageComponent, RouteMockUpComponent],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: BackendService, useClass: StubService }
      ]
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(CitacionesPageComponent);
    component = fixture.componentInstance;
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[citaciones/CitacionesPageComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      let componentAsAny = component as any;
      console.log('><[citaciones/CitacionesPageComponent]> "phase" should be -IDENTIFICATION-');
      expect(component.phase).toBe('-IDENTIFICATION-', '"phase" should be -IDENTIFICATION-');
      console.log('><[citaciones/CitacionesPageComponent]> "showCalendar" should be false');
      expect(component.showCalendar).toBeFalsy('"showCalendar" should be false');
      console.log('><[citaciones/CitacionesPageComponent]> "showAppointments" should be false');
      expect(component.showAppointments).toBeFalsy('"showAppointments" should be false');
      console.log('><[citaciones/CitacionesPageComponent]> "_patientData" should be undefined');
      expect(componentAsAny._patientData).toBeUndefined('"_patientData" should be undefined');
      console.log('><[citaciones/CitacionesPageComponent]> "_activeOrigin" should be undefined');
      expect(componentAsAny._activeOrigin).toBeUndefined('"_activeOrigin" should be undefined');
    });
  });

  // - R E N D E R I N G   P H A S E
  describe('Rendering Phase', () => {
    it('Rendering Phase: some panels should be visible', () => {
      fixture.detectChanges();
      console.log('><[citaciones/CitacionesPageComponent]> "phase" should be -IDENTIFICATION-');
      expect(component.phase).toBe('-IDENTIFICATION-', '"phase" should be -IDENTIFICATION-');
      console.log('><[citaciones/CitacionesPageComponent]> Rendering Phase: some panels should be visible');
      let testPanel = fixture.debugElement.query(By.css('#app-header'));
      console.log('><[citaciones/CitacionesPageComponent]> "app-header" should be visible');
      expect(testPanel).toBeDefined('"app-header" should be visible');
      testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
      console.log('><[citaciones/CitacionesPageComponent]> "ui-menu-bar" should be visible');
      expect(testPanel).toBeDefined('"ui-menu-bar" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-patient-panelv2'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-patient-panelv2" should be visible');
      expect(testPanel).toBeDefined('"c-patient-panelv2" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-patient-appointments-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-patient-appointments-panel" should not be visible');
      expect(testPanel).toBeNull('"c-patient-appointments-panel" should not be visible');
      testPanel = fixture.debugElement.query(By.css('#c-specialities-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-specialities-panel" should not be visible');
      expect(testPanel).toBeNull('"c-specialities-panel" should not be visible');
      testPanel = fixture.debugElement.query(By.css('#medical-acts-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "medical-acts-panel" should not be visible');
      expect(testPanel).toBeNull('"medical-acts-panel" should not be visible');
      testPanel = fixture.debugElement.query(By.css('#c-calendar-free-slots-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-calendar-free-slots-panel" should not be visible');
      expect(testPanel).toBeNull('"c-calendar-free-slots-panel" should not be visible');
      testPanel = fixture.debugElement.query(By.css('#c-appointment-list-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-appointment-list-panel" should not be visible');
      expect(testPanel).toBeNull('"c-appointment-list-panel" should not be visible');
    });
    it('Rendering Phase: more panels should be visible', () => {
      console.log('><[gestion-servicio/GestionCommonPageComponent]> Rendering Phase: more panels should be visible');
      // Activate all the panels
      component.phase = '-PATIENT-DATA-';
      fixture.detectChanges();
      
      expect(component.phase).toBe('-PATIENT-DATA-', '"phase" should be -PATIENT-DATA-');
      console.log('><[citaciones/CitacionesPageComponent]> Rendering Phase: some panels should be visible');
      let testPanel = fixture.debugElement.query(By.css('#app-header'));
      console.log('><[citaciones/CitacionesPageComponent]> "app-header" should be visible');
      expect(testPanel).toBeDefined('"app-header" should be visible');
      testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
      console.log('><[citaciones/CitacionesPageComponent]> "ui-menu-bar" should be visible');
      expect(testPanel).toBeDefined('"ui-menu-bar" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-patient-panelv2'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-patient-panelv2" should be visible');
      expect(testPanel).toBeDefined('"c-patient-panelv2" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-patient-appointments-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-patient-appointments-panel" should be visible');
      expect(testPanel).toBeDefined('"c-patient-appointments-panel" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-specialities-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-specialities-panel" should be visible');
      expect(testPanel).toBeDefined('"c-specialities-panel" should be visible');
      testPanel = fixture.debugElement.query(By.css('#medical-acts-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "medical-acts-panel" should be visible');
      expect(testPanel).toBeDefined('"medical-acts-panel" should be visible');
      // testPanel = fixture.debugElement.query(By.css('#c-calendar-free-slots-panel'));
      // console.log('><[citaciones/CitacionesPageComponent]> "c-calendar-free-slots-panel" should not be visible');
      // expect(testPanel).toBeUndefined('"c-calendar-free-slots-panel" should not be visible');
      // testPanel = fixture.debugElement.query(By.css('#c-appointment-list-panel'));
      // console.log('><[citaciones/CitacionesPageComponent]> "c-appointment-list-panel" should not be visible');
      // expect(testPanel).toBeUndefined('"c-appointment-list-panel" should not be visible');
    });
    it('Rendering Phase: all panels should be visible', () => {
      console.log('><[gestion-servicio/GestionCommonPageComponent]> Rendering Phase: all panels should be visible');
      // Activate all the panels
      component.phase = '-PATIENT-DATA-';
      expect(component.phase).toBe('-PATIENT-DATA-', '"phase" should be -PATIENT-DATA-');
      component.showCalendar = true;
      expect(component.showCalendar).toBeTruthy('"showCalendar" should be true');
      component.showAppointments = true;
      expect(component.showAppointments).toBeTruthy('"showAppointments" should be true');
      fixture.detectChanges();

      console.log('><[citaciones/CitacionesPageComponent]> Rendering Phase: some panels should be visible');
      let testPanel = fixture.debugElement.query(By.css('#app-header'));
      console.log('><[citaciones/CitacionesPageComponent]> "app-header" should be visible');
      expect(testPanel).toBeDefined('"app-header" should be visible');
      testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
      console.log('><[citaciones/CitacionesPageComponent]> "ui-menu-bar" should be visible');
      expect(testPanel).toBeDefined('"ui-menu-bar" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-patient-panelv2'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-patient-panelv2" should be visible');
      expect(testPanel).toBeDefined('"c-patient-panelv2" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-patient-appointments-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-patient-appointments-panel" should be visible');
      expect(testPanel).toBeDefined('"c-patient-appointments-panel" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-specialities-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-specialities-panel" should be visible');
      expect(testPanel).toBeDefined('"c-specialities-panel" should be visible');
      testPanel = fixture.debugElement.query(By.css('#medical-acts-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "medical-acts-panel" should be visible');
      expect(testPanel).toBeDefined('"medical-acts-panel" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-calendar-free-slots-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-calendar-free-slots-panel" should be visible');
      expect(testPanel).toBeDefined('"c-calendar-free-slots-panel" should be visible');
      testPanel = fixture.debugElement.query(By.css('#c-appointment-list-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-appointment-list-panel" should be visible');
      expect(testPanel).toBeDefined('"c-appointment-list-panel" should be visible');
    });
  });

  // - O U T P U T   E M I T T E R S
  describe('Output Emitters Phase [onPatientData]', () => {
    it('onPatientData: receive patient data', () => {
      let componentAsAny = component as any;
      console.log('><[citaciones/PatientPanelv2Component]> onPatientData: receive patient data');
      let patient = new PatientData();
      patient.identificador = 'Test Identifier';

      component.onPatientData(patient);
      console.log('><[citaciones/CitacionesPageComponent]> checking patient data received');
      expect(componentAsAny._patientData).toBeDefined('patient data should exist');
      expect(componentAsAny._patientData.identificador).toBe('Test Identifier', 'patient identifier shoudl match "Test Identifier"');
      console.log('><[citaciones/CitacionesPageComponent]> "phase" should be -PATIENT-DATA-');
      expect(component.phase).toBe('-PATIENT-DATA-', '"phase" should be -PATIENT-DATA-');
    });
  });
  describe('Output Emitters Phase [onActivateSelected]', () => {
    it('onActivateSelected: activate a pending element', () => {
      let componentAsAny = component as any;
      console.log('><[citaciones/PatientPanelv2Component]> onActivateSelected: activate a pending element');
      let selected = new Pendings();
      let medico = new Medico();
      medico.setNombre('Servicio de prueba');
      medico.setEspecialidad('Especialidad de prueba');
      selected.setTarget(medico);

      component.onActivateSelected(selected);
      console.log('><[citaciones/CitacionesPageComponent]> checking pending structure received');
      expect(componentAsAny._activeOrigin).toBeDefined('pending structure should exist');
      let node = componentAsAny._activeOrigin.getNode() as IAppointmentEnabled;
      expect(node.getEspecialidad()).toBe('Especialidad de prueba', 'target speciality should match "Especialidad de prueba"');
    });
  });

  // - I N P U T   A C C E S S O R S   P H A S E
  describe('Input Accessors Phase [transferPatientData]', () => {
    it('transferPatientData: let child panels get patient data', () => {
      let componentAsAny = component as any;
      console.log('><[citaciones/CitacionesPageComponent]> transferPatientData: let child panels get patient data');
      let patient = new PatientData();
      patient.nombre = 'Nombre de prueba';
      componentAsAny._patientData = patient;
      expect(component.transferPatientData()).toBeDefined('patient data should be defined');
      expect(component.transferPatientData().nombre).toBe('Nombre de prueba', 'patient nombre should match "Nombre de prueba"');
    });
  });
  describe('Input Accessors Phase [transferSelectedOrigin]', () => {
    it('transferSelectedOrigin: let child panels access the appointments source', () => {
      let componentAsAny = component as any;
      console.log('><[citaciones/CitacionesPageComponent]> transferSelectedOrigin: let child panels access the appointments source');
      let source = new Pendings();
      let medico = new Medico();
      medico.setNombre('Servicio de prueba');
      medico.setEspecialidad('Especialidad de prueba');
      source.setTarget(medico);
      component.onActivateSelected(source);
      expect(component.transferSelectedOrigin()).toBeDefined('source data should be defined');
      let node = componentAsAny.transferSelectedOrigin().getNode() as IAppointmentEnabled;
      expect(node.getEspecialidad()).toBe('Especialidad de prueba', 'sources speciality should match "Especialidad de prueba"');
    });
  });

  // - I N T E R C O M M U N I C A T I O N   P H A S E
  describe('Intercommunication Phase [communicateClearPage]', () => {
    it('communicateClearPage: clear the page state', () => {
      console.log('><[citaciones/CitacionesPageComponent]> communicateClearPage: clear the page state');
      component.phase = '-TEST-PHASE-';
      component.showCalendar = true;
      component.showAppointments = true;

      component.communicateClearPage();
      expect(component.phase).toBe('-IDENTIFICATION-', 'phase should match "-IDENTIFICATION-"');
      expect(component.showCalendar).toBeFalsy('showCalendar should be false');
      expect(component.showAppointments).toBeFalsy('showCalendar should be false');
    });
  });
  describe('Intercommunication Phase [activateCalendar]', () => {
    it('activateCalendar: activate the calendar panel', () => {
      console.log('><[citaciones/CitacionesPageComponent]> activateCalendar: activate the calendar panel');
      component.activateCalendar();
      expect(component.showCalendar).toBeTruthy('showCalendar should be true');
      let testPanel = fixture.debugElement.query(By.css('#c-calendar-free-slots-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-calendar-free-slots-panel" should be visible');
      expect(testPanel).toBeDefined('"c-calendar-free-slots-panel" should be visible');
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [getItemsPendingAppointment]', () => {
    it('getItemsPendingAppointment: transfer reserved appointments to appointment list', () => {
      console.log('><[citaciones/PatientPanelv2Component]> getItemsPendingAppointment: transfer reserved appointments to appointment list');
      // Setup the scenario that should exist when receiving this message.
      component.phase = '-PATIENT-DATA-';
      fixture.detectChanges();
      let testPanel = fixture.debugElement.query(By.css('#c-patient-appointments-panel'));
      console.log('><[citaciones/CitacionesPageComponent]> "c-patient-appointments-panel" should be visible');
      expect(testPanel).toBeDefined('"c-patient-appointments-panel" should be visible');
      const spy = spyOn(component, 'getItemsPendingAppointment');
      component.getItemsPendingAppointment();
      expect(spy).toHaveBeenCalled();
    });
  });
});
