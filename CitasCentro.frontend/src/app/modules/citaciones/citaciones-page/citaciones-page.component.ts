//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
// --- COMPONENTS
import { CalendarFreeSlotsPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/calendar-free-slots-panel/calendar-free-slots-panel.component';
import { AppointmentListPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/appointment-list-panel/appointment-list-panel.component';
import { PatientAppointmentsPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/patient-appointments-panel/patient-appointments-panel.component';
// --- INTERFACES
import { IDragNDropCoordinator } from '@app/interfaces/IDragNDropCoordinator.interface';
// --- MODELS
import { Pendings } from '@app/models/Pendings.model';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { PatientData } from '@app/models/PatientData.model';

/**
 * This page should controls the different presentation phases. The first phase starts at <b>-IDENTIFICATION-</b> and only shows the Patient panel with the unique identifier entry field.
 * -IDENTIFICATION-: patient-panel but showing only the unique identifier entry field active.
 * -PATIENT-DATA-REQUEST-: There are no othr appointments for this patient so we have not any data recorded about it. We need to request it and store on the form fields while we assing the new appointments.
 * -PATIENT-DATA-AVAILABLE-: There are other appointments that can be used to fill the patient data fields. Set them to not editable until a click break the lock.
 *
 * @export
 * @class CitacionesPageComponent
 * @extends {AppPanelComponent}
 * @implements {OnInit}
 * @implements {ICalendarPageContainer}
 * @implements {IDragNDropCoordinator}
 */
@Component({
  selector: 'citaciones-page',
  templateUrl: './citaciones-page.component.html',
  styleUrls: ['./citaciones-page.component.scss']
})
export class CitacionesPageComponent extends AppPanelComponent implements IDragNDropCoordinator {
  @ViewChild(PatientAppointmentsPanelComponent) private patientPanel: PatientAppointmentsPanelComponent;
  // @ViewChild(CalendarFreeSlotsPanelComponent) private calendar: CalendarFreeSlotsPanelComponent;
  @ViewChild(AppointmentListPanelComponent) private appointmentList: AppointmentListPanelComponent;

  public phase: string = '-IDENTIFICATION-'; // Stores the UI phase to show the right set of panels.
  public showCalendar: boolean = false;
  public showAppointments: boolean = false;
  private _patientData: PatientData;
  private _activeOrigin: Pendings;

  // - I N P U T   A C C E S S O R S
  public transferPatientData(): PatientData {
    return this._patientData;
  }
  public transferSelectedOrigin(): Pendings {
    return this._activeOrigin;
  }

  // - O U T P U T   E M I T T E R S
  public onPatientData(_data: PatientData): void {
    this._patientData = _data;
    this.phase = '-PATIENT-DATA-';
  }
  public onActivateSelected(_event: Pendings): void {
    this._activeOrigin = _event;
  }

  // - I N T E R C O M M U N I C A T I O N   S E C T I O N
  /**
    * Called from panels to clear the state and set the right set of panels to be shown on each phase.
    *
    * @memberof CitacionesPageComponent
    */
  public communicateClearPage(): void {
    this.phase = '-IDENTIFICATION-';
    this.showCalendar = false;
    this.showAppointments = false;
  }
  public activateCalendar(): void {
    this.showCalendar = true;
  }
  public openAppointmentList(_selectedDate: Date/*, _target: IAwaiting*/): void {
    this.showAppointments = true;
    this.appointmentList.setSelectedDate(_selectedDate);
  }

    //--- I D R A G N D R O P C O O R D I N A T O R   I N T E R F A C E
  /**
   * Transport the items pending appointment data from the Patient Panel (consumer) to the Appointment List Panel that is the provider for the drag and drop data.
   * @return [description]
   */
  public getItemsPendingAppointment(): Pendings[] {
    return this.patientPanel.itemsPendingAppointment;
  }
}
