//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
//--- CALENDAR
import { startOfDay } from 'date-fns';
import { isBefore } from 'date-fns';
//--- INTERFACES
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';
import { IDragNDropCoordinator } from '@app/interfaces/IDragNDropCoordinator.interface';
//--- MODELS
import { Cita } from '@models/Cita.model';
//--- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { Filter } from '@app/models/Filter.model';
import { AppointmentGroup } from '@app/models/AppointmentGroup.model';
import { Pendings } from '@app/models/Pendings.model';
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';

@Component({
  selector: 'c-appointment-list-panel',
  templateUrl: './appointment-list-panel.component.html',
  styleUrls: ['./appointment-list-panel.component.scss']
})
export class AppointmentListPanelComponent extends AppPanelComponent implements OnInit, OnDestroy {
  @Input() source: Pendings;

  private _selectedDate: Date = new Date(); // The selected date used to filter the appointments to be shown.
  private _appointmentsSubscription: ISubscription;
  private _appointments: Cita[] = []; // Local copy of the list of selected appointments. Loaded on subject.
  public stateColor: string = "";
  private _filters: any = [];

  //--- L I F E C Y C L E
  ngOnInit() {
    console.log(">>[AppointmentListComponent.ngOnInit]");
    // Access the last version of the appointments.
    this._appointmentsSubscription = this.appStoreService.accessAppointments()
      .subscribe(list => this._appointments = list);
    // Define the processing filters.
    this._filters.push(new Filter()
      .setName('only today')
      .setDescription('Only show appointments for target date')
      .setFilter((_target: Cita): boolean => {
        let testDate = startOfDay(_target.getFecha());
        let year = testDate.getFullYear();
        let month = testDate.getMonth();
        let day = testDate.getDate();
        if (year == startOfDay(this._selectedDate).getFullYear())
          if (month == startOfDay(this._selectedDate).getMonth())
            if (day == startOfDay(this._selectedDate).getDate())
              return false;
        return true;
      }));
    this._filters.push(new Filter()
      .setName('appointment > now')
      .setDescription('Filter out appointment in the past')
      .setFilter((_target: Cita): boolean => {
        return isBefore(_target.getFecha(), new Date());
      }));
    this._filters.push(new Filter()
      .setName('appointment <> LIBRE')
      .setDescription('Filter out appointments not free')
      .setFilter((_target: Cita): boolean => {
        if (_target.estado == 'LIBRE') return false;
        else if (_target.estado == 'BLOQUEADA') return false;
        else return true;
      }));
    console.log("<<[AppointmentListComponent.ngOnInit]");
  }
  ngOnDestroy() {
    if (null != this._appointmentsSubscription) this._appointmentsSubscription.unsubscribe();
  }

  //--- D R A G   S E C T I O N
  /**
   * This implements the complex logic to detect when an item is allowed to be dragged.
   * When the first appointment is moved then all of the items that are on the <b>LIBRE</b> state are candidates to move. But when there is already data on the destination we should filter out all appointmemts that have a distance greater than ZERO from any of the already moved appointments.
   * This is a recurrent process that euires access to the list of appointments already moved to the receiver.
   * @param  _source the appointment that is being tried to move.
   * @return         true if the item is allowed to be dragged.
   */
  public dragEnabledValidation(_source: Cita): boolean {
    // First check if the appointment is dragable because it is free.
    if (null != _source) {
      // Validate we enter with a Cita and not another class.
      if (_source.jsonClass == 'Cita') {
        if (_source.estado != 'LIBRE') return false;
        // Get the destination data to check if the filter and the algorith should be activated.
        // To call new methos on the container convert it to interface.
        let containerCoordinator = this.container as IDragNDropCoordinator;
        let targets = containerCoordinator.getItemsPendingAppointment();
        // Search for a target that matches the current source.
        for (let target of targets) {
          if (target.getServiceIdentifiers() === this.source.getEspecialidad()) {
            // Check if the target already has appointments.
            if (target.getAppointmentsCount() > 0) {
              // Only allow appointments that are distance ZERO from the destination targets.
              for (let appoTarget of target.appointments) {
                // But check for this same appointment to be discarded.
                if (appoTarget.id === _source.id) return false;
                let distance = appoTarget.distance(_source);
                if (distance < 5) return true;
              }
              return false;
            } else return true;
          }
        }
      }
    }
    return false;
  }
  public getScope(_cita: Cita): string {
    if (_cita.jsonClass == 'Cita') {
      if (this.isLibre(_cita)) return this.source.getEspecialidad();
      else return '-NOT-ALLOWED-';
    }
  }
  public getPatientData(cita: Cita): string {
    if (cita.jsonClass == 'Cita') {
      if (cita.estado.toUpperCase() === "LIBRE".toUpperCase()) return "-LIBRE-";
      if (cita.estado.toUpperCase() === "BLOQUEADA".toUpperCase()) return "-EN PROCESO-";
      if (cita.estado.toUpperCase() === "RESERVADA".toUpperCase()) return cita.getPatientData();
      return "-LIBRE-";
    }
  }
  public isLibre(cita: Cita): boolean {
    if (cita.jsonClass == 'Cita') {
      if (cita.estado.toUpperCase() === "LIBRE".toUpperCase()) return true;
      return false;
    }
  }
  public isReservada(cita: Cita): boolean {
    if (cita.jsonClass == 'Cita') {
      if (cita.estado.toUpperCase() === "RESERVADA".toUpperCase()) return true;
      return false;
    }
  }
  public isBloqueada(cita: Cita): boolean {
    if (cita.jsonClass == 'Cita') {
      if (cita.estado.toUpperCase() === "BLOQUEADA".toUpperCase()) return true;
      return false;
    }
  }

  //--- I N T E R C O M M U N I C A T I O N   S E C T I O N
  /**
   * Once the users selects a date on the calendar we get that information. At this point we filter the updated list of appointments stored at _appointments and apply the filters.
   * During the processing we should remove BLOQUEADAS and RESERVADAS appointments.
   * Also appointments that do not belong to the selected date and that belonging to that date are on the past of the current point in time.
   * During the appointment processing we can aggregate to the MAÑANA or TARDE sets so we can show that information differentiated for easy selection. Also the groups can be collapsed for easy selections.
   *
   * @param {Date} _newdate the user selected data on the calendar.
   * @memberof AppointmentListPanelComponent
   */
  public setSelectedDate(_newdate: Date): void {
    console.log(">>[AppointmentListComponent.setSelectedDate]");
    this._selectedDate = _newdate;
    this.dataModelRoot = [];
    let morning = new AppointmentGroup().setTitle("MAÑANA");
    morning.expand();
    let evening = new AppointmentGroup().setTitle("TARDE");
    evening.expand();
    this.dataModelRoot.push(morning);
    this.dataModelRoot.push(evening);

    // Process the list of current appointments (for the whole service) and keep future items.
    for (let cita of this._appointments) {
      let filtered = this.applyFilters(cita);
      if (filtered) continue;
      // Filter out appointments that do not match the tipe if the tipe is active.
      if (this.source.node.getJsonClass() === 'ActoMedicoCentro') {
        let actoMedico : ActoMedicoCentro= this.source.node as ActoMedicoCentro;
        if (actoMedico.tipo != 'TRANSPARENTE') {
          // Apply the tipo filter.
          if (cita.tipo != actoMedico.tipo) continue;
        }
      }
      if (cita.zonaHoraria == "MAÑANA") {
        morning.addAppointment(cita);
        continue;
      }
      if (cita.zonaHoraria == "TARDE") {
        evening.addAppointment(cita);
        continue;
      }
      // Just in case it is not grouped.
      this.dataModelRoot.push(cita);
    }
    this.notifyDataChanged();
    this.leftContent = '[' + (this.renderNodeList.length - 2) + ']';
    console.log("<<[AppointmentListComponent.setSelectedDate]");
  }

  //--- T A R G E T   D A T A   A C C E S S O R S
  public getTarget(): any {
    if (null != this.source) return this.source;
  }
  public getTargetClass(): string {
    if (null != this.source) return this.source.getJsonClass();
    else return '-UNDEFINED-';
  }

  //--- F I L T E R I N G
  /**
   * This will apply all the filters defined in sequence. If any of the fiters drops the appoitment then we do not continue and we terminate the process with a true.
   * If we reach the end of the filters list and the appointment still is present then we return a false.
   * @param  _target the appointment to be checked.
   * @return         <b>true</b> if the appointment has failed the filters and should be discarded. <b>false</b> if the appointmet has to be kept.
   */
  public applyFilters(_target: Cita): boolean {
    for (let filter of this._filters) {
      let filterResult = filter.filterFunction(_target);
      if (filterResult) return true;
    }
    return false;
  }
  public toggleExpanded(_node: Cita): void {
    _node.toggleExpanded();
    this.notifyDataChanged();
  }
  //--- N O D E   I N T E R F A C E
  /**
   * This instance has a particular processing for the variant. If the tardet is a simple target then we show the stabdard view but if the target is a multiple service target we should render a view that has more information to differentiate the service being moved.
   * It also should apply to later selection of more appointments.
   *
   * @returns {string}
   * @memberof AppointmentListPanelComponent
   */
  // public getVariant(): string {
  //   if (this.getTarget().getNodeClass() == 'Especialidad') return '-EXTENDED-APPOINTMENT-';
  //   else return super.getVariant();
  // }
}
