//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
// --- CALENDAR
import { isBefore } from 'date-fns';
// --- INTERFACES
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';
// import { IViewer } from '@models/Cita.model';
import { ICalendarPageContainer } from '@app/interfaces/ICalendarPageContainer.interface';
// --- COMPONENTS
import { CalendarCoreComponent } from '@app/modules/ui/calendar-core/calendar-core.component';
// --- MODELS
import { Payload } from '@models/Payload.model';
import { Cita } from '@models/Cita.model';

@Component({
  selector: 'c-calendar-free-slots-panel',
  templateUrl: './calendar-free-slots-panel.component.html',
  styleUrls: ['./calendar-free-slots-panel.component.scss']
})
export class CalendarFreeSlotsPanelComponent extends CalendarCoreComponent implements OnInit {
  @Input() container: ICalendarPageContainer;
  @Input() source: IAppointmentEnabled;
  @Input() variant: string = "-CITACIONES-PAGE-";
  private _appointmentsSubscription: ISubscription;
  public serviceIdentifier: string = "";
  private filters: any = [];

  //--- L I F E C Y C L E
  ngOnInit() {
    // Define the filters.
    this.filters.push({
      name: 'appointment < now',
      description: 'Filter appointment in the past',
      filter: (_target: Cita): boolean => {
        return isBefore(_target.getFecha(), new Date());
      }
    });
    this.downloading = true;
    // Connect the subscriptions to the subjects.
    // this._downloadingSubscription = this.dynamicCacheService.accessDownloading().subscribe(flag => this.downloading = flag);

    // Get the current list of appointments from the subject. Order by date/time and then filter.
    this._appointmentsSubscription = this.appStoreService.accessAppointments()
      .subscribe((appointmentList) => {
        // Skip this step if the numbe of appointments is ZERO. This an empty start.
        if (appointmentList.length > 0) {
          // Sort the list.
          let sortedAppointments = appointmentList.sort((n1: Cita, n2: Cita) => {
            if (isBefore(n1.getFecha(), n2.getFecha())) return -1;
            else return 1;
          });
          this.eventList = new Map<string, Payload>();
          let citaList = [];
          for (let cita of sortedAppointments) {
            // Apply the list of filters to drop appointments that should not be accesible.
            let filtered = this.applyFilters(cita);
            if (filtered) continue;
            this.citaList.push(cita);
            // Do the accounting to generate the events.
            this.accountAppointment(cita);
          }
          this.processEvents();
          this.downloading = false;
          this.appStoreService.infoNotification("Descarga de citas disponibles completado.", "Completado");
        }
      });
  }
  ngOnDestroy() {
    // this._downloadingSubscription.unsubscribe();
    if (null != this._appointmentsSubscription) this._appointmentsSubscription.unsubscribe();
  }

  //--- C A L E N D A R   E V E N T   M A N A G E M E N T
  public closeCalendar(): void {
    this.show = false;
    this.container.showCalendar = false;
  }

  //--- I N T E R C O M M U N I C A T I O N   S E C T I O N
  /**
   * This method processes the click on a calendar date cell. We first have to check if the cell has appointments before doing anything else like open another panel. Tjis way we avoid opening empty panels.
   *
   * @param _event the angular event that is received by the calendar cell.
   */
  public dayClicked(_event: any): void {
    console.log("><[AppointmentDateSelectorComponent.dayClicked]> event: " + JSON.stringify(_event));
    // If date has free appointments then open the appointment list panel.
    if (_event.day.badgeTotal > 0) {
      this.clearSelection();
      this.add2Selection(_event.day);
      this.container.openAppointmentList(_event.day.date/*, this.pendingSelected.node*/);
    }
  }

  //--- T A R G E T   D A T A   A C C E S S O R S
  public getTarget(): any {
    if (null != this.source) return this.source;
  }
  public getTargetClass(): string {
    if (null != this.source) return this.source.getJsonClass();
    else return '-UNDEFINED-';
  }

  //--- F I L T E R I N G
  /**
   * This will apply all the filters defined in sequence. If any of the fiters drops the appoitment then we do not continue and we terminate the process with a true.
   * If we reach the end of the filters list and the appointment still is present then we return a false.
   * @param  _target the appointment to be checked.
   * @return         <b>true</b> if the appointment has failed the filters and should be discarded. <b>false</b> if the appointmet has to be kept.
   */
  public applyFilters(_target: Cita): boolean {
    for (let filter of this.filters) {
      let filterResult = filter.filter(_target);
      if (filterResult) return true;
    }
    return false;
  }
}
