// - CORE
import { throwError } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// - TESTING
import { TestBed } from '@angular/core/testing';
import { inject } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
import { IsolationService } from '@app/platform/isolation.service';
import { StubService } from '@app/testing/services/StubService.service';
// - COMPONENTS
import { MedicalActsPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/medical-acts-panel/medical-acts-panel.component';

// - S E R V I C E   S E L E C T   P A N E L - V 1
describe('PANEL MedicalActsPanelComponent [Module: LOGIN]', () => {
  let component: MedicalActsPanelComponent;
  let fixture: ComponentFixture<MedicalActsPanelComponent>;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [MedicalActsPanelComponent, RouteMockUpComponent],
      providers: [
        // { provide: IsolationService, useClass: StubService },
        { provide: AppStoreService, useClass: MockAppStoreService }
      ]
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(MedicalActsPanelComponent);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[citaciones/MedicalActsPanelComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[citaciones/MedicalActsPanelComponent]> "_medicalActsSubscription" should be undefined');
      let componentAsAny = component as any;
      expect(componentAsAny._medicalActsSubscription).toBeUndefined();
    });
  });

  // - L I F E C Y C L E   P H A S E
  describe('Lifecycle Phase', () => {
    it('Lifecycle: OnInit -> get the list of medical acts', async(() => {
      console.log('><[citaciones/MedicalActsPanelComponent]> Lifecycle: OnInit -> get the list of medical acts');
      let componentAsAny = component as any;
      // Check that initial state is the expected.
      expect(component.downloading).toBeFalsy('while not processing the "downloading" is false');
      expect(component.dataModelRoot.length).toBe(0, 'model list should be empty');
      expect(component.renderNodeList.length).toBe(0, 'render list should be empty');
      component.ngOnInit();
      expect(component.downloading).toBeFalsy('after processing the "downloading" is false');
      expect(component.dataModelRoot.length).toBe(8, 'medical act list should match the number of 8');
      expect(component.renderNodeList.length).toBe(8, 'render list should be have 8 elements also');
      console.log('><[citaciones/MedicalActsPanelComponent]> the subscription should be defined');
      expect(componentAsAny._medicalActsSubscription).toBeDefined();
    }));
    it('Lifecycle: OnDestroy -> unsubscribe from the list', async(() => {
      console.log('><[citaciones/MedicalActsPanelComponent]> Lifecycle: OnDestroy -> unsubscribe from the list');
      let componentAsAny = component as any;
      // Check that initial state is the expected.
      expect(component.downloading).toBeFalsy('while not processing the "downloading" is false');
      expect(component.dataModelRoot.length).toBe(0, 'model list should be empty');
      expect(component.renderNodeList.length).toBe(0, 'render list should be empty');
      component.ngOnInit();
      expect(component.downloading).toBeFalsy('after processing the "downloading" is false');
      expect(component.dataModelRoot.length).toBe(8, 'medical act list should match the number of 8');
      expect(component.renderNodeList.length).toBe(8, 'render list should be have 8 elements also');
      console.log('><[citaciones/MedicalActsPanelComponent]> the subscription should be defined');
      expect(componentAsAny._medicalActsSubscription).toBeDefined();
      component.ngOnDestroy();
      expect(componentAsAny._medicalActsSubscription).toBeDefined();
    }));
  });
});
