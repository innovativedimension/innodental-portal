// - CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
// - COMPONENTS
import { MVCViewerComponent } from '@app/modules/ui/mvcviewer/mvcviewer.component';
import { AppPanelV2Component } from '@app/modules/ui/app-panelv2/app-panelv2.component';

@Component({
  selector: 'medical-acts-panel',
  templateUrl: './medical-acts-panel.component.html',
  styleUrls: ['./medical-acts-panel.component.scss']
})
export class MedicalActsPanelComponent extends AppPanelV2Component implements OnInit, OnDestroy {
  private _medicalActsSubscription: ISubscription;

  // - C O N S T R U C T O R
  constructor(protected appStoreService: AppStoreService) {
    super();
  }

  // - L I F E C Y C L E   F L O W
  /**
   * Read the list of medical acts from a property file. The backend implementation is now kept back until it is clear that the medical act editor is a must on the client user interface. The code to read center mediacl acts is maintained but disconnected from use and intecepted with an alert.
   *
   * @memberof MedicalActsPanelComponent
   */
  ngOnInit() {
    this.downloading = true;
    // Read the list of medical acts for the current center.
    this._medicalActsSubscription = this.appStoreService.propertiesActosMedicos()
      .subscribe((medicalActs) => {
        this.dataModelRoot = [];
        for (let act of medicalActs) {
          this.dataModelRoot.push(act);
        }
        this.notifyDataChanged();
        this.downloading = false;
      });
  }
  ngOnDestroy() {
    if (null != this._medicalActsSubscription) this._medicalActsSubscription.unsubscribe();
  }
}
