//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
//--- INTERFACES
import { IStore } from '@app/interfaces/IStore.interface';
//--- MODELS
import { Cita } from '@models/Cita.model';
import { PatientData } from '@app/models/PatientData.model';
import { Pendings } from '@app/models/Pendings.model';
import { OpenAppointment } from '@app/models/OpenAppointment.model';
// import { CitacionesPageComponent } from '@app/modules/citaciones/citaciones-page/citaciones-page.component';

@Component({
  selector: 'c-patient-appointments-panel',
  templateUrl: './patient-appointments-panel.component.html',
  styleUrls: ['./patient-appointments-panel.component.scss']
})
export class PatientAppointmentsPanelComponent extends AppPanelComponent implements OnInit {
  @Input() container: any;
  @Input() patientData: PatientData;
  @Output() selectedOrigin = new EventEmitter<Pendings>();
  // public selected: Pendings; // The appointment source selected for appointments search.

  // @Input() phase: string; // The page phase that is active now. Depending on this we should show only part of the contents.
  // @Output() nifvalido = new EventEmitter<number>(); // Outputs the nif validation state.

  // public nifIsValid: boolean = false; // Stores the nif validation value.
  // private _nifValidationSubscription: ISubscription; // Subscription to the nif validation.

  // public identificadorActivated: boolean = false; // Used to toggle the editability of the unique identifier field.
  // public onActivateIdentificador(): void {
  //   this.identificadorActivated = !this.identificadorActivated;
  // }
  // public patientData: PatientData = new PatientData();
  public openAppointments: OpenAppointment[] = [];

  // public aseguradoras: string[] = [];
  public activeAppointments: OpenAppointment[] = [];
  public itemsPendingAppointment: Pendings[] = [];
  public showContents: boolean = true; //Used to toggle the show/hide for the patient appointments.
  // public panelValid: boolean = false; // Stored the general validity of the panel.
  // public panelAlerts: any = {}; // Stores the warnings to complete the panel and activate the assignment button


  //--- L I F E C Y C L E   F L O W
  ngOnInit() {
    console.log(">>[PatientAppointmentsPanelComponent.ngOnInit]");
    this.canBeExpanded = true;
    // Extract the open appointments.
    if (null != this.patientData) this.openAppointments = this.patientData.openAppointments;
    console.log("<<[PatientPanelComponent.ngOnInit]");
  }
  // ngOnDestroy() {
  //   if (null != this._nifValidationSubscription) this._nifValidationSubscription.unsubscribe();
  // }
  // public getStore(): IStore {
  //   return this;
  // }

  //--- A L E R T   M A N A G E M E N T   S E C T I O N
  // public addAlert(_id: string, _message: string) {
  //   this.panelAlerts[_id] = _message;
  // }
  // public getPanelAlerts(): string[] {
  //   let messages = [];
  //   for (let key in this.panelAlerts) {
  //     messages.push(this.panelAlerts[key]);
  //   }
  //   return messages;
  // }

  // - F O R M   I N T E R A C T I O N
  public toggleShowContents(): void {
    this.showContents = !this.showContents;
  }
  public onSaveAppointments() {
    console.log(">>[PatientPanelComponent.saveAppointments]");
    // Process each element and change the appointment state.
    for (let pend of this.itemsPendingAppointment) {
      // Process each appointment.
      for (let appointment of pend.appointments) {
        this.reserveAppointment(appointment);
        // Mark the original appointment as BLOQUEADA
        appointment.estado = "RESERVADA";
        let reserveMessage = 'La cita en ' + pend.getEspecialidad() + ' para el ' +
          appointment.getFechaString() + ' a las ' + appointment.getHour() + ':' +
          appointment.getMinutes() + ' se ha reservado correctamente.'
        this.appStoreService.successNotification(reserveMessage, "Reservada");
      }
    }
    console.log("<<[PatientPanelComponent.saveAppointments]");
  }

  //--- B A C K E N D   A C C E S S
  public reserveAppointment(_cita: Cita) {
    // let patientRecord = new PatientCredential(JSON.parse(data));
    this.backendService.backendReserveAppointment(this.patientData, _cita)
      .subscribe((blockedCita) => {
        // console.log('--[PendingComponent.blockAppointment]> blockedCita: ' + blockedCita[0].estado);
      });
  }

  /**
   * This is the method called when the user drops an speciality or a mdical act. This shoudl add it to the list of items pending appointment.
   * @param  _event [description]
   */
  public onDropNew(_event: any): void {
    console.log(">>[PatientPanelComponent.onDropNew]");
    // Add the item to the end of the list.
    if (null != _event.dragData) {
      this.itemsPendingAppointment.push(new Pendings().setTarget(_event.dragData));
      // this.addAlert(_event.dragData.nombre, 'El elemento añadido necesita asignar una cita.')
    }
    console.log("<<[PatientPanelComponent.onDropNew]");
  }
  public removePending(_target: Pendings): void {
    let results = [];
    for (let pending of this.itemsPendingAppointment) {
      if (pending.node.getServiceIdentifiers() === _target.node.getServiceIdentifiers()) continue;
      else results.push(pending);
    }
    this.itemsPendingAppointment = results;
  }

  //--- I N T E R C O M M U N I C A T I O N   S E C T I O N
  /**
   * This method is called when the user clicks on the IAppointmentEnabled item. At this moment we should access the appointments for the item selected (getting the list of identifiers) and then storing the result on the awaiting subjects so the displays will update accordingly. The problem with this approach is that the visual reinforcement at the calendar is lost because it is now a passive component.
   * Or we can acchieve the same in a general way using another subject for the <b>downloading</b> process.
   * @param _target the item selected by the user.
   */
  public searchAppointments(_target: Pendings): void {
    console.log(">>[PatientPanelComponent.searchAppointments]> Class: " + _target.getNodeClass());
    this.selectedOrigin.emit(_target);
    let serviceIdentifiers: string[] = _target.node.getServiceIdentifiers();
    // Set the calendar tab active.
    this.container.activateCalendar();
    // Activate the downloading flag.
    // Signal the start of the download process.
    // this.appStoreService.setDownloading(true);
    let appointments = [];
    for (let identifier of serviceIdentifiers) {
      // Download the data about the appointments already reserved.
      this.backendService.backendCitas4MedicoById(identifier)
        .subscribe((citas) => {
          console.log("--[PatientPanelComponent.backendCitas4MedicoById]> count: " + citas.length);
          // Aggregate the data from multiples identifiers.
          appointments = appointments.concat(citas);
          // Store the processed appointements on the AppStore for easy access by other components.
          console.log("--[PatientPanelComponent.searchAppointments]> Storing: " + appointments.length + " appointments.");
          this.appStoreService.storeAppointments(appointments);
        }, (error) => {
          console.log("--[PatientPanelComponent.searchAppointments]> Error: " + error.message);
          // Process any 401 exception that means the session is no longer valid.
          if (error.status == 401) {
            this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
            this.router.navigate(['login']);
          }
        });
    }
  }
  // public getAwaitingSelected(): Pendings {
  //   return this.selected;
  // }
}
