//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { FormsModule } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { inject } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
import { IsolationService } from '@app/platform/isolation.service';
import { MockAngularIsolationService } from '@app/testing/services/MockAngularIsolation.service';
import { OpenAppointmentControllerService } from '@app/controllers/open-appointment-controller.service';
import { MockOpenAppointmentControllerService } from '@app/testing/services/MockOpenAppointmentController.service';
// --- COMPONENTS
import { PatientPanelv2Component } from '@app/modules/citaciones/citaciones-page/panels/patient-panel/patient-panel.component';

// - S E R V I C E   S E L E C T   P A N E L - V 1
describe('PANEL PatientPanelv2Component [Module: CITACIONES]', () => {
  let component: PatientPanelv2Component;
  let fixture: ComponentFixture<PatientPanelv2Component>;
  let appStoreService: MockAppStoreService;
  let openAppointmentController: MockOpenAppointmentControllerService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        FormsModule,
        HttpClientModule,
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [PatientPanelv2Component, RouteMockUpComponent],
      providers: [
        { provide: IsolationService, useClass: MockAngularIsolationService },
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: MockAppStoreService, useClass: MockAppStoreService },
        { provide: OpenAppointmentControllerService, useClass: MockOpenAppointmentControllerService }
      ]
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(PatientPanelv2Component);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
    openAppointmentController = TestBed.get(OpenAppointmentControllerService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[citaciones/PatientPanelv2Component]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[citaciones/PatientPanelv2Component]> "nifvalido" should exist');
      expect(component.nifvalido).toBeDefined('"nifvalido" should exist');
      console.log('><[citaciones/PatientPanelv2Component]> "patientDataOutput" should exist');
      expect(component.patientDataOutput).toBeDefined('"patientDataOutput" should exist');
      console.log('><[citaciones/PatientPanelv2Component]> "aseguradoras" should exist and be empty');
      expect(component.aseguradoras).toBeDefined('"aseguradoras" should exist and be empty');
      console.log('><[citaciones/PatientPanelv2Component]> "patientData" should exist');
      expect(component.patientData).toBeDefined('"patientData" should exist');
      console.log('><[citaciones/PatientPanelv2Component]> "localphase" should be "-IDENTIFICATION-"');
      expect(component.localphase).toBe('-IDENTIFICATION-');
      console.log('><[citaciones/PatientPanelv2Component]> "nifIsValid" should be false');
      expect(component.nifIsValid).toBeFalsy('should be false on initilization');
      console.log('><[citaciones/PatientPanelv2Component]> "identifier" should be undefined');
      expect(component.identifier).toBeUndefined('"identifier" should be undefined');
      console.log('><[citaciones/PatientPanelv2Component]> "identificadorIsEditable" should be false');
      expect(component.identificadorIsEditable).toBeFalsy('should be false on initilization');
    });
  });

  // - L I F E C Y C L E   P H A S E
  describe('Lifecycle Phase', () => {
    it('Lifecycle: OnInit -> get the list of insurance corporations', async(() => {
      console.log('><[citaciones/PatientPanelv2Component]> Lifecycle: OnInit -> get the list of insurance corporations');
      console.log('><[citaciones/PatientPanelv2Component]> "aseguradoras" should exist and be empty');
      expect(component.aseguradoras).toBeDefined('"aseguradoras" should exist and be empty');
      expect(component.aseguradoras.length).toBe(0, '"aseguradoras" should be empty');
      component.ngOnInit();
      console.log('><[citaciones/PatientPanelv2Component]> the number of insurances should be 17');
      expect(component.aseguradoras.length).toBe(17, '"aseguradoras" should have 17 elements');
    }));
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [onActivateIdentificador]', () => {
    it('onActivateIdentificador: toggle identifier edit activation', () => {
      console.log('><[citaciones/PatientPanelv2Component]> onActivateIdentificador: toggle identifier edit activation');
      expect(component.identificadorIsEditable).toBeFalsy('should be false');
      component.onActivateIdentificador();
      expect(component.identificadorIsEditable).toBeTruthy('should be false');
    });
  });
  describe('Code Coverage Phase [onIdentificadorBlur]', () => {
    it('onIdentificadorBlur: process identifier validity lenght < 8 characters', () => {
      console.log('><[citaciones/PatientPanelv2Component]> onIdentificadorBlur: process identifier validity lenght < 8 characters');
      component.identifier = '12345678';
      component.onIdentificadorBlur();
      expect(component.nifIsValid).toBeFalsy('should be false');
    });
  });
  describe('Code Coverage Phase [onIdentificadorBlur]', () => {
    it('onIdentificadorBlur: process identifier not valid lenght = 9 characters', () => {
      console.log('><[citaciones/PatientPanelv2Component]> onIdentificadorBlur: process identifier not valid lenght = 9 characters');
      component.identifier = '12345678V';
      component.onIdentificadorBlur();
      expect(component.nifIsValid).toBeFalsy('should be false');
    });
  });
  describe('Code Coverage Phase [onIdentificadorBlur]', () => {
    it('onIdentificadorBlur: process identifier valid lenght = 9 characters. has appointments', () => {
      console.log('><[citaciones/PatientPanelv2Component]> onIdentificadorBlur: process identifier valid lenght = 9 characters. has appointments');
      component.identifier = '13739949W';
      expect(component.localphase).toBe('-IDENTIFICATION-', 'localPhase should have initialized to -IDENTIFICATION-');
      component.onIdentificadorBlur();
      expect(component.nifIsValid).toBeTruthy('should be false');
      expect(component.localphase).toBe('-PATIENT-DATA-AVAILABLE-', 'localPhase should have changed to -PATIENT-DATA-AVAILABLE-');
      // Validate patient data.
      console.log('><[citaciones/PatientPanelv2Component]> onIdentificadorBlur: validate patient data');
      expect(component.patientData.nombre).toBe('Luis', 'nombre should match Luis');
      expect(component.patientData.apellidos).toBe('De Diego Toro', 'apellidos should match De Diego');
      expect(component.patientData.openAppointments).toBeDefined('the patient should have appointments');
      expect(component.patientData.openAppointments.length).toBe(6, 'the patient should have appointments and be 6');
    });
  });
  describe('Code Coverage Phase [onClearCitaciones]', () => {
    it('onClearCitaciones: clean up the data', () => {
      console.log('><[citaciones/PatientPanelv2Component]> onClearCitaciones: clean up the data');
      expect(component.identifier).toBeUndefined('should be initilized to undefined');
      expect(component.patientData).toBeDefined('should be initilized to empty PatientData');
      expect(component.localphase).toBe('-IDENTIFICATION-', 'localPhase should have initialized to -IDENTIFICATION-');
      expect(component.nifIsValid).toBeFalsy('should be initialized to false');
      expect(component.identificadorIsEditable).toBeFalsy('should be initialized to false');
    });
  });

  // - B U G   I D E N T I F I C A T I O N   P H A S E.
  describe('Bug Identification Phase', () => {
    it('B03. an empty NIF will fire an exception', () => {
      console.log('><[citaciones/PatientPanelv2Component]> B03. an empty NIF will fire an exception');
      component.identifier = null;
      component.onIdentificadorBlur();
      expect(component.nifIsValid).toBeFalsy('should be false');
    });
  });
});
