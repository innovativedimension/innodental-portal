//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
//--- ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
//--- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { OpenAppointmentControllerService } from '@app/controllers/open-appointment-controller.service';
//--- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
//--- MODELS
import { PatientData } from '@app/models/PatientData.model';
import { ButtonEvent } from '@models/core/ButtonEvent.model';
import { OpenAppointment } from '@app/models/OpenAppointment.model';

@Component({
  selector: 'c-patient-panelv2',
  templateUrl: './patient-panel.component.html',
  styleUrls: ['./patient-panel.component.scss']
})
export class PatientPanelv2Component extends AppPanelComponent implements OnInit {
  @Output() nifvalido = new EventEmitter<number>(); // Outputs the nif validation state.
  @Output() patientDataOutput = new EventEmitter<PatientData>(); // Output the patient data form contents.
  public aseguradoras: string[] = []; // The list of insurance companies to show
  public patientData: PatientData = new PatientData();
  public localphase: string = '-IDENTIFICATION-';
  public nifIsValid: boolean = false; // Stores the nif validation value.
  public identifier: string; // This is the input field to store the patient NIF identifier.
  public identificadorIsEditable: boolean = false; // Once the identifier is set this if the flag to make it editable

  // - C O N S T R U C T O R
  constructor(
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    protected openAppointmentController: OpenAppointmentControllerService) {
    super(router, activeRoute, appStoreService, backendService);
  }

  //--- L I F E C Y C L E   F L O W
  ngOnInit() {
    console.log(">>[PatientPanelComponent.ngOnInit]");
    // Read the list of insurance companies from the properties service
    this.appStoreService.propertiesAseguradoras()
      .subscribe((aseguradoraList) => {
        this.aseguradoras = aseguradoraList
      });
    console.log("<<[PatientPanelComponent.ngOnInit]");
  }

  // - F O R M   I N T E R A C T I O N
  public onActivateIdentificador(): void {
    this.identificadorIsEditable = !this.identificadorIsEditable;
  }
  public onFieldBlur(): void {
    // Validate the patient data to emit the signal to start appointment assignment.
    if (this.patientData.isValid()) this.patientDataOutput.emit(this.patientData);
  }
  /**
   * The user has exited the <b>identificador</b> field. Check if we have to search for the patient apointments on the backend. The process leaves the list of appointments on the <i>this.activeAppointments</i> variable.
   */
  public onIdentificadorBlur(): void {
    console.log(">>[PatientPanelComponent.onIdentificadorBlur]");
    if (null == this.identifier) {
      this.nifIsValid = false;
      this.localphase = '-IDENTIFICATION-';
      return;
    }
    if (this.identifier.length == 9) {
      this.downloading = true;
      if (this.appStoreService.validateNIF(this.identifier)) {
        // Put the identifier in uppercase.
        this.identifier = this.identifier.toUpperCase();
        this.nifIsValid = true;
        this.localphase = '-PATIENT-DATA-AVAILABLE-';
        // Start yo use the controller to keep track for the open appointments.
        // The controller should use Subjects to make access asynchronous and allow to wait for data.
        this.downloading = true;
        this.openAppointmentController.downloadAppointments4Patient(this.identifier)
          .subscribe((openAppointments) => {
            // Now update the contents for the patinet panel depending on the data received.
            if (openAppointments.length > 0) {
              // Extact the patient data from the list of open appointments.
              // let appointment = openAppointments[0].getCita();
              this.patientData = this.extractPatientData(openAppointments);
              if (null == this.patientData) this.patientData = new PatientData();
              // Activate other panels.
              if (this.patientData.isValid()) this.patientDataOutput.emit(this.patientData);
              // else this.patientDataOutput.emit(new PatientData().setIdentificador(this.identifier));
              // Add the open appointments to the Patient data structures.
              this.patientData.setOpenAppointments(openAppointments);
            }
            this.patientData.identificador = this.identifier;
            this.downloading = false;
          });
      } else {
        this.nifIsValid = false;
        this.localphase = '-IDENTIFICATION-';
        this.downloading = true;
      }
    } else {
      this.nifIsValid = false;
      this.localphase = '-IDENTIFICATION-';
    }
    // this.downloading = false;
    console.log("<<[PatientPanelComponent.onIdentificadorBlur]");
  }
  public onClearPage(_button: ButtonEvent): void {
    this.identifier = null;
    this.patientData = new PatientData();
    this.localphase = '-IDENTIFICATION-';
    this.nifIsValid = false;
    this.identificadorIsEditable = false;
    // Emit the message to the page to also clear the panels.
    let containerAsAny = this.container as any;
    containerAsAny.communicateClearPage();
    // Complete the button action. Send back the completion to the button component.
    _button.button.completeAction();
  }
  protected extractPatientData(_appointments: OpenAppointment[]): PatientData {
    for (let appo of _appointments) {
      if (null != appo.getCita().pacienteData) return new PatientData(appo.getCita().pacienteData);
    }
  }
}
