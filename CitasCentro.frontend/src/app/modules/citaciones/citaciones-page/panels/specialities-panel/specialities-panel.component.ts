//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from "@angular/core";
// import { Input } from "@angular/core";
// import { Inject } from '@angular/core';
// import { ViewChild } from '@angular/core';
// import { Observable } from 'rxjs';
import { ISubscription } from "rxjs/Subscription";
//--- ENVIRONMENT
// import { environment } from '@env/environment';
// //--- ROUTER
// import { Router } from '@angular/router';
// //--- NOTIFICATIONS
// //import { NotificationsService } from 'angular2-notifications';
// //--- WEBSTORAGE
// import { LOCAL_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
//--- SERVICES
// import { AppStoreService } from 'citamed-lib';
// import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
//--- INTERFACES
import { INode } from '@interfaces/core/INode.interface';
// import { IViewer } from '@models/Cita.model';
//--- MODELS
// import { Node } from '@models/Cita.model';
import { Credencial } from '@models/Credencial.model';
import { Especialidad } from '@models/Especialidad.model';
import { Medico } from '@models/Medico.model';
import { Filter } from '@app/models/Filter.model';
// import { PatientData } from '../../models/PatientData.model';

@Component({
  selector: 'c-specialities-panel',
  templateUrl: './specialities-panel.component.html',
  styleUrls: ['./specialities-panel.component.scss']
})
export class SpecialitiesPanelComponent extends AppPanelComponent implements OnInit, OnDestroy {
  // @Input() title: string;
  // public self: IViewer = this;
  private _especialidadList: Map<string, Especialidad> = new Map<string, Especialidad>();
  private _specialitiesSubscription: ISubscription;
  private _filters: Filter[] = [];
  // private filterOutServices: Filter;
  // public specilities: Especialidad[] = [];

  //--- L I F E C Y C L E   F L O W
  ngOnInit() {
    // this.canBeClosed=true;
    this.downloading=true;
    // Define the specialities filters.
    this._filters.push(new Filter()
      .setName('has contents')
      .setDescription('Filter Especialidades empty of Medico')
      .setFilter((_test: Especialidad): boolean => {
        if (_test.getContentSize() < 1) return true;
        else return false;
      }));

    // Access the dynamic cache for the specilities of this centro.
    this._specialitiesSubscription = this.appStoreService.accessSpecialities()
      .subscribe((especialidades) => {
        this.dataModelRoot = [];
        for (let espec of especialidades) {
          if (this.applyFilters(espec)) continue;
          else this.dataModelRoot.push(espec);
        }
        this.notifyDataChanged();
        this.downloading=false;
      });

    // Set the container callback.
    // this.self = this;
    // Get a list of the available specilities on the current Centro. This value should have been set before reaching this point during the login phase.
    // This information is on the stored credential.
    // let credential = this.appStoreService.accessCredential();
    // let centro = credential.getCentro();
    // if (null == centro) {
    //   this.backendService.registerException('SpecilitiesPanelComponent', 'Campo vacío inesperado', 'inicio');
    // } else {
    //   // Get the list of services associated to the Centro and extract the single list of specilities.
    //   this._specialitiesSubscription = this.appStoreService.accessSpecialities()
    //     .subscribe((especialidades) => {
    //       this.dataModelRoot = [];
    //       for (let espec of especialidades) {
    //         if (this.applyFilters(espec)) continue;
    //         else this.dataModelRoot.push(espec);
    //       }
    //       this.notifyDataChanged();
    //     });
    // }
  }
  ngOnDestroy() {
    // Unsubscribe from the endless soure of specialities.
    this._specialitiesSubscription.unsubscribe();
  }

  //--- I V I E W E R   I N T E R F A C E
  public applyPolicies(_nodes: INode[]): INode[] {
    return _nodes.sort((n1: Especialidad, n2: Especialidad) => {
      return n1.nombre.localeCompare(n2.nombre);
    });
  }

  //--- F I L T E R I N G
  /**
   * This will apply all the filters defined in sequence. If any of the fiters detects a true then the record should be discarded.
   * If we reach the end of the filters list and the appointment still is present then we return a false.
   * @param  _target the speciality to be checked.
   * @return         <b>true</b> if the speciality has failed the filters and should be discarded. <b>false</b> if the speciality has to be kept.
   */
  public applyFilters(_target: Especialidad): boolean {
    for (let filter of this._filters) {
      let filterResult = filter.filterFunction(_target);
      if (filterResult) return true;
    }
    return false;
  }
}
