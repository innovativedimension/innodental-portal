// - CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// - ROUTING
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
// - NOTIFICATIONS
import { ToastrModule } from 'ng6-toastr-notifications';
// - DRAG & DROP
import { NgDragDropModule } from 'ng-drag-drop';
// - CALENDAR
import { CalendarModule } from 'angular-calendar';
// - IONIC
import { IonicModule } from 'ionic-angular';
//- APPLICATION MODULES
import { UIModule } from '../../modules/ui/ui.module';
import { SharedModule } from '@app/modules/shared/shared.module';

// - PAGES
import { CitacionesPageComponent } from './citaciones-page/citaciones-page.component';
// - PANELS
import { PatientPanelv2Component } from '@app/modules/citaciones/citaciones-page/panels/patient-panel/patient-panel.component';
import { SpecialitiesPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/specialities-panel/specialities-panel.component';
import { PatientAppointmentsPanelComponent } from './citaciones-page/panels/patient-appointments-panel/patient-appointments-panel.component';
import { MedicalActsPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/medical-acts-panel/medical-acts-panel.component';
import { CalendarFreeSlotsPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/calendar-free-slots-panel/calendar-free-slots-panel.component';
import { AppointmentListPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/appointment-list-panel/appointment-list-panel.component';
import { PendingComponent } from './components/pending/pending.component';
import { AppointmentAssignedComponent } from './components/appointment-assigned/appointment-assigned.component';
import { Speciality4CitationsComponent } from './components/speciality4-citations/speciality4-citations.component';
import { ToolbarPanelComponent } from '@app/modules/citaciones/citaciones-page/panels/toolbar-panel/toolbar-panel.component';
import { CalendarPanelV2Component } from './citaciones-page/panels/calendar-panel-v2/calendar-panel-v2.component';
import { CoreModule } from '../core/core.module';
import { CalendarScreenSizeAdapter } from './citaciones-page/panels/calendar-panel-v2/calendar-screensize-adapter.component';

// - MODULE ROUTES
const routes: Routes = [
  { path: '', component: CitacionesPageComponent },
];

@NgModule({
  imports: [
    // - CORE
    CommonModule,
    // - ROUTING
    RouterModule.forChild(routes),
    // - DRAG & DROP
    NgDragDropModule.forRoot(),
    // - CALENDAR
    CalendarModule.forRoot(),
    // - IONIC
    IonicModule,
    // - APPLICATION MODULES
    UIModule,
    SharedModule,
    CoreModule
  ],
  declarations: [
    // - PAGES
    CitacionesPageComponent,
    // - PANELS
    PatientPanelv2Component,
    SpecialitiesPanelComponent,
    MedicalActsPanelComponent,
    CalendarFreeSlotsPanelComponent,
    AppointmentListPanelComponent,
    // - COMPONENTS
    PendingComponent,
    AppointmentAssignedComponent,
    Speciality4CitationsComponent,
    ToolbarPanelComponent,
    PatientAppointmentsPanelComponent,
    CalendarPanelV2Component,
    CalendarScreenSizeAdapter
  ],
  exports: [
    RouterModule,
    CalendarPanelV2Component,
    CalendarFreeSlotsPanelComponent
  ]
})
export class CitacionesModule { }
