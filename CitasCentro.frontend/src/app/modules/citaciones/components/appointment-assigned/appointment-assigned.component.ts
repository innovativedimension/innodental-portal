//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Inject } from '@angular/core';
import { ViewChild } from '@angular/core';
//--- ENVIRONMENT
import { environment } from '@env/environment';
//--- MODELS
import { Cita } from '@models/Cita.model';

@Component({
  selector: 'appointment-assigned',
  templateUrl: './appointment-assigned.component.html',
  styleUrls: ['./appointment-assigned.component.scss']
})
export class AppointmentAssignedComponent {
  @Input() node: Cita;
  @Input() container: any;

  public removeAppointment(): void {
    this.container.removeAppointment(this.node);
  }
}
