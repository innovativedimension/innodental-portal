//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { ViewChild } from '@angular/core';
//--- ENVIRONMENT
// import { environment } from 'app/../environments/environment';
// //--- ROUTER
// import { Router } from '@angular/router';
// //--- NOTIFICATIONS
// //import { NotificationsService } from 'angular2-notifications';
// //--- WEBSTORAGE
// import { LOCAL_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
// //--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
// import { BasePageComponent } from 'app/pages/basepage.component';
// import { AppointmentDateSelectorComponent } from 'app/components/appointment-date-selector/appointment-date-selector.component';
// import { AppointmentListComponent } from 'app/components/appointment-list/appointment-list.component';
//--- INTERFACES
//--- MODELS
// import { Cita } from '@models/Cita.model';
import { Pendings } from '@app/models/Pendings.model';
import { PatientData } from '@app/models/PatientData.model';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { Cita } from '@models/Cita.model';

@Component({
  selector: 'pending',
  templateUrl: './pending.component.html',
  styleUrls: ['./pending.component.scss']
})
export class PendingComponent extends AppPanelComponent {
  @Input() target: Pendings;
  @Input() patientData: PatientData;
  public dropScopeValue: string = '-CITA-';

  //--- D R O P   S E C T I O N
  /**
   * Process the drop of an appointment into a service awaiting for appointmetn assignment.
   * This process will have to block the appointment and check if with this we have covered the time slot required. If the time slot is covered then we should disable the possibility to drop more appoinements.
   * @param _event the drag event with the payload.
   */
  public onDropCita(_event: any): void {
    console.log("><[PendingComponent.onDropCita]");
    this.blockAppointment(_event.dragData);
    // Mark the original appointment as BLOQUEADA
    _event.dragData.estado = "BLOQUEADA";
    this.target.addAppointment(_event.dragData);
    if (this.target.isCovered()) this.dropScopeValue = '-COVERED-';
  }
  public dropScope(): string {
    if (this.target.isCovered()) return '-DROP-NOT-ALLOWED-';
    else return this.target.getServiceIdentifiers();
  }
  public isDropAllowed(): boolean {
    return (!this.target.isCovered());
  }
  // public hasAssigned(): boolean {
  //   if (this.target.getAppointmentsCount() > 0) return true;
  //   else return false;
  // }
  public getAssignedAppointments(): Cita[] {
    return this.target.appointments;
  }
  // - G E T T E R S   &   S E T T E R S
  public getNode(): any {
    return this.target.node;
  }
  public getNodeClass(): string {
    return this.target.node.getJsonClass();
  }
  /**
   * Delete the appointment from the list of associated appointments. Also cler the block or the reservation on the backend.
   * @param _cita [description]
   */
  public removeAppointment(_cita: Cita): void {
    let newList = [];
    for (let ct of this.target.appointments) {
      if (ct.getId() === _cita.getId()) continue;
      else newList.push(ct);
    }
    this.target.appointments = newList;
    // Remove the block from the backend.
    this.cancelAppointment(_cita);
  }

  // - B A C K E N D   A C C E S S
  public blockAppointment(_cita: Cita) {
    // let patientRecord = new PatientCredential(JSON.parse(data));
    this.backendService.backendBlockAppointment(this.patientData, _cita)
      .subscribe((blockedCita) => {
        // console.log('--[PendingComponent.blockAppointment]> blockedCita: ' + blockedCita[0].estado);
      });
  }
  /**
   * This event is generated when clicking the 'Confirmar Cita' button. Should post it to the backend server for reservation and then save the appointment on the local list.
   */
  public confirmAppointment(_cita: Cita): void {
    console.log('>>[PendingComponent.confirmAppointment]');
    this.backendService.backendReserveAppointment(this.patientData, _cita)
      .subscribe((reservedOpenAppointment) => {
        console.log('--[PendingComponent.confirmAppointment]> reservedOpenAppointment: ' + reservedOpenAppointment.getCita().getId());
      }, (error) => {
        console.log('--[PendingComponent.confirmAppointment]> Exception: ' + error.getMessage());
        // TODO - This is the right point to insert a Toast.
      });
    console.log('<<[PendingComponent.confirmAppointment]');
  }
  public cancelAppointment(_cita: Cita): void {
    console.log('>>[PendingComponent.cancelAppointment]');
    this.backendService.backendCancelAppointment(this.patientData, _cita)
      .subscribe((reservedCita) => {
        console.log('>>[PendingComponent.cancelAppointment]> blockedCita: ' + reservedCita);
        // Update the local appointment state.
        _cita.estado = 'LIBRE';
      });
    console.log('<<[PendingComponent.cancelAppointment]');
  }
}
