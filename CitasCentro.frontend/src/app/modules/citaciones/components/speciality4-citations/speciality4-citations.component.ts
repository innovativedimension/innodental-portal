//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Inject } from '@angular/core';
import { ViewChild } from '@angular/core';
//--- ENVIRONMENT
import { environment } from '@env/environment';
//--- INTERFACES
import { IContainerController } from '@interfaces/core/IContainerController.interface';
//--- MODELS
import { Cita } from '@models/Cita.model';
import { Especialidad } from '@models/Especialidad.model';

@Component({
  selector: 'speciality4-citations',
  templateUrl: './speciality4-citations.component.html',
  styleUrls: ['./speciality4-citations.component.scss']
})
export class Speciality4CitationsComponent {
  @Input() container: IContainerController;
  @Input() node: Especialidad;
  @Input() variant: string = '-DEFAULT-';

  public mouseHovering(): void {
    this.node.expand();
    this.container.notifyDataChanged()
  }
  public mouseLeft(): void {
    this.node.collapse();
    this.container.notifyDataChanged()
  }
  public toggleExpand(): void {
    this.node.toggleExpanded();
    this.container.notifyDataChanged()
  }
}
