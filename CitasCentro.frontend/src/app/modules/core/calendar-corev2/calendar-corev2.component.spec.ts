// - CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// - TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
// - MODULES IMPORTS
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'angular-calendar';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { StubService } from '@app/testing/services/StubService.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// - COMPONENTS
import { TemplateUnderConstructionComponent } from '@app/modules/gestion-servicio/template-constructor-page/panels/template-under-construction/template-under-construction.component';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';
import { CitasTemplate } from '@models/CitasTemplate.model';
// import { CalendarPanelV2Component } from '@app/modules/citaciones/citaciones-page/panels/calendar-panel-v2/calendar-panel-v2.component';
// - MODEL
import { Medico } from '@models/Medico.model';
import { Payload } from '@models/Payload.model';
import { Cita } from '@models/Cita.model';
import { BackendServiceAppointmentsMock } from '@app/testing/services/BackendServiceAppointments.mockup.service';
import { CalendarCorev2Component } from './calendar-corev2.component';

describe('COMPONENT CalendarCorev2Component [Module: CORE]', () => {
  let component: CalendarCorev2Component;
  let fixture: ComponentFixture<CalendarCorev2Component>;
  let appStoreService: MockAppStoreService;
  // let templateOnControl: CitasTemplate;

  beforeEach(async(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        FormsModule,
        CalendarModule.forRoot(),
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: BackendService, useClass: BackendServiceAppointmentsMock }
      ],
      declarations: [CalendarCorev2Component, RouteMockUpComponent],
    })
      .compileComponents();
  }));
  beforeEach(() => {
    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(CalendarCorev2Component);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[citaciones/CalendarPanelV2Component]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
  });

  // - I N P U T / O U T P U T   P H A S E
  // describe('Input/Output Phase', () => {
  //   it('source (Input): validate source reception', () => {
  //     console.log('><[citaciones/CalendarPanelV2Component]> source (Input): validate source reception');
  //     // Check the initial state to undefined.
  //     expect(component.source).toBeUndefined();
  //     // Create the test node to be used on the render.
  //     component.source = new Medico();
  //     expect(component.source).toBeDefined();
  //   });
  //   it('daySelectedOutput (Output): detect click but no emit', () => {
  //     console.log('><[citaciones/CalendarPanelV2Component]> daySelectedOutput (Output): detect click but no emit');
  //     // Build the event to be sent. Event with no payload
  //     let emptyEvent = {
  //       empty: true
  //     };
  //     let event = {
  //       day: {
  //         badgeTotal: 0,
  //         date: new Date(),
  //         events: [],
  //         inMonth: true,
  //         isFuture: false,
  //         isPast: true,
  //         isToday: false,
  //         isWeekend: false
  //       }
  //     }
  //     spyOn(component.daySelectedOutput, 'emit');
  //     component.dayClicked(null);
  //     component.dayClicked(emptyEvent);
  //     component.dayClicked(event);
  //     expect(component.daySelectedOutput.emit).toHaveBeenCalledTimes(0);
  //   });
  //   it('daySelectedOutput (Output): emit a payload', () => {
  //     console.log('><[citaciones/CalendarPanelV2Component]> daySelectedOutput (Output): emit a payload');
  //     // Build the event to be sent. Event with no payload
  //     let thePayload = new Payload()
  //       .addCita(new Cita());
  //     let event = {
  //       day: {
  //         badgeTotal: 0,
  //         date: new Date(),
  //         events: [],
  //         inMonth: true,
  //         isFuture: false,
  //         isPast: true,
  //         isToday: false,
  //         isWeekend: false,
  //         payload: thePayload
  //       }
  //     }
  //     component.dayClicked(event);
  //     component.daySelectedOutput.subscribe((day: any) => {
  //       console.log('><[citaciones/CalendarPanelV2Component]> entering subscription');
  //       expect(day).toBeDefined('the emitted day exists');
  //       expect(day['payload']).toBeDefined('the emitted payload exists');
  //       let emittedPayload = day['payload'] as Payload;
  //       expect(emittedPayload.getOpen()).toBe(1, 'the open counter is 1');
  //     });
  //     expect(event).toBeDefined();
  //   });
  // });

  // // - L I F E C Y C L E   P H A S E
  // describe('Lifecycle Phase', () => {
  //   it('Lifecycle: OnInit -> download the source appointments', async(() => {
  //     console.log('><[citaciones/CalendarPanelV2Component]> Lifecycle: OnInit -> download the source appointments');
  //     let componentAsAny = component as any;
  //     // Check that initial state is the expected.
  //     expect(component.source).toBeUndefined('input "source" should be undefined upon creation');
  //     spyOn(componentAsAny, 'processEvents');
  //     component.ngOnInit();
  //     expect(componentAsAny.processEvents).toHaveBeenCalledTimes(0);
  //   }));
  //   it('Lifecycle: OnInit -> download the source appointments', async(() => {
  //     console.log('><[citaciones/CalendarPanelV2Component]> Lifecycle: OnInit -> download the source appointments');
  //     // Set a valid source to get some appointments.
  //     let componentAsAny = component as any;
  //     spyOn(componentAsAny, 'processEvents');
  //     component.source = new Medico().setId('appointments-medico1');
  //     component.ngOnInit();
  //     expect(componentAsAny.processEvents).toHaveBeenCalledTimes(1);
  //   }));
  // });

  // // - C O D E   C O V E R A G E   P H A S E
  // describe('Code Coverage Phase [getSource]', () => {
  //   it('getSource: getting the service', () => {
  //     console.log('><[citaciones/CalendarPanelV2Component]> getSource: getting the service');
  //     expect(component.source).toBeUndefined('input "source" should be undefined upon creation');
  //     component.source = new Medico().setEspecialidad('Pediatría');
  //     expect(component.source).toBeDefined('input "source" should be valid');
  //     expect(component.getSource().getEspecialidad()).toBe('Pediatría', 'source speciality should match "Pediatría"');
  //   });
  // });
});
