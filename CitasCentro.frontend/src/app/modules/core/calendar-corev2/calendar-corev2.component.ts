//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Subject } from 'rxjs/Subject';
// - ENVIRONMENT
import { environment } from '@env/environment';
// - ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { CalendarAppointmentsControllerService } from '@app/controllers/calendar-appointments-controller.service';
// - CALENDAR
import { isBefore } from 'date-fns';
import { startOfDay } from 'date-fns';
import { endOfDay } from 'date-fns';
import { CalendarEvent } from 'angular-calendar';
// - COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// - MODELS
import { Cita } from '@models/Cita.model';
import { Payload } from '@models/Payload.model';
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';

/**
 * This is the core code that is shared by all pages. Implements most of the code to deal with the model list 
 * data and the external request for data to be rendered on the UI.
 *
 * @export
 * @class CalendarCorev2Component
 * @extends {AppPanelComponent}
 */
@Component({
  selector: 'x-calendar-core-v2',
  templateUrl: './notused.html'
})
export class CalendarCorev2Component extends AppPanelComponent {
  // - CALENDAR VARIABLES
  public viewDate: Date = new Date();
  public events: CalendarEvent[] = [];
  public refresh: Subject<any> = new Subject();
  private view: string = 'month';

  // - CALENDAR CONTENTS VARIABLES
  public selectedCitas: Cita[] = [];
  public citaList: Cita[] = [];
  public eventList = new Map<string, Payload>();

  // - SELECTION MANAGEMENT
  public selectedDates: Map<Date, any> = new Map<Date, any>();
  public selectionCount: number = 0;

  // - C O N S T R U C T O R
  constructor(
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    protected appointmentsController: CalendarAppointmentsControllerService) {
    super(router, activeRoute, appStoreService, backendService);
  }

  // - S E L E C T I O N
  public isCellSelected(_day: any): boolean {
    if (null == _day['selected']) return false;
    else return true;
  }

  // - B A C K E N D   A C C E S S
  // public downloadAppointments(_target: IAppointmentEnabled): void {
  //   console.log("><[CalendarCorev2Component.downloadAppointments]");
  //   return this.appointmentsController.downloadAppointments4Target(_target);
  // }

  // - F I L T E R I N G
  /**
   * This will apply all the filters defined in sequence. If any of the fiters drops the appoitment then we do 
   * not continue and we terminate the process with a true.
   * If we reach the end of the filters list and the appointment still is present then we return a false.
   * @param  _filterList the list of filters to apply to the appointment.
   * @param  _target the appointment to be checked.
   * @return         <b>true</b> if the appointment has failed the filters and should be discarded. <b>false</b> if the 
   * appointmet has to be kept.
   */
  protected applyFilters(_filterList: any[], _target: Cita): boolean {
    for (let filter of _filterList) {
      let filterResult = filter.filter(_target);
      if (filterResult) return true;
    }
    return false;
  }

  // - C A L E N D A R   E V E N T   M A N A G E M E N T
  /**
   * Receives a list of appointments and a set of filters. The result is another list of appointments but
   * filtered out all the appointments that are not stripped out by the filters.
   *
   * @protected
   * @param {any[]} _filterList
   * @param {Cita[]} _appointmentList
   * @returns {Map<string, Payload>}
   * @memberof CalendarCorev2Component
   */
  protected processAppointments(_filterList: any[], _appointmentList: Cita[]): Map<string, Payload> {
    // Skip this step if the numbe of appointments is ZERO. This an empty start.
    this.eventList = new Map<string, Payload>();
    if (_appointmentList.length > 0) {
      // Sort the list.
      let sortedAppointments = _appointmentList.sort((n1: Cita, n2: Cita) => {
        if (isBefore(n1.getFecha(), n2.getFecha())) return -1;
        else return 1;
      });
      let citaList = [];
      for (let cita of sortedAppointments) {
        // Apply the list of filters to drop appointments that should not be accesible.
        let filtered = this.applyFilters(_filterList, cita);
        if (filtered) continue; // Throw away filtered appointments
        this.citaList.push(cita);
        // Do the accounting to generate the events.
        this.accountAppointment(cita);
      }
    }
    return this.eventList;
  }
  /**
   * Once an appointment is added to the resulting list, at the same time it is added to the matching date
   * Payload. So the date of the appointment searches for a Payload instance and then adds this same appointment
   * to the payload and updates the counts for the type of appointment.
   *
   * @protected
   * @param {Cita} _cita
   * @memberof CalendarCorev2Component
   */
  protected accountAppointment(_cita: Cita): void {
    // Do the accounting to generate the events.
    let hit: Payload = this.eventList.get(this.date2BasicISO(_cita.getFecha()));
    if (null == hit) {
      hit = new Payload();
      this.eventList.set(this.date2BasicISO(_cita.getFecha()), hit);
    }
    hit.addCita(_cita);
  }
  public getCurrentSelectedDate(): Date {
    return this.viewDate;
  }
  public dayClicked(_event: any): void {
    console.log("><[AppointmentDateSelectorComponent.dayClicked]> event: " + JSON.stringify(_event));
    // Toggle the selected state for the date selected.
    if (null == _event.day['selected']) {
      _event.day['selected'] = true;
      this.selectedDates.set(_event.day.date, _event.day);
      this.selectionCount++;
    } else {
      // Remove selection.
      _event.day['selected'] = null;
      this.selectedDates.set(_event.day.date, null);
      this.selectionCount--;
    }
  }

  //--- P R I V A T E   M E T H O D S
  // private updateCalendarData(_appointments: Cita[]): void {
  //   // Update the calendar data structures
  //   console.log("--[CitasReservadasComponent.onTemplateDrop]> count: " + _appointments.length);
  //   this.eventList = new Map<string, Pair>();
  //   this.citaList = [];
  //   for (let cita of _appointments) {
  //     this.citaList.push(cita);
  //     // Do the accounting to generate the events.
  //     let hit = this.eventList.get(cita.getFechaString());
  //     if (null == hit) {
  //       hit = new Pair();
  //       this.eventList.set(cita.getFechaString(), hit);
  //     }
  //     // console.log("--[CitasReservadasComponent.onTemplateDrop]> processing hit: " + JSON.stringify(hit));
  //     if (cita.patientIsValid())
  //       hit.addReserved();
  //     else hit.addOpen();
  //   }
  // }
  public clearSelection(): void {
    this.selectedDates.forEach((value, key) => {
      value['selected'] = null;
    });
    this.selectionCount = 0;
  }
  public add2Selection(_day: any): void {
    _day['selected'] = true;
    this.selectedDates.set(_day.date, _day);
    this.selectionCount++;
  }

  // - P`R I V A T E   F U N C T I O N A L I T Y
  /**
   * Converts the list of payload data and apopintments into the internal data structures used on the Angular
   * Calendar component.
   *
   * @protected
   * @param {Map<string, Payload>} _events the map of processed appointments to be set on the calendar
   * @memberof CalendarCorev2Component
   */
  protected processEvents(_events: Map<string, Payload>): void {
    this.events = [];
    _events.forEach((value: Payload, key: string) => {
      console.log("--[CalendarCoreComponent.processEvents]> Day payload: "
        + 'key: ' + key + '-' +
        + value.getReserved() + "/" + value.getFree() + " : " + value.getTotal());
      let newevent = {
        start: startOfDay(key),
        end: endOfDay(key),
        title: 'Appointments: ' + value.getTotal(),
        // total: value.getTotal(),
        // reserved: value.getReserved(),
        // vacancy: value.getVacancy(),
        payload: value,
        selected: false
      };
      this.events.push(newevent);
    });
    // Send a message to update the calendar.
    this.refresh.next();
  }
}
