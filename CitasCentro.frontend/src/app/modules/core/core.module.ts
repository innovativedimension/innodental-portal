//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// // --- ROUTING
// import { RouterModule } from '@angular/router';
// import { Routes } from '@angular/router';
// // --- NOTIFICATIONS
// import { ToastrModule } from 'ng6-toastr-notifications';
// // --- DRAG & DROP
// import { NgDragDropModule } from 'ng-drag-drop';
// // --- CALENDAR
// import { CalendarModule } from 'angular-calendar';
// // --- IONIC
// import { IonicModule } from 'ionic-angular';
// //--- APPLICATION MODULES
// import { UIModule } from '../../modules/ui/ui.module';
// import { SharedModule } from '@app/modules/shared/shared.module';

// - COMPONENTS
import { CalendarCorev2Component } from './calendar-corev2/calendar-corev2.component';


@NgModule({
  imports: [
    // --- CORE
    CommonModule
  ],
  declarations: [
    CalendarCorev2Component
  ],
  exports: [
    CalendarCorev2Component
  ]
})
export class CoreModule { }
