//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- MODELS
import { Medico } from '@models/Medico.model';
//--- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'notused-calendario-page',
  templateUrl: './notused.html'
})
export class GestionCommonPageComponent extends AppPanelComponent implements OnInit {
  public activateSelection: boolean = true; // TRUE means that there is no selected service: we should show the list.
  public selectedService: Medico; // The current selected service stored on the session.

  // - L I F E C Y C L E
  ngOnInit() {
    console.log(">>[GestionCommonPageComponent.ngOnInit]");
    this.selectedService = this.appStoreService.accessActiveService();
    if (null == this.selectedService) {
      // Hide the main panels and open the selecion panel.
      this.activateSelection = true;
    } else {
      // Check if this profile has access to that service. Privileges can be revoked.
      if (this.appStoreService.accessCredential().allservices) this.activateSelection = false;
      else {
        let authorized = this.appStoreService.accessCredential().serviciosAutorizados;
        console.log("--[GestionCommonPageComponent.ngOnInit]> authorized: " + JSON.stringify(authorized));
        for (let service of authorized) {
          console.log("--[GestionCommonPageComponent.ngOnInit]> Validating service autorized: " + service.getId());
          if (service.getId() === this.selectedService.getId())
            this.activateSelection = false;
        }
        if (this.activateSelection) {
          // The service is not authorized. Show the service selector.
          this.selectedService = null;
          this.activateSelection = true;
          this.appStoreService.storeActiveService(null);
        }
      }
    }
    console.log("<<[GestionCommonPageComponent.ngOnInit]");
  }

  // - V I E W   I N T E R A C T I O N
  public getSelectedService(): Medico {
    return this.selectedService;
  }
  public onOpenSelectionPage(_state?: boolean): void {
    this.activateSelection = true;
  }
  public onServiceSelected(selectedService: Medico): void {
    if (null != selectedService) {
      this.selectedService = selectedService;
      this.activateSelection = false;
    }
  }
}
