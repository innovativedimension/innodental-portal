//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
// --- COMPONENTS
import { GestionCommonPageComponent } from '@app/modules/gestion-servicio/common/gestion-common-page.component';
import { CalendarFreeSlotsPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/calendar-free-slots-panel/calendar-free-slots-panel.component';
import { AppointmentListPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/appointment-list-panel/appointment-list-panel.component';
import { MonthCalendarPanelComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/panels/month-calendar-panel/month-calendar-panel.component';

@Component({
  selector: 'citas-by-template-page',
  templateUrl: './definicion-calendario-page.component.html',
  styleUrls: ['./definicion-calendario-page.component.scss']
})
export class CalendarDefinitionPageComponent extends GestionCommonPageComponent {
  @ViewChild(MonthCalendarPanelComponent) private calendarPanel: MonthCalendarPanelComponent;
  @ViewChild(AppointmentListPanelComponent) private appointmentListPanel: AppointmentListPanelComponent;

  // - V I E W   I N T E R A C T I O N
  public onUpdateContents(_event: any): void {
    // Tell the calendar to update the contents reloading the selected service.
    this.calendarPanel.downloadAppointments(this.selectedService);
  }
  public onSelectedDate(_selectedDate: Date): void {
    this.appointmentListPanel.setSelectedDate(_selectedDate);
  }
}
