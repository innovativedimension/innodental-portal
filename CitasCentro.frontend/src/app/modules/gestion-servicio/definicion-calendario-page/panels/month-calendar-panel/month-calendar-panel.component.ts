//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component, EventEmitter, Output } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { OnChanges } from '@angular/core';
import { Input } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
//--- CALENDAR
import { isBefore } from 'date-fns';
//--- INTERFACES
// import { IAppointmentEnabled } from '@models/Limitador.model';
// import { IViewer } from '@models/Limitador.model';
import { ICalendarPageContainer } from '@app/interfaces/ICalendarPageContainer.interface';
//--- COMPONENTS
import { CalendarCoreComponent } from '@app/modules/ui/calendar-core/calendar-core.component';
//--- MODELS
import { Cita } from '@models/Cita.model';
import { Payload } from '@models/Payload.model';
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';

@Component({
  selector: 'dc-month-calendar-panel',
  templateUrl: './month-calendar-panel.component.html',
  styleUrls: ['./month-calendar-panel.component.scss']
})
export class MonthCalendarPanelComponent extends CalendarCoreComponent implements OnInit, OnDestroy {
  // @Input() container: ICalendarPageContainer;
  private _sourceShader: IAppointmentEnabled;
  @Input() set source(_newsource: IAppointmentEnabled) {
    this._sourceShader = _newsource;
    // If the source is not null then reload from the backend the list of appointments.
    if (null != this._sourceShader) this.downloadAppointments(this._sourceShader);
  }
  @Output() selectedDate = new EventEmitter<Date>();

  private _appointmentsSubscription: ISubscription;
  private filters: any = [];
  private _intervalHolder: any; // The variable to perform the periodic refresh.

  //--- L I F E C Y C L E
  ngOnInit() {
    console.log("><[CalendarFreeSlotsPanelComponent.ngOnInit]");
    // Activate the automatic update of the appointments. 60 seconds
    if (null != this._intervalHolder)
      this._intervalHolder = setInterval(() => {
        if (null != this._sourceShader) this.downloadAppointments(this._sourceShader);
      }, 1000 * 60); // 1 minute

    // The other only action to be performed on initialization is a watch on the list of appointments to update the calendar when that list changes, be it because of a periodic refresh, manual refresh or the selection of another target.
    this.accessAppointments();
  }
  ngOnDestroy() {
    if (null != this._appointmentsSubscription) this._appointmentsSubscription.unsubscribe();
  }

  // - S U B S C R I P T I O N   M A N A G E M E N T
  /**
   * Connect a watch on the list of appointments. Each time that list is changed this code will activate and refresh the calendar contents with the new processed list of appointments.
   *
   * @memberof CalendarFreeSlotsPanelComponent
   */
  public accessAppointments(): void {
    console.log(">>[CalendarFreeSlotsPanelComponent.accessAppointments]");
    // Signal to the display that we are doing a lengthy operation.
    this.downloading = true;
    // Get the current list of appointments from the subject each time it changes.
    this._appointmentsSubscription = this.appStoreService.accessAppointments()
      .subscribe((appointmentList) => {
        // Skip this step if the numbe of appointments is ZERO. This an empty start.
        if (appointmentList.length > 0) {
          // Sort the list.
          let sortedAppointments = appointmentList.sort((n1: Cita, n2: Cita) => {
            if (isBefore(n1.getFecha(), n2.getFecha())) return -1;
            else return 1;
          });
          // Clear calendar internal data.
          this.eventList = new Map<string, Payload>();
          this.citaList = [];
          // let citaList = [];
          for (let cita of sortedAppointments) {
            // Apply the list of filters to drop appointments that should not be accesible.
            let filtered = this.applyFilters(cita);
            if (filtered) continue;
            this.citaList.push(cita);
            // Do the accounting to generate the events.
            this.accountAppointment(cita);
          }
          this.processEvents();
          this.downloading = false;
          this.appStoreService.infoNotification("Completado", "Descarga de citas disponibles completado.");
          console.log("<<[CalendarFreeSlotsPanelComponent.accessAppointments]");
        }
      }), (error) => {
        // Process any 401 exception that means the session is no longer valid.
        if (error.status == 401) {
          this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
          this.router.navigate(['login']);
        }
        console.log("--[NewActoMedicoDoctorPanelComponent.onSubmit]> Error: " + error.message);
        this.appStoreService.errorNotification("Error actualizando Médico/Servicio. Mensaje: " + error.message, "¡Atención!");
      };
  }

  //--- C A L E N D A R   E V E N T   M A N A G E M E N T
  public activateRefresh(): void {
    console.log("><[CalendarFreeSlotsPanelComponent.activateRefresh]");
    this.downloadAppointments(this._sourceShader);
  }
  public closeCalendar(): void {
    this.show = false;
  }

  // - D R A G   &  D R O P   S E C T I O N
  public getDragScope(_day: any): string {
    if (this.selectionCount > 0) {
      // Change the scope depending on selection.
      if (null == _day['selected']) return '-DISALLOWED-'
      else return '-ALLOWED-';
    } else return '-ALLOWED-';
  }
  public isCellSelected(_day: any): boolean {
    if (null == _day['selected']) return false;
    else return true;
  }
  public isCellDisallowed(_day: any): boolean {
    if (this.selectionCount > 0)
      if (null == _day['selected']) return true
    return false;
  }
  /**
   * This method is called at the end of the drag and drop operation. Inside the _event comes the dragged data objects and on the _day there is the dropped date. This later parameter is only used when there is no selection active and then the action affect just to that date.
   * If there are one or more calendar cells selected, then we should iterate over the selecion list and do the same insertion for every selected date.
   * Finally we should clean up the selection.
   * @param _event the event received. Contains a field called 'dragData' with the draggable selected objects.
   * @param _day   the day data structure at the calendar cell where de dragged data is deployed.
   */
  public onTemplateDrop(_event: any, _day: any): void {
    // The event contents 'dragData' is the template to be used to generate the appointments.
    console.log(">>[MonthCalendarPanelComponent.onTemplateDrop]");
    // Check that there is a selected service.
    if (null != this.getTarget()) {
      // Check if simple date of use the selection.
      if (this.selectionCount > 0) {
        console.log(">>[MonthCalendarPanelComponent.onTemplateDrop]> Processing " + this.selectionCount + " cells.");
        let processCounter = 0;
        this.selectedDates.forEach((value: any, key: Date, _data: Map<Date, any>) => {
          this.backendService.backendGenerateAppointment4Date(this.getTarget(), _event.dragData, key)
            .subscribe((creationReport) => {
              // Update the display.
              // this.downloadAppointments(this.getTarget());
              this.selectedDates = new Map<Date, any>();
              // this.selectionCount = 0;
              // this.appStoreService.successNotification('Creación de citas por plantilla completado. Fecha ' + key, 'Completado');
            });
          processCounter++;
          if (processCounter == this.selectionCount) {
            // This is the last item on the iteration list.
            this.downloadAppointments(this.getTarget());
            this.selectedDates = new Map<Date, any>();
            this.selectionCount = 0;
            this.appStoreService.successNotification('Creación de citas por plantilla completado. Fecha ' + key, 'Completado');
          }
        });
      } else {
        this.backendService.backendGenerateAppointment4Date(this.getTarget(), _event.dragData, _day.date)
          .subscribe((creationReport) => {
            // Update the display.
            this.downloadAppointments(this.getTarget());
            this.selectedDates = new Map<Date, any>();
            this.selectionCount = 0;
            this.appStoreService.successNotification('Creación de citas por plantilla completado. Fecha ' + _day.date, 'Completado');
          });
      }
    } else this.appStoreService.warningNotification('No se ha detectato un médico/servicio activo. No es posible crear citas. Seleccione uno de la lista de disponibles.', '¡Atención!');
  }

  //--- I N T E R C O M M U N I C A T I O N   S E C T I O N
  public dayClicked(_event: any): void {
    console.log("><[AppointmentDateSelectorComponent.dayClicked]> event: " + JSON.stringify(_event));
    // Toggle the selected state for the date selected.
    if (null == _event.day['selected']) {
      _event.day['selected'] = true;
      this.selectedDates.set(_event.day.date, _event.day);
      this.selectionCount++;
    } else {
      // Remove selection.
      _event.day['selected'] = null;
      this.selectedDates.set(_event.day.date, null);
      this.selectionCount--;
    }
  }

  //--- T A R G E T   D A T A   A C C E S S O R S
  public getTarget(): any {
    if (null != this._sourceShader) return this._sourceShader;
  }
  public getTargetClass(): string {
    if (null != this._sourceShader) return this._sourceShader.getJsonClass();
    else return '-UNDEFINED-';
  }

  //--- F I L T E R I N G
  /**
   * This will apply all the filters defined in sequence. If any of the fiters drops the appoitment then we do not continue and we terminate the process with a true.
   * If we reach the end of the filters list and the appointment still is present then we return a false.
   * @param  _target the appointment to be checked.
   * @return         <b>true</b> if the appointment has failed the filters and should be discarded. <b>false</b> if the appointmet has to be kept.
   */
  public applyFilters(_target: Cita): boolean {
    for (let filter of this.filters) {
      let filterResult = filter.filter(_target);
      if (filterResult) return true;
    }
    return false;
  }
}
