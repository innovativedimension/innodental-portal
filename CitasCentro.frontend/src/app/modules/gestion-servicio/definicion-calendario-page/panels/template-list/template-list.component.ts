//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Inject } from '@angular/core';
//--- ENVIRONMENT
// import { environment } from '@env/environment';
//--- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
//--- ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
//--- NOTIFICATIONS
import { ToastrManager } from 'ng6-toastr-notifications';
//import { NotificationsService } from 'angular2-notifications';
//--- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { ModalService } from '@app/services/modal.service';
import { TemplateControllerService } from '@app/controllers/template-controller.service';
//--- MODELS
import { Cita } from '@models/Cita.model';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { CitasTemplate } from '@models/CitasTemplate.model';

@Component({
  selector: 'dc-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.scss']
})
export class TemplateListComponent extends AppPanelComponent implements OnInit {
  public targetTemplate: CitasTemplate = new CitasTemplate(); // Storage for the request template to action.
  private passwordValidacion: string; // Field where to store the form input filed value.
  public processing: boolean = false; // Used to signal the processing of the state change to the service.
  public validated: boolean = false; // TRUE means that the password has been validated succesfully.
  private appointments: Cita[] = [];
  public selectedCitas: Cita[] = [];
  public stateColor: string = "";

  // - C O N S T R U C T O R
  constructor(
    // @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    // @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    // //protected toasterService: NotificationsService,
    // //protected notifier: ToastrManager,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    private modalService: ModalService,
    protected templateController: TemplateControllerService) {
    super(router, activeRoute, appStoreService, backendService);
  }

  // - L I F E C Y C L E
  ngOnInit() {
    console.log(">>[TemplateListComponent.ngOnInit]");
    super.ngOnInit();
    this.downloading = true;
    // TODO - During mockup load the list of templates from the mock up system until the backend is implemented
    this.backendService.readServiceTemplates(this.appStoreService.accessActiveService())
      .subscribe((data) => {
        for (let template of data) {
          this.dataModelRoot.push(template);
        }
        this.notifyDataChanged();
        this.downloading = false;
      });
    console.log("<<[TemplateListComponent.ngOnInit]");
  }

  // - V I E W   I N T E R A C T I O N
  public onEditTemplateRequest(_template: CitasTemplate): void {
    // Store the template on the controller and pass it the action.
    this.templateController.editTemplate(_template);
  }
  /**
   * To remove templates or any other elaborated structure we should request again the password for the current credential.
   *
   * @param {CitasTemplate} _template the template returned by the output event
   * @memberof TemplateListComponent
   */
  public onDeleteTemplateRequest(_template: CitasTemplate): void {
    console.log(">>[TemplateListComponent.onDeleteTemplateRequest]");
    if (null != _template) {
      this.targetTemplate = _template;
      this.templateController.storeTemplate(_template);
      // Show the notification panel and get the authorization for deletion.
      this.modalService.open('delete-template');
    }
    console.log("<<[TemplateListComponent.onDeleteTemplateRequest]");
  }

  // - F O R M   I N T E R A C T I O N
  public onValidateCredential() {
    // Go to the background to check for the password.
    this.backendService.validatePassword(this.passwordValidacion)
      .subscribe((validationResult) => {
        if (validationResult) this.validated = true;
      })
  }
  public onDeleteTemplate(): void {
    console.log(">>[TemplateListComponent.onDeleteTemplate]");
    this.templateController.deleteTemplate()
      .subscribe((identifier) => {
        if (identifier < 0) {
          // If the identifier is negative then there was an error.
          this.appStoreService.errorNotification('No ha sido posible borrar el template.', "Error de Backend",
            { dismiss: 'click' });
        } else {
          this.appStoreService.successNotification('Plantilla borrada satisfactoriamente.', "¡Borrada!")
          this.modalService.close('delete-template');
          this.ngOnInit();
        }
      });
    console.log("<<[TemplateListComponent.onDeleteTemplate]");
  }
  public onCancelDelete() {
    // Dismiss the modal.
    this.modalService.close('delete-template');
  }
}
