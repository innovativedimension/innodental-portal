//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { ToolbarPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/toolbar-panel/toolbar-panel.component';
// --- MODELS
import { Cita } from '@models/Cita.model';
import { Payload } from '@models/Payload.model';

@Component({
  selector: 'dc-toolbar-panel',
  templateUrl: './toolbar-panel.component.html',
  styleUrls: ['./toolbar-panel.component.scss']
})
export class DefinicionToolbarPanelComponent extends ToolbarPanelComponent {
  @Output() update = new EventEmitter<boolean>();

  // - V I E W   I N T E R A C T I O N
  public newTemplate(): void {
    this.router.navigate(['/gestionservicio/constructorplantilla'], { queryParams: { returnUrl: this.router.url } });
  }

  // // - D R A G   &  D R O P   S E C T I O N
  // public onDrop2DeleteAppointment(_targetData: any) {
  //   console.log(">>[ToolbarPanelComponent.onDrop2DeleteAppointment]");
  //   // Extract the data to be deleted. can be a single appointment or a day full of appointments.
  //   let payload = _targetData.dragData;
  //   if (null != payload) {
  //     let dataType = payload.jsonClass;
  //     if (null == dataType) {
  //       // This is a day and we have to extract the list of appointments.
  //       for (let event of payload.events) {
  //         let appointmentsPayload: Payload = event['payload'];
  //         if (null != appointmentsPayload) {
  //           let citas = appointmentsPayload.citas;
  //           this.backendService.backendDeleteAppointments(citas)
  //             .subscribe((deletedAppointments) => {
  //               this.appStoreService.successNotification("Citas borradas correctamente.", "Completado");
  //               this.update.emit(true);
  //             }), (error) => {
  //               // Process any 401 exception that means the session is no longer valid.
  //               if (error.status == 401) {
  //                 this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
  //                 this.router.navigate(['login']);
  //               }
  //               console.log("--[ToolbarPanelComponent.onDrop2DeleteAppointment]> Error: " + error.message);
  //               this.appStoreService.errorNotification("Error borrando citas libres. Mensaje: " + error.message, "¡Atención!");
  //             };
  //         }
  //       }
  //     } else {
  //       // This is a single appointment. Check if we can delete it because it is LIBRE.
  //       let appointmentTarget = payload;
  //       if (appointmentTarget.isFree()) {
  //         let citas: Cita[] = [];
  //         citas.push(appointmentTarget);
  //         this.backendService.backendDeleteAppointments(citas)
  //           .subscribe((deletedAppointments) => {
  //             this.appStoreService.successNotification("Cita borrada correctamente.", "Completado");
  //             this.update.emit(true);
  //           }), (error) => {
  //             // Process any 401 exception that means the session is no longer valid.
  //             if (error.status == 401) {
  //               this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
  //               this.router.navigate(['login']);
  //             }
  //             console.log("--[ToolbarPanelComponent.onDrop2DeleteAppointment]> Error: " + error.message);
  //             this.appStoreService.errorNotification("Error borrando citas libres. Mensaje: " + error.message, "¡Atención!");
  //           };
  //       }
  //     }
  //   }
  //   console.log("--[ToolbarPanelComponent.onDrop2DeleteAppointment]> _targetData: " + JSON.stringify(_targetData));
  // }
  // public onDrop2VacacionesAppointment(_targetData: any) {
  //   console.log(">>[ToolbarPanelComponent.onDrop2VacacionesAppointment]");
  //   // Extract the data to be deleted. can be a single appointment or a day full of appointments.
  //   let payload = _targetData.dragData;
  //   if (null != payload) {
  //     let dataType = payload.jsonClass;
  //     if (null == dataType) {
  //       // This is a day and we have to extract the list of appointments.
  //       for (let event of payload.events) {
  //         let appointmentsPayload: Payload = event['payload'];
  //         if (null != appointmentsPayload) {
  //           let citas = appointmentsPayload.citas;
  //           this.backendService.backendVacancyAppointments(citas)
  //             .subscribe((deletedAppointments) => {
  //               this.appStoreService.successNotification("Citas marcadas en vacaciones correctamente.", "Completado");
  //               this.update.emit(true);
  //             }), (error) => {
  //               // Process any 401 exception that means the session is no longer valid.
  //               if (error.status == 401) {
  //                 this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
  //                 this.router.navigate(['login']);
  //               }
  //               console.log("--[ToolbarPanelComponent.onDrop2VacacionesAppointment]> Error: " + error.message);
  //               this.appStoreService.errorNotification("Error marcando citas libres. Mensaje: " + error.message, "¡Atención!");
  //             };
  //         }
  //       }
  //     } else {
  //       // This is a single appointment. Check if we can delete it because it is LIBRE.
  //       let appointmentTarget = payload;
  //       if (appointmentTarget.isFree()) {
  //         let citas: Cita[] = [];
  //         citas.push(appointmentTarget);
  //         this.backendService.backendVacancyAppointments(citas)
  //           .subscribe((deletedAppointments) => {
  //             this.appStoreService.successNotification("Cita marcada como vacaciones correctamente.", "Completado");
  //             this.update.emit(true);
  //           }), (error) => {
  //             // Process any 401 exception that means the session is no longer valid.
  //             if (error.status == 401) {
  //               this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
  //               this.router.navigate(['login']);
  //             }
  //             console.log("--[ToolbarPanelComponent.onDrop2VacacionesAppointment]> Error: " + error.message);
  //             this.appStoreService.errorNotification("Error marcando citas libres. Mensaje: " + error.message, "¡Atención!");
  //           };
  //       }
  //     }
  //   }
  //   console.log("--[ToolbarPanelComponent.onDrop2VacacionesAppointment]> _targetData: " + JSON.stringify(_targetData));
  // }
}
