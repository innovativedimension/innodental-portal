//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- COMPONENTS
import { GestionCalendarioPageComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/gestion-calendario-page.component';
// --- MODELS
import { Perfil } from '@app/models/Perfil.model';
import { Medico } from '@models/Medico.model';

describe('PAGE GestionCalendarioPageComponent [Module: GESTION-SERVICIO]', () => {
  let component: GestionCalendarioPageComponent;
  let fixture: ComponentFixture<GestionCalendarioPageComponent>;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService }
      ],
      declarations: [GestionCalendarioPageComponent, RouteMockUpComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(GestionCalendarioPageComponent);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[gestion-servicio/GestionCommonPageComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
  });

  // - R E N D E R I N G   P H A S E
  describe('Rendering Phase', () => {
    it('Rendering Phase: some panels should be visible', () => {
      console.log('><[gestion-servicio/GestionCommonPageComponent]> Rendering Phase: some panels should be visible');
      console.log('><[gestion-servicio/GestionCommonPageComponent]> "activateSelection" should be true');
      expect(component.activateSelection).toBeTruthy();
      let testPanel = fixture.debugElement.query(By.css('#gs-header-panel'));
      console.log('><[gestion-servicio/GestionCommonPageComponent]> "gs-header-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
      console.log('><[gestion-servicio/GestionCommonPageComponent]> "ui-menu-bar"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#gs-service-select-panel'));
      console.log('><[gestion-servicio/GestionCommonPageComponent]> "gs-service-select-panel"');
      expect(testPanel).toBeDefined();
    });
    it('Rendering Phase: all panels should be visible', () => {
      console.log('><[gestion-servicio/GestionCommonPageComponent]> Rendering Phase: all panels should be visible');
      // Activate selection be setting a service
      let credential: Perfil = new Perfil();
      credential.allservices = true;
      let service: Medico = new Medico();
      appStoreService.storeCredential(credential);
      appStoreService.storeActiveService(service);

      let testcred = appStoreService.accessCredential();
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> credential: '
        + JSON.stringify(testcred));

      component.ngOnInit();
      expect(component.activateSelection).toBeFalsy('should be false');
      // Check again the list of panels visible
      let testPanel = fixture.debugElement.query(By.css('#gs-header-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "gs-header-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "ui-menu-bar"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#gs-service-select-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "gs-service-select-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#dc-toolbar-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "dc-toolbar-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#dc-month-calendar-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "dc-month-calendar-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#dc-template-list'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "dc-template-list"');
      expect(testPanel).toBeDefined();
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [onUpdateContents]', () => {
    it('onUpdateContents: sending messages to child panels', () => {
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> onUpdateContents: sending messages to child panels');
      // Activate selection be setting a service
      let credential: Perfil = new Perfil();
      credential.allservices = true;
      let service: Medico = new Medico();
      appStoreService.storeCredential(credential);
      appStoreService.storeActiveService(service);

      // COVERAGE. Children components need mock ups to test the code. No need 
      const spy = spyOn(component, 'onUpdateContents');
      component.onUpdateContents(null);
      expect(spy).toHaveBeenCalled();
    });
  });
  describe('Code Coverage Phase [onSelectedDate]', () => {
    it('onSelectedDate: setting the selected date', () => {
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> onSelectedDate: setting the selected date');
      // Activate selection be setting a service
      let credential: Perfil = new Perfil();
      credential.allservices = true;
      let service: Medico = new Medico();
      appStoreService.storeCredential(credential);
      appStoreService.storeActiveService(service);

      const spy = spyOn(component, 'onSelectedDate');
      component.onSelectedDate(new Date());
      expect(spy).toHaveBeenCalled();
    });
  });
});