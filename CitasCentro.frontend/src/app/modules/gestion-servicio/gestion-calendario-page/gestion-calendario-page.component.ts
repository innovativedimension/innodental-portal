// - CORE
import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
// - COMPONENTS
import { GestionCommonPageComponent } from '@app/modules/gestion-servicio/common/gestion-common-page.component';
import { CalendarFreeSlotsPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/calendar-free-slots-panel/calendar-free-slots-panel.component';
import { AppointmentListPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/appointment-list-panel/appointment-list-panel.component';

/**
 * This component is the core controller for the Service Management page. It will centralize the intercommunicactions between the different panels and manage whose of those panels should be visible at a ny time.
 * The process starts checking of there is already a stored service on the session. If not we should start selecting one. If there is already a service then we should change the display configuration to show another set of panels.
 *
 * @export
 * @class GestionServicioPageComponent
 * @extends {AppPanelComponent}
 * @implements {OnInit}
 * @implements {OnDestroy}
 */
@Component({
  selector: 'gestion-calendario-page',
  templateUrl: './gestion-calendario-page.component.html',
  styleUrls: ['./gestion-calendario-page.component.scss']
})
export class GestionCalendarioPageComponent extends GestionCommonPageComponent {
  @ViewChild(CalendarFreeSlotsPanelComponent) private calendarPanel: CalendarFreeSlotsPanelComponent;
  @ViewChild(AppointmentListPanelComponent) private appointmentListPanel: AppointmentListPanelComponent;

  private selectedDate: any;

  // - V I E W   I N T E R A C T I O N
  public onUpdateContents(_event: any): void {
    // Tell the calendar to update the contents reloading the selected service.
    this.calendarPanel.downloadAppointments(this.selectedService);
    this.appointmentListPanel.setSelectedDate(this.selectedDate);
  }
  // public onSelectedDate1(_selectedDate: Date): void {
  //   this.selectedDate = _selectedDate;
  //   this.appointmentListPanel.setSelectedDate(_selectedDate);
  // }
  public onSelectedDate(_selectedDate: any): void {
    this.selectedDate = _selectedDate;
    this.appointmentListPanel.setSelectedDate(_selectedDate);    
  }
}
