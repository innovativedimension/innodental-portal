//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
//--- CALENDAR
import { startOfDay } from 'date-fns';
import { isBefore } from 'date-fns';
//--- INTERFACES
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';
//--- MODELS
import { Cita } from '@models/Cita.model';
//--- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { WEEKDAYS } from '@app/modules/ui/app-panel/app-panel.component';
import { MONTHNAMES } from '@app/modules/ui/app-panel/app-panel.component';
import { Filter } from '@app/models/Filter.model';

@Component({
  selector: 'gs-appointment-list-panel',
  templateUrl: './appointment-list-panel.component.html',
  styleUrls: ['./appointment-list-panel.component.scss']
})
export class AppointmentListPanelComponent extends AppPanelComponent implements OnInit, OnDestroy {
  @Input() source: IAppointmentEnabled;
  public morningList: Cita[] = [];
  public eveningList: Cita[] = [];

  private _selectedDate: Date = new Date(); // The selected date used to filter the appointments to be shown.
  private _appointmentsSubscription: ISubscription; // The input subject for the appointments to process.
  private _appointments: Cita[] = []; // Local copy of the list of selected appointments. Loaded on subject.
  private _filters: any = [];

  //--- L I F E C Y C L E
  ngOnInit() {
    console.log(">>[AppointmentListComponent.ngOnInit]");
    // Access the last version of the appointments.
    this._appointmentsSubscription = this.appStoreService.accessAppointments()
      .subscribe((list) => {
        this._appointments = list;
      });
    // Define the processing filters.
    this._filters.push(new Filter()
      .setName('only today')
      .setDescription('Only show appointments for target date')
      .setFilter((_target: Cita): boolean => {
        let testDate = startOfDay(_target.getFecha());
        let year = testDate.getFullYear();
        let month = testDate.getMonth();
        let day = testDate.getDate();
        if (year == startOfDay(this._selectedDate).getFullYear())
          if (month == startOfDay(this._selectedDate).getMonth())
            if (day == startOfDay(this._selectedDate).getDate())
              return false;
        return true;
      }));
    // this._filters.push(new Filter()
    //   .setName('appointment > now')
    //   .setDescription('Filter out appointment in the past')
    //   .setFilter((_target: Cita): boolean => {
    //     return isBefore(_target.getFecha(), new Date());
    //   }));
    console.log("<<[AppointmentListComponent.ngOnInit]");
  }
  ngOnDestroy() {
    this._appointmentsSubscription.unsubscribe();
  }

  //--- D R A G   S E C T I O N
  public getPatientData(cita: Cita): string {
    if (cita.estado.toUpperCase() === "LIBRE".toUpperCase()) return "-LIBRE-";
    if (cita.estado.toUpperCase() === "BLOQUEADA".toUpperCase()) return "-EN PROCESO-";
    if (cita.estado.toUpperCase() === "RESERVADA".toUpperCase()) return cita.getPatientData();
    return "-LIBRE-";
  }
  public isLibre(cita: Cita): boolean {
    if (cita.estado.toUpperCase() === "LIBRE".toUpperCase()) return true;
    return false;
  }
  public isReservada(cita: Cita): boolean {
    if (cita.estado.toUpperCase() === "RESERVADA".toUpperCase()) return true;
    return false;
  }
  public isBloqueada(cita: Cita): boolean {
    if (cita.estado.toUpperCase() === "BLOQUEADA".toUpperCase()) return true;
    return false;
  }

  //--- I N T E R C O M M U N I C A T I O N   S E C T I O N
  public setSelectedDate(_newdate: any): void {
    this._selectedDate = _newdate.date;
    this.dataModelRoot = [];
    this.morningList = [];
    this.eveningList = [];

    // Load the payload list of appointments.
    this._appointments = _newdate.events[0]['payload'].citas;
    // Process the list of current appointments (for the whole service) and keep future items.
    for (let cita of this._appointments) {
      let filtered = this.applyFilters(cita);
      if (filtered) continue;
      // this.dataModelRoot.push(cita);
      if (cita.zonaHoraria === 'MAÑANA')
        this.morningList.push(cita);
      if (cita.zonaHoraria === 'TARDE')
        this.eveningList.push(cita);
    }
    this.notifyDataChanged();
  }

  //--- T A R G E T   D A T A   A C C E S S O R S
  public getTarget(): any {
    if (null != this.source) return this.source;
  }
  public getTargetClass(): string {
    if (null != this.source) return this.source.getJsonClass();
    else return '-UNDEFINED-';
  }
  public getSelectedDate(): Date {
    return this._selectedDate;
  }
  public getAppointmentDateDisplay(_date: Date): string {
    let display = "";
    display = display + WEEKDAYS[_date.getDay()] + ', ';
    display = display + _date.getDate() + ' ';
    display = display + MONTHNAMES[_date.getMonth()] + ' ';
    display = display + _date.getFullYear();
    return display;
  }

  //--- F I L T E R I N G
  /**
   * This will apply all the filters defined in sequence. If any of the fiters drops the appoitment then we do not continue and we terminate the process with a true.
   * If we reach the end of the filters list and the appointment still is present then we return a false.
   * @param  _target the appointment to be checked.
   * @return         <b>true</b> if the appointment has failed the filters and should be discarded. <b>false</b> if the appointmet has to be kept.
   */
  public applyFilters(_target: Cita): boolean {
    for (let filter of this._filters) {
      let filterResult = filter.filterFunction(_target);
      if (filterResult) return true;
    }
    return false;
  }
}
