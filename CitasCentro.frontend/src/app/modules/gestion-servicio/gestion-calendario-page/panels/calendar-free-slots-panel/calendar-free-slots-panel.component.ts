//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component, EventEmitter, Output } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { OnChanges } from '@angular/core';
import { Input } from '@angular/core';
import { ISubscription } from "rxjs/Subscription";
//--- CALENDAR
import { isBefore } from 'date-fns';
//--- INTERFACES
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';
// import { IViewer } from '@models/Medico.model';
import { ICalendarPageContainer } from '@app/interfaces/ICalendarPageContainer.interface';
//--- COMPONENTS
import { CalendarCoreComponent } from '@app/modules/ui/calendar-core/calendar-core.component';
//--- MODELS
import { Cita } from '@models/Cita.model';
import { Payload } from '@models/Payload.model';

@Component({
  selector: 'gs-calendar-free-slots-panel',
  templateUrl: './calendar-free-slots-panel.component.html',
  styleUrls: ['./calendar-free-slots-panel.component.scss']
})
export class CalendarFreeSlotsPanelComponent extends CalendarCoreComponent implements OnInit, OnDestroy {
  @Input() container: ICalendarPageContainer;
  private _sourceShader: IAppointmentEnabled;
  @Input() set source(_newsource: IAppointmentEnabled) {
    this._sourceShader = _newsource;
    // If the source is not null then reload from the backend the list of appointments.
    if (null != this._sourceShader) this.downloadAppointments(this._sourceShader);
  }
  @Output() selectedDate = new EventEmitter<Date>();

  private _appointmentsSubscription: ISubscription;
  private filters: any = [];
  private _intervalHolder: any; // The variable to perform the periodic refresh.

  //--- L I F E C Y C L E
  ngOnInit() {
    console.log("><[CalendarFreeSlotsPanelComponent.ngOnInit]");
    // Define the filters.
    // WARNING. Setting filters on the Management calendar imposes limits.
    // this.filters.push({
    //   name: 'appointment < now',
    //   description: 'Filter appointment in the past',
    //   filter: (_target: Cita): boolean => {
    //     return isBefore(_target.getFecha(), new Date());
    //   }
    // });

    // Activate the automatic update of the appointments. 60 seconds
    if (null != this._intervalHolder)
      this._intervalHolder = setInterval(() => {
        if (null != this._sourceShader) this.downloadAppointments(this._sourceShader);
      }, 1000 * 60); // 1 minute

    // The other only action to be performed on initialization is a watch on the list of appointments to update the calendar when that list changes, be it because of a periodic refresh, manual refresh or the selection of another target.
    this.accessAppointments();

    // If we are initializing and the source is set then we should start getting the list of appointments.
    // No need since the setter will trigger this event.
    // if (null != this._sourceShader) this.downloadAppointments(this._sourceShader);
  }
  ngOnDestroy() {
    // if (null != this._activeServiceSubscription) this._activeServiceSubscription.unsubscribe();
    if (null != this._appointmentsSubscription) this._appointmentsSubscription.unsubscribe();
  }
  public hasVacancies(_day: any): boolean {
    if (null != _day) if (null != _day.events[0]) if (null != _day.events[0].vacancy)
      return (_day.events[0].vacancy > 0);
  }

  // - S U B S C R I P T I O N   M A N A G E M E N T
  /**
   * Connect a watch on the list of appointments. Each time that list is changed this code will activate and refresh the calendar contents with the new processed list of appointments.
   *
   * @memberof CalendarFreeSlotsPanelComponent
   */
  public accessAppointments(): void {
    console.log(">>[CalendarFreeSlotsPanelComponent.accessAppointments]");
    // Signal to the display that we are doing a lengthy operation.
    this.downloading = true;
    // Get the current list of appointments from the subject each time it changes.
    this._appointmentsSubscription = this.appStoreService.accessAppointments()
      .subscribe((appointmentList) => {
        // Skip this step if the numbe of appointments is ZERO. This an empty start.
        if (appointmentList.length > 0) {
          // Sort the list.
          let sortedAppointments = appointmentList.sort((n1: Cita, n2: Cita) => {
            if (isBefore(n1.getFecha(), n2.getFecha())) return -1;
            else return 1;
          });
          // Clear calendar internal data.
          this.eventList = new Map<string, Payload>();
          this.citaList = [];
          for (let cita of sortedAppointments) {
            // Apply the list of filters to drop appointments that should not be accesible.
            let filtered = this.applyFilters(cita);
            if (filtered) continue;
            this.citaList.push(cita);
            // Do the accounting to generate the events.
            this.accountAppointment(cita);
          }
          this.processEvents();
          this.downloading = false;
          this.appStoreService.infoNotification("Descarga de citas disponibles completado.", "Completado");
          console.log("<<[CalendarFreeSlotsPanelComponent.accessAppointments]");
        }
      }), (error) => {
        // Process any 401 exception that means the session is no longer valid.
        // if (error.status == 401) {
        //   this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
        //   this.router.navigate(['login']);
        // } else {
        console.log("--[NewActoMedicoDoctorPanelComponent.onSubmit]> Error: " + error.message);
        this.appStoreService.processBackendError(error);
        // }
      };
  }

  //--- C A L E N D A R   E V E N T   M A N A G E M E N T
  public activateRefresh(): void {
    console.log("><[CalendarFreeSlotsPanelComponent.activateRefresh]");
    this.downloadAppointments(this._sourceShader);
  }

  // - B A C K E N D   A C C E S S
  public downloadAppointments(_target: IAppointmentEnabled): void {
    console.log(">>[CalendarFreeSlotsPanelComponent.downloadAppointments]");
    if (null != _target) {
      this.downloading = true;
      let serviceIdentifiers: string[] = _target.getServiceIdentifiers();
      let appointments = [];
      for (let identifier of serviceIdentifiers) {
        // Download the data about the appointments already reserved.
        this.backendService.backendCitas4MedicoById(identifier)
          .subscribe((citas) => {
            console.log("--[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]> count: " + citas.length);
            // Aggregate the data from multiples identifiers.
            appointments = appointments.concat(citas);
            // Store the processed appointements on the AppStore for easy access by other components.
            console.log("--[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]> Storing: " + appointments.length + " appointments.");
            this.appStoreService.storeAppointments(appointments);
            this.downloading = false;
            console.log("<<[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]");
          }), (error) => {
            // Process any 401 exception that means the session is no longer valid.
            if (error.status == 401) {
              this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
              this.router.navigate(['login']);
            }
            console.log("--[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]> Error: " + error.message);
            this.appStoreService.errorNotification("Error actualizando Médico/Servicio. Mensaje: " + error.message, "¡Atención!");
          };
      }
    }
  }
  public closeCalendar(): void {
    this.show = false;
    this.container.showCalendar = false;
  }

  //--- I N T E R C O M M U N I C A T I O N   S E C T I O N
  /**
   * This method processes the click on a calendar date cell. We first have to check if the cell has appointments before doing anything else like open another panel. Tjis way we avoid opening empty panels.
   *
   * @param _event the angular event that is received by the calendar cell.
   */
  public dayClicked(_event: any): void {
    console.log("><[AppointmentDateSelectorComponent.dayClicked]> event: " + JSON.stringify(_event));
    // If date has free appointments then open the appointment list panel.
    if (_event.day.badgeTotal > 0) {
      this.clearSelection();
      this.add2Selection(_event.day);
      this.selectedDate.emit(_event.day.date);
      // this.container.openAppointmentList(_event.day.date/*, this.pendingSelected.node*/);
    }
  }

  //--- T A R G E T   D A T A   A C C E S S O R S
  public getTarget(): any {
    if (null != this._sourceShader) return this._sourceShader;
  }
  public getTargetClass(): string {
    if (null != this._sourceShader) return this._sourceShader.getJsonClass();
    else return '-UNDEFINED-';
  }

  //--- F I L T E R I N G
  /**
   * This will apply all the filters defined in sequence. If any of the fiters drops the appoitment then we do not continue and we terminate the process with a true.
   * If we reach the end of the filters list and the appointment still is present then we return a false.
   * @param  _target the appointment to be checked.
   * @return         <b>true</b> if the appointment has failed the filters and should be discarded. <b>false</b> if the appointmet has to be kept.
   */
  public applyFilters(_target: Cita): boolean {
    for (let filter of this.filters) {
      let filterResult = filter.filter(_target);
      if (filterResult) return true;
    }
    return false;
  }
}
