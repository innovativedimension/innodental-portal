import { Component, OnInit } from '@angular/core';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// import { AppPanelComponent } from '@app/components/app-panel/app-panel.component';

@Component({
  selector: 'gestion-dashboard-page',
  templateUrl: './gestion-dashboard-page.component.html',
  styleUrls: ['./gestion-dashboard-page.component.scss']
})
export class GestionDashboardPageComponent extends AppPanelComponent {
  public activatePage(_pageName : string):void {
    this.router.navigate([_pageName]);
  }
}
