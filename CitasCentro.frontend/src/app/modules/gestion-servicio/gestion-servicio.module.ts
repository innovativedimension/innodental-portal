// - CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// - ROUTING
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
// - TOAST NOTIFICATIONS
// - DRAG & DROP
import { NgDragDropModule } from 'ng-drag-drop';
// - CALENDAR
import { CalendarModule } from 'angular-calendar';
// - IONIC
import { IonicModule } from 'ionic-angular';
// - APPLICATION LIBRARIES
// import { CitaMedLibModule } from '@citamed-lib';
// - APPLICATION MODULES
import { UIModule } from '../../modules/ui/ui.module';
// import { LibModule } from '@app/modules/lib/lib.module';

// - GUARDS
import { AuthManagerGuard } from '@app/modules/authorization/auth-manager.guard';

// - PAGES
import { GestionDashboardPageComponent } from './gestion-dashboard-page/gestion-dashboard-page.component';
import { GestionCalendarioPageComponent } from './gestion-calendario-page/gestion-calendario-page.component';
import { CalendarDefinitionPageComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/definicion-calendario-page.component';
import { TemplateConstructorPageComponent } from '@app/modules/gestion-servicio/template-constructor-page/template-constructor-page.component';
import { LimitsSetupPageComponent } from './pages/limits-setup-page/limits-setup-page.component';
// - PANELS - COMMON
import { GestionCommonPageComponent } from '@app/modules/gestion-servicio/common/gestion-common-page.component';
import { ServiceSelectionPanelComponent } from './panels/service-selection-panel/service-selection-panel.component';
// - PANELS - DEFINICION-CALENDARIO
import { TemplateListComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/panels/template-list/template-list.component';
import { MonthCalendarPanelComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/panels/month-calendar-panel/month-calendar-panel.component';
import { DefinicionToolbarPanelComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/panels/toolbar-panel/toolbar-panel.component';
// - PANELS - TEMPLATE CONSTRUCTOR
import { TemplateBuildBlocksComponent } from '@app/modules/gestion-servicio/template-constructor-page/panels/template-build-blocks/template-build-blocks.component';
import { TemplateUnderConstructionComponent } from '@app/modules/gestion-servicio/template-constructor-page/panels/template-under-construction/template-under-construction.component';
// - PANELS - GESTION CALENDARIO
import { AppointmentListPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/appointment-list-panel/appointment-list-panel.component';
import { CalendarFreeSlotsPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/calendar-free-slots-panel/calendar-free-slots-panel.component';
import { ToolbarPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/toolbar-panel/toolbar-panel.component';


import { LimitsActivePanelComponent } from './panels/limits-active-panel/limits-active-panel.component';
import { LimitsAvailablePanelComponent } from './panels/limits-available-panel/limits-available.component';
// - COMPONENTS
import { LimitadorComponent } from './pages/limits-setup-page/components/limitador/limitador.component';
import { LimitadorBlockingComponent } from './pages/limits-setup-page/components/limitador-blocking/limitador-blocking.component';
import { LimitadorPercentageComponent } from './pages/limits-setup-page/components/limitador-percentage/limitador-percentage.component';
import { ServiceSelectPanelComponent } from '@app/modules/gestion-servicio/panels/service-select-panel/service-select-panel.component';
// import { AuthManagerGuard } from '@app/auth-manager.guard';
import { HeaderPanelComponent } from './panels/header-panel/header-panel.component';
import { SharedModule } from '@app/modules/shared/shared.module';
import { CalendarPanelV2Component } from './gestion-calendario-page/panels/calendar-panel-v2/calendar-panel-v2.component';
import { CalendarScreenSizeAdapter } from './gestion-calendario-page/panels/calendar-panel-v2/calendar-screensize-adapter.component';

// --- MODULE ROUTES
const routes: Routes = [
  { path: '', component: GestionDashboardPageComponent },
  { path: 'gestioncalendario', component: GestionCalendarioPageComponent },
  { path: 'definicionlimites', component: LimitsSetupPageComponent },
  { path: 'definicioncalendario', component: CalendarDefinitionPageComponent },
  { path: 'constructorplantilla', component: TemplateConstructorPageComponent, canActivate: [AuthManagerGuard] },
  { path: 'constructorplantilla/:templateId', component: TemplateConstructorPageComponent, canActivate: [AuthManagerGuard] }
];

@NgModule({
  imports: [
    //--- CORE
    CommonModule,
    //--- ROUTING
    RouterModule.forChild(routes),
    //--- TOAST NOTIFICATIONS
    // SimpleNotificationsModule.forRoot(),
    //--- DRAG & DROP
    NgDragDropModule.forRoot(),
    //--- CALENDAR
    CalendarModule.forRoot(),
    //--- IONIC
    IonicModule.forRoot({}),
    //--- APPLICATION MODULES
    UIModule,
    // CitaMedLibModule,
    SharedModule
  ],
  declarations: [
    //--- PAGES
    GestionDashboardPageComponent,
    GestionCalendarioPageComponent,
    CalendarDefinitionPageComponent,
    TemplateConstructorPageComponent,
    LimitsSetupPageComponent,
    // --- PANELS - COMMON
    GestionCommonPageComponent,
    ServiceSelectionPanelComponent,
    //--- PANELS - GESTION-CALENDARIO
    AppointmentListPanelComponent,
    CalendarFreeSlotsPanelComponent,
    ToolbarPanelComponent,
    CalendarScreenSizeAdapter,
    CalendarPanelV2Component,

    //--- PANELS - DEFINICION-CALENDARIO
    //  CitaMonthPanelComponent,
    TemplateListComponent,
    MonthCalendarPanelComponent,
    DefinicionToolbarPanelComponent,

    // --- PANELS - TEMPLATE CONSTRUCTOR
    TemplateBuildBlocksComponent,
    TemplateUnderConstructionComponent,

    // CitaMonthPanelComponent,

    //--- COMPONENTS
    LimitsActivePanelComponent,
    LimitsAvailablePanelComponent,
    LimitadorComponent,
    LimitadorBlockingComponent,
    LimitadorPercentageComponent,
    ServiceSelectPanelComponent,
    HeaderPanelComponent
  ],
  exports: [
    RouterModule,
    // - Export for Visual Testing
    ServiceSelectionPanelComponent
    //   ConsultaCitasPageComponent,
    //   // UnselectedServicePageComponent,
    //   LimitsSetupPageComponent
  ]
})
export class GestionServicioModule { }
