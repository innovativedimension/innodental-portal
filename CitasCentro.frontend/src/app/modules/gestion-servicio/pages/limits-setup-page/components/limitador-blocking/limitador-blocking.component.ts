//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
// import { OnChanges } from '@angular/core';
// import { SimpleChanges } from '@angular/core';
// import { Input } from '@angular/core';
// import { Inject } from '@angular/core';
// //--- ENVIRONMENT
// import { environment } from 'app/../environments/environment';
// //--- ROUTER
// import { Router } from '@angular/router';
// //--- NOTIFICATIONS
// //import { NotificationsService } from 'angular2-notifications';
// //--- WEBSTORAGE
// import { LOCAL_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
// //--- CALENDAR
// import { startOfDay } from 'date-fns';
//--- INTERFACES
// import { IViewer } from '@citamed-lib';
//--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
import { LimitadorComponent } from '../limitador/limitador.component';
//--- INTERFACES
// import { INode } from '@citamed-lib';
// import { IDataSource } from '@citamed-lib';
// //--- MODELS
// import { DailyAppointmentsTemplate } from 'app/models/DailyAppointmentsTemplate.model';
// import { Cita } from '@citamed-lib';
// import { Limitador } from '@citamed-lib';

@Component({
  selector: 'limitador-blocking',
  templateUrl: './limitador-blocking.component.html',
  styleUrls: ['./limitador-blocking.component.scss']
})
export class LimitadorBlockingComponent extends LimitadorComponent {
}
