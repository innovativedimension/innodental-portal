//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnChanges } from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { Input } from '@angular/core';
// import { Inject } from '@angular/core';
// import { map } from 'rxjs/operators';
// //--- ENVIRONMENT
// import { environment } from 'app/../environments/environment';
// //--- ROUTER
// import { Router } from '@angular/router';
// //--- NOTIFICATIONS
// //import { NotificationsService } from 'angular2-notifications';
// //--- WEBSTORAGE
// import { LOCAL_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
// //--- CALENDAR
// import { startOfDay } from 'date-fns';
//--- INTERFACES
// import { IViewer, Limitador } from '@citamed-lib';
//--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
import { LimitadorComponent } from '../limitador/limitador.component';
// import { FormControl, FormGroup } from '@angular/forms';
import { LimitsActivePanelComponent } from '@app/modules/gestion-servicio/panels/limits-active-panel/limits-active-panel.component';

@Component({
  selector: 'limitador-percentage',
  templateUrl: './limitador-percentage.component.html',
  styleUrls: ['./limitador-percentage.component.scss']
})
export class LimitadorPercentageComponent extends LimitadorComponent implements OnInit {
  // @Input() node : Limitador;
  @Input() container: LimitsActivePanelComponent;

  // private aseguradoraControl = new FormControl('');
  // private valorControl = new FormControl('');
  // private estadoControl = new FormControl('');
  // public limitsForm: FormGroup;
  public aseguradoras: string[];

  ngOnInit() {
    // this.aseguradoraControl.registerOnChange((value) => {
    //   this.container.setDirty(true);
    //   this.node.valorReferencia = value;
    // });
    // this.valorControl.registerOnChange((value) => {
    //   this.container.setDirty(true);
    //   this.node.valorLimite = value;
    // });
    // this.estadoControl.registerOnChange((value) => {
    //   this.container.setDirty(true);
    //   this.node.activeState = value;
    // });
    // this.limitsForm = new FormGroup({
    //   aseguradora: this.aseguradoraControl,
    //   valor: this.valorControl,
    //   estado: this.estadoControl
    // });

    // Load the list of insurance corporations.
    this.appStoreService.propertiesAseguradoras()
      .subscribe((_aseguradoras) => {
        this.aseguradoras = _aseguradoras;
      });

      // Load the node values into the form ans start watching for changes.
      // this.aseguradoraControl.setValue(this.node.valorReferencia);
      // this.valorControl.setValue(this.node.valorLimite);
      // this.valorControl.setValue(this.node.activeState);
    // Set up the list of valid pecentages.
    // this.validValues = Array(9).fill().pipe(map((x, i) => (i + 1) * 10));
    // this.limitsForm.subscribe((aseguradora) => {
    //   // Detect changes to update the dirty state.
    //   this.getContainer().setDirty(true);
    // })
  }
  public onFormBlur():void{
    this.container.setDirty(true);
  }
}
