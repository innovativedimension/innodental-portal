//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// import { OnChanges } from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { Input } from '@angular/core';
// import { Inject } from '@angular/core';
// //--- ENVIRONMENT
// import { environment } from '@env/environment';
// //--- ROUTER
// import { Router } from '@angular/router';
// //--- NOTIFICATIONS
// //import { NotificationsService } from 'angular2-notifications';
// //--- WEBSTORAGE
// import { LOCAL_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
// //--- CALENDAR
// import { startOfDay } from 'date-fns';
//--- INTERFACES
// import { IViewer } from '@models/Limitador.model';
//--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
// //--- COMPONENTS
// import { MVCViewerComponent } from '@UIModule/mvcviewer/mvcviewer.component';
//--- INTERFACES
// import { INode } from '@models/Limitador.model';
import { IDataSource } from '@interfaces/core/IDataSource.interface';
//--- MODELS
// import { DailyAppointmentsTemplate } from 'app/models/DailyAppointmentsTemplate.model';
// import { Cita } from '@models/Limitador.model';
import { Limitador } from '@models/Limitador.model';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// import { AppPanelComponent } from '@app/components/app-panel/app-panel.component';

@Component({
  selector: 'notused-limitador',
  templateUrl: './notused.html'
})
export class LimitadorComponent extends AppPanelComponent implements OnInit {
  @Input() viewer: IDataSource;
  @Input() node: Limitador;
  // @Input() variant: string;
  private previousNode: Limitador;

  public getIconPath(): string {
    return this.node.icon;
  }
  public removeLimit(): void {
    this.viewer.removeNode(this.node);
  }
  //--- L I F E C Y C L E
  ngOnInit() {
    this.previousNode = new Limitador(this.node);
  }
  ngDoCheck() {
    let check = this.node.hasEqualValues(this.previousNode);
    check = true;
    if (!check) {
      this.previousNode = new Limitador(this.node);
      this.viewer.updateNode(this.node);
    }
  }
}
