//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
// --- COMPONENTS
// import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// import { Medico } from '@citamed-lib';
import { GestionCommonPageComponent } from '@app/modules/gestion-servicio/common/gestion-common-page.component';

@Component({
  selector: 'limits-setup-page',
  templateUrl: './limits-setup-page.component.html',
  styleUrls: ['./limits-setup-page.component.scss']
})
export class LimitsSetupPageComponent extends GestionCommonPageComponent /*implements OnInit*/ {
  // private _selectedService: Medico;

  // - L I F E C Y C L E
  // ngOnInit() {
  //   console.log(">>[LimitsSetupPageComponent.ngOnInit]");
  //   this._selectedService = this.appStoreService.accessActiveService();
  //   if (null == this._selectedService) {
  //     // Hide the main panels and open the selecion panel.
  //     this.router.navigate(['gestionsservicio/gestioncitas'])
  //   } else {
  //     // this.selectedService = service;
  //     // this.activateSelection = false;
  //   }
  // }
  // - V I E W   I N T E R A C T I O N
  // public onServiceSelected(_selectedService: Medico): void {
  //   if (null != _selectedService) {
  //     this._selectedService = _selectedService;
  //     // this.activateSelection = false;
  //     // Activate the downlad and presentation of the data on the calendar.
  //     // The calendar is not available if it is not visible. So this communication way does not work. But it works anyway.
  //     // if (null != this.calendarPanel) this.calendarPanel.searchAppointments(_selectedService);
  //   }
  // }
  // public getSelectedService(): Medico {
  //   return this._selectedService;
  // }
}
