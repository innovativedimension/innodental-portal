import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Credencial } from '@models/Credencial.model';
// import {  IAppointmentEnabled } from '@models/Medico.model';
import {  Medico } from '@models/Medico.model';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'gs-header-panel',
  templateUrl: './header-panel.component.html',
  styleUrls: ['./header-panel.component.scss']
})
export class HeaderPanelComponent extends AppPanelComponent implements OnInit {
  @Input() source: Medico;
  @Output() activation = new EventEmitter<boolean>();
  public credencial: Credencial = new Credencial();

  ngOnInit() {
    this.credencial = this.appStoreService.accessCredential();
  }
  public getCredentialName(): string {
    let name = this.credencial.getNombre() + " " + this.credencial.getApellidos();
    if (environment.development) name = name + " [" + this.credencial.getId() + "]";
    return name;
  }
  public isServiceActive(): boolean {
    if (null != this.source) return true;
    else return false;
  }
  public getSelectedService(): Medico {
    return this.source;
  }
  public activateSelector(): void {
    this.activation.emit(true);
  }
}
