//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// import { Input } from '@angular/core';
// import { Inject } from '@angular/core';
// //--- ENVIRONMENT
// import { environment } from 'app/../environments/environment';
// //--- ROUTER
// import { Router } from '@angular/router';
// //--- NOTIFICATIONS
// //import { NotificationsService } from 'angular2-notifications';
// //--- WEBSTORAGE
// import { LOCAL_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
// //--- CALENDAR
// import { startOfDay } from 'date-fns';
//--- INTERFACES
// import { IViewer } from '@models/Medico.model';
//--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
// import { MVCViewerComponent } from '@UIModule/mvcviewer/mvcviewer.component';
//--- INTERFACES
import { INode } from '@interfaces/core/INode.interface';
import { IDataSource } from '@interfaces/core/IDataSource.interface';
//--- MODELS
// import { Cita } from '@models/Medico.model';
// import { Credencial } from '@models/Medico.model';
// import { DailyAppointmentsTemplate } from 'app/models/DailyAppointmentsTemplate.model';
import { Limitador } from '@models/Limitador.model';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'gs-limits-active-panel',
  templateUrl: './limits-active-panel.component.html',
  styleUrls: ['./limits-active-panel.component.scss']
})
export class LimitsActivePanelComponent extends AppPanelComponent implements OnInit, IDataSource {
  public saving: boolean = false;
  public dirty: boolean = true;
  // private credencial: Credencial = new Credencial();

  //--- L I F E C Y C L E
  ngOnInit() {
    // Get the current active service.
    let service = this.appStoreService.accessActiveService();
    if (null == service) this.router.navigate(['gestionservicio']);
    // Download from the backend the list of limits registered to this profile.
    this.backendService.backendLimits4Service(service)
      .subscribe((limits) => {
        // Process the limits read to the mvc render list.
        this.dataModelRoot = [];
        for (let limit of limits) {
          this.dataModelRoot.push(limit);
        }
        this.notifyDataChanged();
      }, (error) => {
        console.log("E-[LimitsActivePanelComponent.ngOnInit]> error: " + error.message);
      });
  }

  //--- F O R M   E V E N T S
  public setDirty(_dirtyValue: boolean): void {
    this.dirty = _dirtyValue;
  }
  public onTemplateDrop(_event: any): void {
    console.log("><[LimitsActivePanelComponent.onTemplateDrop]> event: " + JSON.stringify(_event));
    // Receive the data and add to the list pending configuration for insurance corporation items that get default values.
    // MOCKUP - The data is stored on the LocalStorage for the mockup demo.
    if (null != _event.dragData) {
      this.dataModelRoot.push(_event.dragData);
      this.notifyDataChanged();
      this.dirty = true;
      // MOCKUP - store the result on the LocalStore.
      // this.storage.set('LOCALSTORAGE::credencial-limits' + ':' + this.credencial.getId(),
      //   JSON.stringify(this.dataModelRoot));
    }
  }
  public removeNode(_node: INode): boolean {
    let limit = new Limitador(_node);
    let result = [];
    for (let node of this.dataModelRoot) {
      if (limit.hasEqualValues(new Limitador(node))) continue;
      else result.push(node);
    }
    this.dataModelRoot = result;
    this.notifyDataChanged();
    this.dirty = true;
    // MOCKUP - store the result on the LocalStore.
    // this.storage.set('LOCALSTORAGE::credencial-limits' + ':' + this.credencial.getId(),
    //   JSON.stringify(this.dataModelRoot));
    return true;
  }
  public updateNode(_node: INode): boolean {
    let limit = new Limitador(_node);
    let result = [];
    for (let node of this.dataModelRoot) {
      if (limit.hasEqualValues(new Limitador(node))) result.push(limit);
      else result.push(node);
    }
    this.dataModelRoot = result;
    this.notifyDataChanged();
    this.dirty = true;
    // MOCKUP - store the result on the LocalStore.
    // this.storage.set('LOCALSTORAGE::credencial-limits' + ':' + this.credencial.getId(),
    //   JSON.stringify(this.dataModelRoot));
    return true;
  }
  public saveLimits(): void {
    this.saving = true;
    // Get the current active service.
    let service = this.appStoreService.accessActiveService();
    if (null == service) this.router.navigate(['gestionservicio']);
    // Save the limits data to the backend.
    this.backendService.backendSaveLimits4Service(service, this.dataModelRoot as Limitador[])
      .subscribe((limits) => {
        // Process the limits read to the mvc render list.
        this.dataModelRoot = [];
        for (let limit of limits) {
          this.dataModelRoot.push(limit);
        }
        this.notifyDataChanged();
        this.appStoreService.successNotification("Limites de presentación de citas salvados satisfactoriamente.", "¡Exito!");
        // TODO - Activate when I am aple to detect changes on components all the time.
        // this.dirty = false;
        this.saving = false;
      }, (error) => {
        console.log("E-[LimitsActivePanelComponent.ngOnInit]> error: " + error.message);
      });
    // MOCKUP - store the result on the LocalStore.
    // this.storage.set('LOCALSTORAGE::credencial-limits' + ':' + this.credencial.getId(),
    //   JSON.stringify(this.dataModelRoot));
  }
}
