//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// import { Input } from '@angular/core';
// import { Inject } from '@angular/core';
// //--- ENVIRONMENT
// import { environment } from 'app/../environments/environment';
// //--- ROUTER
// import { Router } from '@angular/router';
// //--- NOTIFICATIONS
// //import { NotificationsService } from 'angular2-notifications';
// //--- WEBSTORAGE
// import { LOCAL_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
// //--- CALENDAR
// import { startOfDay } from 'date-fns';
//--- INTERFACES
// import { IViewer } from 'citamed-lib';
//--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
// //--- COMPONENTS
// import { MVCViewerComponent } from '@UIModule/mvcviewer/mvcviewer.component';
//--- MODELS
// import { DailyAppointmentsTemplate } from 'app/models/DailyAppointmentsTemplate.model';
// import { Cita } from 'citamed-lib';
// import { AppPanelComponent } from '@app/components/app-panel/app-panel.component';
// import { FormControl } from '@angular/forms';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'gs-limits-available-panel',
  templateUrl: './limits-available.component.html',
  styleUrls: ['./limits-available.component.scss']
})
export class LimitsAvailablePanelComponent extends AppPanelComponent implements OnInit {

  ngOnInit() {
    console.log(">>[LimitsAvailablePanelComponent.ngOnInit]");
    // Read the list of Limits from the data provider.
    this.appStoreService.propertiesLimitadores()
      .subscribe((limitList) => {
        // Insert the list of limits into the model renderer.
        for (let limit of limitList) {
          this.dataModelRoot.push(limit);
        }
        this.notifyDataChanged();
      });
    console.log("<<[LimitsAvailablePanelComponent.ngOnInit]");
  }

  //--- C O N T E N T   A D A P T E R S
}
