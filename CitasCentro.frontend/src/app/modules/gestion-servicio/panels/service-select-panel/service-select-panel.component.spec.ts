//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { throwError } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { inject } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- COMPONENTS
import { ServiceSelectPanelComponent } from '@app/modules/gestion-servicio/panels/service-select-panel/service-select-panel.component';
import { Perfil } from '@app/models/Perfil.model';
import { Centro } from '@models/Centro.model';
import { Medico } from '@models/Medico.model';

// - S E R V I C E   S E L E C T   P A N E L - V 1
describe('PANEL ServiceSelectPanelComponent [Module: LOGIN]', () => {
  let component: ServiceSelectPanelComponent;
  let fixture: ComponentFixture<ServiceSelectPanelComponent>;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [ServiceSelectPanelComponent, RouteMockUpComponent],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService }
      ]
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(ServiceSelectPanelComponent);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[gestion-servicio/ServiceSelectPanelComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[login/ServiceSelectPanelComponent]> "leftContent" should be undefined');
      expect(component.leftContent).toBeUndefined();
    });
  });

  // - L I F E C Y C L E   P H A S E
  describe('Lifecycle Phase', () => {
    it('Lifecycle: OnInit -> get the list of services', async(() => {
      console.log('><[gestion-servicio/ServiceSelectPanelComponent]> Lifecycle: OnInit -> get the list of services');
      let componentAsAny = component as any;
      // Check that initial state is the expected.
      expect(component.downloading).toBeFalsy('while not processing the "downloading" is false');
      expect(component.dataModelRoot.length).toBe(0, 'model list should be empty');
      expect(component.renderNodeList.length).toBe(0, 'render list should be empty');
      // Prepare the data so the parameters are read with the right values.
      let credential = new Perfil();
      credential.centro = new Centro({
        id: 100001
      });
      credential.allservices = true;
      appStoreService.storeCredential(credential);
      // Block on the access to the list because the mock up.
      component.ngOnInit();
      expect(component.downloading).toBeTruthy('started the processing so "downloading" is true');
      // Give the data for the processing.
      appStoreService.downloadDoctors(appStoreService.directAccessMockResource('accessdoctors'));
      expect(component.downloading).toBeFalsy('cmpleted processing so "downloading" is false');
      expect(component.dataModelRoot.length).toBe(9, 'model list should match the number of services of 9');
      expect(component.renderNodeList.length).toBe(9, 'render list should be have 9 elements also');
    }));
    it('Lifecycle: OnInit -> process 404 response', async(() => {
      console.log('><[gestion-servicio/ServiceSelectPanelComponent]> Lifecycle: OnInit ->  process 404 response');
      let componentAsAny = component as any;
      // Check that initial state is the expected.
      expect(component.downloading).toBeFalsy('while not processing the "downloading" is false');
      expect(component.dataModelRoot.length).toBe(0, 'model list should be empty');
      expect(component.renderNodeList.length).toBe(0, 'render list should be empty');
      // Prepare the data so the parameters are read with the right values.
      let credential = new Perfil();
      credential.centro = new Centro({
        id: 100002
      });
      credential.allservices = true;
      appStoreService.storeCredential(credential);
      // Insall the mock to process an exception.
      let exception = new HttpErrorResponse({
        error: '404 Not Found',
        status: 404,
        statusText: '404 Not Found',
        url: '/api/v1/centros/100001/medicos'
      });
      const mockCall = spyOn(appStoreService, 'accessDoctors')
        .and.returnValue(throwError(exception));
      // On this call there is no blocking. The exception is returned by previous line.
      component.ngOnInit();
      // expect(component.downloading).toBeTruthy('started the processing so "downloading" is true');
      // Give the data for the processing.
      // appStoreService.injectException(exception);
      expect(component.downloading).toBeFalsy('cmpleted processing so "downloading" is false');
      expect(component.dataModelRoot.length).toBe(0, 'model list should be empty');
      expect(component.renderNodeList.length).toBe(0, 'render list should be empty');
    }));
    it('Lifecycle: OnInit -> process 500 response', async(() => {
      console.log('><[gestion-servicio/ServiceSelectPanelComponent]> Lifecycle: OnInit ->  process 500 response');
      let componentAsAny = component as any;
      // Check that initial state is the expected.
      expect(component.downloading).toBeFalsy('while not processing the "downloading" is false');
      expect(component.dataModelRoot.length).toBe(0, 'model list should be empty');
      expect(component.renderNodeList.length).toBe(0, 'render list should be empty');
      // Prepare the data so the parameters are read with the right values.
      let credential = new Perfil();
      credential.centro = new Centro({
        id: 100002
      });
      credential.allservices = true;
      appStoreService.storeCredential(credential);
      // Insall the mock to process an exception.
      let exception = new HttpErrorResponse({
        error: '500 Gateway Error',
        status: 404,
        statusText: '404 Gateway Error',
        url: '/api/v1/centros/100001/medicos'
      });
      const mockCall = spyOn(appStoreService, 'accessDoctors')
        .and.returnValue(throwError(exception));
      // On this call there is no blocking. The exception is returned by previous line.
      component.ngOnInit();
      // expect(component.downloading).toBeTruthy('started the processing so "downloading" is true');
      // Give the data for the processing.
      // appStoreService.injectException(exception);
      expect(component.downloading).toBeFalsy('completed processing so "downloading" is false');
      expect(component.dataModelRoot.length).toBe(0, 'model list should be empty');
      expect(component.renderNodeList.length).toBe(0, 'render list should be empty');
    }));
  });
  
  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [selectService]', () => {
    it('selectService: select a new service', () => {
      console.log('><[gestion-servicio/ServiceSelectPanelComponent]>selectService: select a new service');
      let service = new Medico();
      service.setNombre('Nombre de prueba');
      component.service.subscribe((newService) => {
        expect(newService).toBeDefined('should exist a response');
        expect(newService.getNombre()).toBe('Nombre de prueba', 'should match the given name of "Nombre de prueba"');
      });
      component.selectService(service);
      expect(appStoreService.accessActiveService()).toBeDefined('should exist a selected service');
      expect(appStoreService.accessActiveService().getNombre()).toBe('Nombre de prueba', 'should match the given name of "Nombre de prueba"');
    });
  });
});
