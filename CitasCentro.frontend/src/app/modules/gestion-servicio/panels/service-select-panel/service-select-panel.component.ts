//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Component } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
// import { AppPanelComponent } from '../../../../components/app-panel/app-panel.component';
// --- MODELS
import { Medico } from '@models/Medico.model';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { ServiceAuthorized } from '@app/models/ServiceAuthorized.model';

@Component({
  selector: 'gs-service-select-panel',
  templateUrl: './service-select-panel.component.html',
  styleUrls: ['./service-select-panel.component.scss']
})
export class ServiceSelectPanelComponent extends AppPanelComponent {
  @Output() service = new EventEmitter<Medico>();

  ngOnInit() {
    console.log(">>[ServiceSelectPanelComponent.ngOnInit]");
    // We sould expect that the data has beed started the download so we ony have to subscribe to the subjects.
    this.downloading = true;
    let medicos = this.appStoreService.accessDoctors()
      .subscribe((doctorList) => {
        this.dataModelRoot = [];
        let authorized: ServiceAuthorized[] = this.appStoreService.accessCredential().serviciosAutorizados;
        for (let doctor of doctorList) {
          if (doctor.activo) {
            // Check if the profile has the right privileges to see the services.
            if (this.appStoreService.accessCredential().allservices)
              this.dataModelRoot.push(doctor);
            else {
              for (let service of authorized) {
                if (service.serviceIdentifier === doctor.getId()) this.dataModelRoot.push(doctor);
              }
            }
          }
        }
        this.notifyDataChanged();
        this.downloading = false;
      }, (error) => {
        this.downloading = false;
        // The single error that can be processed at this point is a missing center. All others must be reported and end at the login page or the connections failure page.
        if (error.status == 404) {
          this.appStoreService.errorNotification("El centro solicitado en la credencial no existe. Pongase en contacto con el servicio técnico", "¡Atención!");
          // this.router.navigate(['login']);
        } else {
          this.appStoreService.processBackendError(error);
        }
      });
  }
  public selectService(_target: Medico): void {
    // Store the selected service for later use.
    this.appStoreService.storeActiveService(_target);
    // Report on the output the service selected.
    this.service.emit(_target);
  }
}
