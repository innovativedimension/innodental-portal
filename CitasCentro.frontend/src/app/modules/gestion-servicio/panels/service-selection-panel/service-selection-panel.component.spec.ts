// - CORE
import { throwError } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// - TESTING
import { TestBed } from '@angular/core/testing';
import { inject } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
import { ServiceSelectionPanelComponent } from '@app/modules/gestion-servicio/panels/service-selection-panel/service-selection-panel.component';
import { IonicModule } from 'ionic-angular';
import { SharedModule } from '@app/modules/shared/shared.module';
import { AvailableServicesController } from '@app/controllers/available-services-controller.service';
import { AvailableServicesControllerStub } from '@app/testing/services/available-services-controller.mock.service';
// - MODELS
import { Medico } from '@models/Medico.model';

// - S E R V I C E   S E L E C T   P A N E L - V 1
describe('PANEL ServiceSelectionPanelComponent [Module: GESTION-SERVICIO]', () => {
  let component: ServiceSelectionPanelComponent;
  let fixture: ComponentFixture<ServiceSelectionPanelComponent>;
  let appStoreService: MockAppStoreService;
  let availableServicesController: AvailableServicesControllerStub;

  beforeEach(async(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes),
        IonicModule.forRoot({}),
        SharedModule
      ],
      declarations: [ServiceSelectionPanelComponent, RouteMockUpComponent],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: AvailableServicesController, useClass: AvailableServicesControllerStub }
      ]
    })
      .compileComponents();
  }));
  beforeEach(() => {
    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(ServiceSelectionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    appStoreService = TestBed.get(AppStoreService);
    availableServicesController = TestBed.get(AvailableServicesController);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> "openSelection" should be false');
      expect(component.openSelection).toBeFalsy('"openSelection" should be false');
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> "serviceList" should exist');
      expect(component.serviceList).toBeDefined('"serviceList" should exist');
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> "serviceList" should have no elements');
      expect(component.serviceList.length).toBe(0, '"serviceList" should have no elements');
    });
  });

  // - I N P U T / O U T P U T   P H A S E
  describe('Input/Output Phase', () => {
    it('activeService (Input): validate service reception', () => {
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> activeService (Input): validate service reception');
      // Check the initial state to be the empty doctor.
      expect(component.activeService).toBeDefined();
      expect(component.activeService.getId()).toBe('-EMPTY-', 'the default identifier should be "-EMPTY-"');
      // Create the test node to be used on the render.
      component.activeService = new Medico()
        .setId('TEST ID')
      expect(component.activeService).toBeDefined();
      expect(component.activeService.getId()).toBe('TEST ID', 'the default identifier should be "TEST ID"');
    });
    it('serviceSelected (Output): emit the new selected service', async(() => {
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> serviceSelected (Output): emit the new selected service');
      // Build the event to be sent. Event with no payload
      let service = new Medico()
        .setNombre('Nombre de prueba')
        .setApellidos('Apellidos');
      component.serviceSelected.subscribe((serviceSelected: Medico) => {
        console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> checking emission');
        expect(serviceSelected).toBeDefined('the emitted service exists');
        expect(serviceSelected.getNombre()).toBe('Nombre de prueba', 'the emitted service name matches "Nombre de prueba"');
      });
      component.doSelection(service);
      expect(true).toBeTruthy();
    }));
  });

  // - L I F E C Y C L E   P H A S E
  describe('Lifecycle Phase', () => {
    it('Lifecycle: OnInit -> get the list of available services. Full access user', async(() => {
      console.log('><[citaciones/ServiceSelectionPanelComponent]> Lifecycle: OnInit -> get the list of available services. Full access user');
      // Check the initial state before loading the list of services.
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> "serviceList" should exist');
      expect(component.serviceList).toBeDefined('"serviceList" should exist');
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> "serviceList" should have no elements');
      expect(component.serviceList.length).toBe(0, '"serviceList" should have no elements');
      availableServicesController.downloadAvailableServices(appStoreService.directAccessMockResource('accessdoctors') as Medico[])
      component.ngOnInit();
      console.log('><[gestion-servicio/ServiceSelectionPanelComponent]> "serviceList" should have 9 elements');
      expect(component.serviceList.length).toBe(9, '"serviceList" should have 9 elements');
    }));
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [activateSelection]', () => {
    it('activateSelection: activate a new service', () => {
      console.log('><[citaciones/CalendarPanelV2Component]> activateSelection: activate a new service');
      expect(component.openSelection).toBeFalsy('"openSelection" should be false upon creation');
      component.activateSelection();
      expect(component.openSelection).toBeTruthy('"openSelection" should be true after selection');
    });
  });
});
