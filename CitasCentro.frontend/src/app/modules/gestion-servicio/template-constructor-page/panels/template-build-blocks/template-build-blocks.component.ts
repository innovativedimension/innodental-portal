//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Inject } from '@angular/core';
//--- ENVIRONMENT
import { environment } from '@env/environment';
//--- ROUTER
import { Router } from '@angular/router';
//--- NOTIFICATIONS
//import { NotificationsService } from 'angular2-notifications';
//--- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
//--- CALENDAR
import { startOfDay } from 'date-fns';
//--- INTERFACES
import { IContainerController } from '@interfaces/core/IContainerController.interface';
//--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- MODELS
import { DailyAppointmentsTemplate } from '@app/models/DailyAppointmentsTemplate.model';
import { Cita } from '@models/Cita.model';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';
import { AppStoreService } from '@app/services/appstore.service';

@Component({
  selector: 'template-build-blocks',
  templateUrl: './template-build-blocks.component.html',
  styleUrls: ['./template-build-blocks.component.scss']
})
export class TemplateBuildBlocksComponent implements OnInit {
  @Input() title: string;
  @Input() show : boolean = true;

  public templateBuildingBlocks: TemplateBuildingBlock[] = [];
  // @Input() selectedDate: Date = new Date;

  // private appointments: Cita[] = [];
  // public selectedCitas: Cita[] = [];
  // public stateColor: string = "";

  //--- C O N S T R U C T O R
  constructor(
    @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    protected router: Router,
    // //protected toasterService: NotificationsService,
    protected appStoreService: AppStoreService) {
  }

  ngOnInit() {
    console.log(">>[TemplateBuildBlocksComponent.ngOnInit]");
    // Load the list of predefined template building blocks.
    this.appStoreService.propertiesTemplateBuildingBlocks()
      .subscribe((_buildingBlocks) => {
        this.templateBuildingBlocks = _buildingBlocks;
      });
    console.log("<<[TemplateBuildBlocksComponent.ngOnInit]");
  }
}
