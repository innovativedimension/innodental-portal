//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- MODELS
import { Cita } from '@models/Cita.model';
import { CitasTemplate } from '@models/CitasTemplate.model';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

// - C O N S T A N T S
const DEFAULT_MORNING_START_TIME = "08:00";
const DEFAULT_EVENING_START_TIME = "15:00";

@Component({
  selector: 'template-under-construction',
  templateUrl: './template-under-construction.component.html',
  styleUrls: ['./template-under-construction.component.scss']
})
export class TemplateUnderConstructionComponent extends AppPanelComponent {
  @Input() templateUnderControl: CitasTemplate = new CitasTemplate();
  public savePending: boolean = false;
  // public saveCompleted: boolean = false;

  //--- F O R M   E V E N T S
  public onNameBlur(): void {
    if (this.appStoreService.isNonEmptyString(this.templateUnderControl.nombre)) this.savePending = true;
  }
  public onMorningTimeBlur(): void {
    this.templateUnderControl.fireTemplateChange();
    this.savePending = true;
  }
  public onEveningTimeBlur(): void {
    this.templateUnderControl.fireTemplateChange();
    this.savePending = true;
  }
  public getMorningData(): Cita[] {
    return this.templateUnderControl.getMorningList();
  }
  public getEveningData(): Cita[] {
    return this.templateUnderControl.getEveningList();
  }
  public onDetectChanges(_newvalue: string): void {
    // It is not possible to check here is the value has changed. The reception of the message says so.
    this.savePending = true;
  }
  /**
   * This is the method called when the save button is clicked. The method receives the button so when the action terminates we can report to the button and deactivate the button and return it to the initial state.
   *
   * @param {*} _eventButton the structure with the button event so we can have a pointer to the button component.
   * @memberof TemplateUnderConstructionComponent
   */
  public onSaveTemplate(_eventButton: any) {
    console.log(">>[TemplateUnderConstructionComponent.saveTemplate]");
    // Call the backend to save the template. Choose between a new template and a template edition.
    if (this.templateUnderControl.id == -1)
      this.backendService.backendCreateTemplate(this.templateUnderControl,
        this.appStoreService.accessActiveService().getId())
        .subscribe((template) => {
          this.templateUnderControl = new CitasTemplate(template);
          this.savePending = false;
          this.appStoreService.successNotification("Template creada satisfactoriamente.", "¡Exito!");
          // Change the card title to show that we are now editing an existing template.
          this.title = "Plantilla en Edición";
          _eventButton.button.completeAction();
        }, (error) => {
          if (error.status == 409) {
            this.appStoreService.errorNotification(error.message, "Error HTTP 409. Not Found",
              { dismiss: 'click' });
            // The medico or service is not found on this database. Reset the selected service.
            this.appStoreService.clearActiveService();
            // Read the last location to return back.
            this.activeRoute.queryParams.subscribe((params) => {
              let returnUrl = params.returnUrl;
              if (null != returnUrl) this.router.navigateByUrl(returnUrl);
              else this.router.navigate(['gestionservicio/definicioncalendario']);
            });
          } else {
            this.appStoreService.errorNotification(error.message, "Error HTTP " + error.status,
              { dismiss: 'click' });
          }
        });
    else this.backendService.backendUpdateTemplate(this.templateUnderControl,
      this.appStoreService.accessActiveService().getId())
      .subscribe((template) => {
        this.templateUnderControl = new CitasTemplate(template);
        this.savePending = false;
        this.appStoreService.successNotification("Template actalizada correctamente.", "¡Exito!");
        _eventButton.button.completeAction();
      });
  }
  public removeMorningSlot(_slot: Cita): void {
    this.templateUnderControl.removeMorning(_slot);
  }
  public removeEveningSlot(_slot: Cita): void {
    this.templateUnderControl.removeEvening(_slot);
  }

  // - D R A G   &  D R O P   S E C T I O N
  public onBlockDrop(_event: any, _timeZone: string) {
    console.log(">>[TemplateUnderConstructionComponent.onBlockDrop]");
    // Add the item to the end of the list.
    if ((null != _event) && (null != _event.dragData)) {
      console.log("--[TemplateUnderConstructionComponent.onBlockDrop]> Template dropped: "
        + JSON.stringify(_event.dragData));
      if (_timeZone === '-MORNING-') {
        // If the morning start time is not defeined define it with the default value.
        if (null == this.templateUnderControl.tmhoraInicio)
          this.templateUnderControl.tmhoraInicio = DEFAULT_MORNING_START_TIME;
        this.templateUnderControl.addMorning(_event.dragData);
        this.templateUnderControl.getMorningList();
      }
      if (_timeZone === '-EVENING-') {
        if (null == this.templateUnderControl.tthoraInicio)
          this.templateUnderControl.tthoraInicio = DEFAULT_EVENING_START_TIME;
        this.templateUnderControl.addEvening(_event.dragData);
        this.templateUnderControl.getEveningList();
      }
      // Activate the save only if the name is defined
      if (this.appStoreService.isNonEmptyString(this.templateUnderControl.nombre)) this.savePending = true;
    }
    console.log("<<[TemplateUnderConstructionComponent.onBlockDrop]");
  }
  public onBlockDropOver(_event: any, _slot: Cita, _timeZone: string): void {
    console.log(">>[TemplateUnderConstructionComponent.onBlockDrop]");
    // The drop is on top of another slot. Put the new slot just behind the target.
    if ((null != _event) && (null != _event.dragData)) {
      console.log("--[TemplateUnderConstructionComponent.onBlockDrop]> Template dropped: "
        + JSON.stringify(_event.dragData));
      if (_timeZone === '-MORNING-') {
        this.templateUnderControl.addMorningOnTop(_slot, _event.dragData);
        this.templateUnderControl.getMorningList();
      }
      if (_timeZone === '-EVENING-') {
        this.templateUnderControl.addEveningOnTop(_slot, _event.dragData);
        this.templateUnderControl.getEveningList();
      }
      if (this.appStoreService.isNonEmptyString(this.templateUnderControl.nombre)) this.savePending = true;
    }
    console.log("<<[TemplateUnderConstructionComponent.onBlockDrop]");
  }
}
