//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Location } from "@angular/common";
// --- TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
import { async } from '@angular/core/testing';
import { fakeAsync } from '@angular/core/testing';
import { tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { EmptyMockBackendService } from '@app/testing/services/EmptyMockBackendService.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- COMPONENTS
import { TemplateConstructorPageComponent } from '@app/modules/gestion-servicio/template-constructor-page/template-constructor-page.component';
import { MockTemplateControllerService } from '@app/testing/services/MockTemplateController.service';
// --- MODELS
import { CitasTemplate } from '@models/CitasTemplate.model';
import { TemplateControllerService } from '@app/controllers/template-controller.service';
import { ActivatedRouteStub } from '@app/testing/services/ActivatedRouteStub.service';
import { StubService } from '@app/testing/services/StubService.service';
import { AppComponent } from '@app/app.component';

describe('PAGE TemplateConstructorPageComponent [Module: GESTION-SERVICIO]', () => {
  let component: TemplateConstructorPageComponent;
  let fixture: ComponentFixture<TemplateConstructorPageComponent>;
  let appComponent: ComponentFixture<AppComponent>;
  let location: Location;
  let router: Router;
  let activeRoute: ActivatedRouteStub;
  let appStoreService: MockAppStoreService;
  let backendService: EmptyMockBackendService;
  let templateController: MockTemplateControllerService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: BackendService, useClass: StubService },
        { provide: TemplateControllerService, useClass: MockTemplateControllerService }
      ],
      declarations: [TemplateConstructorPageComponent, RouteMockUpComponent, AppComponent],
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(TemplateConstructorPageComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    location = TestBed.get(Location);
    activeRoute = TestBed.get(ActivatedRoute);
    appComponent = TestBed.createComponent(AppComponent);
    appStoreService = TestBed.get(AppStoreService);
    backendService = TestBed.get(BackendService);
    templateController = TestBed.get(TemplateControllerService);

    fixture.ngZone.run(() => {
      router.initialNavigation();  // Prepate the router-outlet and do the initial navigation.
    });
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> "titulo" should be "Plantilla en Construcción"');
      expect(component.titulo).toBe('Plantilla en Construcción', '"titulo" should be "Plantilla en Construcción"');
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> "templateUnderControl" should be defined');
      expect(component.templateUnderControl).toBeDefined('template should exist but empty');
    });
  });

  // - R E N D E R I N G   P H A S E
  describe('Rendering Phase', () => {
    it('Rendering Phase: some panels should be visible', () => {
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> Rendering Phase: some panels should be visible');
      let testPanel = fixture.debugElement.query(By.css('#gs-header-panel'));
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> "gs-header-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#template-under-construction'));
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> "template-under-construction"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#template-build-blocks'));
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> "template-build-blocks"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> "ui-menu-bar"');
      expect(testPanel).toBeDefined();
    });
  });

  // - L I F E C Y C L E   P H A S E
  describe('Lifecycle Phase', () => {
    it('Lifecycle: OnInit. new template', fakeAsync(() => {
      spyOn(component, 'ngOnInit');
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> Lifecycle: OnInit: new template');
      // Set up the route for a new template.
      component.ngOnInit();
      expect(component.ngOnInit).toHaveBeenCalledTimes(1);
    }));

    it('Lifecycle: OnInit. not found template', fakeAsync(() => {
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> Lifecycle: OnInit: not found template');
      // We should call the ngOnInit because it is never called even if the page is routed.
      fixture.ngZone.run(() => {
        // Do the runs inside a zone to remove warnings and do the routing.
        component.ngOnInit();
        activeRoute.setParamMap(null);
        // Now wait until the tasks complete.
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(component.titulo).toBe('Plantilla en Construcción', '"title" should match "Plantilla en Construcción');
        });
      });
      tick(1000);
      expect(true).toBeTruthy();
    }));
    it('Lifecycle: OnInit. valid template', fakeAsync(() => {
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> Lifecycle: OnInit: valid template');
      // We should call the ngOnInit because it is never called even if the page is routed.
      fixture.ngZone.run(() => {
        // Do the runs inside a zone to remove warnings and do the routing.
        templateController.storeTemplate(new CitasTemplate().setNombre('Nombre de prueba'));
        component.ngOnInit();
        activeRoute.setParamMap({ templateId: 10001 });
        // Now wait until the tasks complete.
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          expect(component.titulo).toBe('Plantilla en Edición', '"title" should match "Plantilla en Edición');
          expect(component.templateUnderControl.getNombre()).toBe('Nombre de prueba', 'template name should be "Nombre de prueba"');
        });
      });
      tick(1000);
      expect(true).toBeTruthy();
    }));
  });

  // - F U N C T I O N A L I T Y   P H A S E
  describe('Functionality Phase', () => {
    it('Functionality: The Template Under Construction Panel sould have the title set depending on the route received. New template.', fakeAsync(() => {
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> Functionality: title set to new template');
      // We should call the ngOnInit because it is never called even if the page is routed.
      fixture.ngZone.run(() => {
        // Do the runs inside a zone to remove warnings and do the routing.
        router.navigate(['/gestionservicio/constructorplantilla']);
        component.ngOnInit();
        activeRoute.setParamMap(null);
        // Now wait until the tasks complete.
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          console.log('><[gestion-servicio/TemplateConstructorPageComponent]> location: ' + location.path());
          expect(location.path()).toBe('/gestionservicio/constructorplantilla');
          expect(component.titulo).toBe('Plantilla en Construcción', '"title" should match "Plantilla en Construcción');
        });
      });
      tick(1000);
      expect(true).toBeTruthy();
    }));
    it('Functionality: The Template Under Construction Panel sould have the title set depending on the route received. Edit Template.', fakeAsync(() => {
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> Functionality: title set to editing template');
      // We should call the ngOnInit because it is never called even if the page is routed.
      fixture.ngZone.run(() => {
        // Do the runs inside a zone to remove warnings and do the routing.
        router.navigate(['/gestionservicio/constructorplantilla', 10001]);
        component.ngOnInit();
        activeRoute.setParamMap({ templateId: 10001 });
        // Now wait until the tasks complete.
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          console.log('><[gestion-servicio/TemplateConstructorPageComponent]> location: ' + location.path());
          expect(location.path()).toBe('/gestionservicio/definicioncalendario');
          expect(component.titulo).toBe('Plantilla en Edición', '"title" should match "Plantilla en Edición');
        });
      });
      tick(1000);
      expect(true).toBeTruthy();
    }));
  });

  describe('Functionality Phase', () => {
    it('Functionality: If the page receives a template identified route but there is no template stored on the service the routing should change to the Service Definition Page.', fakeAsync(() => {
      console.log('><[gestion-servicio/TemplateConstructorPageComponent]> Functionality: If the page receives a template identified route but there is no template stored on the service the routing should change to the Service Definition Page.');
      // We should call the ngOnInit because it is never called even if the page is routed.
      fixture.ngZone.run(() => {
        // Do the runs inside a zone to remove warnings and do the routing.
        component.ngOnInit();
        activeRoute.setParamMap({ templateId: 10001 });
        // Now wait until the tasks complete.
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          console.log('><[gestion-servicio/TemplateConstructorPageComponent]> location: ' + location.path());
          console.log('><[gestion-servicio/TemplateConstructorPageComponent]> titulo: ' + component.titulo);
          expect(location.path()).toBe('/gestionservicio/definicioncalendario');
          expect(component.titulo).toBe('Plantilla en Edición', '"titulo" should match "Plantilla en Edición');
        });
      });
      tick(1000);
      expect(true).toBeTruthy();
    }));
  });
});
