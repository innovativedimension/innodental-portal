//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
//--- ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
//--- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { TemplateControllerService } from '@app/controllers/template-controller.service';
//--- COMPONENTS
import { GestionCommonPageComponent } from '@app/modules/gestion-servicio/common/gestion-common-page.component';
//--- MODELS
import { CitasTemplate } from '@models/CitasTemplate.model';

@Component({
  selector: 'template-constructor-page',
  templateUrl: './template-constructor-page.component.html',
  styleUrls: ['./template-constructor-page.component.scss']
})
export class TemplateConstructorPageComponent extends GestionCommonPageComponent {
  public titulo: string = "Plantilla en Construcción";
  public templateUnderControl: CitasTemplate = new CitasTemplate();

  // - C O N S T R U C T O R
  constructor(
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    protected templateController: TemplateControllerService) {
    super(router, activeRoute, appStoreService, backendService);
  }

  //--- L I F E C Y C L E
  /**
   * We can enter this page from two places. In case of a new template we will not have any parameter and we come from the 'new templete' button at the appointment build with templates page. If the route has a parameter it is the template unique identifier and we are editing an existing template and not creating a new one.
   * @return Part of the lifecycle.
   */
  ngOnInit() {
    console.log(">>[TemplateConstructorPageComponent.ngOnInit]");
    // If the page defines an OnInit then it should call their parent first.
    super.ngOnInit();
    // Detect if new or old template by reading the route.
    this.activeRoute.params
      .subscribe((routeParams) => {
        if (null != routeParams) if (null != routeParams.templateId) {
          console.log("--[TemplateConstructorPageComponent.ngOnInit]> templateId: " + routeParams.templateId);
          this.titulo = "Plantilla en Edición";
          // Load the selected template and pass it to the components.
          this.templateUnderControl = this.templateController.accessTemplate();
          // If the tharget is null we have entered the page from restart. Go back to the template selection.
          if (null == this.templateUnderControl) {
            console.log("--[TemplateConstructorPageComponent.ngOnInit]> The template is not found. Going back to selection page.");
            // console.log("--[TemplateConstructorPageComponent.ngOnInit]> router: " + JSON.stringify(this.router));
            this.router.navigate(['gestionservicio/definicioncalendario']);
          }
          // } else
          //   console.log("--[TemplateConstructorPageComponent.ngOnInit]> Editing template: " +
          //     this.templateUnderControl.nombre);
        }
      });
    console.log("<<[TemplateConstructorPageComponent.ngOnInit]");
  }
}
