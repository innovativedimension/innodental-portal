//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
// --- ROUTING
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
// --- NOTIFICATIONS
// import { ToastrModule } from 'ng6-toastr-notifications';
// --- IONIC
import { IonicModule } from 'ionic-angular';

// --- APPLICATION MODULES
import { UIModule } from '@app/modules/ui/ui.module';

// --- PAGES
import { LoginPageComponent } from '@app/modules/login/pages/login-page/login-page.component';
import { DashboardPageComponent } from '@app/modules/login/pages/dashboard-page/dashboard-page.component';
// --- PANELS
import { ModulesPanelComponent } from '@app/modules/login/panels/modules-panel/modules-panel.component';
import { LoginPanelComponent } from '@app/modules/login/panels/login-panel/login-panel.component';

// --- MODULE ROUTES
const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
];

@NgModule({
  imports: [
    // --- CORE MODULES
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // --- ROUTING
    RouterModule.forChild(routes),
    // --- NOTIFICATIONS
    // ToastrModule.forRoot(),
    // --- IONIC
    IonicModule,
    // --- APPLICATION MODULES
    UIModule
  ],
  declarations: [
    // --- PAGES
    LoginPageComponent,
    DashboardPageComponent,
    // --- PANELS
    ModulesPanelComponent,
    LoginPanelComponent
  ],
  exports: [
    RouterModule
  ]
})
export class LoginModule { }
