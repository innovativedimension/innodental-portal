//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Router } from '@angular/router';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';

@Component({
  selector: 'dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent {
  public development: boolean = false;
  public hovering: boolean = false;
  public menuOpen: boolean = false;

  // - C O N S T R U C T O R
  constructor(
    protected router: Router,
    protected appStoreService: AppStoreService) {
    this.development = environment.development;
  }

  // - V I E W   I N T E R A C T I O N
  public activatePage(_pageName: string): void {
    this.router.navigate([_pageName]);
  }
  /**
   * Check the required role level received as parameter against the current role level. Roles are transformed to level number for easy comparison.
   *
   * @returns {boolean} TRUE is the current authorization level is greater or equal to the lever requested.
   * @memberof MenuBarComponent
   */
  public requestsRole(_roleRequested: string): boolean {
    // Conver the roles to their equivalent authorization numbers.
    const requestLevel: number = this.convertRole2Level(_roleRequested);
    let loginLevel = this.appStoreService.accessLoginProfile().getLoginLevel();
    if (loginLevel >= requestLevel) return true;
    else return false;
  }
  protected convertRole2Level(_role: string): number {
    if (_role == 'ADMIN') return 999;
    if (_role == 'RESPONSABLE') return 300;
    if (_role == 'CALLCENTER') return 200;
    if (_role == 'USUARIO') return 100;
    return 0;
  }
}
