//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
//--- ENVIRONMENT
import { environment } from '@env/environment';
//--- MODELS
import { CredentialRequest } from '@app/models/CredentialRequest.model';
//--- COMPONENTS
import { AppFormPanelComponent } from '@app/modules/ui/app-form-panel/app-form-panel.component';
import { ValidateNIF } from '@app/validators/validate-nif.validator';

@Component({
  selector: 'cc-login-panel',
  templateUrl: './login-panel.component.html',
  styleUrls: ['./login-panel.component.scss']
})
export class LoginPanelComponent extends AppFormPanelComponent implements OnInit {
  public credentialForm: FormGroup;
  public signalError: boolean = false; // Used to put the enter button in red because a previous error.

  //--- L I F E C Y C L E
  ngOnInit() {
    this.development = false;
    // Initialize the form.
    this.credentialForm = this.formBuilder.group({
      identificador: ['', [
        Validators.required,
        Validators.minLength(9),
        ValidateNIF]],
      password: ['', [
        Validators.required,
        Validators.minLength(4)]],
    });
  }

  //-- F O R M   I N T E R A C T I O N S
  // public getIdentificadorState(): boolean {
  //   return this.credentialForm.value.identificador.valid;
  // }
  // public getPasswordState(): boolean {
  //   return this.credentialForm.value.password.valid;
  // }
  get identificador() { return this.credentialForm.get('identificador'); }
  get password() { return this.credentialForm.get('password'); }
  /**
   * The user has exited the <b>identificador</b> field. Check if we have to search for the patient apointments on the backend.
   * The process leaves the list of appointments on the <i>this.activeAppointments</i> variable.
   */
  // public onIdentificadorBlur(): void {
  //   console.log(">>[LoginPanelComponent.onIdentificadorBlur]");
  //   if (this.credentialForm.value.identificador.length > 7) {
  //     // Convert the NOF to uppercase.
  //     this.credentialForm.value.identificador = this.credentialForm.value.identificador.toUpperCase();
  //     // Validate the current NIF.
  //     this.nifvalido = this.appStoreService.validateNIF(this.credentialForm.value.identificador);
  //   }
  //   console.log("<<[LoginPanelComponent.onIdentificadorBlur]");
  // }
  public onKeyUpIdentificador(): void {
    this.credentialForm.value.identificador = this.credentialForm.value.identificador.toUpperCase();
  }
  /**
   * The forst is verified and complete. We can start checking the identify against the backend.
   */
  public onSubmitLogin(): void {
    console.log(">>[LoginPanelComponent.onSubmitLogin]");
    // Send the credential request to the backen. If the password matches move to the application.
    this.downloading = true;
    this.signalError = false;
    this.backendService.checkCredential(new CredentialRequest({
      identificador: this.credentialForm.value.identificador.toUpperCase(),
      password: this.credentialForm.value.password
    }))
      .subscribe((profile) => {
        this.appStoreService.successNotification('Credencial validada. Descargando datos.', 'Validada');
        // Save the data.
        console.log("--[LoginPanelComponent.onSubmitLogin] Token: " + profile.authorizationToken);
        let token = profile.authorizationToken;
        this.backendService.setToSession(environment.TOKEN_KEY, token);
        this.backendService.setToStorage(environment.CREDENTIAL_KEY, JSON.stringify(profile));
        this.appStoreService.storeCredential(profile);
        // Start the download for the Centro data.
        this.appStoreService.accessDoctors().
          subscribe((dummy) => {
            this.appStoreService.accessSpecialities()
              .subscribe((dummy) => {
                // this.appStoreService.accessMedicalActs()
                //   .subscribe((dummy) => {
                this.downloading = false;
                this.router.navigate(['dashboard']);
                //   })
              }, (error) => {
                // Process any 401 exception that means the session is no longer valid.
                if (error.status == 401) {
                  this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.",
                    "¡Atención!", { dismiss: 'click' });
                  this.router.navigate(['login']);
                }
              })
          }, (error) => {
            // Process any 401 exception that means the session is no longer valid.
            if (error.status == 401) {
              this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.",
                "¡Atención!", { dismiss: 'click' });
              this.signalError = true;
            }
          })
      }, (error) => {
        console.log("--[LoginPanelComponent.onSubmit.error]> Message: " + error.message);
        console.log("--[LoginPanelComponent.onSubmit.error]> Exception: " + error.name +
          ' [' + error.status + '] ' + error.statusText);
        this.appStoreService.errorNotification("Las credenciales introducidas no son válidas. Compruebe los datos o solicite una nueva credencial.", "¡Atención!",
          { dismiss: 'click' });
        this.signalError = true;
      });
    console.log("<<[LoginPanelComponent.onSubmit]");
  }
  // - D E B U G
  public serializeErrors(_error: any): string {
    return JSON.stringify(_error);
  }
}
