//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// --- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'cc-modules-panel',
  templateUrl: './modules-panel.component.html',
  styleUrls: ['./modules-panel.component.scss']
})
export class ModulesPanelComponent extends AppPanelComponent implements OnInit {
  ngOnInit() {
    // Read the list of modules and put on the render list.
    this.appStoreService.propertiesModulos()
      .subscribe((modulos) => {
        // this.dataModelRoot=[];
        for (let mod of modulos) {
          // Process the modules
          this.dataModelRoot.push(mod);
        }
        this.notifyDataChanged();
      });
  }
  public getAwesomeIconClass(_module: any): string {
    return 'fas ' + _module.icono.iconPath + ' app-icon';
  }
}
