//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- INTERFACES
// import { INode } from '../interfaces/core/Inode.interface';
import { IAppointmentEnabled } from '../interfaces/IAppointmentEnabled.interface';
//--- MODELS
import { Node } from './core/Node.model';
import { Medico } from './Medico.model';

export class ActoMedicoCentro extends Node implements IAppointmentEnabled {
  public etiqueta: string = "R";
  public nombre: string = "RADIOLOGÍA";
  public tiempoRequerido: number = 15;
  public tipo: string = "TRANSPARENTE"; // Appointment tipe to be selected. Allows appointment filter
  public maquina: Medico;
  public recurso: Medico;

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "ActoMedicoCentro";

    // Transform dependency objects
    if (null != this.maquina) {
      let maquina = new Medico(this.maquina);
      this.maquina = maquina;
    }
    if (null != this.recurso) {
      let recurso = new Medico(this.recurso);
      this.recurso = recurso;
    }
  }

  //--- I A P P O I N T M E N T E N A B L E D   I N T E R F A C E
  /**
   * Return the unique identifier of the associated service. The same that for a Medico. See if it is advisable to return lists of vallues.
   */
  public getServiceIdentifiers(): string[] {
    let result: string[] = [];
    result.push(this.maquina.getId());
    return result;
  }
  public getEspecialidad(): string {
    if (null == this.maquina) return 'Radiología';
    else return this.maquina.especialidad;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getServiceId(): string {
    // Return the unique identifier for the service so I can go to the backend and retrieve the list of appointments.
    return this.maquina.referencia;
  }
}
