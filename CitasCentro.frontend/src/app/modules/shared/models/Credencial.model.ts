//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- INTERFACES
// import { ETratamiento } from '../interfaces/EPack.enumerated';
//--- MODELS
import { Node } from './core/Node.model';
import { Centro } from './Centro.model';

export class Credencial extends Node {
  public nif: string = "";
  public nombre: string = "";
  public apellidos: string = "";
  public centro: Centro = new Centro();

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Credencial";

    // Transform dependency objects
    if (null != this.centro) {
      let centroInter = new Centro(this.centro);
      this.centro = centroInter;
    }
  }

  //--- G E T T E R S   &   S E T T E R S
  public getId(): string {
    return this.nif;
  }
  public getReferencia(): string {
    return this.nif;
  }
  public getNombre(): string {
    return this.nombre;
  }
  public getApellidos(): string {
    return this.apellidos;
  }
  public getEspecialidad(): string {
    return "Radiología";
  }
  public getCentro(): Centro {
    return new Centro(this.centro);
  }
  public isValid(): boolean {
    if (this.isEmptyString(this.nif)) return false;
    if (this.isEmptyString(this.nombre)) return false;
    return true;
  }
}
