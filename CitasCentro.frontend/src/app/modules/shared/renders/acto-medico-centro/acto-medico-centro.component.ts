//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- COMPONENTS
import { RenderComponent } from '@renders/render/render.component';
//--- MODELS
import { ActoMedico } from '@models/ActoMedico.model';
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';

@Component({
  selector: 'cc-acto-medico-centro',
  templateUrl: './acto-medico-centro.component.html',
  styleUrls: ['./acto-medico-centro.component.scss']
})
export class ActoMedicoCentroRenderComponent extends RenderComponent {
  @Input() node: ActoMedicoCentro;

  public getCitaTypeColor() : string {
    if (null != this.node) return this.node.tipo.toLowerCase();
  }
}
