//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
// --- COMPONENTS
import { RenderComponent } from '@renders/render/render.component';
import { WEEKDAYS } from '@renders/render/render.component';
import { MONTHNAMES } from '@renders/render/render.component';
// --- MODELS
import { Cita } from '@models/Cita.model';
import { Medico } from '@models/Medico.model';
import { OpenAppointment } from '@app/models/OpenAppointment.model';

@Component({
  selector: 'appointment4-citations',
  templateUrl: './appointment4-citations.component.html',
  styleUrls: ['./appointment4-citations.component.scss']
})
export class Appointment4CitationsRenderComponent extends RenderComponent implements OnInit {
  @Input() open: OpenAppointment;
  public cita: Cita;
  public medico: Medico;

  ngOnInit() {
    this.cita = this.open.getCita();
    this.medico = this.open.getMedico();
  }
  public getAppointmentDateDisplay(): string {
    let display = "";
    let cita = this.open.getCita();
    display = display + WEEKDAYS[cita.getFecha().getDay()] + ', ';
    display = display + cita.getFecha().getDate() + ' ';
    display = display + MONTHNAMES[cita.getFecha().getMonth()] + ' ';
    display = display + cita.getFecha().getFullYear();
    return display;
  }
  public getCitaTypeColor(): string {
    if (null != this.cita) return this.cita.tipo.toLowerCase();
  }
}
