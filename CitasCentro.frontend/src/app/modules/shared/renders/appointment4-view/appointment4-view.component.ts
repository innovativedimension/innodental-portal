//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
// --- COMPONENTS
import { RenderComponent } from '@renders/render/render.component';
import { WEEKDAYS } from '@renders/render/render.component';
import { MONTHNAMES } from '@renders/render/render.component';
//--- MODELS
import { Cita } from '@models/Cita.model';

@Component({
  selector: 'appointment4-view',
  templateUrl: './appointment4-view.component.html',
  styleUrls: ['./appointment4-view.component.scss']
})
export class Appointment4ViewComponent extends RenderComponent {
  @Input() cita: Cita;
  public hovering: boolean = false;

  public getAppointmentDateDisplay(): string {
    let display = "";
    display = display + WEEKDAYS[this.cita.getFecha().getDay()] + ', ';
    display = display + this.cita.getFecha().getDate() + ' ';
    display = display + MONTHNAMES[this.cita.getFecha().getMonth()] + ' ';
    display = display + this.cita.getFecha().getFullYear();
    return display;
  }
  public mouseHovering(): void {
    if (this.cita.estado === "RESERVADA")
      this.hovering = true;
  } public mouseLeft(): void {
    this.hovering = false;
  }
  public pacienteData(): string {
    let data = '';
    data = this.cita.pacienteData.nombre + '<br>' +
      this.cita.pacienteData.identificador + '<br>' +
      this.cita.pacienteData.aseguradora;
    return data;
  }
}
