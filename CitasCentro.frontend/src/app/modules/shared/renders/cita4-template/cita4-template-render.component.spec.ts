//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- MODULES IMPORTS
import { FormsModule } from '@angular/forms';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
// --- MODELS
import { Cita4TemplateRenderComponent } from '@renders/cita4-template/cita4-template.component';
import { Cita } from '@models/Cita.model';
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
import { CitasTemplate } from '@models/CitasTemplate.model';

// - R E N D E R   T E S T S . V 2
describe('RENDER Cita4TemplateRenderComponent [Module: SHARED]', () => {
  let component: Cita4TemplateRenderComponent;
  let fixture: ComponentFixture<Cita4TemplateRenderComponent>;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        FormsModule
      ],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService }
      ],
      declarations: [Cita4TemplateRenderComponent]
    })
      .compileComponents();
    // Create the component and all the rest of the dependencies.
    fixture = TestBed.createComponent(Cita4TemplateRenderComponent);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[shared/Cita4TemplateRenderComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[shared/Cita4TemplateRenderComponent]> Input "Render.node" should be undefined');
      expect(component.node).toBeUndefined('Input "Render.node" should be undefined');
    });
  });

  // - I N P U T / O U T P U T   P H A S E
  describe('Input/Output Phase', () => {
    it('node (Input): validate node reception', () => {
      console.log('><[shared/Cita4TemplateRenderComponent]> node (Input): validate node reception');
      // Check the initial state to undefined.
      expect(component.node).toBeUndefined();

      // Create the test node to be used on the render.
      console.log('><[shared/Cita4TemplateRenderComponent]> Input: node');
      component.node = new Cita();
      expect(component.node).toBeDefined();
    });
    it('optionChange (Output): emit optionChange message', async(() => {
      console.log('><[shared/TemplateRenderComponent]> optionChange (Output): emit optionChange message');
      component.optionChange.subscribe((newtipo: string) => {
        expect(newtipo).toBe('AMARILLO');
      });
      component.node = appStoreService.directAccessMockResource('cita-complete');
      component.onOptionChange('AMARILLO');
    }));
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code coverage Phase [getClassType]', () => {
    it('getClassType: get the style matching type', () => {
      console.log('><[RenderComponent]> getClassType: get the style matching type');
      component.node = appStoreService.directAccessMockResource('cita-complete');

      expect(component.getClassType()).toBe('transparent');
      component.node.tipo = 'ROSA';
      expect(component.getClassType()).toBe('pink');
      component.node.tipo = 'ROJO';
      expect(component.getClassType()).toBe('red');
      component.node.tipo = 'NARANJA';
      expect(component.getClassType()).toBe('orange');
      component.node.tipo = 'AMARILLO';
      expect(component.getClassType()).toBe('yellow');
      component.node.tipo = 'LIMA';
      expect(component.getClassType()).toBe('lime');
      component.node.tipo = 'VERDE';
      expect(component.getClassType()).toBe('green');
      component.node.tipo = 'AZUL CLARO';
      expect(component.getClassType()).toBe('lightblue');
      component.node.tipo = 'AZUL OSCURO';
      expect(component.getClassType()).toBe('darkblue');
      component.node.tipo = 'VIOLETA';
      expect(component.getClassType()).toBe('violet');
    });
  });
});
