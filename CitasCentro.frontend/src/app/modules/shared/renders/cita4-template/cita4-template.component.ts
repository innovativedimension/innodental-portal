//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
//--- MODELS
import { Cita } from '@models/Cita.model';
// --- COMPONENTS
import { RenderComponent } from '@renders/render/render.component';


@Component({
  selector: 'cita4-template',
  templateUrl: './cita4-template.component.html',
  styleUrls: ['./cita4-template.component.scss']
})
export class Cita4TemplateRenderComponent extends RenderComponent {
  @Input() node: Cita;
  @Output() optionChange = new EventEmitter<string>();

  public getTipo(): string {
    return this.node.tipo;
  }
  public getClassType(): string {
    if (this.node.tipo == 'TRANSPARENTE') return 'transparent';
    if (this.node.tipo == 'ROSA') return 'pink';
    if (this.node.tipo == 'ROJO') return 'red';
    if (this.node.tipo == 'NARANJA') return 'orange';
    if (this.node.tipo == 'AMARILLO') return 'yellow';
    if (this.node.tipo == 'LIMA') return 'lime';
    if (this.node.tipo == 'VERDE') return 'green';
    if (this.node.tipo == 'AZUL CLARO') return 'lightblue';
    if (this.node.tipo == 'AZUL OSCURO') return 'darkblue';
    if (this.node.tipo == 'VIOLETA') return 'violet';
  }
  public onOptionChange(_newvalue: string) {
    // Detect changes to the select filed and transmit them to the parent container.
    console.log(">>[Cita4TemplateRenderComponent.onOptionChange]");
    this.optionChange.emit(_newvalue);
  }
}
