//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- COMPONENTS
import { RenderComponent } from '@renders/render/render.component';
import { WEEKDAYS } from '@renders/render/render.component';
import { MONTHNAMES } from '@renders/render/render.component';
//--- MODELS
// import { Cita } from '../../models/Cita.model';
import { Cita } from '@models/Cita.model';

@Component({
  selector: 'cc-cita',
  templateUrl: './cita.component.html',
  styleUrls: ['./cita.component.scss']
})
export class CitaRenderV1Component extends RenderComponent {
  @Input() node: Cita;

  //--- V I S U A L   A D A P T E R S
  public isTypeRed(): boolean {
    if (this.node.tipo === '-RED-') return true;
    else return false;
  }
  public isTypeOrange(): boolean {
    if (this.node.tipo === '-ORANGE-') return true;
    else return false;
  }
  public isTypeYellow(): boolean {
    if (this.node.tipo === '-YELLOW-') return true;
    else return false;
  }
  public getAppointmentDateDisplay(): string {
    let display = "";
    display = display + WEEKDAYS[this.node.getFecha().getDay()] + ', ';
    display = display + this.node.getFecha().getDate() + ' ';
    display = display + MONTHNAMES[this.node.getFecha().getMonth()] + ' ';
    display = display + this.node.getFecha().getFullYear();
    return display;
  }
  public getCitaTypeColor(): string {
    if (null != this.node) return this.node.tipo.toLowerCase();
  }
}
