//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- COMPONENTS
import { RenderComponent } from '@renders/render/render.component';
import { WEEKDAYS } from '@renders/render/render.component';
import { MONTHNAMES } from '@renders/render/render.component';
//--- MODELS
// import { Cita } from '@citamed-lib';
import { PatientData } from '@app/models/PatientData.model';
import { Cita } from '@models/Cita.model';

@Component({
  selector: 'gs-cita',
  templateUrl: './cita.component.html',
  styleUrls: ['./cita.component.scss']
})
export class CitaRenderV2Component extends RenderComponent {
  @Input() node: Cita;

  public showPatientData: boolean = false;

  // - V I E W   I N T E R A C T I O N
  public accessPatientData(): PatientData{
    return new PatientData(this.node.pacienteData);
  }
  public getStateCode(): string {
    return this.node.estado.substr(0, 3);
  }
  public onEnter(): void {
    // Show the patient data only for non free appointments.
    if (this.node.estado.toUpperCase() === "LIBRE") return;
    if (this.node.estado.toUpperCase() === "BLOQUEADA") return;
    if (this.node.estado.toUpperCase() === "VACACIONES") return;
    if (this.node.estado.toUpperCase() === "DESCANSO") return;
    else this.showPatientData = true;
  }
  public onExit(): void {
    // Hide the patient data.
    this.showPatientData = false;
  }
  public getAppointmentTypeClass(): string {
    if (this.node.estado.toUpperCase() === "LIBRE") return 'cita-libre';
    if (this.node.estado.toUpperCase() === "BLOQUEADA") return 'cita-bloqueada';
    if (this.node.estado.toUpperCase() === "RESERVADA") return 'cita-reservada';
    if (this.node.estado.toUpperCase() === "VACACIONES") return 'cita-vacaciones';
    if (this.node.estado.toUpperCase() === "DESCANSO") return 'cita-descanso';
    return 'cita-item-gestion-servicio';
  }
  public getPatientData(): string {
    if (this.node.estado.toUpperCase() === "LIBRE".toUpperCase()) return "-LIBRE-";
    if (this.node.estado.toUpperCase() === "BLOQUEADA".toUpperCase()) return "-EN PROCESO-";
    if (this.node.estado.toUpperCase() === "RESERVADA".toUpperCase()) return this.node.getPatientData();
    return "-LIBRE-";
  }
  public isLibre(): boolean {
    if (this.node.estado.toUpperCase() === "LIBRE".toUpperCase()) return true;
    return false;
  }
  public isReservada(): boolean {
    if (this.node.estado.toUpperCase() === "RESERVADA".toUpperCase()) return true;
    return false;
  }
  public isBloqueada(): boolean {
    if (this.node.estado.toUpperCase() === "BLOQUEADA".toUpperCase()) return true;
    return false;
  }
  // public isTypeRed(): boolean {
  //   if (this.node.tipo === '-RED-') return true;
  //   else return false;
  // }
  // public isTypeOrange(): boolean {
  //   if (this.node.tipo === '-ORANGE-') return true;
  //   else return false;
  // }
  // public isTypeYellow(): boolean {
  //   if (this.node.tipo === '-YELLOW-') return true;
  //   else return false;
  // }
  public getAppointmentDateDisplay(): string {
    let display = "";
    display = display + WEEKDAYS[this.node.getFecha().getDay()] + ', ';
    display = display + this.node.getFecha().getDate() + ' ';
    display = display + MONTHNAMES[this.node.getFecha().getMonth()] + ' ';
    display = display + this.node.getFecha().getFullYear();
    return display;
  }
  public getCitaTypeColor(): string {
    if (null != this.node) return this.node.tipo.toLowerCase();
  }
}
