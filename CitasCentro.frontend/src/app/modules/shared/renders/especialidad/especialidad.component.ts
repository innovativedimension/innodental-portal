//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- COMPONENTS
import { RenderComponent } from '../render/render.component';
//--- MODELS
import { Especialidad } from '../../models/Especialidad.model';

@Component({
  selector: 'cc-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.scss']
})
export class EspecialidadRenderComponent extends RenderComponent {
  @Input() node: Especialidad;

  public isExpandable(): boolean {
    if (null != this.node.expanded) return true;
    else return false;
  }
  public clickArrow(): void {
    this.node.toggleExpanded();
    this.container.notifyDataChanged()
  }
  public toggleExpand(): boolean {
    this.node.toggleExpanded();
    this.container.notifyDataChanged();
    return this.node.isExpanded();
  }
}
