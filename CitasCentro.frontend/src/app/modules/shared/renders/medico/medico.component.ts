//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- NOTIFICATIONS
//import { NotificationsService } from 'angular2-notifications';
// --- SERVICES
import { MedicoControllerService } from '@app/controllers/medico-controller.service';
// --- COMPONENTS
import { RenderComponent } from '../render/render.component';
// --- MODELS
import { Medico } from '@models/Medico.model';
import { ActoMedico } from '@models/ActoMedico.model';
import { Cita } from '@models/Cita.model';
import { AppStoreService } from '@app/services/appstore.service';

@Component({
  selector: 'cc-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.scss']
})
export class MedicoRenderComponent extends RenderComponent implements OnInit {
  @Input() node: Medico;

  // - C O N S T R U C T O R
  constructor(
    protected appStoreService : AppStoreService,
    protected medicoController: MedicoControllerService) {
    super();
  }

  // - L I F E C Y C L E
  ngOnInit() {
    this.development = true;
  }

  // - V I E W   I N T E R A C T I O N
  public representativeName(): string {
    if (this.node.isService()) return this.node.nombre;
    else return this.node.apellidos;
  }
  public reservedAppointments(): number {
    return Number(this.node.appointmentsCount) - Number(this.node.freeAppointmentsCount);
  }
  public getNodeActiveState(): boolean {
    console.log(">> [MedicoRenderComponent.getNodeActiveState]> active: " + this.node.activo);
    return this.node.activo;
  }
  public removeActoMedico(_target: ActoMedico, _event: any): void {
    // Stop the click for reaching the parent Medico and activating the edit.
    _event.stopPropagation();
    // Search this medical act on the list of acts for this service and remove it.
    this.node.removeActoMedico(_target);
    // After removal update the backend with the new service state.
    this.medicoController.updateMedico(this.node)
      .subscribe((valid) => {
        if (valid) {
          // Search this medical act on the list of acts for this service and remove it.
          // this.node.removeActoMedico(_target);
          this.appStoreService.successNotification("Médico/Servicio actualizado correctamente.", "Actualizado");
          console.log("--[MedicoRenderComponent.removeActoMedico]> Update completed");
        } else {
          this.appStoreService.errorNotification("Error durante la actualización del Médico/Servicio.", "Excepción");
          console.log("--[MedicoRenderComponent.removeActoMedico]> Error during update");
        }
        // return valid;
      });
  }
  public getTargetHeaderIdentifier(): string {
    if (this.node.isService) return this.node.nombre;
    else return this.node.apellidos;
  }
}
