//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// - TESTING
import { TestBed } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
// - COMPONENTS
import { ServiceSelectedRenderComponent } from './service-selected-render.component';
// - MODELS
import { Medico } from '@models/Medico.model';

// - S E R V I C E   S E L E C T E D   T E S T S . V 1
describe('RENDER ServiceSelectedRenderComponent [Module: SHARED]', () => {
  let component: ServiceSelectedRenderComponent;
  let fixture: ComponentFixture<ServiceSelectedRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ServiceSelectedRenderComponent],
    })
      .compileComponents();
    // Create the component and all the rest of the dependencies.
    // fixture = TestBed.createComponent(ServiceSelectedRenderComponent);
    // component = fixture.componentInstance;
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceSelectedRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[shared/ServiceSelectedRenderComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('fields should be on initial state', () => {
      console.log('><[shared/ServiceSelectedRenderComponent]> Input "ServiceSelectedRenderComponent.node" should be undefined');
      expect(component.container).toBeUndefined('Input "ServiceSelectedRenderComponent.node" should be undefined');
    });
  });

  // - I N P U T / O U T P U T   P H A S E
  describe('Input/Output Phase', () => {
    it('node (Input): validate node reception', () => {
      console.log('><[shared/ServiceSelectedRenderComponent]> node (Input): validate node reception');
      // Check the initial state to undefined.
      expect(component.node).toBeUndefined();

      // Create the test node to be used on the render.
      console.log('><[shared/ServiceSelectedRenderComponent]> Input: node');
      component.node = new Medico();
      expect(component.node).toBeDefined();
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code coverage Phase [getTargetClass]', () => {
    it('getTargetClass: get the class of the node', () => {
      console.log('><[shared/ServiceSelectedRenderComponent]> getTargetClass: get the class of undefined node');
      expect(component.getTargetClass()).toBe('-UNDEFINED-', 'should be "-UNDEFINED-"');
      component.node = new Medico();
      expect(component.getTargetClass()).toBe('Medico', 'should be "Medico"');
    });
  });
  describe('Code coverage Phase [getTarget]', () => {
    it('getTarget: get the node', () => {
      console.log('><[shared/ServiceSelectedRenderComponent]> getTarget: get the node');
      expect(component.getTarget()).toBeUndefined('should be undefined');
      component.node = new Medico();
      expect(component.getTarget()).toBeDefined('should exist');
    });
  });
});
