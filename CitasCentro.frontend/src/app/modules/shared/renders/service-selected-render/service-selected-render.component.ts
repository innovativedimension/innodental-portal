//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
// - COMPONENTS
import { RenderComponent } from '@renders/render/render.component';
// - INTERFACES
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';

@Component({
  selector: 'service-selected-render',
  templateUrl: './service-selected-render.component.html',
  styleUrls: ['./service-selected-render.component.scss']
})
export class ServiceSelectedRenderComponent extends RenderComponent {
  @Input() node: IAppointmentEnabled;

  public getTargetClass(): string {
    if (null != this.node) return this.node.getJsonClass();
    else return '-UNDEFINED-';
  }
  public getTarget(): IAppointmentEnabled {
    if (null != this.node) return this.node;
  }
}
