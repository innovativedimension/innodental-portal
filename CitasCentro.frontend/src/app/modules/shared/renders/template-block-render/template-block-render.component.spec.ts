//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
// --- COMPONENTS
import { TemplateBlockRenderComponent } from '@renders/template-block-render/template-block-render.component';
// --- MODELS
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';

describe('RENDER TemplateBlockRenderComponent [Module: SHARED]', () => {
  let component: TemplateBlockRenderComponent;
  let fixture: ComponentFixture<TemplateBlockRenderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [TemplateBlockRenderComponent],
    })
      .compileComponents();
    // Create the component and all the rest of the dependencies.
    fixture = TestBed.createComponent(TemplateBlockRenderComponent);
    component = fixture.componentInstance;
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('ui/TemplateBlockRenderComponent Construction Phase', () => {
    it('should be created', () => {
      console.log('><[shared/TemplateBlockRenderComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[shared/TemplateBlockRenderComponent]> "node" should be undefined');
      expect(component.node).toBeUndefined();
    });
  });

  // - I N P U T / O U T P U T   P H A S E
  describe('ui/TemplateBlockRenderComponent Input/Output Phase', () => {
    it('Inputs receivers: input compliance', () => {
      console.log('><[shared/TemplateBlockRenderComponent]> Inputs receivers: input compliance');
      // Check the initial state to undefined.
      expect(component.node).toBeUndefined();

      // Create the test node to be used on the render.
      let block = new TemplateBuildingBlock();
      component.node = block;
      expect(component.node).toBeDefined();
    });
  });
});
