//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
// --- MODELS
import { TemplateRenderComponent } from '@renders/template-render/template-render.component';
import { CitasTemplate } from '@models/CitasTemplate.model';

describe('RENDER TemplateRenderComponent [Module: SHARED]', () => {
  let component: TemplateRenderComponent;
  let fixture: ComponentFixture<TemplateRenderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [TemplateRenderComponent],
    })
      .compileComponents();
    // Create the component and all the rest of the dependencies.
    fixture = TestBed.createComponent(TemplateRenderComponent);
    component = fixture.componentInstance;
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[shared/TemplateRenderComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[shared/TemplateRenderComponent]> "node" should be undefined');
      expect(component.node).toBeUndefined();
      console.log('><[shared/TemplateRenderComponent]> "editRequest" should exist');
      expect(component.editRequest).toBeDefined();
      console.log('><[shared/TemplateRenderComponent]> "deleteRequest" should should exist');
      expect(component.deleteRequest).toBeDefined();
    });
  });

  // - I N P U T / O U T P U T   P H A S E
  describe('Input/Output Phase', () => {
    it('node (Input): validate node reception', () => {
      console.log('><[shared/TemplateRenderComponent]> node (Input): validate node reception');
      // Check the initial state to undefined.
      expect(component.node).toBeUndefined();

      // Create the test node to be used on the render.
      console.log('><[shared/TemplateRenderComponent]> Input: node');
      let block = new CitasTemplate();
      component.node = block;
      expect(component.node).toBeDefined();
    });
    it('editRequest (Output): emit editRequest message', () => {
      console.log('><[shared/TemplateRenderComponent]> editRequest (Output): emit editRequest message');
       spyOn(component.editRequest, 'emit');
      component.editRequest.subscribe((template: CitasTemplate) => {
        expect(template.nombre).toBe('Nombre de Prueba');
        expect(template.tmhoraInicio).toBeUndefined();
        expect(template.getMorningList().length).toBe(0);
      });
      component.node = new CitasTemplate();
      component.node.nombre = 'Nombre de Prueba';
      component.editTemplate();
      expect(component.editRequest.emit).toHaveBeenCalled();
    });
    it('deleteRequest (Output): emit deleteRequest message', () => {
      console.log('><[shared/TemplateRenderComponent]> deleteRequest (Output): emit deleteRequest message');
      spyOn(component.deleteRequest, 'emit');
      component.deleteRequest.subscribe((template: CitasTemplate) => {
        expect(template.nombre).toBe('Delete de Prueba');
        expect(template.tmhoraInicio).toBeUndefined();
        expect(template.getMorningList().length).toBe(0);
      });
      component.node = new CitasTemplate();
      component.node.nombre = 'Delete de Prueba';
      component.deleteTemplate();
      expect(component.deleteRequest.emit).toHaveBeenCalled();
    });
  });
});
