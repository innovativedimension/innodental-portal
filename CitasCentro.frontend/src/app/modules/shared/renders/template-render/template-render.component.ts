//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
// --- COMPONENTS
import { RenderComponent } from '@renders/render/render.component';
// --- MODELS
import { CitasTemplate } from '@models/CitasTemplate.model';

@Component({
  selector: 'template4-list',
  templateUrl: './template-render.component.html',
  styleUrls: ['./template-render.component.scss']
})
export class TemplateRenderComponent extends RenderComponent {
  @Input() node: CitasTemplate;
  @Output() editRequest = new EventEmitter<CitasTemplate>();
  @Output() deleteRequest = new EventEmitter<CitasTemplate>();

  // - O U T P U T   E M I T T E R S
  public editTemplate(): void {
    this.editRequest.emit(this.node);
  }
  public deleteTemplate(): void {
    this.deleteRequest.emit(this.node);
  }
}
