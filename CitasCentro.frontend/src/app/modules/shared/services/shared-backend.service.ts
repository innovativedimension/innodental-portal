//  PROJECT:     Citas.library(CIT.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  SITE:        citascentro.com
//  DESCRIPTION: Citas. Componente libreria. Este proyecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitasCentro/CitasPaciente).
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
// --- ENVIRONMENT
// import { environment } from '@env/environment';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
// --- HTTP PACKAGE
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
// --- ROUTER
import { Router } from '@angular/router';
// --- NOTIFICATIONS
//import { NotificationsService } from 'angular2-notifications';
//--- DATE FUNCTIONS
import { differenceInMilliseconds } from 'date-fns';
// --- SERVICES
import { IsolationService } from '../../../platform/isolation.service';
import { RuntimeBackendService } from '../../../platform/runtime-backend.service';
// --- INTERFACES
import { INode } from '../interfaces/core/INode.interface';
import { IAppointmentEnabled } from '../interfaces/IAppointmentEnabled.interface';
// --- MODELS
import { Centro } from '../models/Centro.model';
import { Cita } from '../models/Cita.model';
import { Medico } from '../models/Medico.model';
import { Credencial } from '../models/Credencial.model';
import { Limitador } from '../models/Limitador.model';
import { TemplateBuildingBlock } from '../models/TemplateBuildingBlock.model';
import { CitasTemplate } from '../models/CitasTemplate.model';
import { ActoMedicoCentro } from '../models/ActoMedicoCentro.model';

import { CredentialRequest } from '@app/models/CredentialRequest.model';
import { AppointmentReport } from '@app/models/AppointmentReport.model';
import { FormularioCreacionCitas } from '@app/models/FormularioCreacionCitas.model';
import { PatientData } from '@app/models/PatientData.model';
import { OpenAppointment } from '@app/models/OpenAppointment.model';
import { Perfil } from '@app/models/Perfil.model';
import { ServiceAuthorized } from '@app/models/ServiceAuthorized.model';
import { AppModule } from '@app/models/AppModule.model';
import { NewMedico } from '@app/models/NewMedico.model';

@Injectable({
  providedIn: 'root'
})
export class SharedBackendService extends RuntimeBackendService {
  // --- C O N S T R U C T O R
  constructor(
    public isolation: IsolationService,
    protected http: HttpClient) {
    super();
  }

  // - A P P O I N T M E N T S   T E M P L A T E S
  public backendCreateTemplate(_template: CitasTemplate, _owner: string): Observable<CitasTemplate> {
    console.log("><[BackendService.readAppointmentTemplates]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' +
      _owner + '/templates/new';
    return this.wrapHttpPOSTCall(request, JSON.stringify(_template));
  }
  public backendDeleteTemplate(_templateIdentifier: number): Observable<number> {
    console.log("><[BackendService.backendDeleteTemplate]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/templates/delete/' +
      _templateIdentifier;
    return this.wrapHttpDELETECall(request);
  }
  public backendUpdateTemplate(_template: CitasTemplate): Observable<CitasTemplate> {
    console.log("><[BackendService.readAppointmentTemplates]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/templates/update/' +
      _template.id;
    return this.wrapHttpPUTCall(request, JSON.stringify(_template));
  }
  // @Deprecated
  public readCredentialTemplates(_credential: Credencial): Observable<CitasTemplate[]> {
    console.log("><[BackendService.readCredentialTemplates]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' +
      _credential.getId() + '/templates';
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as CitasTemplate[];
      }));
  }
  public readServiceTemplates(_service: Medico): Observable<CitasTemplate[]> {
    console.log("><[BackendService.readServiceTemplates]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' +
      _service.getId() + '/templates';
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as CitasTemplate[];
      }));
  }

  //---  H T T P   W R A P P E R S
  // private wrapHttpHandleError(error: HttpErrorResponse) {
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurred:', error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     console.error(
  //       `Backend returned code ${error.status}, ` +
  //       `Error message ${error.message}, `);

  //     // Do some generic error processing.
  //     // 401 are accesses without the token so we should move right to the login page.
  //     if (error.status == 401) {
  //       // this.router.navigate(['login']);
  //       return throwError('Autenticacion ya no valida. Es necesario logarse de nuevo.');
  //     }
  //   }
  //   // return an observable with a user-facing error message
  //   return throwError(
  //     'Something bad happened; please try again later.');
  // }
  public wrapHttpRESOURCECall(request: string): Observable<any> {
    console.log("><[AppStoreService.wrapHttpGETCall]> request: " + request);
    return this.http.get(request);
  }
  /**
   * This method wraps the HTTP access to the backend. It should add any predefined headers, any request specific headers and will also deal with mock data.
   * Mock data comes now into two flavours. he first one will search for the request on the list of defined requests (if the mock is active). If found then it will check if the request should be sent to the file system ot it should be resolved by accessing the LocalStorage.
   * @param  request [description]
   * @return         [description]
   */
  public wrapHttpGETCall(_request: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpGETCall]> request: " + adjustedRequest);
      // let newheaders = this.isolation.wrapHttpSecureHeaders(_requestHeaders);
      // return this.http.get(adjustedRequest, { headers: newheaders });
    } else {
      // The requst is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpGETCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  public wrapHttpPUTCall(_request: string, _body: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpPUTCall]> request: " + adjustedRequest);
      console.log("><[AppStoreService.wrapHttpPUTCall]> body: " + _body);
      // let newheaders = this.isolation.wrapHttpSecureHeaders(_requestHeaders);
      // return this.http.put(adjustedRequest, _body, { headers: newheaders });
      // .pipe(
      //   catchError(this.wrapHttpHandleError)
      // );
    } else {
      // The requst is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpPUTCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  public wrapHttpPOSTCall(_request: string, _body: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpPOSTCall]> request: " + adjustedRequest);
      console.log("><[AppStoreService.wrapHttpPOSTCall]> body: " + _body);
      // let newheaders = this.isolation.wrapHttpSecureHeaders(_requestHeaders);
      // return this.http.post(adjustedRequest, _body, { headers: newheaders });
      // .pipe(
      //   catchError(this.wrapHttpHandleError)
      // );
    } else {
      // The requst is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpPOSTCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  public wrapHttpDELETECall(_request: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpDELETECall]> request: " + adjustedRequest);
      // let newheaders = this.isolation.wrapHttpSecureHeaders(_requestHeaders);
      // return this.http.delete(adjustedRequest, { headers: newheaders });
    } else {
      // The request is a LOCALSTORAGE mockup and should return with no more processing.
      console.log("><[AppStoreService.wrapHttpDELETECall]> request: " + _request);
      return adjustedRequest;
    }
  }
  protected wrapHttpSecureRequest(request: string): string | Observable<any> {
    // Check if we should use mock data.
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) {
        // Check if the resolution should be from the LocalStorage. URL should start with LOCALSTORAGE::.
        if (hit.search('LOCALSTORAGE::') > -1) {
          return Observable.create((observer) => {
            // WARNING: This implies a dependency we do not like. We are removing this functionality
            let targetData = this.isolation.getFromStorage(hit + ':' + '1234567890');
            try {
              if (null != targetData) {
                // .then((data) => {
                console.log('--[AppStoreService.wrapHttpPOSTCall]> Mockup data: ', targetData);
                // Process and convert the data string to the class instances.
                let results = this.transformRequestOutput(JSON.parse(targetData));
                observer.next(results);
                observer.complete();
              } else {
                observer.next([]);
                observer.complete();
              }
            } catch (mockException) {
              observer.next([]);
              observer.complete();
            }
          })
        } else request = hit;
      }
    }
    return request;
  }

  //--- R E S P O N S E   T R A N S F O R M A T I O N
  public transformRequestOutput(entrydata: any): INode[] | INode {
    let results: INode[] = [];
    // Check if the entry data is a single object. If so process it because can be an exception.
    if (entrydata instanceof Array) {
      for (let key in entrydata) {
        // Access the object into the spot.
        let node = entrydata[key] as INode;
        // Convert and add the node.
        results.push(this.convertNode(node));
      }
    } else {
      // Process a single element.
      let jclass = entrydata["jsonClass"];
      if (null == jclass) return [];
      return this.convertNode(entrydata);
    }
    return results;
  }
  protected convertNode(node): INode {
    switch (node.jsonClass) {
      case "Centro":
        let convertedCentro = new Centro(node);
        console.log("--[AppModelStoreService.convertNode]> Centro node: " + convertedCentro.getId());
        return convertedCentro;
      case "Medico":
        let convertedMedico = new Medico(node);
        console.log("--[AppModelStoreService.convertNode]> Medico node: " + convertedMedico.getId());
        return convertedMedico;
      case "Cita":
        let convertedCita = new Cita(node);
        // console.log("--[AppModelStoreService.convertNode]> Cita node: " + convertedCita.huecoId);
        return convertedCita;
      case "AppointmentReport":
        let reportCita = new AppointmentReport(node);
        console.log("--[AppModelStoreService.convertNode]> AppointmentReport node: " + reportCita.reportdate);
        return reportCita;
      case "Credencial":
        let convertedCredential = new Perfil(node);
        console.log("--[AppModelStoreService.convertNode]> AppointmentReport node: " + convertedCredential.getId());
        return convertedCredential;
      case "CitasTemplate":
        let convertedTemplate = new CitasTemplate(node);
        console.log("--[AppModelStoreService.convertNode]> CitasTemplate node: " + convertedTemplate.nombre);
        return convertedTemplate;
      case "Limitador":
        let convertedLimit = new Limitador(node);
        console.log("--[AppModelStoreService.convertNode]> Limitador node: " + convertedLimit.titulo);
        return convertedLimit;
      case "TemplateBuildingBlock":
        let convertedBuildingBlock = new TemplateBuildingBlock(node);
        console.log("--[AppModelStoreService.convertNode]> TemplateBuildingBlock node: " + convertedBuildingBlock.nombre);
        return convertedBuildingBlock;
      case "ActoMedicoCentro":
        let convertedActoMedico = new ActoMedicoCentro(node);
        console.log("--[AppModelStoreService.convertNode]> ActoMedicoCentro node: " + convertedActoMedico.nombre);
        return convertedActoMedico;
      case "OpenAppointment":
        let convertedAppointment = new OpenAppointment(node);
        console.log("--[AppModelStoreService.convertNode]> OpenAppointment node: " + convertedAppointment.getId());
        return convertedAppointment;
      case "ServiceAuthorized":
        let convertedService = new ServiceAuthorized(node);
        console.log("--[AppModelStoreService.convertNode]> ServiceAuthorized node: " + convertedService.getId());
        return convertedService;
      case "AppModule":
        let convertedAppModule = new AppModule(node);
        console.log("--[AppModelStoreService.convertNode]> AppModule node: " + convertedAppModule.nombre);
        return convertedAppModule;
    }
  }
  protected date2BasicISO(_date: Date): string {
    var local = new Date(_date);
    local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }
}