//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
// --- MODELS
import { ButtonEvent } from '@models/core/ButtonEvent.model';

@Component({
  selector: 'app-button-v2',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponentv2 {
  @Input() disabled: boolean = false;
  @Input() title: string = '-BUTTON-';
  @Input() activationTitle: string = 'Validación -BUTTON-...';
  @Input() standByIcon: string = 'fas fa-check';
  @Input() styleClass: string = 'blue-button';
  @Output() activateAction = new EventEmitter<ButtonEvent>();
  public processing: boolean = false;
  public signalError: boolean = false;

  public isDisabled(): boolean {
    return this.disabled;
  }
  public onClick(_event: any): void {
    console.log(">>[ButtonComponent.onClick]> event.type: " + JSON.stringify(_event));
    console.log(">>[ButtonComponent.onClick]> event.altKey: " + _event.altKey);
    console.log(">>[ButtonComponent.onClick]> event.ctrlKey: " + _event.ctrlKey);
    this.processing = true;
    this.activateAction.emit(new ButtonEvent({
      button: this,
      event: _event
    }));
  }
  public getButtonClass(): string {
    if (this.processing) return 'btn ' + 'processing-' + this.styleClass;
    else return 'btn ' + this.styleClass;
  }
  public completeAction(): void {
    this.processing = false;
  }

  // - I N T E R N A L
  public async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms));
  }
}
