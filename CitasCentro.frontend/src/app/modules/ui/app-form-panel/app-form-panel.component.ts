//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
//--- ENVIRONMENT
import { environment } from '@env/environment';
//--- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
//--- ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
//--- NOTIFICATIONS
import { ToastrManager } from 'ng6-toastr-notifications';
//import { NotificationsService } from 'angular2-notifications';
//--- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
//--- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'notused-app-form-panel',
  templateUrl: './notused.html'
})
export class AppFormPanelComponent extends AppPanelComponent {
  // - C O N S T R U C T O R
  constructor(
    // @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    // @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    // //protected toasterService: NotificationsService,
    // //protected notifier: ToastrManager,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    protected formBuilder: FormBuilder) {
    super(router, activeRoute, appStoreService, backendService);
  }
}
