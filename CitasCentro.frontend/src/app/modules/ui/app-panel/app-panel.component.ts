//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
// - ENVIRONMENT
import { environment } from '@env/environment';
// - ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// - COMPONENTS
import { MVCViewerComponent } from '@app/modules/ui/mvcviewer/mvcviewer.component';

// - CONSTANTS
export const WEEKDAYS = [
  'Domingo',
  'Lunes',
  'Martes',
  'Miércoles',
  'Jueves',
  'Viernes',
  'Sábado'
];
export const MONTHNAMES = [
  'enero',
  'febrero',
  'marzo',
  'abril',
  'mayo',
  'junio',
  'julio',
  'agosto',
  'septiembre',
  'octubre',
  'noviembre',
  'diciembre'
];

@Component({
  selector: 'notused-app-panel',
  templateUrl: './notused.html'
})
export class AppPanelComponent extends MVCViewerComponent {
  @Input() title: string = '-PANEL TITLE-'; // The title to setup on the panel header.
  @Input() show: boolean = true; // By default all panels are visible. Other logic should hide them.
  public canBeClosed: boolean = false; // If TRUE then we can show the close icon and activate the closing action.
  public canBeExpanded: boolean = false; // If TRUE then we can show the expand collapse indicator.
  public leftContent: string; // Variable to store the left content of the panel header
  public development: boolean = false; // tells if we have to show development only elements.

  // - C O N S T R U C T O R
  constructor(
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) {
    super();
    this.development = environment.development;
  }

  // - G E T T E R S   &   S E T T E R S
  public getTitle(): string {
    return this.title;
  }
  public getShowState(): boolean {
    return this.show;
  }

  // - V I E W   I N T E R A C T I O N
  public closePanel(): void {
    this.show = false;
  }
}
