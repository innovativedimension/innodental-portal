//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
//--- CALENDAR
import { isBefore } from 'date-fns';
import { startOfDay } from 'date-fns';
import { endOfDay } from 'date-fns';
import { CalendarEvent } from 'angular-calendar';
//--- COMPONENTS
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
// --- INTERFACES
// import { IAppointmentEnabled } from '@shared/interfaces/IAppointmentEnabled.interface';
//--- MODELS
// import { Centro } from '@models/Cita.model';
import { Cita } from '@models/Cita.model';
import { Payload } from '@models/Payload.model';
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';

/**
This is the core code that is shared by all pages. Implements most of the code to deal with the model list data and the external request for data to be rendered on the UI.
*/
@Component({
  selector: 'x-calendar-core',
  templateUrl: './notused.html'
})
export class CalendarCoreComponent extends AppPanelComponent {
  //--- CALENDAR VARIABLES
  public viewDate: Date = new Date();
  public events: CalendarEvent[] = [];
  public refresh: Subject<any> = new Subject();
  private view: string = 'month';

  //--- CALENDAR CONTENTS VARIABLES
  public selectedCitas: Cita[] = [];
  public citaList: Cita[] = [];
  public eventList = new Map<string, Payload>();

  //--- SELECTION MANAGEMENT
  public selectedDates: Map<Date, any> = new Map<Date, any>();
  public selectionCount: number = 0;

  //--- S E L E C T I O N
  public isCellSelected(_day: any): boolean {
    if (null == _day['selected']) return false;
    else return true;
  }

  // - B A C K E N D   A C C E S S
  public downloadAppointments(_target: IAppointmentEnabled): void {
    console.log(">>[CalendarFreeSlotsPanelComponent.downloadAppointments]");
    if (null != _target) {
      this.downloading = true;
      let serviceIdentifiers: string[] = _target.getServiceIdentifiers();
      let appointments = [];
      for (let identifier of serviceIdentifiers) {
        // Download the data about the appointments already reserved.
        this.backendService.backendCitas4MedicoById(identifier)
          .subscribe((citas) => {
            console.log("--[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]> count: " + citas.length);
            // Aggregate the data from multiples identifiers.
            appointments = appointments.concat(citas);
            // Store the processed appointements on the AppStore for easy access by other components.
            console.log("--[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]> Storing: " + appointments.length + " appointments.");
            this.appStoreService.storeAppointments(appointments);
            this.downloading = false;
            console.log("<<[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]");
          }), (error) => {
            // Process any 401 exception that means the session is no longer valid.
            if (error.status == 401) {
              this.appStoreService.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
              this.router.navigate(['login']);
            }
            console.log("--[CalendarFreeSlotsPanelComponent.backendCitas4MedicoById]> Error: " + error.message);
            this.appStoreService.errorNotification("Error actualizando Médico/Servicio. Mensaje: " + error.message, "¡Atención!");
          };
      }
    }
  }

  //--- C A L E N D A R   E V E N T   M A N A G E M E N T
  public accountAppointment(_cita: Cita): void {
    // Do the accounting to generate the events.
    let hit: Payload = this.eventList.get(this.date2BasicISO(_cita.getFecha()));
    if (null == hit) {
      hit = new Payload();
      this.eventList.set(this.date2BasicISO(_cita.getFecha()), hit);
    }
    // Do the reserved/available accounting
    // if (_cita.getEstado() === 'VACACIONES')
    //   hit.addVacancy();
    // else if (_cita.patientIsValid())
    //   hit.addReserved();
    // else hit.addOpen();
    hit.addCita(_cita);
  }
  public getCurrentSelectedDate(): Date {
    return this.viewDate;
  }
  public dayClicked(_event: any): void {
    console.log("><[AppointmentDateSelectorComponent.dayClicked]> event: " + JSON.stringify(_event));
    // Toggle the selected state for the date selected.
    if (null == _event.day['selected']) {
      _event.day['selected'] = true;
      this.selectedDates.set(_event.day.date, _event.day);
      this.selectionCount++;
    } else {
      // Remove selection.
      _event.day['selected'] = null;
      this.selectedDates.set(_event.day.date, null);
      this.selectionCount--;
    }
  }

  //--- P R I V A T E   M E T H O D S
  // private updateCalendarData(_appointments: Cita[]): void {
  //   // Update the calendar data structures
  //   console.log("--[CitasReservadasComponent.onTemplateDrop]> count: " + _appointments.length);
  //   this.eventList = new Map<string, Pair>();
  //   this.citaList = [];
  //   for (let cita of _appointments) {
  //     this.citaList.push(cita);
  //     // Do the accounting to generate the events.
  //     let hit = this.eventList.get(cita.getFechaString());
  //     if (null == hit) {
  //       hit = new Pair();
  //       this.eventList.set(cita.getFechaString(), hit);
  //     }
  //     // console.log("--[CitasReservadasComponent.onTemplateDrop]> processing hit: " + JSON.stringify(hit));
  //     if (cita.patientIsValid())
  //       hit.addReserved();
  //     else hit.addOpen();
  //   }
  // }
  public clearSelection(): void {
    this.selectedDates.forEach((value, key) => {
      value['selected'] = null;
    });
    this.selectionCount = 0;
  }
  public add2Selection(_day: any): void {
    _day['selected'] = true;
    this.selectedDates.set(_day.date, _day);
    this.selectionCount++;
  }

  // - P`R I V A T E   F U N C T I O N A L I T Y
  /**
   * Converts the list of payload data and apopintments into the internal data structures used on the Angular
   * Calendar component.
   *
   * @protected
   * @memberof CalendarCoreComponent
   */
  protected processEvents(): void {
    this.events = [];
    this.eventList.forEach((value: Payload, key: string) => {
      console.log("--[CalendarCoreComponent.processEvents]> Day payload: "
        + 'key: ' + key +
        + value.getReserved() + "/" + value.getOpen() + " : " + value.getFree());
      let newevent = {
        start: startOfDay(key),
        end: endOfDay(key),
        title: value.getReserved() + "/" + value.getOpen(),
        total: value.getTotal(),
        reserved: value.getReserved(),
        vacancy: value.getVacancy(),
        payload: value,
        selected: false
      };
      this.events.push(newevent);
    });
    // Send a message to update the calendar.
    this.refresh.next();
  }
}
