//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import { Input } from '@angular/core';
//--- ENVIRONMENT
import { environment } from '@env/environment';
// //--- ROUTER
// import { Router } from '@angular/router';
// //--- NOTIFICATIONS
// //import { NotificationsService } from 'angular2-notifications';
// //--- WEBSTORAGE
// import { LOCAL_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
//--- INTERFACES
// import { ETaskState } from 'app/interfaces/EPack.enumerated';
// import { EColorTheme } from 'app/interfaces/EPack.enumerated';
// import { ETratamiento } from 'app/interfaces/EPack.enumerated';
//--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
import { AppStoreService } from '@app/services/appstore.service';
//--- MODELS
// import { TaskFlag } from 'app/models/TaskFlag.model';
import { Credencial } from '@models/Credencial.model';
import { Medico } from '@models/Medico.model';
// import { FormularioCreacionCitas } from 'app/models/FormularioCreacionCitas.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public credencial: Credencial = new Credencial();
  // public isServiceActive: boolean = false;
  public activeService: Medico;

  constructor(
    protected appStoreService: AppStoreService) {
  }

  ngOnInit() {
    this.credencial = this.appStoreService.accessCredential();
    // this.activeService = this.appStoreService.accessActiveService();
    // if (null != this.activeService) this.isServiceActive = true;
  }
  public isCredentialValid(): boolean {
    // let credential = this.appStoreService.accessCredential();
    return this.credencial.isValid();
  }
  public isServiceActive(): boolean {
    this.activeService = this.appStoreService.accessActiveService();
    if (null != this.activeService) return true;
    else return false;
  }
  public getCredentialName(): string {
    // let credential = this.appStoreService.accessCredential();
    let name = this.credencial.getNombre() + " " + this.credencial.getApellidos();
    if (environment.development) name = name + " [" + this.credencial.getId() + "]";
    return name;
  }
  public getEspecialidad(): string {
    // let credential = this.appStoreService.accessCredential();
    return this.credencial.getEspecialidad();
  }
  public getActiveService(): Medico {
    if (null != this.activeService) return this.activeService;
    else return new Medico();
  }
}
