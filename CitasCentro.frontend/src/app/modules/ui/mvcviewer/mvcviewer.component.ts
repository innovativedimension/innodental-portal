//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- INTERFACES
import { INode } from '@interfaces/core/INode.interface';
import { IContainerController } from '@interfaces/core/IContainerController.interface';

@Component({
  selector: 'notused-mvcviewer',
  templateUrl: './notused.html'
})
export class MVCViewerComponent implements IContainerController {
  @Input() container: IContainerController; // reference to the parent container.
  /** This defines the rendering variant that can be used when collaborating nodes. */
  @Input() variant: string = '-DEFAULT-';
  public self: IContainerController = this; // Auto pointer to me to be used as container.

  /** Node activated by hovering over it with the mouse cursor. May be null. */
  protected selectedNode: INode;
  /** This exportable property will be used by the UI to know when to draw/hide the spinner. */
  public downloading: boolean = false;
	/** This is the single pointer to the model data that is contained on this page. This is the first element than when processed with the collaborate2View process will generate the complete list of nodes to render and received by the factory from the getBodyComponents().
	This variable is accessed directly (never been null) and it if shared with all descendans during the generation process. */
  public dataModelRoot: INode[] = [];
  /** The real time updated list of nodes to render. */
  public renderNodeList: INode[] = [];

  // - L I F E C Y C L E
  ngOnInit() {
    console.log(">>[MVCViewerComponent.ngOnInit]");
    this.cleanModel();
    console.log("<<[MVCViewerComponent.ngOnInit]");
  }

  // --- G E T T E R S   &   S E T T E R S
  public getVariant(): string {
    return this.variant;
  }
  public setVariant(variant: string): void {
    this.variant = variant;
  }

  //--- I C O N T A I N E R C O N T R O L L E R   I N T E R F A C E
  /**
  * Return the reference to the component that knows how to locate the Page to transmit the refresh events when any user action needs to update the UI. This is usually the parent container or Page for the target Panel.
  *
  * @returns {IContainerController} Returns the Input references as the pointer to the container of this instance.
  * @memberof MVCViewerComponent
  */
  public getContainer(): IContainerController {
    return this.container;
  }
  /**
  * Reconstructs the list of nodes to be rendered from the current DataRoot and their collaborations to the view.
  * @memberof MVCViewerComponent
  */
  public notifyDataChanged(): void {
    console.log(">>[BasePageComponent.notifyDataChanged]");
    // Clear the current list while reprocessing the new nodes.
    let copyList = [];
    // Get the initial list by applying the policies defined at the page to the initial root node contents. Policies may be sorting or filtering actions.
    let initialList = this.applyPolicies(this.dataModelRoot);
    // Generate the contents by collaborating to the view all the nodes.
    for (let node of initialList) {
      let nodes = node.collaborate2View(this.getVariant());
      console.log("--[BasePageComponent.notifyDataChanged]> Collaborating " + nodes.length + " nodes.");
      // Add the collaborated nodes to the list of nodes to return.
      for (let childNode of nodes) {
        copyList.push(childNode);
      }
    }
    this.renderNodeList = copyList;
    console.log("<<[BasePageComponent.notifyDataChanged]");
  }
  public applyPolicies(_entries: INode[]): INode[] {
    return _entries;
  }
  /**
   * Clear the model data structure to avoid adding more items without removing the previous list.
   *
   * @memberof MVCViewerComponent
   */
  public cleanModel(): void {
    this.dataModelRoot = [];
  }

  // - A D D I T I O N A L   F U N C T I O N A L I T Y
  public isEmpty(obj: any): boolean {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }
  public isNotEmpty(obj: any): boolean {
    return !this.isEmpty(obj);
  }
  /**
   * Adds time to a date. Modelled after MySQL DATE_ADD function.
   * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
   * https://stackoverflow.com/a/1214753/18511
   *
   * @param date  Date to start with
   * @param interval  One of: year, quarter, month, week, day, hour, minute, second
   * @param units  Number of units of the given interval to add.
   * @memberof MVCViewerComponent
   */
  public dateAdd(date: Date, interval: string, units: number) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }
  /**
   * Converts a Javascript Date to a format compatible with the Basic ISO date format suitable to be used on url parameters.
   *
   * @param {Date} _date The date to be converted
   * @returns {string} A string with the date in format BasicISO '000Y0M0D'.
   * @memberof MVCViewerComponent
   */
  public date2BasicISO(_date: Date): string {
    var local = new Date(_date);
    local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }
}
