//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// --- NOTIFICATIONS
import { ToastrModule } from 'ng6-toastr-notifications';
// --- PIPES
import { CapitalizeLetterPipe } from './pipes/capitalize-letter.pipe';
import { ISKNoDecimalsPipe } from './pipes/iskno-decimals.pipe';
import { IskScaledPipe } from './pipes/iskscaled.pipe';
import { ArraySortPipe } from './pipes/array-sort.pipe';

// --- APPLICATION MODULES
import { SharedModule } from '@app/modules/shared/shared.module';
// --- COMPONENTS-UI
import { MVCViewerComponent } from './mvcviewer/mvcviewer.component';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { AppFormPanelComponent } from '@app/modules/ui/app-form-panel/app-form-panel.component';
import { CalendarCoreComponent } from '@app/modules/ui/calendar-core/calendar-core.component';
import { HeaderComponent } from './header/header.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { ModalComponent } from '@app/modules/ui/modal.directive/modal.directive';
import { ButtonComponent } from '@app/modules/ui/app-button/button.component';
import { ButtonComponentv2 } from '@app/modules/ui/app-button-v2/button.component';
import { AppPanelV2Component } from './app-panelv2/app-panelv2.component';

@NgModule({
  imports: [
    // --- CORE
    CommonModule,
    // --- NOTIFICATIONS
    ToastrModule.forRoot(),
    // --- APPLICATION MODULES
    // CitaMedLibModule,
    SharedModule
  ],
  declarations: [
    //--- PIPES
    CapitalizeLetterPipe,
    ISKNoDecimalsPipe,
    IskScaledPipe,
    ArraySortPipe,
    //--- COMPONENTS-UI
    MVCViewerComponent,
    AppPanelComponent,
    AppPanelV2Component,
    AppFormPanelComponent,
    CalendarCoreComponent,
    HeaderComponent,
    MenuBarComponent,
    ModalComponent,
    ButtonComponent,
    ButtonComponentv2
  ],
  exports: [
    //--- PIPES
    CapitalizeLetterPipe,
    ISKNoDecimalsPipe,
    IskScaledPipe,
    ArraySortPipe,
    //--- COMPONENTS-UI
    MVCViewerComponent,
    AppPanelComponent,
    AppPanelV2Component,
    AppFormPanelComponent,
    CalendarCoreComponent,
    HeaderComponent,
    MenuBarComponent,
    ModalComponent,
    ButtonComponent,
    ButtonComponentv2
  ]
})
export class UIModule { }
