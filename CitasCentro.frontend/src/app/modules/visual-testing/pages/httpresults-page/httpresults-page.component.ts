//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
// --- HTTP PACKAGE
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
// --- SERVICES
import { BackendService } from '@app/services/backend.service';
import { AppStoreService } from '@app/services/appstore.service';
import { IsolationService } from '@app/platform/isolation.service';
// --- MODELS
import { Perfil } from '@app/models/Perfil.model';
import { CredentialRequest } from '@app/models/CredentialRequest.model';
// import { HttpErrorResponse } from '@angular/common/http';

// export type HandleError = <T> (operation?: string, result?: T) => (error: HttpErrorResponse) => Observable<T>;

@Component({
  selector: 'httpresults-page',
  templateUrl: './httpresults-page.component.html',
  styleUrls: ['./httpresults-page.component.scss']
})
export class HTTPResultsPageComponent /*implements OnInit*/ {
  public request: string = "-AWAITING-";
  public responseOkraw: string;
  public responseOk: string;

  // private handleError: HandleError;

  // - C O N S T R U C T O R
  constructor(
    protected http: HttpClient,
    protected isolation: IsolationService,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) { }

  // - R E Q U E S T S   T E S T S
  public onReqNoHeaders(_data: any) {
    this.onReqNoHeadersCall().subscribe((result) => {
      console.log(">>[HTTPResultsPageComponent.onReqNoHeaders]");
      console.log(">>[HTTPResultsPageComponent.onReqNoHeaders]> result: " + result);
      this.appStoreService.successNotification('Request completado correctamente.')
    }, (error) => {
      // Process any 401 exception that means the session is no longer valid.
      this.responseOkraw = JSON.stringify(error);
      this.responseOk = error.message;
      if (error.status == 400) {
        this.appStoreService.errorNotification(error.message, "Error HTTP 400.",
          { dismiss: 'click' });
      }
      if (error.status == 401) {
        this.appStoreService.errorNotification(error.message, "Error HTTP 401.",
          { dismiss: 'click' });
      }
      if (error.status == 412) {
        this.appStoreService.errorNotification(error.message, "Error HTTP 412.",
          { dismiss: 'click' });
      }
    });
  }
  public onInvalidMethod(_data: any) {
    this.onInvalidMethodCall().subscribe((result) => {
      console.log(">>[HTTPResultsPageComponent.onReqNoHeaders]");
      console.log(">>[HTTPResultsPageComponent.onReqNoHeaders]> result: " + result);
      this.appStoreService.successNotification('Request completado correctamente.')
    }, (error) => {
      // Process any 401 exception that means the session is no longer valid.
      this.responseOkraw = JSON.stringify(error);
      this.responseOk = error.message;
      if (error.status == 400) {
        this.appStoreService.errorNotification(error.message, "Error HTTP 400.",
          { dismiss: 'click' });
      }
      if (error.status == 401) {
        this.appStoreService.errorNotification(error.message, "Error HTTP 401.",
          { dismiss: 'click' });
      }
      if (error.status == 405) {
        this.appStoreService.errorNotification(error.message, "Error HTTP 405.",
          { dismiss: 'click' });
      }
      if (error.status == 412) {
        this.appStoreService.errorNotification(error.message, "Error HTTP 412.",
          { dismiss: 'click' });
      }
    });
  }

  public onInvalidMethodCall(): Observable<Perfil> {
    console.log("><[HTTPResultsPageComponent.onReqNoHeadersCall]");
    // Construct the request to call the backend.
    this.request = this.isolation.getServerName() + this.isolation.getApiV1() + "/credencial/validate";
    console.log("--[HTTPResultsPageComponent.onReqNoHeadersCall]> request = " + ~this.request);
    let credential = new CredentialRequest({
      identificador: '85456123Z',
      password: '1234'
    })
    // Encode credentials to use Basic REST authentication.
    let plainClientCredentials: string = credential.identificador + ':' + credential.password;
    let base64ClientCredentials: string = window.btoa(plainClientCredentials);
    console.log("--[HTTPResultsPageComponent.onReqNoHeadersCall]> plainClientCredentials = " + plainClientCredentials);
    console.log("--[HTTPResultsPageComponent.onReqNoHeadersCall]> base64ClientCredentials = " + base64ClientCredentials);
    let totalHeaders = new HttpHeaders();
    // .set('Content-Type', 'application/json; charset=utf-8')
    // .set('Access-Control-Allow-Origin', '*')
    // .set('xApp-Name', environment.name)
    // .set('xApp-version', environment.version)
    // .set('xApp-Platform', 'Angular 6.1.x')
    // .set('xApp-Brand', 'CitasCentro-Demo')
    // .set('xApp-Signature', 'S0000.0100.0000');
    // totalHeaders.set('xApp-Authorization', "Basic " + base64ClientCredentials);
    // console.log("--[HttpResponseTesting.notWrappedGet]> header = " + 'xApp-Authorization: ' + "Basic " + base64ClientCredentials);
    return this.http.post<Perfil>(this.request, null, { headers: totalHeaders })
      .pipe(map(data => {
        this.responseOkraw = JSON.stringify(data);
        let profile = this.backendService.transformRequestOutput(data) as Perfil;
        this.responseOk = JSON.stringify(profile);
        return profile;
      }));
  }
  public onReqNoHeadersCall(): Observable<Perfil> {
    console.log("><[HTTPResultsPageComponent.onReqNoHeadersCall]");
    // Construct the request to call the backend.
    this.request = this.isolation.getServerName() + this.isolation.getApiV1() + "/credencial/validate";
    console.log("--[HTTPResultsPageComponent.onReqNoHeadersCall]> request = " + this.request);
    let credential = new CredentialRequest({
      identificador: '85456123Z',
      password: '1234'
    })
    // Encode credentials to use Basic REST authentication.
    let plainClientCredentials: string = credential.identificador + ':' + credential.password;
    let base64ClientCredentials: string = window.btoa(plainClientCredentials);
    console.log("--[HTTPResultsPageComponent.onReqNoHeadersCall]> plainClientCredentials = " + plainClientCredentials);
    console.log("--[HTTPResultsPageComponent.onReqNoHeadersCall]> base64ClientCredentials = " + base64ClientCredentials);
    let totalHeaders = new HttpHeaders();
    // .set('Content-Type', 'application/json; charset=utf-8')
    // .set('Access-Control-Allow-Origin', '*')
    // .set('xApp-Name', environment.name)
    // .set('xApp-version', environment.version)
    // .set('xApp-Platform', 'Angular 6.1.x')
    // .set('xApp-Brand', 'CitasCentro-Demo')
    // .set('xApp-Signature', 'S0000.0100.0000');
    // totalHeaders.set('xApp-Authorization', "Basic " + base64ClientCredentials);
    // console.log("--[HttpResponseTesting.notWrappedGet]> header = " + 'xApp-Authorization: ' + "Basic " + base64ClientCredentials);
    return this.http.get<Perfil>(this.request, { headers: totalHeaders })
      .pipe(map(data => {
        this.responseOkraw = JSON.stringify(data);
        let profile = this.backendService.transformRequestOutput(data) as Perfil;
        this.responseOk = JSON.stringify(profile);
        return profile;
      }))
      .pipe(
        catchError(error => {
          console.log('Handling error locally and rethrowing it...', error);
          if (error.status == 400) {
            this.appStoreService.errorNotification(error.message, "Error HTTP 400.",
              { dismiss: 'click' });
          }
          if (error.status == 401) {
            this.appStoreService.errorNotification(error.message, "Error HTTP 401.",
              { dismiss: 'click' });
          }
          if (error.status == 405) {
            this.appStoreService.errorNotification(error.message, "Error HTTP 405.",
              { dismiss: 'click' });
          }
          if (error.status == 412) {
            this.appStoreService.errorNotification(error.message, "Error HTTP 412.",
              { dismiss: 'click' });
          }
          return [];
        })
      );
  }
  public notWrappedGet(): Observable<Perfil> {
    console.log("><[HttpResponseTesting.notWrappedGet]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/credencial/validate";
    console.log("--[HttpResponseTesting.notWrappedGet]> request = " + request);
    let credential = new CredentialRequest({
      identificador: '85456123Z',
      password: '1234'
    })
    // Encode credentials to use Basic REST authentication.
    let plainClientCredentials: string = credential.identificador + ':' + credential.password;
    let base64ClientCredentials: string = window.btoa(plainClientCredentials);
    console.log("--[BackendService.backendCitaCreationProcess]> body = " + JSON.stringify(credential));
    let totalHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Origin', '*')
      .set('xApp-Name', this.isolation.getAppName())
      .set('xApp-version', this.isolation.getAppVersion())
      .set('xApp-Platform', 'Angular 6.1.x')
      .set('xApp-Brand', 'CitasCentro-Demo')
      .set('xApp-Signature', 'S0000.0100.0000');
    totalHeaders.set('xApp-Authorization', "Basic " + base64ClientCredentials);
    console.log("--[HttpResponseTesting.notWrappedGet]> header = " + 'xApp-Authorization: ' + "Basic " + base64ClientCredentials);
    return this.http.post<Perfil>(request, JSON.stringify(credential), { headers: totalHeaders })
      .pipe(map(data => {
        let profile = this.backendService.transformRequestOutput(data) as Perfil;
        return profile;
      }));
  }
  //---  H T T P   W R A P P E R S
  private wrapHttpHandleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `Error message ${error.message}, `);

      // Do some generic error processing.
      // 401 are accesses without the token so we should move right to the login page.
      // if (error.status == 401) {
      //   // this.router.navigate(['login']);
      //   return throwError('Autenticacion ya no valida. Es necesario logarse de nuevo.');
      // }
    }
    // return an observable with a user-facing error message
    // return throwError(
    //   'Something bad happened; please try again later.');
  };

}

