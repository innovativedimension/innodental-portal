import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewUIPageComponent } from './new-uipage.component';

xdescribe('NewUIPageComponent', () => {
  let component: NewUIPageComponent;
  let fixture: ComponentFixture<NewUIPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewUIPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewUIPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
