import { Component, OnInit } from '@angular/core';
import { Cita } from '@models/Cita.model';
import { Medico } from '@models/Medico.model';
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'app-new-uipage',
  templateUrl: './new-uipage.component.html',
  styleUrls: ['./new-uipage.component.scss']
})
export class NewUIPageComponent extends AppPanelComponent implements OnInit {
  public sampleCita: Cita = new Cita();
  public sampleMedico: Medico = new Medico();
  // constructor() { }

  ngOnInit() {
    this.sampleMedico = this.directAccessMockResource('singlemedico');
    this.sampleMedico.setId('MID:100001:CIE:942 3');
    // this.sampleMedico.setNombre('Nombre').setApellidos('Apellido Apell');
    // this.sampleMedico.setTratamiento('Dr.').setId('IDENTIFICADOR UNICO');
  }
  public detectChanges(): void {

  }
  public getTestSource(): IAppointmentEnabled {
    return this.sampleMedico;
  }
  public directAccessMockResource(_location: string): any {
    console.log(">>[MockAppStoreService.directAccessMockResource]> location: " + _location);
    let rawdata = require('../../../../../assets/mockdata/testing/' + _location.toLowerCase() + '.mock.json');
    let data = this.backendService.transformRequestOutput(rawdata) as any;
    console.log("--[MockAppStoreService.directAccessMockResource]> item count: " + data.length);
    return data;
  }
}
