// - CORE
import { throwError } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// - TESTING
import { TestBed } from '@angular/core/testing';
import { inject } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
import { ServiceSelectionPanelComponent } from '@app/modules/gestion-servicio/panels/service-selection-panel/service-selection-panel.component';
import { IonicModule } from 'ionic-angular';
import { SharedModule } from '@app/modules/shared/shared.module';
import { AvailableServicesController } from '@app/controllers/available-services-controller.service';
import { AvailableServicesControllerStub } from '@app/testing/services/available-services-controller.mock.service';
// - MODELS
import { Medico } from '@models/Medico.model';
import { ServiceSelectionPageComponent } from './service-selection-page.component';

describe('ServiceSelectionPageComponent', () => {
  let component: ServiceSelectionPageComponent;
  let fixture: ComponentFixture<ServiceSelectionPageComponent>;

  beforeEach(async(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes),
        IonicModule.forRoot({}),
        SharedModule
      ],
      declarations: [ServiceSelectionPageComponent, RouteMockUpComponent],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: AvailableServicesController, useClass: AvailableServicesControllerStub }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceSelectionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
