import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { AppStoreService } from '@app/services/appstore.service';
import { Medico } from '@models/Medico.model';

@Component({
  selector: 'service-selection-page',
  templateUrl: './service-selection-page.component.html',
  styleUrls: ['./service-selection-page.component.scss']
})
export class ServiceSelectionPageComponent implements OnInit {
  public service: Medico;

  // - C O N S T R U C T O R
  constructor(protected appStoreService: AppStoreService) { }

  ngOnInit() {
    this.service = this.appStoreService.accessActiveService();
  }
  public getActiveService(): Medico {
    return this.service;
  }
  public onServiceSelected(_service: Medico): void {
    this.service = _service;
  }
}
