import { Component } from '@angular/core';
import { ButtonComponent } from '@app/modules/ui/app-button/button.component';

@Component({
  selector: 'visualize-buttons-page',
  templateUrl: './visualize-buttons-page.component.html',
  styleUrls: ['./visualize-buttons-page.component.scss']
})
export class VisualizeButtonsPageComponent extends ButtonComponent {
  public onButtonClick(_data?: any, _event?: any): void {
    console.log(">>[VisualizeButtonsPageComponent.onClick]> event: " + JSON.stringify(_event));
    this.delay(5000).then(any => {
      this.processing = false;
    });
  }
}
