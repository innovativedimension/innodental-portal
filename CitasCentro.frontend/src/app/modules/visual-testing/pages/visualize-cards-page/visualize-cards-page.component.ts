import { Component, OnInit } from '@angular/core';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'visualize-cards-page',
  templateUrl: './visualize-cards-page.component.html',
  styleUrls: ['./visualize-cards-page.component.scss']
})
export class VisualizeCardsPageComponent extends AppPanelComponent implements OnInit {
  public leftContent: string = '[contenido]';

  ngOnInit() {
    this.canBeClosed = true;
    this.downloading = true;
  }
  public closePanel(): void {
    this.appStoreService.infoNotification('Pulsado boton de cierre.');
  }
}
