import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Cita } from '@models/Cita.model';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'visualize-citas-page',
  templateUrl: './visualize-citas-page.component.html',
  styleUrls: ['./visualize-citas-page.component.scss']
})
export class VisualizeCitasPageComponent extends AppPanelComponent implements OnInit {
  ngOnInit() {
    let cita: Cita = new Cita();
    cita.estado = 'LIBRE';
    cita.huecoId = 100;
    cita.huecoIdentifier = "0300";
    this.dataModelRoot.push(cita);

    cita = new Cita();
    cita.estado = 'LIBRE';
    cita.huecoId = 100;
    cita.tipo = 'ROJO';
    cita.huecoIdentifier = "0300";
    this.dataModelRoot.push(cita);

    cita = new Cita();
    cita.estado = 'LIBRE';
    cita.huecoId = 100;
    cita.tipo = 'ROSA';
    cita.huecoIdentifier = "0300";
    this.dataModelRoot.push(cita);

    cita = new Cita();
    cita.estado = 'LIBRE';
    cita.huecoId = 100;
    cita.tipo = 'NARANJA';
    cita.huecoIdentifier = "0300";
    this.dataModelRoot.push(cita);

    cita = new Cita();
    cita.estado = 'LIBRE';
    cita.huecoId = 100;
    cita.tipo = 'AMARILLO';
    cita.huecoIdentifier = "0300";
    this.dataModelRoot.push(cita);

    cita = new Cita();
    cita.estado = 'RESERVADA';
    cita.huecoId = 9999;
    cita.huecoIdentifier = "0300";
    this.dataModelRoot.push(cita);

    cita = new Cita();
    cita.estado = 'BLOQUEADA';
    cita.huecoId = 999;
    this.dataModelRoot.push(cita);

    cita = new Cita();
    cita.estado = 'VACACIONES';
    cita.huecoId = 1000;
    this.dataModelRoot.push(cita);

    cita = new Cita();
    cita.estado = 'DESCANSO';
    cita.huecoId = 1500;
    cita.tipo = 'LIMA';
    this.dataModelRoot.push(cita);

    this.notifyDataChanged();
  }
  public isHover(_testValue: boolean): boolean {
    return _testValue;
  }
}