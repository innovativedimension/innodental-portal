import { Component } from '@angular/core';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';

@Component({
  selector: 'visualize-services-page',
  templateUrl: './visualize-services-page.component.html',
  styleUrls: ['./visualize-services-page.component.scss']
})
export class VisualizeServicesPageComponent extends AppPanelComponent {
  public leftContent: string = '[contenido]';

  ngOnInit() {
    this.title = 'Lista Servicios : -ESPECIALIDADES-';
    // Read the list of services from mockup data.
    let serviceList = this.directAccessMockResource('accessdoctors');
    for (let service of serviceList) {
      this.dataModelRoot.push(service);
    }
    this.notifyDataChanged();
    this.leftContent = '[' + this.renderNodeList.length + ']';
  }
  public directAccessMockResource(_location: string): any {
    let rawdata = require('../../../../../assets/mockdata/testing/' + _location.toLowerCase() + '.mock.json');
    let data = this.backendService.transformRequestOutput(rawdata);
    return data;
  }

}
