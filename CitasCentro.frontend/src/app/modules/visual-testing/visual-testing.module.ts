// - CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// - ROUTING
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
// - DRAG & DROP
import { NgDragDropModule } from 'ng-drag-drop';
// - NOTIFICATIONS
import { ToastrModule } from 'ng6-toastr-notifications';
// - IONIC
import { IonicModule } from 'ionic-angular';
// - APPLICATION MODULES
import { UIModule } from '@app/modules/ui/ui.module';
import { SharedModule } from '@app/modules/shared/shared.module';

// - PAGES
import { VisualizeTestDashboardComponent } from './pages/visualize-test-dashboard/visualize-test-dashboard.component';
import { VisualizeButtonsPageComponent } from './pages/visualize-buttons-page/visualize-buttons-page.component';
import { VisualizeCitasPageComponent } from './pages/visualize-citas-page/visualize-citas-page.component';
import { VisualizeCardsPageComponent } from './pages/visualize-cards-page/visualize-cards-page.component';
import { TestPageComponent } from './test/testpage/testpage.component';
import { HTTPResultsPageComponent } from './pages/httpresults-page/httpresults-page.component';
import { VisualizeServicesPageComponent } from './pages/visualize-services-page/visualize-services-page.component';
import { NewUIPageComponent } from './pages/new-uipage/new-uipage.component';
import { CitacionesModule } from '../citaciones/citaciones.module';
import { ServiceSelectionPageComponent } from './pages/service-selection-page/service-selection-page.component';
import { GestionServicioModule } from '../gestion-servicio/gestion-servicio.module';

// - MODULE ROUTES
const routes: Routes = [
  { path: '', component: VisualizeTestDashboardComponent },
  { path: 'dashboard', component: VisualizeTestDashboardComponent },
  { path: 'buttons', component: VisualizeButtonsPageComponent },
  { path: 'citas', component: VisualizeCitasPageComponent },
  { path: 'cards', component: VisualizeCardsPageComponent },
  { path: 'http', component: HTTPResultsPageComponent },
  { path: 'services', component: VisualizeServicesPageComponent },
  { path: 'newui', component: NewUIPageComponent },
  { path: 'serviceselection', component: ServiceSelectionPageComponent }
];

@NgModule({
  imports: [
    // - CORE
    CommonModule,
    RouterModule.forChild(routes),
    NgDragDropModule.forRoot(),
    ToastrModule.forRoot(),
    IonicModule.forRoot({}),
    // - APPLICATION MODULES
    UIModule,
    SharedModule,
    CitacionesModule,
    GestionServicioModule
  ],
  declarations: [
    // - PAGES
    TestPageComponent,
    VisualizeTestDashboardComponent,
    VisualizeButtonsPageComponent,
    VisualizeCitasPageComponent,
    VisualizeCardsPageComponent,
    HTTPResultsPageComponent,
    VisualizeServicesPageComponent,
    NewUIPageComponent,
    ServiceSelectionPageComponent
  ],
  exports: [
    RouterModule
  ]

})
export class VisualTestingModule { }
