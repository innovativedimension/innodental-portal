//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { ErrorHandler } from '@angular/core';
import { Injector } from '@angular/core';
import { Inject } from '@angular/core';
import { InjectionToken } from '@angular/core';
// --- ERROR INTERCEPTION
import * as Rollbar from 'rollbar';
import { AppStoreService } from '@app/services/appstore.service';

export const rollbarConfig = {
  accessToken: '9a8dc1d8aa094a9bbe4e4d093faf1e1a',
  captureUncaught: true,
  captureUnhandledRejections: true,
};

@Injectable()
export class RollbarErrorHandler implements ErrorHandler {
  // constructor(@Inject(RollbarService) private rollbar: Rollbar) { }
  constructor(private injector: Injector) { }

  handleError(err: any): void {
    let rollbar = this.injector.get(RollbarService);
    let appStoreService = this.injector.get(AppStoreService);
    // Use type checking to detect the different types of errors.
    if (err.constructor.name === 'TypeError') {
      // Those are syntax exceptions detected on the runtime.
      console.log('ERR[RollbarErrorHandler.handleError]> Error intercepted: ' + JSON.stringify(err.message));
      if (appStoreService.showExceptions())
        appStoreService.errorNotification(err.message, 'EXCEPCION ' + err.status)
      rollbar.error(err);
    } else if (err.constructor.name === 'Error') {
      // Those are syntax exceptions detected on the runtime.
      console.log('ERR[RollbarErrorHandler.handleError]> Error intercepted: ' + JSON.stringify(err.message));
      if (appStoreService.showExceptions())
        appStoreService.errorNotification(err.message, 'EXCEPCION ' + err.status)
      rollbar.error(err);
    } else {
      console.log('ERR[RollbarErrorHandler.handleError]> Error intercepted: ' + JSON.stringify(err.message));
      if (appStoreService.showExceptions())
        appStoreService.errorNotification(err.message, 'EXCEPCION ' + err.status)
      rollbar.error(err);
    }
  }
}

export function rollbarFactory() {
  return new Rollbar(rollbarConfig);
}
export const RollbarService = new InjectionToken<Rollbar>('rollbar');
