//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
// --- ENVIRONMENT
// import { environment } from '@env/environment';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
// --- HTTP PACKAGE
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
// --- ROUTER
import { Router } from '@angular/router';
// --- NOTIFICATIONS
//import { NotificationsService } from 'angular2-notifications';
//--- DATE FUNCTIONS
import { differenceInMilliseconds } from 'date-fns';
// --- SERVICES
import { IsolationService } from '@app/platform/isolation.service';
// --- INTERFACES
import { INode } from '@interfaces/core/INode.interface';
// --- MODELS
import { Centro } from '@models/Centro.model';
import { Cita } from '@models/Cita.model';
import { Medico } from '@models/Medico.model';
import { Credencial } from '@models/Credencial.model';
import { Limitador } from '@models/Limitador.model';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';
import { CitasTemplate } from '@models/CitasTemplate.model';
import { CredentialRequest } from '@app/models/CredentialRequest.model';
import { AppointmentReport } from '@app/models/AppointmentReport.model';
import { FormularioCreacionCitas } from '@app/models/FormularioCreacionCitas.model';
import { PatientData } from '@app/models/PatientData.model';
import { OpenAppointment } from '@app/models/OpenAppointment.model';
import { Perfil } from '@app/models/Perfil.model';
import { ServiceAuthorized } from '@app/models/ServiceAuthorized.model';
import { AppModule } from '@app/models/AppModule.model';
import { NewMedico } from '@app/models/NewMedico.model';
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';
import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';
import { RuntimeBackendService } from '@app/platform/runtime-backend.service';
// import { CommonBackendService } from '@app/common/common-backend.service';

// --- C O N S T A N T S
/**
 // tslint:disable-next-line:max-line-length
 * This service will be the responsible to manage all the api interface with the backend. Most of the data can be cached so to keep consistency with the asynchronicity of all other calls we will implement an Observable pattern to access the data also asynchronously.
 * This service must be completely isolated and can only depend on external configuration.
 * The service also defines a URL translation to mock data to be replaced during development or demo modes.
 * @param @Inject(LOCAL_STORAGE [description]
 */
@Injectable({
  providedIn: 'root'
})
export class BackendService extends RuntimeBackendService {
  private HEADERS;

  // --- C O N S T R U C T O R
  constructor(
    public isolation: IsolationService,
    protected http: HttpClient) {
    super();
    // Initialize the list of header as a constant. Do this in code because the isolation dependency.
    this.HEADERS = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Origin', '*')
      .set('xApp-Name', this.isolation.getAppName())
      .set('xApp-version', this.isolation.getAppVersion())
      .set('xApp-Platform', 'Angular 6.1.x')
      .set('xApp-Brand', 'CitasCentro-Demo')
      .set('xApp-Signature', 'S0000.0011.0000');
  }

  // --- G L O B A L S
  // protected static BACKEND_SERVICE_URL: string = this.isolation.getServerName();
  // protected static RESOURCE_SERVICE_URL: string = this.isolation.getServerName();
  // protected apiv1: string = this.isolation.getApiV1();
  // protected apiv2: string = this.isolation.getApiV2();

  // --- C A C H E
  // protected cacheTimes = {
  //   'CENTER-DATA': 60 * 30
  // };

  // - E N V I R O N M E N T    C A L L S
  public getApplicationName(): string {
    return this.isolation.getAppName();
  }
  public getApplicationVersion(): string {
    return this.isolation.getAppVersion();
  }
  public inDevelopment(): boolean {
    return this.isolation.inDevelopment();
  }
  public showExceptions(): boolean {
    return this.isolation.showExceptions();
  }


  // --- B A C K E N D    S E C T I O N
  // - C R E D E N T I A L
  /**
  * Get the currrent credential. This should be called when the credential is already validaded so in case it is empty we should return a new empty instance.
  * @return [description]
  */
  public _credential: Perfil;
  public accessCredential(): Perfil {
    // Search the credential on the app store or the session storage.
    if (null == this._credential) {
      let cred = this.getFromStorage(this.isolation.CREDENTIAL_KEY);
      if (null == cred) {
        // If the credential is not found then we should go back to the login page.
        this.isolation.route2FirstPage();
      } else {
        this._credential = new Perfil(JSON.parse(cred));
        return this._credential;
      }
      this._credential = new Perfil();
      return this._credential;
    } else return this._credential;
  }

  // - C R E D E N C I A L
  public checkCredential(credential: CredentialRequest): Observable<Perfil> {
    console.log("><[BackendService.checkCredentials]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/credencial/validate";
    console.log("--[BackendService.backendCitaCreationProcess]> request = " + request);
    // console.log("--[BackendService.backendCitaCreationProcess]> body = " + JSON.stringify(credential));
    // TODO --- Encode credentials to use Basic REST authentication.
    let plainClientCredentials: string = credential.identificador + ':' + credential.password;
    let base64ClientCredentials: string = window.btoa(plainClientCredentials);
    let totalHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Origin', '*')
      .set('xApp-Name', this.isolation.getAppName())
      .set('xApp-version', this.isolation.getAppVersion())
      .set('xApp-Platform', 'Angular 6.1.x')
      .set('xApp-Brand', 'CitasCentro-Demo')
      .set('xApp-Signature', 'S0000.0100.0000')
      .set('xApp-Authorization', "Basic " + base64ClientCredentials);
    console.log("--[BackendService.backendCitaCreationProcess]> header = " + 'xApp-Authorization: ' + "Basic " + base64ClientCredentials);
    return this.http.get<Perfil>(request, { headers: totalHeaders })
      .pipe(map(data => {
        let profile = this.transformRequestOutput(data) as Perfil;
        return profile;
      }));
  }
  public validatePassword(_password: string): Observable<boolean> {
    console.log("><[BackendService.validatePassword]");
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/credencial/password/validate';
    // Encrypth the password before transmission.
    let base64Payload: string = window.btoa(_password);
    console.log("--[BackendService.backendCitaCreationProcess]> request = " + request);
    console.log("--[BackendService.backendCitaCreationProcess]> body = " + base64Payload);
    let additionalHeaders = new HttpHeaders()
      .set('xApp-Payload', base64Payload);
    return this.wrapHttpGETCall(request, additionalHeaders);
  }
  // - C E N T R O
  public getBackendCenters(): Observable<Centro[]> {
    console.log("><[BackendService.getMedicalCenters]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/centros";
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        return this.transformRequestOutput(data.content) as Centro[];
      }));
  }

  // - M E D I C O
  /**
   * Gets the complete list of medical service providers from each of the available centers. This is the global list that will cached and shown to the caller services. The list first downloads the list of centers, then for each of them the list of providers and then we coalesce tll this information into a list that is then rendered to the view.
   * @return List of Centers with all the Specialties and then the Doctors.
   */
  public backendMedicalServiceProviders(centerIdentifier: number): Observable<Medico[]> {
    console.log("><[BackendService.backendMedicalServiceProviders]> centerIdentifier: " + centerIdentifier);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/centros/" + centerIdentifier + "/medicos";
    return this.wrapHttpGETCall(request)
      .pipe(map((data) => {
        // Transform received data and return the node list to the caller for aggregation.
        let medicoList = this.transformRequestOutput(data) as Medico[];
        console.log("--[BackendService.backendMedicalServiceProviders]> Medico.count: " + medicoList.length);
        return medicoList;
      }));
  }
  public backendCreateConsultaMedico(center: Centro, data: NewMedico): Observable<Medico[]> {
    console.log(">>[BackendService.backendCreateConsultaMedico]>");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/centros/" + center.getId() + "/medicos/new";
    // Call the HTTP wrapper to construct the request and fetch the data.
    console.log("--[BackendService.backendCitaCreationProcess]> request = " + request);
    console.log("--[BackendService.backendCitaCreationProcess]> body = " + JSON.stringify(data));
    return this.wrapHttpPOSTCall(request, JSON.stringify(data));
  }
  public backendUpdateConsultaMedico(center: Centro, data: Medico): Observable<Medico[]> {
    console.log(">>[BackendService.backendUpdateConsultaMedico]>");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/centros/" + center.getId() + "/medicos/update";
    // Call the HTTP wrapper to construct the request and fetch the data.
    console.log("--[BackendService.backendUpdateConsultaMedico]> request = " + request);
    console.log("--[BackendService.backendUpdateConsultaMedico]> body = " + JSON.stringify(data));
    return this.wrapHttpPUTCall(request, JSON.stringify(data));
  }
  public backendCitaCreationProcess(centro: Centro, data: FormularioCreacionCitas): Observable<AppointmentReport[]> {
    console.log("><[BackendService.backendCitaCreationProcess]");
    // Access the creadential to get the data to be used on the creation process.
    let credential = this.accessCredential();
    if (credential.isValid()) {
      let identifier = credential.getId();
      // Construct the Consulta unique identifier.
      let medicoIdentifier = "MID:" + centro.getId() + ":" + identifier;
      // Construct the request to call the backend.
      let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/processor/creacitas/" + medicoIdentifier;
      const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
      console.log("--[BackendService.backendCitaCreationProcess]> request = " + request);
      console.log("--[BackendService.backendCitaCreationProcess]> body = " + JSON.stringify(data));
      return this.http.post<AppointmentReport[]>(request, JSON.stringify(data), { headers: this.HEADERS });
    } else {
      // The credential is not defined. MOve to the credential page.
      this.isolation.route2FirstPage();
    }
  }

  // - C I T A S
  public backendCitas4MedicoById(identifier: string) {
    console.log("><[AppStoreService.backendCitas4Medico]> medicoIdentifier: " + identifier);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/medicos/" + identifier + "/citas";
    // Start timing data.
    let chrono = new Date();
    return this.wrapHttpGETCall(request)
      .pipe(map((data) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data) as Cita[];
        console.log("--[AppStoreService.getMedicalCenters]> Cita.count: " + citaList.length);
        // Calculate and print elapsed.
        let elapsed = new Date();
        console.log("--[AppStoreService.getMedicalCenters]> [TIMING]: " + differenceInMilliseconds(chrono, elapsed) + "ms");
        return citaList;
      }));
  }
  public backendCitas4MedicoByReferencia(identifier: string) {
    console.log("><[AppStoreService.backendCitas4Medico]> medicoIdentifier: " + identifier);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/medicos/" + identifier + "/citas/byreferencia";
    // Start timing data.
    let chrono = new Date();
    return this.wrapHttpGETCall(request)
      .pipe(map((data) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data) as Cita[];
        console.log("--[AppStoreService.getMedicalCenters]> Cita.count: " + citaList.length);
        // Calculate and print elapsed.
        let elapsed = new Date();
        console.log("--[AppStoreService.getMedicalCenters]> [TIMING]: " + differenceInMilliseconds(chrono, elapsed) + "ms");
        return citaList;
      }));
  }
  public backendAppointmentsByMonth(identifier: string, year: number, month: number): Observable<Cita[]> {
    console.log("><[BackendService.backendAppointmentsByMonth]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/medicos/" + identifier + "/citas/bymonth/" + year + "/" + month;
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as Cita[];
      }));
  }
  public backendGenerateAppointment4Date(_service: IAppointmentEnabled, _template: CitasTemplate, _date: Date): Observable<AppointmentReport[]> {
    console.log("><[BackendService.backendGenerateAppointment4Date]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV2() + "/processor/creacitas/" +
      _service.getServiceIdentifiers()[0] + '/date/' + this.date2BasicISO(_date);
    console.log("--[BackendService.backendCitaCreationProcess]> request = " + request);
    console.log("--[BackendService.backendCitaCreationProcess]> body = " + JSON.stringify(_template));
    return this.http.post<AppointmentReport[]>(request, JSON.stringify(_template), { headers: this.HEADERS });
  }
  public backendAppointments4Patient(_identificador: string): Observable<OpenAppointment[]> {
    console.log("><[BackendService.backendAppointments4Patient]> credential: " + _identificador);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/paciente/" + _identificador + "/citas";
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let appointments = this.transformRequestOutput(data) as OpenAppointment[];
        console.log("--[BackendService.backendAppointments4Patient]> Cita.count: " + appointments.length);
        // Store a copy of the appointment list on the appointments observable.
        // this.openAppointments = appointments;
        return appointments;
      }));
  }
  public backendBlockAppointment(patient: PatientData, cita: Cita): Observable<Cita[]> {
    console.log("><[BackendService.backendBlockAppointment]> Cita.status: " + cita.estado);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/citas/" + cita.getId() + "/block/" + patient.getIdentificador();
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        console.log("--[BackendService.backendBlockAppointment]> Cita.status: " + cita.estado);
        return this.transformRequestOutput(data) as Cita[];
      }));
  }
  /**
   * Changes the state for an appointment specified by its ID from any state to RESERVADA.
   * @param  patient the patient identifier. The method should port also the rest of the patient data.
   * @param  cita    the selected appointment date and time.
   * @return         a reserved OpenAppointment where we have the Cita and the Medico stored and ready.
   */
  public backendReserveAppointment(patient: PatientData, cita: Cita): Observable<OpenAppointment> {
    console.log("><[BackendService.backendReserveAppointment]> cita: " + JSON.stringify(cita));
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/citas/" + cita.getId() + "/reserve/" + patient.getIdentificador();
    console.log("--[BackendService.backendReserveAppointment]> request = " + request);
    console.log("--[BackendService.backendReserveAppointment]> body = " + JSON.stringify(patient));
    return this.wrapHttpPOSTCall(request, JSON.stringify(patient))
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let appointments = this.transformRequestOutput(data) as OpenAppointment[];
        // console.log("--[BackendService.backendReserveAppointment]> Cita.count: " + appointments.length);
        console.log("--[BackendService.backendReserveAppointment]> Cita.status: " + cita.estado);
        return appointments[0];
      }));
  }
  public backendCancelAppointment(patient: PatientData, cita: Cita): Observable<Cita[]> {
    console.log("><[BackendService.backendCancelAppointment]> cita: " + JSON.stringify(cita));
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/citas/" + cita.getId() + "/cancel/" + patient.getIdentificador();
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data) as Cita[];
        // console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
        console.log("--[BackendService.backendCancelAppointment]> Cita.status: " + cita.estado);
        return citaList;
      }));
  }
  public backendDeleteAppointments(_citas: Cita[]): Observable<Cita[]> {
    console.log("><[BackendService.backendDeleteAppointments]> nro citas: " + _citas.length);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV2() + '/citas/delete';
    // Create the list of appointments id to be deleted.
    let ids: number[] = [];
    for (let cita of _citas) {
      ids.push(cita.getId());
    }
    return this.wrapHttpPOSTCall(request, JSON.stringify(ids))
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as Cita[];
      }));
  }
  public backendVacancyAppointments(_citas: Cita[]): Observable<Cita[]> {
    console.log("><[BackendService.backendDeleteAppointments]> nro citas: " + _citas.length);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV2() + '/citas/vacaciones';
    // Create the list of appointments id to be deleted.
    let ids: number[] = [];
    for (let cita of _citas) {
      ids.push(cita.getId());
    }
    return this.wrapHttpPOSTCall(request, JSON.stringify(ids))
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as Cita[];
      }));
  }

  // - A P P O I N T M E N T S   T E M P L A T E S
  public backendCreateTemplate(_template: CitasTemplate, _owner: string): Observable<CitasTemplate> {
    console.log("><[BackendService.readAppointmentTemplates]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' +
      _owner + '/templates/new';
    return this.wrapHttpPOSTCall(request, JSON.stringify(_template));
  }
  public backendDeleteTemplate(_templateIdentifier: number, _owner: string): Observable<number> {
    console.log("><[BackendService.backendDeleteTemplate]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' +
      _owner + '/templates/delete/' +
      _templateIdentifier;
    return this.wrapHttpDELETECall(request);
  }
  public backendUpdateTemplate(_template: CitasTemplate, _owner: string): Observable<CitasTemplate> {
    console.log("><[BackendService.readAppointmentTemplates]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' +
      _owner + '/templates/update';
    return this.wrapHttpPUTCall(request, JSON.stringify(_template));
  }
  // @Deprecated
  public readCredentialTemplates(_credential: Credencial): Observable<CitasTemplate[]> {
    console.log("><[BackendService.readCredentialTemplates]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' +
      _credential.getId() + '/templates';
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as CitasTemplate[];
      }));
  }
  public readServiceTemplates(_service: Medico): Observable<CitasTemplate[]> {
    console.log("><[BackendService.readServiceTemplates]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' +
      _service.getId() + '/templates';
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as CitasTemplate[];
      }));
  }

  // - L I M I T E S
  public backendLimits4Service(_service: Medico): Observable<Limitador[]> {
    console.log("><[BackendService.backendLimits4Service]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' + _service.getId() + '/limites';
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as Limitador[];
      }));
  }
  public backendSaveLimits4Service(_service: Medico, _limits: Limitador[]): Observable<Limitador[]> {
    console.log("><[BackendService.backendSaveLimits4Service]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/medicos/' + _service.getId() + '/limites';
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpPOSTCall(request, JSON.stringify(_limits))
      .pipe(map(data => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as Limitador[];
      }));
  }

  // -  A C T O S   M E D I C O S
  // public fireDownloadCenterMedicalActs(_centerId: number): void {
  //   this.backendActosMedicos4Centro(_centerId)
  //     .pipe(map(medicalActs => {
  //       this._medicalActsList = medicalActs;
  //       // Use the subjects to synchronize to the availability of the data.
  //       this.medicalActsSource.next(this._medicalActsList);
  //     }));
  // }
  // MOCKUP - This call not gets the data from the LocalStorage as it is was from the backend.
  public backendActosMedicos4Centro(_centerId: number): Observable<ActoMedicoCentro[]> {
    console.log("><[BackendService.backendActosMedicos4Centro]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + '/centros/' + _centerId + '/actosmedicos';
    // Call the HTTP wrapper to construct the request and fetch the data.
    this.isolation.warningNotification('Se esta llamando a una función desactivada. Reporte esta notificación al equipo de soporte', 'Llamada desactivada temporalmente.')
    return this.wrapHttpRESOURCECall('/assets/properties/actosmedicos.json')
      // return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        // Store the list on a subject.
        let actos = this.transformRequestOutput(data) as ActoMedicoCentro[];
        // Transform received data and return the node list to the caller for aggregation.
        return actos;
      }));
  }

  //--- E X C E P T I O N   M A N A G E M E N T
  // public registerException(_componentName: string, _message: string, _redirection?: string): void {
  //   console.log("E [AppStoreService.registerException]> C: " + _componentName +
  //     ' M: ' + _message);
  //   try {
  //     if (null != _redirection) this.router.navigate([_redirection]);
  //   } catch (exception) {
  //     this.isolation.route2FirstPage();
  //   }
  // }

  // - S T O R A G E
  public getFromStorage(_key: string): any {
    return this.isolation.getFromStorage(_key);
  }
  public setToStorage(_key: string, _content: any): any {
    return this.isolation.setToStorage(_key, _content);
  }
  public removeFromStorage(_key: string): any {
    return this.isolation.removeFromStorage(_key);
  }
  public getFromSession(_key: string): any {
    return this.isolation.getFromSession(_key);
  }
  public setToSession(_key: string, _content: any): any {
    return this.isolation.setToSession(_key, _content);
  }
  public removeFromSession(_key: string): any {
    return this.isolation.removeFromSession(_key);
  }

  //---  H T T P   W R A P P E R S
  private wrapHttpHandleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `Error message ${error.message}, `);

      // Do some generic error processing.
      // 401 are accesses without the token so we should move right to the login page.
      if (error.status == 401) {
        // this.router.navigate(['login']);
        return throwError('Autenticacion ya no valida. Es necesario logarse de nuevo.');
      }
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
  public wrapHttpRESOURCECall(request: string): Observable<any> {
    console.log("><[AppStoreService.wrapHttpGETCall]> request: " + request);
    return this.http.get(request);
  }
  /**
   * This method wraps the HTTP access to the backend. It should add any predefined headers, any request specific headers and will also deal with mock data.
   * Mock data comes now into two flavours. he first one will search for the request on the list of defined requests (if the mock is active). If found then it will check if the request should be sent to the file system ot it should be resolved by accessing the LocalStorage.
   * @param  request [description]
   * @return         [description]
   */
  public wrapHttpGETCall(_request: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpGETCall]> request: " + adjustedRequest);
      let newheaders = this.wrapHttpSecureHeaders(_requestHeaders);
      return this.http.get(adjustedRequest, { headers: newheaders });
    } else {
      // The requst is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpGETCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  public wrapHttpPUTCall(_request: string, _body: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpPUTCall]> request: " + adjustedRequest);
      console.log("><[AppStoreService.wrapHttpPUTCall]> body: " + _body);
      let newheaders = this.wrapHttpSecureHeaders(_requestHeaders);
      return this.http.put(adjustedRequest, _body, { headers: newheaders });
      // .pipe(
      //   catchError(this.wrapHttpHandleError)
      // );
    } else {
      // The requst is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpPUTCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  public wrapHttpPOSTCall(_request: string, _body: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpPOSTCall]> request: " + adjustedRequest);
      console.log("><[AppStoreService.wrapHttpPOSTCall]> body: " + _body);
      let newheaders = this.wrapHttpSecureHeaders(_requestHeaders);
      return this.http.post(adjustedRequest, _body, { headers: newheaders });
      // .pipe(
      //   catchError(this.wrapHttpHandleError)
      // );
    } else {
      // The requst is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpPOSTCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  public wrapHttpDELETECall(_request: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpDELETECall]> request: " + adjustedRequest);
      let newheaders = this.wrapHttpSecureHeaders(_requestHeaders);
      return this.http.delete(adjustedRequest, { headers: newheaders });
    } else {
      // The request is a LOCALSTORAGE mockup and should return with no more processing.
      console.log("><[AppStoreService.wrapHttpDELETECall]> request: " + _request);
      return adjustedRequest;
    }
  }

  protected wrapHttpSecureRequest(request: string): string | Observable<any> {
    // Check if we should use mock data.
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) {
        // Check if the resolution should be from the LocalStorage. URL should start with LOCALSTORAGE::.
        if (hit.search('LOCALSTORAGE::') > -1) {
          return Observable.create((observer) => {
            let targetData = this.getFromStorage(hit + ':' + this.accessCredential().getId());
            try {
              if (null != targetData) {
                // .then((data) => {
                console.log('--[AppStoreService.wrapHttpPOSTCall]> Mockup data: ', targetData);
                // Process and convert the data string to the class instances.
                let results = this.transformRequestOutput(JSON.parse(targetData));
                observer.next(results);
                observer.complete();
              } else {
                observer.next([]);
                observer.complete();
              }
            } catch (mockException) {
              observer.next([]);
              observer.complete();
            }
          })
        } else request = hit;
      }
    }
    return request;
  }
  /**
   * This is the common code to all secure calls. It will check if the call can use the mockup system and if that system has a mockup destionation for the request.
   * This call also should create a new set of headers to be used on the next call and should put inside the current authentication data.
   *
   * @protected
   * @param {string} request
   * @param {string} _body
   * @param {HttpHeaders} [_requestHeaders]
   * @returns {HttpHeaders}
   * @memberof BackendService
   */
  protected wrapHttpSecureHeaders(_requestHeaders?: HttpHeaders): HttpHeaders {
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Origin', '*')
      .set('xApp-Name', this.isolation.getAppName())
      .set('xApp-Version', this.isolation.getAppVersion())
      .set('xApp-Platform', 'Angular 6.1.x')
      .set('xApp-Brand', 'CitasCentro-development')
      // .set('xApp-Signature', 'S0000.0011.0000');
      .set('xApp-Signature', 'S0000.0100.0000');

    // Add authentication token but only for authorization required requests.
    let cred = this.accessCredential();
    if (null != cred) {
      let auth = this.accessCredential().getAuthorization();
      if (null != auth) headers = headers.set('xApp-Authentication', auth);
      console.log("><[AppStoreService.wrapHttpSecureHeaders]> xApp-Authentication: " + auth);
    }
    if (null != _requestHeaders) {
      for (let key of _requestHeaders.keys()) {
        headers = headers.set(key, _requestHeaders.get(key));
      }
    }
    return headers;
  }

  //--- R E S P O N S E   T R A N S F O R M A T I O N
  public transformRequestOutput(entrydata: any): INode[] | INode {
    let results: INode[] = [];
    // Check if the entry data is a single object. If so process it because can be an exception.
    if (entrydata instanceof Array) {
      for (let key in entrydata) {
        // Access the object into the spot.
        let node = entrydata[key] as INode;
        // Convert and add the node.
        results.push(this.convertNode(node));
      }
    } else {
      // Process a single element.
      let jclass = entrydata["jsonClass"];
      if (null == jclass) return [];
      return this.convertNode(entrydata);
    }
    return results;
  }
  protected convertNode(node): INode {
    switch (node.jsonClass) {
      case "Centro":
        let convertedCentro = new Centro(node);
        console.log("--[AppModelStoreService.convertNode]> Centro node: " + convertedCentro.getId());
        return convertedCentro;
      case "Medico":
        let convertedMedico = new Medico(node);
        console.log("--[AppModelStoreService.convertNode]> Medico node: " + convertedMedico.getId());
        return convertedMedico;
      case "Cita":
        let convertedCita = new Cita(node);
        // console.log("--[AppModelStoreService.convertNode]> Cita node: " + convertedCita.huecoId);
        return convertedCita;
      case "AppointmentReport":
        let reportCita = new AppointmentReport(node);
        console.log("--[AppModelStoreService.convertNode]> AppointmentReport node: " + reportCita.reportdate);
        return reportCita;
      case "Credencial":
        let convertedCredential = new Perfil(node);
        console.log("--[AppModelStoreService.convertNode]> AppointmentReport node: " + convertedCredential.getId());
        return convertedCredential;
      case "CitasTemplate":
        let convertedTemplate = new CitasTemplate(node);
        console.log("--[AppModelStoreService.convertNode]> CitasTemplate node: " + convertedTemplate.nombre);
        return convertedTemplate;
      case "Limitador":
        let convertedLimit = new Limitador(node);
        console.log("--[AppModelStoreService.convertNode]> Limitador node: " + convertedLimit.titulo);
        return convertedLimit;
      case "TemplateBuildingBlock":
        let convertedBuildingBlock = new TemplateBuildingBlock(node);
        console.log("--[AppModelStoreService.convertNode]> TemplateBuildingBlock node: " + convertedBuildingBlock.nombre);
        return convertedBuildingBlock;
      case "ActoMedicoCentro":
        let convertedActoMedico = new ActoMedicoCentro(node);
        console.log("--[AppModelStoreService.convertNode]> ActoMedicoCentro node: " + convertedActoMedico.nombre);
        return convertedActoMedico;
      case "OpenAppointment":
        let convertedAppointment = new OpenAppointment(node);
        console.log("--[AppModelStoreService.convertNode]> OpenAppointment node: " + convertedAppointment.getId());
        return convertedAppointment;
      case "ServiceAuthorized":
        let convertedService = new ServiceAuthorized(node);
        console.log("--[AppModelStoreService.convertNode]> ServiceAuthorized node: " + convertedService.getId());
        return convertedService;
      case "AppModule":
        let convertedAppModule = new AppModule(node);
        console.log("--[AppModelStoreService.convertNode]> AppModule node: " + convertedAppModule.nombre);
        return convertedAppModule;
    }
  }
  private date2BasicISO(_date: Date): string {
    var local = new Date(_date);
    local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }
}
