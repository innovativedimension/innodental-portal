//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Routes } from '@angular/router';

/**
 * This is an empty component to be pointed with valid soutes.
 *
 * @export
 * @class HomeComponent
 */
@Component({
  template: `Home`
})
export class RouteMockUpComponent {
}
export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: RouteMockUpComponent },
  { path: 'dashboard', component: RouteMockUpComponent },
  { path: 'gestionservicio/definicioncalendario', component: RouteMockUpComponent },
  { path: 'citas/generadorplantilla', component: RouteMockUpComponent },
  { path: 'citas/generadorplantilla/:id', component: RouteMockUpComponent },
  { path: 'gestionservicio/constructorplantilla', component: RouteMockUpComponent },
  { path: 'gestionservicio/constructorplantilla/:templateId', component: RouteMockUpComponent }
];
// --- E N D   O F   R O U T I N G   C O M P O N E N T
