import { convertToParamMap, ParamMap, Params } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { Subject } from 'rxjs';

/**
 * An ActivateRoute test double with a `paramMap` observable.
 * Use the `setParamMap()` method to add the next `paramMap` value.
 */
export class ActivatedRouteStub {
  // public params : 
  // Use a ReplaySubject to share previous values with subscribers
  // and pump new values into the `paramMap` observable
  // private subject = new ReplaySubject<ParamMap>();
  private subject = new Subject<any>();

  // constructor(initialParams?: Params) {
  //   this.setParamMap(initialParams);
  // }

  /** The mock paramMap observable */
  readonly params = this.subject.asObservable();

  /** Set the paramMap observables's next value */
  setParamMap(params?: any) {
    // this.subject.next(convertToParamMap(params));
    this.subject.next(params);
  };
}