//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - CORE
import { Injectable } from '@angular/core';
import { CoreServiceFunctions } from './CoreServiceFunctions';
import { Cita } from '@models/Cita.model';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class BackendServiceAppointmentsMock extends CoreServiceFunctions {
  // - C I T A S
  public backendCitas4MedicoById(identifier: string): Observable<Cita[]> {
    console.log("><[BackendServiceAppointmentsMock.backendCitas4MedicoById]> medicoIdentifier: " + identifier);
    // Get the selected file and process their contents to return a prerecorded list of appointments.
    let rawdata = require('../../../assets/mockdata/testing/' + identifier.toLowerCase() + '.mock.json');
    console.log("><[BackendServiceAppointmentsMock.backendCitas4MedicoById]> size: " + rawdata.length);
    let data = this.transformRequestOutput(rawdata) as Cita[];
    console.log("--[BackendServiceAppointmentsMock.backendCitas4MedicoById]> item count: " + data.length);
    return Observable.create((observer) => {
      observer.next(data);
      observer.complete();
    });
  }
}
