//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - INTERFACES
import { INode } from '@interfaces/core/INode.interface';
// - MODELS
import { Medico } from '@models/Medico.model';
import { Perfil } from '@app/models/Perfil.model';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';
import { AppModule } from '@app/models/AppModule.model';
import { CitasTemplate } from '@models/CitasTemplate.model';
import { Cita } from '@models/Cita.model';
import { Centro } from '@models/Centro.model';
import { Limitador } from '@models/Limitador.model';
import { OpenAppointment } from '@app/models/OpenAppointment.model';
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';

export class CoreServiceFunctions {
  // - R E S P O N S E   T R A N S F O R M A T I O N
  public transformRequestOutput(entrydata: any): INode[] | INode {
    let results: INode[] = [];
    // Check if the entry data is a single object. If so process it because can be an exception.
    if (entrydata instanceof Array) {
      for (let key in entrydata) {
        // Access the object into the spot.
        let node = entrydata[key] as INode;
        // Convert and add the node.
        results.push(this.convertNode(node));
      }
    } else {
      // Process a single element.
      let jclass = entrydata["jsonClass"];
      if (null == jclass) return [];
      return this.convertNode(entrydata);
    }
    return results;
  }
  protected convertNode(node): INode {
    switch (node.jsonClass) {
      case "Centro":
        let convertedCentro = new Centro(node);
        console.log("--[AppModelStoreService.convertNode]> Centro node: " + convertedCentro.getId());
        return convertedCentro;
      case "Medico":
        let convertedMedico = new Medico(node);
        console.log("--[AppModelStoreService.convertNode]> Medico node: " + convertedMedico.getId());
        return convertedMedico;
      case "Cita":
        let convertedCita = new Cita(node);
        console.log("--[AppModelStoreService.convertNode]> Cita node: " + convertedCita.huecoId);
        return convertedCita;
      // case "AppointmentReport":
      //   let reportCita = new AppointmentReport(node);
      //   console.log("--[AppModelStoreService.convertNode]> AppointmentReport node: " + reportCita.reportdate);
      //   return reportCita;
      case "Credencial":
        let convertedCredential = new Perfil(node);
        console.log("--[AppModelStoreService.convertNode]> AppointmentReport node: " + convertedCredential.getId());
        return convertedCredential;
      case "CitasTemplate":
        let convertedTemplate = new CitasTemplate(node);
        console.log("--[AppModelStoreService.convertNode]> CitasTemplate node: " + convertedTemplate.nombre);
        return convertedTemplate;
      case "Limitador":
        let convertedLimit = new Limitador(node);
        console.log("--[AppModelStoreService.convertNode]> Limitador node: " + convertedLimit.titulo);
        return convertedLimit;
      case "TemplateBuildingBlock":
        let convertedBuildingBlock = new TemplateBuildingBlock(node);
        console.log("--[AppModelStoreService.convertNode]> TemplateBuildingBlock node: " + convertedBuildingBlock.nombre);
        return convertedBuildingBlock;
      case "ActoMedicoCentro":
        let convertedActoMedico = new ActoMedicoCentro(node);
        console.log("--[AppModelStoreService.convertNode]> ActoMedicoCentro node: " + convertedActoMedico.nombre);
        return convertedActoMedico;
      case "OpenAppointment":
        let convertedAppointment = new OpenAppointment(node);
        console.log("--[AppModelStoreService.convertNode]> OpenAppointment node: " + convertedAppointment.getId());
        return convertedAppointment;
      // case "ServiceAuthorized":
      //   let convertedService = new ServiceAuthorized(node);
      //   console.log("--[AppModelStoreService.convertNode]> ServiceAuthorized node: " + convertedService.getId());
      //   return convertedService;
      case "AppModule":
        let convertedAppModule = new AppModule(node);
        console.log("--[AppModelStoreService.convertNode]> AppModule node: " + convertedAppModule.nombre);
        return convertedAppModule;
    }
  }
}