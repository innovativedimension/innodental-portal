//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';

@Injectable()
export class MockAngularIsolationService {
  // - M O C K   S T O R A G E
  public storage: Map<string, any> = new Map<string, any>();
  public sessionStorage: Map<string, any> = new Map<string, any>();
  // - C O N S T A N T S
  public CREDENTIAL_KEY: string;

  // - C O N S T R U C T O R
  constructor() {
    this.CREDENTIAL_KEY = environment.CREDENTIAL_KEY;
  }

  // - E N V I R O N M E N T   A C C E S S
  public getServerName(): string {
    return environment.serverName;
  }
  public getApiV1(): string {
    return environment.apiVersion1;
  }
  public getApiV2(): string {
    return environment.apiVersion2;
  }
  public getAppName(): string {
    return environment.appName;
  }
  public getAppVersion(): string {
    return environment.appVersion;
  }
  public inDevelopment(): boolean {
    return environment.development;
  }
  public getMockStatus(): boolean {
    return environment.mockStatus;
  }
  public showExceptions(): boolean {
    return environment.showexceptions;
  }

  // - S T O R A G E
  public getFromStorage(_key: string): any {
    return this.storage.get(_key);
  }
  public setToStorage(_key: string, _content: any): any {
    return this.storage.set(_key, _content)
  }
  public removeFromStorage(_key: string): any {
    this.storage.delete(_key);
  }
  public getFromSession(_key: string): any {
    return this.sessionStorage.get(_key);
  }
  public setToSession(_key: string, _content: any): any {
    return this.sessionStorage.set(_key, _content)
  }
  public removeFromSession(_key: string): any {
    this.sessionStorage.delete(_key);
  }
}