//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
// - HTTP PACKAGE
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
// import { HttpHeaders } from '@angular/common/http';
// - SERVICES
import { EmptyMockAppStoreService } from '@app/testing/services/EmptyMockAppStoreService.service';
// - INTERFACES
import { INode } from '@interfaces/core/INode.interface';
// - MODELS
import { Medico } from '@models/Medico.model';
import { Perfil } from '@app/models/Perfil.model';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';
import { AppModule } from '@app/models/AppModule.model';
import { CitasTemplate } from '@models/CitasTemplate.model';
import { Cita } from '@models/Cita.model';
import { Centro } from '@models/Centro.model';
import { Limitador } from '@models/Limitador.model';
import { OpenAppointment } from '@app/models/OpenAppointment.model';
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';

@Injectable({
  providedIn: 'root'
})
export class MockAppStoreService extends EmptyMockAppStoreService {
  // - M O C K   F I E L D S
  private _credential: Perfil;
  private _activeService: Medico;
  private _doctorsActiveCache: Subject<Medico[]> = new Subject();

  // - M O C K   D A T A   A C C E S S
  public directAccessMockResource(_location: string): any | any[] {
    console.log(">>[MockAppStoreService.directAccessMockResource]> location: " + _location);
    let rawdata = require('../../../assets/mockdata/testing/' + _location.toLowerCase() + '.mock.json');
    let data = this.transformRequestOutput(rawdata);
    // console.log("--[MockAppStoreService.directAccessMockResource]> item count: " + data.length);
    return data;
  }
  public directAccessMockBug(_location: string): any {
    let rawdata = require('../../../assets/mockdata/' + _location.toLowerCase() + '.mock.json');
    let data = this.transformRequestOutput(rawdata);
    return data;
  }

  // - C R E D E N T I A L
  public accessLoginProfile(): Perfil {
    return this.accessCredential();
  }
  /**
  * Get the currrent credential. This should be called when the credential is already validaded so in case it is empty we should return a new empty instance.
  * @return [description]
  */
  public accessCredential(): Perfil {
    return this._credential;
  }
  /**
  * Store the credential in the app store and also on the session storage. On the session we avoid to request the credentials on every page reload. Maybe we can remove that storage when going to production.
  * @param _credential the credential to be stored.
  */
  public storeCredential(_credential: Perfil): void {
    if (null != _credential) {
      this._credential = _credential;
    }
  }

  // - A C T I V E   S E R V I C E
  public accessActiveService(): Medico {
    return this._activeService;
  }
  public storeActiveService(_service: Medico): void {
    if (null != _service) {
      this._activeService = _service;
    }
  }
  public clearActiveService(): void {
    this._activeService = null;
  }

  // - D O C T O R S
  public accessDoctors(): Observable<Medico[]> {
    return this._doctorsActiveCache;
  }
  public downloadDoctors(_newdoctorlist: Medico[]): void {
    this._doctorsActiveCache.next(_newdoctorlist);
  }
  public clearDoctors(): void {
    this._doctorsActiveCache.next([]);
  }

  //--- P R O P E R T I E S   S E R V I C E
  public propertiesEspecialidades(): Observable<string[]> {
    console.log("><[AppStoreServiceDefinitions.getSpecialityList]");
    console.log("><[AppStoreServiceDefinitions.propertiesAseguradoras]");
    return Observable.create((observer) => {
      let rawdata = require('../../../assets/properties/' + 'especialidades.json');
      observer.next(rawdata);
      observer.complete();
    });
  }
  public propertiesLimitadores(): Observable<Limitador[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesLimitadores]");
    return Observable.create((observer) => {
      let rawdata = require('../../../assets/properties/' + 'limitadores.json');
      observer.next(rawdata);
      observer.complete();
    });
  }
  public propertiesAseguradoras(): Observable<string[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesAseguradoras]");
    return Observable.create((observer) => {
      let rawdata = require('../../../assets/properties/' + 'aseguradoras.json');
      observer.next(rawdata);
      observer.complete();
    });
  }
  public propertiesActosMedicos(): Observable<ActoMedicoCentro[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesActosMedicos]");
    return Observable.create((observer) => {
      let rawdata = require('../../../assets/properties/' + 'actosmedicos.json');
      observer.next(this.transformRequestOutput(rawdata));
      observer.complete();
    });
  }

  //---  H T T P   W R A P P E R S
  // public wrapHttpRESOURCECall(request: string): Observable<any> {
  //   console.log("><[AppStoreService.wrapHttpGETCall]> request: " + request);
  //   return this.http.get(request);
  // }

  //--- G L O B A L   A C C E S S   M E T H O D S
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }
  public isNonEmptyArray(data: any[]): boolean {
    if (null == data) return false;
    if (data.length < 1) return false;
    return true;
  }
  public isEmptyString(str: string): boolean {
    let empty = str && str.length > 0; // Or any other logic, removing whitespace, etc.
    return !empty;
  }
  public isEmptyArray(data: any[]): boolean {
    if (null == data) return true;
    if (data.length < 1) return true;
    return false;
  }
  /**
   * Adds time to a date. Modelled after MySQL DATE_ADD function.
   * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
   * https://stackoverflow.com/a/1214753/18511
   *
   * @param date  Date to start with
   * @param interval  One of: year, quarter, month, week, day, hour, minute, second
   * @param units  Number of units of the given interval to add.
   */
  public dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }
  public date2BasicISO(_date: Date): string {
    var local = new Date(_date);
    local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }
  /**
   * Funtion to validate NIF/CIF/NIE
   * @param  value the field value at least with 8 characters.
   * @return       boolean being true that the field is value.
   */
  public validateNIF(value: string): boolean {
    let validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
    let nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    let nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    let str = value.toString().toUpperCase();

    if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

    let nie = str
      .replace(/^[X]/, '0')
      .replace(/^[Y]/, '1')
      .replace(/^[Z]/, '2');

    let letter = str.substr(-1);
    let charIndex = parseInt(nie.substr(0, 8)) % 23;

    if (validChars.charAt(charIndex) === letter) return true;

    return false;
  }

  // - R E S P O N S E   T R A N S F O R M A T I O N
  public transformRequestOutput(entrydata: any): INode[] | INode {
    let results: INode[] = [];
    // Check if the entry data is a single object. If so process it because can be an exception.
    if (entrydata instanceof Array) {
      for (let key in entrydata) {
        // Access the object into the spot.
        let node = entrydata[key] as INode;
        // Convert and add the node.
        results.push(this.convertNode(node));
      }
    } else {
      // Process a single element.
      let jclass = entrydata["jsonClass"];
      if (null == jclass) return [];
      return this.convertNode(entrydata);
    }
    return results;
  }
  protected convertNode(node): INode {
    switch (node.jsonClass) {
      case "Centro":
        let convertedCentro = new Centro(node);
        console.log("--[AppModelStoreService.convertNode]> Centro node: " + convertedCentro.getId());
        return convertedCentro;
      case "Medico":
        let convertedMedico = new Medico(node);
        console.log("--[AppModelStoreService.convertNode]> Medico node: " + convertedMedico.getId());
        return convertedMedico;
      case "Cita":
        let convertedCita = new Cita(node);
        console.log("--[AppModelStoreService.convertNode]> Cita node: " + convertedCita.huecoId);
        return convertedCita;
      // case "AppointmentReport":
      //   let reportCita = new AppointmentReport(node);
      //   console.log("--[AppModelStoreService.convertNode]> AppointmentReport node: " + reportCita.reportdate);
      //   return reportCita;
      case "Credencial":
        let convertedCredential = new Perfil(node);
        console.log("--[AppModelStoreService.convertNode]> AppointmentReport node: " + convertedCredential.getId());
        return convertedCredential;
      case "CitasTemplate":
        let convertedTemplate = new CitasTemplate(node);
        console.log("--[AppModelStoreService.convertNode]> CitasTemplate node: " + convertedTemplate.nombre);
        return convertedTemplate;
      case "Limitador":
        let convertedLimit = new Limitador(node);
        console.log("--[AppModelStoreService.convertNode]> Limitador node: " + convertedLimit.titulo);
        return convertedLimit;
      case "TemplateBuildingBlock":
        let convertedBuildingBlock = new TemplateBuildingBlock(node);
        console.log("--[AppModelStoreService.convertNode]> TemplateBuildingBlock node: " + convertedBuildingBlock.nombre);
        return convertedBuildingBlock;
      case "ActoMedicoCentro":
        let convertedActoMedico = new ActoMedicoCentro(node);
        console.log("--[AppModelStoreService.convertNode]> ActoMedicoCentro node: " + convertedActoMedico.nombre);
        return convertedActoMedico;
      case "OpenAppointment":
        let convertedAppointment = new OpenAppointment(node);
        console.log("--[AppModelStoreService.convertNode]> OpenAppointment node: " + convertedAppointment.getId());
        return convertedAppointment;
      // case "ServiceAuthorized":
      //   let convertedService = new ServiceAuthorized(node);
      //   console.log("--[AppModelStoreService.convertNode]> ServiceAuthorized node: " + convertedService.getId());
      //   return convertedService;
      case "AppModule":
        let convertedAppModule = new AppModule(node);
        console.log("--[AppModelStoreService.convertNode]> AppModule node: " + convertedAppModule.nombre);
        return convertedAppModule;
    }
  }

  // - B A C K E N D   M O C K   S Y S T E M
  public backendAccessMock(_reference: string, _fireException?: any): string {
    let rawdata = require('../../../assets/mockdata/' + _reference.toLowerCase() + '.mock.json');
    return rawdata;
  }
  public injectResponse(_rawData: string): void { }
  public injectException(_exception: HttpErrorResponse) {
    throw (_exception);
  }

  // - N O T I F I C A T I O N S
  public successNotification(_message: string, _title?: string, _options?: any): void {
    console.log("--[AppModelStoreService.successNotification]> "
      + _title + ':' + _message);
  }
  public errorNotification(_message: string, _title?: string, _options?: any): void {
    console.log("--[AppModelStoreService.errorNotification]> "
      + _title + ':' + _message);
  }
  public warningNotification(_message: string, _title?: string, _options?: any): void {
    console.log("--[AppModelStoreService.warningNotification]> "
      + _title + ':' + _message);
  }
  public infoNotification(_message: string, _title?: string, _options?: any): void {
    console.log("--[AppModelStoreService.infoNotification]> "
      + _title + ':' + _message);
  }

  // - E R R O R   P R O C E S S I N G
  public processBackendError(_error: any): void {
    // Process any 401 exception that means the session is no longer valid.
    if (_error.status == 401) {
      this.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
      this.navigate(['login']);
    }
    if (_error.name === "HttpErrorResponse") {
      this.errorNotification("Error inesperado durante las comunicaciones. Es necesario logarse de nuevo.", "¡Atención!");
      this.navigate(['login']);
    }
  }
  public navigate(_route: string[]): void {
    let routeData: string = '';
    for (let data of _route) {
      routeData = routeData + ' ' + data;
    }
    console.log("--[AppModelStoreService.navigate]> "
      + routeData);
  }
}
