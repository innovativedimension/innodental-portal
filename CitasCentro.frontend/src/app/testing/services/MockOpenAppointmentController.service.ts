//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
// --- MODELS
import { CitasTemplate } from '@models/CitasTemplate.model';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
import { OpenAppointment } from '@app/models/OpenAppointment.model';

@Injectable({
  providedIn: 'root'
})
export class MockOpenAppointmentControllerService {
  private _appointmentsSource: OpenAppointment[] = [];

  // - C O N S T R U C T O R
  constructor(protected appStoreService: MockAppStoreService) { }

  public downloadAppointments4Patient(_identifier: string): Observable<OpenAppointment[]> {
    console.log(">>[MockOpenAppointmentControllerService.downloadAppointments4Patient]");
    return Observable.create((observer) => {
      let openAppointments = this.appStoreService.directAccessMockResource('openappointments');
      // Read all the opena appointments and store them on the Subject.
      // Filtering is performed on the access call.
      let activeAppointments: OpenAppointment[] = [];
      for (let open of openAppointments) {
        // On the first approach use all the appointments
        console.log("--[OpenAppointmentControllerService.downloadAppointments4Patient.backendAppointments4Patient]> Appointment: " +
          open.getId() + ' state: ' + open.getEstado());
        activeAppointments.push(open);
      }
      // Store on the subject the new list of open appointments.
      this.storeOpenAppointments(activeAppointments);

      observer.next(activeAppointments);
      observer.complete();
    });
  }
  public accessOpenAppointments(_filter?: string): Observable<OpenAppointment[]> {
    if (null == _filter) return Observable.create((observer) => {
      observer.next(this._appointmentsSource);
      observer.complete();
    });
    if (_filter == 'ALL') return Observable.create((observer) => {
      observer.next(this._appointmentsSource);
      observer.complete();
    });
  }
  public accessAllOpenAppointments(): Observable<OpenAppointment[]> {
    return Observable.create((observer) => {
      observer.next(this._appointmentsSource);
      observer.complete();
    });
  }
  public storeOpenAppointments(_newappointments: OpenAppointment[]): void {
    this._appointmentsSource = _newappointments;
  }
}
