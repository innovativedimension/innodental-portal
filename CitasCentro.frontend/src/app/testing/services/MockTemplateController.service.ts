//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Injectable } from '@angular/core';
// --- MODELS
import { CitasTemplate } from '@models/CitasTemplate.model';

@Injectable({
  providedIn: 'root'
})
export class MockTemplateControllerService {
  private _templateUnderControl: CitasTemplate;

  // - T E M P L A T E   A C C E S S
  public accessTemplate(): CitasTemplate {
    console.log("><[TemplateControllerService.accessTemplate]");
    return this._templateUnderControl;
  }
  public storeTemplate(_newtemplate: CitasTemplate): void {
    console.log(">>[TemplateControllerService.storeTemplate]> _newtemplate.name: " + _newtemplate.nombre);
    this._templateUnderControl = _newtemplate;
    console.log("<<[TemplateControllerService.storeTemplate]");
  }
}
