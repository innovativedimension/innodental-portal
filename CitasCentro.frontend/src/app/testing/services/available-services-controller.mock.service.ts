// - CORE
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
// - SERVICES
import { AppStoreService } from '@app/services/appstore.service';
// - MODELS
import { ServiceAuthorized } from '@app/models/ServiceAuthorized.model';
import { Medico } from '@models/Medico.model';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class AvailableServicesControllerStub {
  private _availableServices: Subject<Medico[]> = new Subject();

  // - A V A I L A B L E   S E R V I C E S
  public accessAvailableServices(): Observable<Medico[]> {
    return this._availableServices;
  }
  public downloadAvailableServices(_newdoctorlist: Medico[]): void {
    this._availableServices.next(_newdoctorlist);
  }
}
