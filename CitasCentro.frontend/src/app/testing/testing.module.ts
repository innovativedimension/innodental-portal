import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    RouteMockUpComponent
  ]
})
export class TestingModule { }
