//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- TEST
import { TestBed } from '@angular/core/testing';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- MODELS
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';

describe('MODEL ActoMedicoCentro [Module: MODELS]', () => {
  let model: ActoMedicoCentro;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService }
      ]
    })
      .compileComponents();

    model = new ActoMedicoCentro();
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should create an instance', () => {
      console.log('><[model/ActoMedicoCentro]> should create an instance');
      expect(model).toBeDefined('instance has not been created.');
    });
    it('fields should be on initial state', () => {
      console.log('><[model/ActoMedicoCentro]> "etiqueta" should be "R"');
      expect(model.etiqueta).toBe('R', 'etiqueta should be "R" as initial value');
      console.log('><[model/ActoMedicoCentro]> "nombre" should be "RADIOLOGÍA"');
      expect(model.nombre).toBe('RADIOLOGÍA', 'nombre should be "RADIOLOGÍA" as initial value');
      console.log('><[model/ActoMedicoCentro]> "tiempoRequerido" should be 15');
      expect(model.tiempoRequerido).toBe(15, 'tiempoRequerido should be 15 as initial value');
      console.log('><[model/ActoMedicoCentro]> "tipo" should be "TRANSPARENTE"');
      expect(model.tipo).toBe('TRANSPARENTE', 'tipo should be "TRANSPARENTE" as initial value');
      console.log('><[model/ActoMedicoCentro]> "maquina" should exist');
      expect(model.maquina).toBeUndefined('maquina should exist as initial value');
      console.log('><[model/ActoMedicoCentro]> "recurso" should exist');
      expect(model.recurso).toBeUndefined('recurso should exist as initial value');
    });
    it('Constructor transformations from backend', () => {
      console.log('><[model/ActoMedicoCentro]> Constructor transformations from backend. Acto transparente');
      let alterModel = appStoreService.directAccessMockResource('singleActoMedicoCentro.transparente') as ActoMedicoCentro;

      expect(alterModel.etiqueta).toBe('Rx', 'should be Rx');
      expect(alterModel.nombre).toBe('RAYOS X Especial', 'should be "RAYOS X Especial"');
      expect(alterModel.tiempoRequerido).toBe(20, 'should be 20');
      expect(alterModel.tipo).toBe('NARANJA', 'should be "NARANJA"');
      expect(alterModel.maquina).toBeDefined('maquina should exist and point to a valid service');
      expect(alterModel.maquina.id).toBe('MID:100001:SRVC:RX', 'should be "MID:100001:SRVC:RX"');
      expect(alterModel.maquina.referencia).toBe('Rayos X', 'should be "Rayos X"');
      expect(alterModel.maquina.nombre).toBe('Rayos X', 'should be "Rayos X"');
      expect(alterModel.maquina.especialidad).toBe('Radiología', 'should be "Radiología"');
    });
  });

  // - G E T T E R S / S E T T E R S   P H A S E
  describe('Getters/Setters Phase', () => {
    it('Identifier: getter', () => {
      console.log('><[model/ActoMedicoCentro]> Identifier: getter');
      let alterModel = appStoreService.directAccessMockResource('singleActoMedicoCentro.transparente') as ActoMedicoCentro;
      expect(alterModel.getServiceIdentifiers().length).toBe(1, 'should be an array with 1 value');
      expect(alterModel.getServiceIdentifiers()).toEqual(["MID:100001:SRVC:RX"], 'should be an array with values [MID:100001:SRVC:RX]');
    });
    it('Especialidad: getter/setter', () => {
      console.log('><[model/ActoMedicoCentro]> Especialidad: getter/setter');
      expect(model.getEspecialidad()).toBe('Radiología', 'should be "Radiología"');
      let alterModel = appStoreService.directAccessMockResource('singleActoMedicoCentro.transparente') as ActoMedicoCentro;
      expect(alterModel.getEspecialidad()).toBe('Radiología', 'should be "Radiología"');
    });
  });
});