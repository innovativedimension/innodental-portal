//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- MODULES IMPORTS
import { HttpClientModule } from '@angular/common/http';
// --- TEST
import { TestBed } from '@angular/core/testing';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- MODELS
import { Cita } from '@models/Cita.model';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';

describe('MODEL Cita [Module: MODELS]', () => {
  let model: Cita;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService }
      ]
    })
      .compileComponents();

    model = new Cita();
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should create an instance', () => {
      console.log('><[model/Cita]> should create an instance');
      expect(model).toBeDefined('instance has not been created.');
    });
    it('fields should be on initial state', () => {
      console.log('><[model/Cita]> "id" should be -1');
      expect(model.id).toBe(-1, 'id should be -1 as initial value');
      console.log('><[model/Cita]> "referencia" should be undefined');
      expect(model.referencia).toBeUndefined('referencia should be undefined as initial value');
      console.log('><[model/Cita]> "fecha" should be undefined');
      expect(model.fecha).toBeUndefined('fecha should be undefined as initial value');
      console.log('><[model/Cita]> "fechaDate" should be empty');
      expect(model.fechaDate).toBeDefined('fechaDate should be today as initial value');
      console.log('><[model/Cita]> "huecoId" should be 800');
      expect(model.huecoId).toBe(800, 'huecoId should be 800 as initial value');
      console.log('><[model/Cita]> "huecoDuracion" should be 15');
      expect(model.huecoDuracion).toBe(15, 'huecoDuracion should be 15 as initial value');
      console.log('><[model/Cita]> "estado" should be LIBRE');
      expect(model.estado).toBe('LIBRE', 'estado should be LIBRE as initial value');
      console.log('><[model/Cita]> "tipo" should be TRANSPARENTE');
      expect(model.tipo).toBe('TRANSPARENTE', 'tipo should be TRANSPARENTE as initial value');
      console.log('><[model/Cita]> "zonaHoraria" should be MAÑANA');
      expect(model.zonaHoraria).toBe('MAÑANA', 'zonaHoraria should be MAÑANA as initial value');
      console.log('><[model/Cita]> "pacienteLocalizador" should be undefined');
      expect(model.pacienteLocalizador).toBeUndefined('pacienteLocalizador should be undefined as initial value');
      console.log('><[model/Cita]> "pacienteData" should be undefined');
      expect(model.pacienteData).toBeUndefined('pacienteData should be undefined as initial value');
    });
    it('Constructor transformations', () => {
      console.log('><[model/Cita]> Constructor transformations');
      let alterModel = new Cita({
        id: 200,
        referencia: 'Nombre Test',
        huecoId: 900,
        huecoDuracion: 45,
        estado: 'BLOQUEADA',
        tipo: 'AMARILLO',
        zonaHoraria: 'TARDE'
      });
      expect(alterModel.id).toBe(200, 'should be 00');
      expect(alterModel.referencia).toBe('Nombre Test', 'should be "Nombre Test"');
      expect(alterModel.huecoId).toBe(900, 'shouldbe 900');
      expect(alterModel.huecoDuracion).toBe(45, 'shouldbe 45');
      expect(alterModel.estado).toBe('BLOQUEADA', 'shouldbe BLOQUEADA');
      expect(alterModel.tipo).toBe('AMARILLO', 'shouldbe AMARILLO');
      expect(alterModel.zonaHoraria).toBe('TARDE', 'shouldbe TARDE');
    });
    it('Constructor transformation from backend. Template configuration', () => {
      console.log('><[model/Cita]> Constructor transformations');
      let template = appStoreService.directAccessMockResource('cita-template') as Cita;
      expect(template.id).toBe(2, 'should be 2');
      // TEST-PENDING: Date comparisons solution still pending.
      // expect(template.fechaDate).toEqual(new Date('2018-12-11T21:11:42.293Z'), 'should be "2018-12-11T21:11:42.293Z"');
      expect(template.estado).toBe('LIBRE', 'should be LIBRE');
      expect(template.tipo).toBe('TRANSPARENTE', 'should be TRANSPARENTE');
      expect(template.zonaHoraria).toBe('MAÑANA', 'should be MAÑANA');
      expect(template.huecoId).toBe(910, 'should be 910');
      expect(template.huecoDuracion).toBe(10, 'should be 10');
    });
  });

  // - B U G   I D E N T I F I C A T I O N   P H A S E.
  describe('Bug Identification Phase', () => {
    it('B04. the hour slot time is not calculated right', () => {
      console.log('><[citaciones/PatientPanelv2Component]> B04. the hour slot time is not calculated right');
      // Setup the hueco for some hours and check the correct conversion.
      model.huecoId = 800;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 800');
      expect(model.getHour()).toBe(8, 'should be 8');
      expect(model.getMinutes()).toBe(0, 'should be 0');
      model.huecoId = 850;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 850');
      expect(model.getHour()).toBe(8, 'should be 8');
      expect(model.getMinutes()).toBe(50, 'should be 50');
      model.huecoId = 945;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 945');
      expect(model.getHour()).toBe(9, 'should be 9');
      expect(model.getMinutes()).toBe(45, 'should be 45');
      model.huecoId = 950;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 950');
      expect(model.getHour()).toBe(9, 'should be 9');
      expect(model.getMinutes()).toBe(50, 'should be 50');
      model.huecoId = 955;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 955');
      expect(model.getHour()).toBe(9, 'should be 9');
      expect(model.getMinutes()).toBe(55, 'should be 55');
      model.huecoId = 1000;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 1000');
      expect(model.getHour()).toBe(10, 'should be 10');
      expect(model.getMinutes()).toBe(0, 'should be 0');
      model.huecoId = 1050;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 1050');
      expect(model.getHour()).toBe(10, 'should be 10');
      expect(model.getMinutes()).toBe(50, 'should be 50');

      model.huecoId = 1145;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 1145');
      expect(model.getHour()).toBe(11, 'should be 11');
      expect(model.getMinutes()).toBe(45, 'should be 45');
      model.huecoId = 1150;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 1150');
      expect(model.getHour()).toBe(11, 'should be 11');
      expect(model.getMinutes()).toBe(50, 'should be 50');
      model.huecoId = 1155;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 1155');
      expect(model.getHour()).toBe(11, 'should be 11');
      expect(model.getMinutes()).toBe(55, 'should be 55');
      model.huecoId = 1200;
      console.log('><[citaciones/PatientPanelv2Component]> valdationg 1200');
      expect(model.getHour()).toBe(12, 'should be 12');
      expect(model.getMinutes()).toBe(0, 'should be 0');
    });
  });

  // // - G E T T E R S / S E T T E R S   P H A S E
  // describe('Getters/Setters Phase', () => {
  //   it('getNombre: getter/setter', () => {
  //     console.log('><[model/Cita]> getNombre: getter/setter');
  //     model.setNombre('Nombre de Prueba');
  //     expect(model.getNombre()).toBe('Nombre de Prueba', 'should be "Nombre de Prueba"');
  //   });
  //   it('getMorningList: getter', () => {
  //     console.log('><[model/Cita]> getMorningList: getter');
  //     expect(model.getMorningList()).toBeDefined('should exist');
  //     expect(model.getMorningList().length).toBe(0, 'should be empty');
  //   });
  //   it('getEveningList: getter', () => {
  //     console.log('><[model/Cita]> getEveningList: getter');
  //     expect(model.getEveningList()).toBeDefined('should exist');
  //     expect(model.getEveningList().length).toBe(0, 'should be empty');
  //   });
  // });

  // // - C O D E   C O V E R A G E   P H A S E
  // describe('Code Coverage Phase [addMorning]', () => {
  //   it('addMorning: adding appointments', () => {
  //     console.log('><[model/Cita]> addMorning: adding appointments');
  //     expect(model.getMorningList()).toBeDefined('should exist');
  //     expect(model.getMorningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addMorning(blockFree);
  //     let list = model.getMorningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');
  //     console.log('><[model/Cita]> addMorning: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(800, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(10, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [addEvening]', () => {
  //   it('addEvening: adding appointments', () => {
  //     console.log('><[model/Cita]> addEvening: adding appointments');
  //     expect(model.getEveningList()).toBeDefined('should exist');
  //     expect(model.getEveningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addEvening(blockFree);
  //     let list = model.getEveningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');
  //     console.log('><[model/Cita]> addEvening: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(1500, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(10, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('TARDE', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [removeMorning]', () => {
  //   it('removeMorning: remove added appointments', () => {
  //     console.log('><[model/Cita]> removeMorning: remove added appointments');
  //     expect(model.getMorningList()).toBeDefined('should exist');
  //     expect(model.getMorningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addMorning(blockFree);
  //     model.addMorning(blockFree);
  //     expect(model.getMorningList().length).toBe(2, 'should be have 2 elements');
  //     let list = model.getMorningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     // Remove the first element and check the other
  //     model.removeMorning(appointment)
  //     expect(model.getMorningList().length).toBe(1, 'should be have 1 element');
  //     list = model.getMorningList();
  //     appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     console.log('><[model/Cita]> removeMorning: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(800, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(10, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [removeEvening]', () => {
  //   it('removeEvening: remove added appointments', () => {
  //     console.log('><[model/Cita]> removeEvening: remove added appointments');
  //     expect(model.getEveningList()).toBeDefined('should exist');
  //     expect(model.getEveningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addEvening(blockFree);
  //     model.addEvening(blockFree);
  //     expect(model.getEveningList().length).toBe(2, 'should be have 2 elements');
  //     let list = model.getEveningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     // Remove the first element and check the other
  //     model.removeEvening(appointment)
  //     expect(model.getEveningList().length).toBe(1, 'should be have 1 element');
  //     list = model.getEveningList();
  //     appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     console.log('><[model/Cita]> removeEvening: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(1500, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(10, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('TARDE', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [addMorningOnTop]', () => {
  //   it('addMorningOnTop: adding appointments', () => {
  //     console.log('><[model/Cita]> addMorningOnTop: adding appointments');
  //     expect(model.getMorningList()).toBeDefined('should exist');
  //     expect(model.getMorningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addMorning(blockFree);
  //     model.addMorning(blockFree);
  //     model.addMorning(blockFree);
  //     let list = model.getMorningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     // Add a new block on top of the first appointment.
  //     let blockOnTop = new TemplateBuildingBlock({
  //       "nombre": "Cita de 30 minutos",
  //       "duracion": 30,
  //       "tipo": "ROJO",
  //       "estado": "LIBRE"
  //     });
  //     model.addMorningOnTop(appointment, blockOnTop);
  //     expect(model.getMorningList().length).toBe(4, 'should be have 4 elements');
  //     list = model.getMorningList();
  //     appointment = list[1];
  //     expect(appointment).toBeDefined('checking the existence for appointment');
  //     console.log('><[model/Cita]> addMorningOnTop: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(810, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(30, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('ROJO', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [addEveningOnTop]', () => {
  //   it('addEveningOnTop: adding appointments', () => {
  //     console.log('><[model/Cita]> addEveningOnTop: adding appointments');
  //     expect(model.getEveningList()).toBeDefined('should exist');
  //     expect(model.getEveningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addEvening(blockFree);
  //     model.addEvening(blockFree);
  //     model.addEvening(blockFree);
  //     let list = model.getEveningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     // Add a new block on top of the first appointment.
  //     let blockOnTop = new TemplateBuildingBlock({
  //       "nombre": "Cita de 30 minutos",
  //       "duracion": 30,
  //       "tipo": "ROJO",
  //       "estado": "LIBRE"
  //     });
  //     model.addEveningOnTop(appointment, blockOnTop);
  //     expect(model.getEveningList().length).toBe(4, 'should be have 4 elements');
  //     list = model.getEveningList();
  //     appointment = list[1];
  //     expect(appointment).toBeDefined('checking the existence for appointment');
  //     console.log('><[model/Cita]> addEveningOnTop: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(1510, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(30, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('ROJO', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('TARDE', 'checking value for appointment.zonaHoraria');
  //   });
  // });
});