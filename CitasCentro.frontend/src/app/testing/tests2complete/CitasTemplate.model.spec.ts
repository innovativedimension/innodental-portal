//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- MODULES IMPORTS
import { HttpClientModule } from '@angular/common/http';
// --- TEST
import { TestBed } from '@angular/core/testing';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- MODELS
import { CitasTemplate } from '@models/CitasTemplate.model';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';

describe('CitasTemplate [Module: MODELS]', () => {
  let model: CitasTemplate;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      // imports: [
      //   HttpClientModule
      // ],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService }
      ]
    })
      .compileComponents();

    model = new CitasTemplate();
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should create an instance', () => {
      console.log('><[model/CitasTemplate]> should create an instance');
      expect(model).toBeDefined('instance has not been created.');
    });
    it('fields should be on initial state', () => {
      console.log('><[model/CitasTemplate]> "id" should be -1');
      expect(model.id).toBe(-1, 'id should be -1 as initial value');
      console.log('><[model/CitasTemplate]> "nombre" should be empty');
      expect(model.nombre).toBe('', 'nombre should be empty as initial value');
      console.log('><[model/CitasTemplate]> "tmhoraInicio" should be undefined');
      expect(model.tmhoraInicio).toBeUndefined('tmhoraInicio should be undefined as initial value');
      console.log('><[model/CitasTemplate]> "tthoraInicio" should be undefined');
      expect(model.tthoraInicio).toBeUndefined('tthoraInicio should be undefined as initial value');
      console.log('><[model/CitasTemplate]> "morningList" should be empty');
      expect(model.getMorningList()).toBeDefined('morningList should be empty as initial value');
      console.log('><[model/CitasTemplate]> "eveningList" should be empty');
      expect(model.getEveningList()).toBeDefined('eveningList should be empty as initial value');
    });
    it('Constructor transformations', () => {
      console.log('><[model/CitasTemplate]> Constructor transformations');
      let alterModel = new CitasTemplate({
        nombre: 'Nombre Test',
        tmhoraInicio: '09:00',
        tthoraInicio: '16:00'
      });
      expect(alterModel.id).toBe(-1, 'should be -1');
      expect(alterModel.nombre).toBe('Nombre Test', 'should be "Nombre Test"');
      expect(alterModel.tmhoraInicio).toBe('09:00', 'shouldbe 09:00');
      expect(alterModel.tthoraInicio).toBe('16:00', 'shouldbe 16:00');
    });
    it('Constructor transformation from backend', () => {
      console.log('><[model/CitasTemplate]> Constructor transformations');
      let template = appStoreService.directAccessMockResource('CitasTemplate') as CitasTemplate;
      expect(template.id).toBe(200152, 'should be 200152');
      expect(template.nombre).toBe('Pruebas Mañanas', 'should be "Pruebas Mañanas"');
      expect(template.tmhoraInicio).toBe('09:00', 'should be 09:00');
      expect(template.tthoraInicio).toBe('16:00', 'should be 16:00');
      expect(template.getMorningList().length).toBe(12, 'should have 12 items');
      expect(template.getEveningList().length).toBe(7, 'should have 7 items');
    });
  });

  // - G E T T E R S / S E T T E R S   P H A S E
  describe('Getters/Setters Phase', () => {
    it('getNombre: getter/setter', () => {
      console.log('><[model/CitasTemplate]> getNombre: getter/setter');
      model.setNombre('Nombre de Prueba');
      expect(model.getNombre()).toBe('Nombre de Prueba', 'should be "Nombre de Prueba"');
    });
    it('getMorningList: getter', () => {
      console.log('><[model/CitasTemplate]> getMorningList: getter');
      expect(model.getMorningList()).toBeDefined('should exist');
      expect(model.getMorningList().length).toBe(0, 'should be empty');
    });
    it('getEveningList: getter', () => {
      console.log('><[model/CitasTemplate]> getEveningList: getter');
      expect(model.getEveningList()).toBeDefined('should exist');
      expect(model.getEveningList().length).toBe(0, 'should be empty');
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [addMorning]', () => {
    it('addMorning: adding appointments', () => {
      console.log('><[model/CitasTemplate]> addMorning: adding appointments');
      expect(model.getMorningList()).toBeDefined('should exist');
      expect(model.getMorningList().length).toBe(0, 'should be empty');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      model.addMorning(blockFree);
      let list = model.getMorningList();
      let appointment = list[0];
      expect(appointment).toBeDefined('checking the existence for appointment');
      console.log('><[model/CitasTemplate]> addMorning: validating appointment data');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(800, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(10, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
    });
  });
  describe('Code Coverage Phase [addEvening]', () => {
    it('addEvening: adding appointments', () => {
      console.log('><[model/CitasTemplate]> addEvening: adding appointments');
      expect(model.getEveningList()).toBeDefined('should exist');
      expect(model.getEveningList().length).toBe(0, 'should be empty');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      model.addEvening(blockFree);
      let list = model.getEveningList();
      let appointment = list[0];
      expect(appointment).toBeDefined('checking the existence for appointment');
      console.log('><[model/CitasTemplate]> addEvening: validating appointment data');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(1500, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(10, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('TARDE', 'checking value for appointment.zonaHoraria');
    });
  });
  describe('Code Coverage Phase [removeMorning]', () => {
    it('removeMorning: remove added appointments', () => {
      console.log('><[model/CitasTemplate]> removeMorning: remove added appointments');
      expect(model.getMorningList()).toBeDefined('should exist');
      expect(model.getMorningList().length).toBe(0, 'should be empty');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      model.addMorning(blockFree);
      model.addMorning(blockFree);
      expect(model.getMorningList().length).toBe(2, 'should be have 2 elements');
      let list = model.getMorningList();
      let appointment = list[0];
      expect(appointment).toBeDefined('checking the existence for appointment');

      // Remove the first element and check the other
      model.removeMorning(appointment)
      expect(model.getMorningList().length).toBe(1, 'should be have 1 element');
      list = model.getMorningList();
      appointment = list[0];
      expect(appointment).toBeDefined('checking the existence for appointment');

      console.log('><[model/CitasTemplate]> removeMorning: validating appointment data');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(800, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(10, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
    });
  });
  describe('Code Coverage Phase [removeEvening]', () => {
    it('removeEvening: remove added appointments', () => {
      console.log('><[model/CitasTemplate]> removeEvening: remove added appointments');
      expect(model.getEveningList()).toBeDefined('should exist');
      expect(model.getEveningList().length).toBe(0, 'should be empty');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      model.addEvening(blockFree);
      model.addEvening(blockFree);
      expect(model.getEveningList().length).toBe(2, 'should be have 2 elements');
      let list = model.getEveningList();
      let appointment = list[0];
      expect(appointment).toBeDefined('checking the existence for appointment');

      // Remove the first element and check the other
      model.removeEvening(appointment)
      expect(model.getEveningList().length).toBe(1, 'should be have 1 element');
      list = model.getEveningList();
      appointment = list[0];
      expect(appointment).toBeDefined('checking the existence for appointment');

      console.log('><[model/CitasTemplate]> removeEvening: validating appointment data');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(1500, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(10, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('TARDE', 'checking value for appointment.zonaHoraria');
    });
  });
  describe('Code Coverage Phase [addMorningOnTop]', () => {
    it('addMorningOnTop: adding appointments', () => {
      console.log('><[model/CitasTemplate]> addMorningOnTop: adding appointments');
      expect(model.getMorningList()).toBeDefined('should exist');
      expect(model.getMorningList().length).toBe(0, 'should be empty');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      model.addMorning(blockFree);
      model.addMorning(blockFree);
      model.addMorning(blockFree);
      let list = model.getMorningList();
      let appointment = list[0];
      expect(appointment).toBeDefined('checking the existence for appointment');

      // Add a new block on top of the first appointment.
      let blockOnTop = new TemplateBuildingBlock({
        "nombre": "Cita de 30 minutos",
        "duracion": 30,
        "tipo": "ROJO",
        "estado": "LIBRE"
      });
      model.addMorningOnTop(appointment, blockOnTop);
      expect(model.getMorningList().length).toBe(4, 'should be have 4 elements');
      list = model.getMorningList();
      appointment = list[1];
      expect(appointment).toBeDefined('checking the existence for appointment');
      console.log('><[model/CitasTemplate]> addMorningOnTop: validating appointment data');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(810, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(30, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('ROJO', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
    });
  });
  describe('Code Coverage Phase [addEveningOnTop]', () => {
    it('addEveningOnTop: adding appointments', () => {
      console.log('><[model/CitasTemplate]> addEveningOnTop: adding appointments');
      expect(model.getEveningList()).toBeDefined('should exist');
      expect(model.getEveningList().length).toBe(0, 'should be empty');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      model.addEvening(blockFree);
      model.addEvening(blockFree);
      model.addEvening(blockFree);
      let list = model.getEveningList();
      let appointment = list[0];
      expect(appointment).toBeDefined('checking the existence for appointment');

      // Add a new block on top of the first appointment.
      let blockOnTop = new TemplateBuildingBlock({
        "nombre": "Cita de 30 minutos",
        "duracion": 30,
        "tipo": "ROJO",
        "estado": "LIBRE"
      });
      model.addEveningOnTop(appointment, blockOnTop);
      expect(model.getEveningList().length).toBe(4, 'should be have 4 elements');
      list = model.getEveningList();
      appointment = list[1];
      expect(appointment).toBeDefined('checking the existence for appointment');
      console.log('><[model/CitasTemplate]> addEveningOnTop: validating appointment data');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(1510, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(30, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('ROJO', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('TARDE', 'checking value for appointment.zonaHoraria');
    });
  });
});