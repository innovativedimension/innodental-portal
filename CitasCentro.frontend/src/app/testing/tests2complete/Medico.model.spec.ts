//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- TEST
import { TestBed } from '@angular/core/testing';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- MODELS
import { Medico } from '@models/Medico.model';

xdescribe('MODEL Medico [Module: MODELS]', () => {
  let model: Medico;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService }
      ]
    })
      .compileComponents();

    model = new Medico();
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should create an instance', () => {
      console.log('><[model/Medico]> should create an instance');
      expect(model).toBeDefined('instance has not been created.');
    });
    it('fields should be on initial state', () => {
      console.log('><[model/Medico]> "id" should be -1');
      expect(model.id).toBe('-EMPTY-', 'id should be "-EMPTY-" as initial value');
      console.log('><[model/Medico]> "referencia" should exist');
      expect(model.referencia).toBeUndefined('referencia should exist as initial value');
      console.log('><[model/Medico]> "tratamiento" should exist');
      expect(model.tratamiento).toBeUndefined('tratamiento should exist as initial value');
      console.log('><[model/Medico]> "nombre" should be empty');
      expect(model.nombre).toBe('', 'nombre should be empty as initial value');
      console.log('><[model/Medico]> "apellidos" should be empty');
      expect(model.apellidos).toBe('', 'apellidos should be empty as initial value');
      console.log('><[model/Medico]> "especialidad" should exist');
      expect(model.especialidad).toBeUndefined('especialidad should exist as initial value');
      console.log('><[model/Medico]> "activo" should be empty');
      expect(model.activo).toBeTruthy('activo should be true');

      console.log('><[model/Medico]> "localidad" should exist');
      expect(model.localidad).toBeUndefined('localidad should exist as initial value');
      console.log('><[model/Medico]> "direccion" should exist');
      expect(model.direccion).toBeUndefined('direccion should exist as initial value');
      console.log('><[model/Medico]> "horario" should exist');
      expect(model.horario).toBeUndefined('horario should exist as initial value');
      console.log('><[model/Medico]> "telefono" should exist');
      expect(model.telefono).toBeUndefined('telefono should exist as initial value');

      console.log('><[model/Medico]> "appointmentsCount" should exist');
      expect(model.appointmentsCount).toBe(0, 'appointmentsCount should count 0 items');
      console.log('><[model/Medico]> "freeAppointmentsCount" should exist');
      expect(model.freeAppointmentsCount).toBe(0, 'freeAppointmentsCount should count 0 items');
      console.log('><[model/Medico]> "firstAppointmentId" should be -1');
      expect(model.firstAppointmentId).toBe(-1, 'firstAppointmentId should be -1');
      console.log('><[model/Medico]> "firstAppointment" should exist');
      expect(model.firstAppointment).toBeDefined('firstAppointment should be and empty Cita');
      console.log('><[model/Medico]> "hasOpenAppointmentFlag" should be empty');
      expect(model.hasOpenAppointmentFlag).toBeFalsy('hasOpenAppointmentFlag should be false');

      console.log('><[model/Medico]> "actosMedicos" should be empty');
      expect(model.getActosMedicos()).toBeDefined('actosMedicos should be an empty list as initial value');
    });
    it('Constructor transformations from backend', () => {
      console.log('><[model/Medico]> Constructor transformations from backend');
      let alterModel = appStoreService.directAccessMockResource('singlemedico') as Medico;

      expect(alterModel.id).toBe('MID:100001:CIE:942 314 112', 'should be MID:100001:CIE:942 314 112');
      expect(alterModel.referencia).toBe('CIE:942 314 112', 'should be "CIE:942 314 112"');
      expect(alterModel.tratamiento).toBe('Dra.', 'should be "Dra."');
      expect(alterModel.nombre).toBe('Marta', 'should be "Marta"');
      expect(alterModel.apellidos).toBe('Lastra Olano', 'should be "Lastra Olano"');
      expect(alterModel.especialidad).toBe('Pediatría', 'should be "Pediatría"');
      expect(alterModel.activo).toBeTruthy('activo should be true');
      expect(alterModel.appointmentsCount).toBe(210, 'should be 210');
      expect(alterModel.freeAppointmentsCount).toBe(208, 'should be 208');
      expect(alterModel.firstAppointmentId).toBe(400421, 'should be "Pediatría"');
      expect(model.firstAppointment).toBeDefined('firstAppointment should be and empty Cita');
      expect(model.hasOpenAppointmentFlag).toBeFalsy('hasOpenAppointmentFlag should be false');
      expect(model.getActosMedicos()).toBeDefined('actosMedicos should be an empty list');
      expect(model.getActosMedicos().length).toBe(0, 'actosMedicos should be an empty list');
    });
  });

  // - G E T T E R S / S E T T E R S   P H A S E
  describe('Getters/Setters Phase', () => {
    xit('Identifier: getter', () => {
      console.log('><[model/Medico]> Identifier: getter');
      let alterModel = appStoreService.directAccessMockResource('singlemedico') as Medico;
      expect(alterModel.getServiceIdentifiers()).toBe("MID:100001:CIE:942 314 112", 'should have the value MID:100001:CIE:942 314 112');
    });
    it('Especialidad: getter/setter', () => {
      console.log('><[model/Medico]> Especialidad: getter/setter');
      expect(model.getEspecialidad()).toBe('Médico Familia', 'should be "Médico Familia"');

      let alterModel = appStoreService.directAccessMockResource('singlemedico') as Medico;
      expect(alterModel.getEspecialidad()).toBe('Pediatría', 'should be "Pediatría"');
      alterModel.setEspecialidad('Especialidad de prueba');
      expect(alterModel.getEspecialidad()).toBe('Especialidad de prueba', 'should be "Especialidad de prueba"');
    });
    // it('getMorningList: getter', () => {
    //   console.log('><[model/Medico]> getMorningList: getter');
    //   expect(model.getMorningList()).toBeDefined('should exist');
    //   expect(model.getMorningList().length).toBe(0, 'should be empty');
    // });
    // it('getEveningList: getter', () => {
    //   console.log('><[model/Medico]> getEveningList: getter');
    //   expect(model.getEveningList()).toBeDefined('should exist');
    //   expect(model.getEveningList().length).toBe(0, 'should be empty');
    // });
  });

  // - C O D E   C O V E R A G E   P H A S E
  // describe('Code Coverage Phase [addMorning]', () => {
  //   it('addMorning: adding appointments', () => {
  //     console.log('><[model/Medico]> addMorning: adding appointments');
  //     expect(model.getMorningList()).toBeDefined('should exist');
  //     expect(model.getMorningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addMorning(blockFree);
  //     let list = model.getMorningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');
  //     console.log('><[model/Medico]> addMorning: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(800, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(10, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [addEvening]', () => {
  //   it('addEvening: adding appointments', () => {
  //     console.log('><[model/Medico]> addEvening: adding appointments');
  //     expect(model.getEveningList()).toBeDefined('should exist');
  //     expect(model.getEveningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addEvening(blockFree);
  //     let list = model.getEveningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');
  //     console.log('><[model/Medico]> addEvening: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(1500, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(10, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('TARDE', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [removeMorning]', () => {
  //   it('removeMorning: remove added appointments', () => {
  //     console.log('><[model/Medico]> removeMorning: remove added appointments');
  //     expect(model.getMorningList()).toBeDefined('should exist');
  //     expect(model.getMorningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addMorning(blockFree);
  //     model.addMorning(blockFree);
  //     expect(model.getMorningList().length).toBe(2, 'should be have 2 elements');
  //     let list = model.getMorningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     // Remove the first element and check the other
  //     model.removeMorning(appointment)
  //     expect(model.getMorningList().length).toBe(1, 'should be have 1 element');
  //     list = model.getMorningList();
  //     appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     console.log('><[model/Medico]> removeMorning: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(800, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(10, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [removeEvening]', () => {
  //   it('removeEvening: remove added appointments', () => {
  //     console.log('><[model/Medico]> removeEvening: remove added appointments');
  //     expect(model.getEveningList()).toBeDefined('should exist');
  //     expect(model.getEveningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addEvening(blockFree);
  //     model.addEvening(blockFree);
  //     expect(model.getEveningList().length).toBe(2, 'should be have 2 elements');
  //     let list = model.getEveningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     // Remove the first element and check the other
  //     model.removeEvening(appointment)
  //     expect(model.getEveningList().length).toBe(1, 'should be have 1 element');
  //     list = model.getEveningList();
  //     appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     console.log('><[model/Medico]> removeEvening: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(1500, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(10, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('TARDE', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [addMorningOnTop]', () => {
  //   it('addMorningOnTop: adding appointments', () => {
  //     console.log('><[model/Medico]> addMorningOnTop: adding appointments');
  //     expect(model.getMorningList()).toBeDefined('should exist');
  //     expect(model.getMorningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addMorning(blockFree);
  //     model.addMorning(blockFree);
  //     model.addMorning(blockFree);
  //     let list = model.getMorningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     // Add a new block on top of the first appointment.
  //     let blockOnTop = new TemplateBuildingBlock({
  //       "nombre": "Cita de 30 minutos",
  //       "duracion": 30,
  //       "tipo": "ROJO",
  //       "estado": "LIBRE"
  //     });
  //     model.addMorningOnTop(appointment, blockOnTop);
  //     expect(model.getMorningList().length).toBe(4, 'should be have 4 elements');
  //     list = model.getMorningList();
  //     appointment = list[1];
  //     expect(appointment).toBeDefined('checking the existence for appointment');
  //     console.log('><[model/Medico]> addMorningOnTop: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(810, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(30, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('ROJO', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
  //   });
  // });
  // describe('Code Coverage Phase [addEveningOnTop]', () => {
  //   it('addEveningOnTop: adding appointments', () => {
  //     console.log('><[model/Medico]> addEveningOnTop: adding appointments');
  //     expect(model.getEveningList()).toBeDefined('should exist');
  //     expect(model.getEveningList().length).toBe(0, 'should be empty');
  //     let blockFree = new TemplateBuildingBlock({
  //       "nombre": "Cita de 10 minutos",
  //       "duracion": 10,
  //       "tipo": "TRANSPARENTE",
  //       "estado": "LIBRE"
  //     });
  //     expect(blockFree).toBeDefined('checking the existence for blockFree');
  //     model.addEvening(blockFree);
  //     model.addEvening(blockFree);
  //     model.addEvening(blockFree);
  //     let list = model.getEveningList();
  //     let appointment = list[0];
  //     expect(appointment).toBeDefined('checking the existence for appointment');

  //     // Add a new block on top of the first appointment.
  //     let blockOnTop = new TemplateBuildingBlock({
  //       "nombre": "Cita de 30 minutos",
  //       "duracion": 30,
  //       "tipo": "ROJO",
  //       "estado": "LIBRE"
  //     });
  //     model.addEveningOnTop(appointment, blockOnTop);
  //     expect(model.getEveningList().length).toBe(4, 'should be have 4 elements');
  //     list = model.getEveningList();
  //     appointment = list[1];
  //     expect(appointment).toBeDefined('checking the existence for appointment');
  //     console.log('><[model/Medico]> addEveningOnTop: validating appointment data');
  //     // Check the added appointment data.
  //     expect(appointment.huecoId)
  //       .toBe(1510, 'checking value for appointment.huecoId');
  //     expect(appointment.huecoDuracion)
  //       .toBe(30, 'checking value for appointment.huecoDuracion');
  //     expect(appointment.estado)
  //       .toBe('LIBRE', 'checking value for appointment.estado');
  //     expect(appointment.tipo)
  //       .toBe('ROJO', 'checking value for appointment.tipo');
  //     expect(appointment.zonaHoraria)
  //       .toBe('TARDE', 'checking value for appointment.zonaHoraria');
  //   });
  // });
});