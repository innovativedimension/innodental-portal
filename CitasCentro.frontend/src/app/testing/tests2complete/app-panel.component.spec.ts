//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - MODULES IMPORTS
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StorageServiceModule } from 'angular-webstorage-service';
//import { SimpleNotificationsModule } from 'angular2-notifications';
import { ToastrModule } from 'ng6-toastr-notifications';
import { HttpClientModule } from '@angular/common/http';
// - APPLICATION MODULES
import { UIModule } from '@app/modules/ui/ui.module';
import { SharedModule } from '@app/modules/shared/shared.module';
// - TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
// - ENVIRONMENT
import { environment } from '@env/environment';
// - CORE
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Routes } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// - SERVICES
import { WebStorageService } from 'angular-webstorage-service';
//import { NotificationsService } from 'angular2-notifications';
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// import { TestAppStoreService } from '@app/testing/TestAppStoreService.service';
// import { TestBackendService } from '@app/testing/TestBackendService.service';
// - COMPONENTS
import { MVCViewerComponent } from '@app/modules/ui/mvcviewer/mvcviewer.component';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { EmptyMockAppStoreService } from '@app/testing/services/EmptyMockAppStoreService.service';
import { EmptyMockBackendService } from '@app/testing/services/EmptyMockBackendService.service';

/**
 * This is an empty component to be pointed with valid soutes.
 *
 * @export
 * @class HomeComponent
 */
@Component({
  template: `Home`
})
export class RouteMockUpComponent {
}
export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: RouteMockUpComponent }
];
// --- E N D   O F   R O U T I N G   C O M P O N E N T

xdescribe('AppPanelComponent CORE UI', () => {
  let component: AppPanelComponent;
  let fixture: ComponentFixture<AppPanelComponent>;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes),
        StorageServiceModule,
        // SimpleNotificationsModule.forRoot(),
        ToastrModule.forRoot(),
        HttpClientModule,
        SharedModule
      ],
      providers: [
        // NotificationsService,
        { provide: AppStoreService, useClass: EmptyMockAppStoreService },
        { provide: BackendService, useClass: EmptyMockBackendService }
      ],
      declarations: [AppPanelComponent, RouteMockUpComponent, MVCViewerComponent],
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(AppPanelComponent);
    component = fixture.componentInstance;
  });

  // - C O M P I L A T I O N / C R E A T I O N   T E S T S
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('development should be false when production', () => {
    if (environment.development)
      expect(environment.production).toBeFalsy();
    if (environment.production)
      expect(environment.development).toBeFalsy();
  });
  // - O N I N I T   P H A S E
  it('OnInit phase Input() "title" should be "-PANEL TITLE-"', () => {
    expect(component.title).toBe("-PANEL TITLE-");
  });
  it('OnInit phase Input() "show" should be true', () => {
    expect(component.show).toBeTruthy();
  });
  it('OnInit phase field "canBeClosed" should be false', () => {
    expect(component.canBeClosed).toBeFalsy();
  });
  it('OnInit phase field "development" should be false', () => {
    if (environment.development) expect(component.development).toBeFalsy();
    else expect(component.development).toBeFalsy();
  });
  // - M E T H O D  T E S T I N G
  it('method getTitle() should return "-PANEL TITLE-"', () => {
    expect(component.getTitle()).toBe('-PANEL TITLE-');
  });
  // - I N T E R A C T I O N   T E S T S
  it('method closePanel() should change show state', () => {
    component.closePanel();
    expect(component.show).toBeFalsy();
  });
});
