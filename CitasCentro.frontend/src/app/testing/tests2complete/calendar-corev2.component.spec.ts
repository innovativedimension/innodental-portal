import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarCorev2Component } from '../../modules/core/calendar-corev2/calendar-corev2.component';

// import { CalendarCorev2Component } from './calendar-corev2.component';

xdescribe('CalendarCorev2Component', () => {
  let component: CalendarCorev2Component;
  let fixture: ComponentFixture<CalendarCorev2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarCorev2Component]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarCorev2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
