import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CalendarScreenSizeAdapter } from '../../modules/citaciones/citaciones-page/panels/calendar-panel-v2/calendar-screensize-adapter.component';
// import { CalendarCorev2Component } from '../../modules/core/calendar-corev2/calendar-corev2.component';

// import { CalendarCorev2Component } from './calendar-corev2.component';

xdescribe('CalendarCorev2Component', () => {
  let component: CalendarScreenSizeAdapter;
  let fixture: ComponentFixture<CalendarScreenSizeAdapter>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarScreenSizeAdapter]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarScreenSizeAdapter);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
