//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Location } from "@angular/common";
// --- TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
import { fakeAsync } from '@angular/core/testing';
import { tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- MODULES IMPORTS
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { StorageServiceModule, WebStorageService } from 'angular-webstorage-service';
//import { SimpleNotificationsModule } from 'angular2-notifications';
import { ToastrModule, ToastrManager } from 'ng6-toastr-notifications';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CalendarModule } from 'angular-calendar';
import { IonicModule } from 'ionic-angular';
// --- APPLICATION MODULES
import { UIModule } from '@app/modules/ui/ui.module';
import { SharedModule } from '@app/modules/shared/shared.module';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { StubService } from '@app/testing/services/StubService.service';
import { EmptyMockAppStoreService } from '@app/testing/services/EmptyMockAppStoreService.service';
// import { EmptyMockBackendService } from '@app/testing/services/StubService.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- COMPONENTS
import { MVCViewerComponent } from '@app/modules/ui/mvcviewer/mvcviewer.component';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { LoginPanelComponent } from '@app/modules/login/panels/login-panel/login-panel.component';
import { TemplateConstructorPageComponent } from '@app/modules/gestion-servicio/template-constructor-page/template-constructor-page.component';
import { MockTemplateControllerService } from '@app/testing/services/MockTemplateController.service';
import { CalendarDefinitionPageComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/definicion-calendario-page.component';
// --- MODELS
import { Perfil } from '@app/models/Perfil.model';
import { Medico } from '@models/Medico.model';
import { CalendarFreeSlotsPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/calendar-free-slots-panel/calendar-free-slots-panel.component';
import { AppointmentListPanelComponent } from '@app/modules/gestion-servicio/gestion-calendario-page/panels/appointment-list-panel/appointment-list-panel.component';
import { MonthCalendarPanelComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/panels/month-calendar-panel/month-calendar-panel.component';

describe('PAGE CalendarDefinitionPageComponent [Module: GESTION-SERVICIO]', () => {
  let component: CalendarDefinitionPageComponent;
  let fixture: ComponentFixture<CalendarDefinitionPageComponent>;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        StorageServiceModule,
        RouterTestingModule.withRoutes(routes),
        CalendarModule.forRoot(),
      ],
      providers: [
        { provide: ToastrManager, useClass: EmptyMockAppStoreService },
        // { provide: NotificationsService, useClass: EmptyMockAppStoreService },
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: BackendService, useClass: StubService }
      ],
      declarations: [CalendarDefinitionPageComponent, RouteMockUpComponent, 
        MonthCalendarPanelComponent,
        AppointmentListPanelComponent]
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(CalendarDefinitionPageComponent);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
  });

  // - R E N D E R I N G   P H A S E
  describe('Rendering Phase', () => {
    it('Rendering Phase: some panels should be visible', () => {
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> Rendering Phase: some panels should be visible');
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "activateSelection" should be true');
      expect(component.activateSelection).toBeTruthy();
      let testPanel = fixture.debugElement.query(By.css('#gs-header-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "gs-header-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "ui-menu-bar"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#gs-service-select-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "gs-service-select-panel"');
      expect(testPanel).toBeDefined();
    });
    it('Rendering Phase: all panels should be visible', () => {
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> Rendering Phase: all panels should be visible');
      // Activate selection be setting a service
      let credential: Perfil = new Perfil();
      credential.allservices = true;
      let service: Medico = new Medico();
      appStoreService.storeCredential(credential);
      appStoreService.storeActiveService(service);

      let testcred = appStoreService.accessCredential();
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> credential: '
        + JSON.stringify(testcred));

      component.ngOnInit();
      expect(component.activateSelection).toBeFalsy('should be false');
      // Check again the list of panels visible
      let testPanel = fixture.debugElement.query(By.css('#gs-header-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "gs-header-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "ui-menu-bar"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#gs-service-select-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "gs-service-select-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#dc-toolbar-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "dc-toolbar-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#dc-month-calendar-panel'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "dc-month-calendar-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#dc-template-list'));
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> "dc-template-list"');
      expect(testPanel).toBeDefined();
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  xdescribe('Code Coverage Phase', () => {
    it('onUpdateContents: sending messages to child panels', () => {
      // Activate selection be setting a service
      let credential: Perfil = new Perfil();
      credential.allservices = true;
      let service: Medico = new Medico();
      appStoreService.storeCredential(credential);
      appStoreService.storeActiveService(service);

      // COVERAGE. Children components need mocks ups to test the code. No need 
      component.ngOnInit();
      component.onUpdateContents(null);
      component.onSelectedDate(new Date());
      expect(true).toBeTruthy();
    });
  });
  describe('Code Coverage Phase [onUpdateContents]', () => {
    it('onUpdateContents: sending messages to child panels', () => {
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> onUpdateContents: sending messages to child panels');
      // Activate selection be setting a service
      let credential: Perfil = new Perfil();
      credential.allservices = true;
      let service: Medico = new Medico();
      appStoreService.storeCredential(credential);
      appStoreService.storeActiveService(service);

      const spy = spyOn(component, 'onUpdateContents');
      component.onUpdateContents(null);
      expect(spy).toHaveBeenCalled();
    });
  });
  describe('Code Coverage Phase [onSelectedDate]', () => {
    it('onSelectedDate: setting the selected date', () => {
      console.log('><[gestion-servicio/CalendarDefinitionPageComponent]> onSelectedDate: setting the selected date');
      // Activate selection be setting a service
      let credential: Perfil = new Perfil();
      credential.allservices = true;
      let service: Medico = new Medico();
      appStoreService.storeCredential(credential);
      appStoreService.storeActiveService(service);

      const spy = spyOn(component, 'onSelectedDate');
      component.onSelectedDate(new Date());
      expect(spy).toHaveBeenCalled();
    });
  });
});