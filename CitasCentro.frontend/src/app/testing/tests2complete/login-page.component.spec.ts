//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
// import { async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- COMPONENTS
import { LoginPageComponent } from '@app/modules/login/pages/login-page/login-page.component';

xdescribe('LoginPageComponent [Module: LOGIN]', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [LoginPageComponent],
    })
      .compileComponents();
    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[login/LoginPageComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
  });

  // - R E N D E R I N G   P H A S E
  describe('Rendering Phase', () => {
    it('Rendering Phase: some panels should be visible', () => {
      console.log('><[login/LoginPageComponent]> Rendering Phase: some panels should be visible');
      let testPanel = fixture.debugElement.query(By.css('#cc-modules-panel'));
      console.log('><[login/LoginPageComponent]> "cc-modules-panel"');
      expect(testPanel).toBeDefined();
      testPanel = fixture.debugElement.query(By.css('#cc-login-panel'));
      console.log('><[login/LoginPageComponent]> "cc-login-panel"');
      expect(testPanel).toBeDefined();
    });
  });

  // - M E T H O D   C H E C K S
  describe('Method Testing Phase', () => {
    it('expected application name is "CitasCentro"', () => {
      expect(component.getAppName()).toBe("CitasCentro");
    });
    xit('expected version name is ' + environment.appVersion, () => {
      if (environment.production)
        expect(component.getAppVersion()).toBe("0.8.3");
      if (environment.development)
        expect(component.getAppVersion()).toBe("0.8.3 dev");
    });
  });
});
