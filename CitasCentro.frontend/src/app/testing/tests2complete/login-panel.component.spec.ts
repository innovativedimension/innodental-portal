//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { Component } from '@angular/core';
import { Routes } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { DebugElement } from '@angular/core';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- MODULES IMPORTS
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { StorageServiceModule } from 'angular-webstorage-service';
// //import { SimpleNotificationsModule } from 'angular2-notifications';
import { ToastrModule } from 'ng6-toastr-notifications';
import { HttpClientModule } from '@angular/common/http';
// --- APPLICATION MODULES
import { SharedModule } from '@app/modules/shared/shared.module';
// --- SERVICES
//import { NotificationsService } from 'angular2-notifications';
// import { TestAppStoreService } from '@app/testing/TestAppStoreService.service';
// import { TestBackendService } from '@app/testing/TestBackendService.service';
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// --- COMPONENTS
import { MVCViewerComponent } from '@app/modules/ui/mvcviewer/mvcviewer.component';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { LoginPanelComponent } from '@app/modules/login/panels/login-panel/login-panel.component';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';

xdescribe('LoginPanelComponent [Module: LOGIN]', () => {
  let component: LoginPanelComponent;
  let fixture: ComponentFixture<LoginPanelComponent>;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule, FormsModule,
        RouterTestingModule.withRoutes(routes),
        StorageServiceModule,
        // SimpleNotificationsModule.forRoot(),
        ToastrModule.forRoot(),
        HttpClientModule,
        SharedModule
      ],
      declarations: [LoginPanelComponent, RouteMockUpComponent],
      providers: [
        // NotificationsService,
        { provide: AppStoreService, useClass: MockAppStoreService },
        // { provide: BackendService, useClass: TestBackendService }
      ]
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(LoginPanelComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[login/LoginPanelComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[login/LoginPanelComponent]> "credentialForm" should be undefined');
      expect(component.credentialForm).toBeUndefined();
      console.log('><[login/LoginPanelComponent]> "signalError" should be false');
      expect(component.signalError).toBeFalsy();
    });
  });


  // - C O M P I L A T I O N / C R E A T I O N   T E S T S
  // it('should be created', () => {
  //   expect(component).toBeTruthy();
  // });
  // - O N I N I T   P H A S E
  // it('OnInit phase field "credentialForm" should exist', () => {
  //   expect(component.credentialForm).toBeDefined();
  // });
  // it('OnInit phase field "signalError" should be false', () => {
  //   expect(component.signalError).toBeFalsy();
  // });
  it('OnInit phase should show 0 alert messages', () => {
    let identificador = fixture.debugElement.query(By.css('#identificador'));
    expect(identificador).toBeDefined();
    let alertMessages = fixture.debugElement.queryAll(By.css('.alert-danger div'));
    // Check initial alert messages on page entry.
    expect(alertMessages.length).toEqual(0);
  });
  it('OnInit phase form should be invalid when empty', () => {
    expect(component.credentialForm.valid).toBeFalsy();
  });
  // it('OnInit phase form fields should be empty', () => {
  //   let identificador = component.credentialForm.controls['identificador'];
  //   let password = component.credentialForm.controls['password'];
  //   console.log('[LoginPanelComponent]> identificador: ' + identificador.value);
  //   console.log('[LoginPanelComponent]> password: ' + password.value);
  //   expect(identificador.value).toBeUndefined();
  //   expect(password.value).toBeUndefined();
  // });
  it('OnInit phase identificador error list should have 2 elements', () => {
    let identificador = component.credentialForm.controls['identificador'];
    let errors = identificador.errors || {};
    expect(identificador.errors).toBeDefined();
    console.log('[LoginPanelComponent]> identificador.errors: ' + JSON.stringify(errors));
    expect(errors['required']).toBeTruthy();
    expect(errors['validNIF']).toBeTruthy();
  });
  it('OnInit phase password error list should have 1 elements', () => {
    let password = component.credentialForm.controls['password'];
    let errors = password.errors || {};
    expect(password.errors).toBeDefined();
    console.log('[LoginPanelComponent]> password.errors: ' + JSON.stringify(errors));
    expect(errors['required']).toBeTruthy();
  });
  // - F O R M   I N T E R A C T I O N
  it('Form validation entering partial NIF should have two errors', () => {
    let identificador = component.credentialForm.controls['identificador'];
    identificador.setValue("85456");
    let errors = identificador.errors || {};
    expect(identificador.errors).toBeDefined();
    console.log('[LoginPanelComponent]> identificador.errors: ' + JSON.stringify(errors));
    expect(errors['minlength']).toBeTruthy();
    expect(errors['validNIF']).toBeTruthy();
  });
  it('Form validation entering invalid letter NIF should have one errors', () => {
    let identificador = component.credentialForm.controls['identificador'];
    identificador.setValue("85456123w");
    let errors = identificador.errors || {};
    expect(identificador.errors).toBeDefined();
    console.log('[LoginPanelComponent]> identificador.errors: ' + JSON.stringify(errors));
    expect(errors['validNIF']).toBeTruthy();
  });
  it('Form validation entering valid NIF should have no errors', () => {
    let identificador = component.credentialForm.controls['identificador'];
    identificador.setValue("85456123Z");
    let errors = identificador.errors || {};
    expect(identificador.errors).toBeNull();
  });
  it('Form validation entering short password should show one error', () => {
    let password = component.credentialForm.controls['password'];
    password.setValue("85");
    let errors = password.errors || {};
    expect(password.errors).toBeDefined();
    console.log('[LoginPanelComponent]> identificador.errors: ' + JSON.stringify(errors));
    expect(errors['minlength']).toBeTruthy();
  });
  it('Form validation entering valid password should show no errors', () => {
    let password = component.credentialForm.controls['password'];
    password.setValue("85567");
    let errors = password.errors || {};
    expect(password.errors).toBeNull();
  });
  it('Form validation entering invalid data should disable button', () => {
    let identificador = component.credentialForm.controls['identificador'];
    let password = component.credentialForm.controls['password'];
    let button = fixture.debugElement.query(By.css('#login-button'));
    identificador.setValue("854561");
    password.setValue("85");
    console.log('[LoginPanelComponent]> form state: ' + component.credentialForm.valid);
    expect(component.credentialForm.valid).toBeFalsy();
    console.log('[LoginPanelComponent]> button: ' + JSON.stringify(button.nativeElement));
    // expect(component.credentialForm.valid).toBeFalsy();
  });
  // - F O R M   S U B M I S I O N
  it('Form submision an invalid credential', () => {
    let identificador = component.credentialForm.controls['identificador'];
    identificador.setValue("85456123Z");
    let password = component.credentialForm.controls['password'];
    password.setValue("1234");
    component.onSubmitLogin();

    // expect(password.errors).toBeNull();
  });
});

