//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
// import { Component } from '@angular/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
// import { TestAppStoreService } from '@app/testing/TestAppStoreService.service';
import { BackendService } from '@app/services/backend.service';
// import { MockBackendService } from '@app/testing/MockBackendService.service';
// --- COMPONENTS
import { MVCViewerComponent } from '@app/modules/ui/mvcviewer/mvcviewer.component';
// --- INTERFACES
import { INode } from '@interfaces/core/INode.interface';
import { EmptyMockAppStoreService } from '@app/testing/services/EmptyMockAppStoreService.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';

class TestingNode implements INode {
  public jsonClass: string = "TestingNode";
  public target: string = 'untargeted';

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    Object.assign(this, values);
    this.jsonClass = "Node";
  }

  //--- I N O D E   I N T E R F A C E
  public getJsonClass(): string {
    return this.jsonClass;
  }

  //--- I C O L L A B O R A T I O N   I N T E R F A C E
  /**
   * This is the special case for the Node that even being expandable it will not show any internal data because it does not ahve any field to store children.
   * @param  _variant [description]
   * @return          [description]
   */
  public collaborate2View(_variant?: string): INode[] {
    let collab: INode[] = [];
    collab.push(this);
    return collab;
  }
}
// - M V C V I W E R   T E S T S . V 1
/**
 * Tests for the abstract component that defines and manages the Model-View-Controller for page node rendering. This class is used on most of the panels and pages acrosss the application and it is based on the IContainerController Interface.
 * Test all the core model management and the definicions of the component dependencies.
 */
describe('MVCViewerComponent [Module: UI]', () => {
  let component: MVCViewerComponent;
  let fixture: ComponentFixture<MVCViewerComponent>;
  let appStoreService: MockAppStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      // imports: [
      //   HttpClientModule,
      //   HttpClientTestingModule,
      // ],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService }
        // { provide: BackendService, useClass: MockBackendService },
      ],
      declarations: [MVCViewerComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(MVCViewerComponent);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('ui/MVCViewerComponent Construction Phase', () => {
    it('should be created', () => {
      console.log('><[ui/MVCViewerComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[ui/MVCViewerComponent]> "container" should be undefined');
      expect(component.container).toBeUndefined();
      console.log('><[ui/MVCViewerComponent]> "variant" should be "-DEFAULT-"');
      expect(component.variant).toBe('-DEFAULT-');
      console.log('><[ui/MVCViewerComponent]> "self" should be the component');
      expect(component.self).toBeDefined();
      expect(component.self).toBe(component);
      console.log('><[ui/MVCViewerComponent]> "selectedNode" should be undefined');
      let protectedComponent = component as any;
      expect(protectedComponent.selectedNode).toBeUndefined()
      console.log('><[ui/MVCViewerComponent]> "downloading" should be false');
      expect(component.downloading).toBeFalsy();
      console.log('><[ui/MVCViewerComponent]> "dataModelRoot" should be defined empty');
      expect(component.dataModelRoot).toBeDefined();
      expect(component.dataModelRoot.length).toBe(0)
      console.log('><[ui/MVCViewerComponent]> "renderNodeList" should be defined empty');
      expect(component.renderNodeList).toBeDefined();
      expect(component.renderNodeList.length).toBe(0)
    });
  });

  // - I N P U T / O U T P U T   P H A S E
  describe('ui/MVCViewerComponent Input/Output Phase', () => {
    let hostComponent: MVCViewerComponent;

    beforeEach(() => {
      hostComponent = TestBed.createComponent(MVCViewerComponent).componentInstance;
    });

    // - M E T H O D S   T E S T S
    it('Inputs receivers: input compliance', () => {
      console.log('><[ui/MVCViewerComponent]> Inputs receivers: input compliance');
      component.variant = 'VARIANT PRUEBA';
      expect(component.getVariant()).toBe('VARIANT PRUEBA');
      // Use a dummy panel to host the Render element.
      expect(hostComponent).toBeDefined();
      component.container = component;
      expect(component.getContainer()).toBeDefined();
    });
  });

  // - L I F E C Y C L E   P H A S E
  describe('ui/MVCViewerComponent Lifecycle Phase', () => {
    it('Lifecycle: OnInit', () => {
      console.log('><[ui/MVCViewerComponent]> Lifecycle: OnInit');
      console.log('><[ui/MVCViewerComponent]> "dataModelRoot" should be defined empty');
      expect(component.dataModelRoot).toBeDefined();
      expect(component.dataModelRoot.length).toBe(0)
      console.log('><[ui/MVCViewerComponent]> "renderNodeList" should be defined empty');
      expect(component.renderNodeList).toBeDefined();
      expect(component.renderNodeList.length).toBe(0)
      // Add elements to the 'dataModelRoot' and make sure are deleted on the initization.
      // Other data should not be touched.
      component.dataModelRoot.push(new TestingNode({ target: 'dataModelRoot' }));
      component.renderNodeList.push(new TestingNode({ target: 'renderNodeList' }));
      console.log('><[ui/MVCViewerComponent]> "dataModelRoot" should be have 1 item');
      expect(component.dataModelRoot.length).toBe(1)
      console.log('><[ui/MVCViewerComponent]> "renderNodeList" should be have 1 item');
      expect(component.renderNodeList.length).toBe(1)
      component.ngOnInit();
      console.log('><[ui/MVCViewerComponent]> "dataModelRoot" should be be empty');
      expect(component.dataModelRoot.length).toBe(0)
      console.log('><[ui/MVCViewerComponent]> "renderNodeList" should be have 1 item');
      expect(component.renderNodeList.length).toBe(1)
    });
  });

  // - G E T T E R S / S E T T E R S   P H A S E
  describe('ui/MVCViewerComponent Getters/Setters Phase', () => {
    it('Getters/Setters: Variant', () => {
      console.log('><[ui/MVCViewerComponent]> Getters/Setters: Variant');
      expect(component.variant).toBe('-DEFAULT-');
      component.setVariant('TEST-VARIANT');
      expect(component.variant).toBe('TEST-VARIANT');
      expect(component.getVariant()).toBe('TEST-VARIANT');
    });
  });

  // - F U N C T I O N A L I T Y   P H A S E
  describe('ui/MVCViewerComponent Functionality Phase', () => {
    it('Model -> Render transformation', () => {
      console.log('><[ui/MVCViewerComponent]> Model -> Render transformation');
      component.ngOnInit();
      // Add test data to the model list and transform into a render list.
      // Use the list of modules.
      let moduleList = appStoreService.directAccessMockResource('modulos')
      // .subscribe((moduleList) => {
      for (let module of moduleList) {
        component.dataModelRoot.push(module);
      }
      console.log('><[ui/MVCViewerComponent]> Checking test data values');
      expect(moduleList.length).toBe(3);
      expect(component.dataModelRoot.length).toBe(3);
      expect(component.renderNodeList.length).toBe(0);
      console.log('><[ui/MVCViewerComponent]> Checking transformation');
      component.notifyDataChanged();
      expect(component.dataModelRoot.length).toBe(3);
      expect(component.renderNodeList.length).toBe(3);
      // });
      expect(component.variant).toBe('-DEFAULT-');
      component.setVariant('TEST-VARIANT');
      expect(component.variant).toBe('TEST-VARIANT');
      expect(component.getVariant()).toBe('TEST-VARIANT');
    });
  });

  // - A D D I T I O N A L   F U N C T I O N A L I T Y   P H A S E
  describe('ui/MVCViewerComponent Additional Functionality Phase', () => {
    it('Date management', () => {
      console.log('><[ui/MVCViewerComponent]> Date management');
      let baseData = new Date('2018-10-15T01:24:00.000Z');
      console.log('><[ui/MVCViewerComponent]> basedate: ' + JSON.stringify(baseData));
      expect(baseData.getFullYear()).toBe(2018);
      expect(baseData.getMonth()).toBe(9);
      expect(baseData.getDate()).toBe(15);
      // expect(baseData.getHours()).toBe(1);
      expect(baseData.getMinutes()).toBe(24);
      expect(baseData.getSeconds()).toBe(0);
      expect(baseData.getMilliseconds()).toBe(0);
      console.log('><[ui/MVCViewerComponent]> Date management: checking year addition');
      let endDate = component.dateAdd(baseData, 'year', 1);
      expect(endDate.getFullYear()).toBe(2019);
      expect(endDate.getMonth()).toBe(9);
      expect(endDate.getDate()).toBe(15);
      // expect(baseData.getHours()).toBe(1);
      expect(endDate.getMinutes()).toBe(24);
      expect(endDate.getSeconds()).toBe(0);
      expect(endDate.getMilliseconds()).toBe(0);
      endDate = component.dateAdd(baseData, 'month', 1);
      expect(endDate.getFullYear()).toBe(2018);
      expect(endDate.getMonth()).toBe(10);
      expect(endDate.getDate()).toBe(15);
      // expect(endDate.getHours()).toBe(1);
      expect(endDate.getMinutes()).toBe(24);
      expect(endDate.getSeconds()).toBe(0);
      expect(endDate.getMilliseconds()).toBe(0);
      endDate = component.dateAdd(baseData, 'quarter', 1);
      expect(endDate.getFullYear()).toBe(2019);
      expect(endDate.getMonth()).toBe(0);
      expect(endDate.getDate()).toBe(15);
      // expect(endDate.getHours()).toBe(1);
      expect(endDate.getMinutes()).toBe(24);
      expect(endDate.getSeconds()).toBe(0);
      expect(endDate.getMilliseconds()).toBe(0);
      endDate = component.dateAdd(baseData, 'week', 1);
      expect(endDate.getFullYear()).toBe(2018);
      expect(endDate.getMonth()).toBe(9);
      expect(endDate.getDate()).toBe(22);
      // expect(endDate.getHours()).toBe(1);
      expect(endDate.getMinutes()).toBe(24);
      expect(endDate.getSeconds()).toBe(0);
      expect(endDate.getMilliseconds()).toBe(0);
      endDate = component.dateAdd(baseData, 'day', 1);
      expect(endDate.getFullYear()).toBe(2018);
      expect(endDate.getMonth()).toBe(9);
      expect(endDate.getDate()).toBe(16);
      // expect(endDate.getHours()).toBe(1);
      expect(endDate.getMinutes()).toBe(24);
      expect(endDate.getSeconds()).toBe(0);
      expect(endDate.getMilliseconds()).toBe(0);
      endDate = component.dateAdd(baseData, 'hour', 1);
      expect(endDate.getFullYear()).toBe(2018);
      expect(endDate.getMonth()).toBe(9);
      expect(endDate.getDate()).toBe(15);
      expect(endDate.getMinutes()).toBe(24);
      expect(endDate.getSeconds()).toBe(0);
      expect(endDate.getMilliseconds()).toBe(0);
      endDate = component.dateAdd(baseData, 'minute', 1);
      expect(endDate.getFullYear()).toBe(2018);
      expect(endDate.getMonth()).toBe(9);
      expect(endDate.getDate()).toBe(15);
      expect(endDate.getMinutes()).toBe(25);
      expect(endDate.getSeconds()).toBe(0);
      expect(endDate.getMilliseconds()).toBe(0);
      endDate = component.dateAdd(baseData, 'second', 1);
      expect(endDate.getFullYear()).toBe(2018);
      expect(endDate.getMonth()).toBe(9);
      expect(endDate.getDate()).toBe(15);
      expect(endDate.getMinutes()).toBe(24);
      expect(endDate.getSeconds()).toBe(1);
      expect(endDate.getMilliseconds()).toBe(0);
    });
    it('Date conversion', () => {
      console.log('><[ui/MVCViewerComponent]> Date conversion');
      let baseData = new Date('2018-10-15T01:24:00.000Z');
      expect(baseData.getFullYear()).toBe(2018);
      expect(baseData.getMonth()).toBe(9);
      expect(baseData.getDate()).toBe(15);
      // expect(baseData.getHours()).toBe(1);
      expect(baseData.getMinutes()).toBe(24);
      expect(baseData.getSeconds()).toBe(0);
      expect(baseData.getMilliseconds()).toBe(0);
      console.log('><[ui/MVCViewerComponent]> Date conversion ' + component.date2BasicISO(baseData));
      expect(component.date2BasicISO(baseData)).toBe('20181015')
      baseData = new Date('2018-02-15T01:24:00.000Z');
      console.log('><[ui/MVCViewerComponent]> Date conversion ' + component.date2BasicISO(baseData));
      expect(component.date2BasicISO(baseData)).toBe('20180215')
      baseData = new Date('2018-02-05T01:24:00.000Z');
      console.log('><[ui/MVCViewerComponent]> Date conversion ' + component.date2BasicISO(baseData));
      expect(component.date2BasicISO(baseData)).toBe('20180205')
    });
  });
});
