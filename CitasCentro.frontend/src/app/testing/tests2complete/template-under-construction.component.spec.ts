//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouteMockUpComponent } from '@app/testing/RouteMockUp.component';
import { routes } from '@app/testing/RouteMockUp.component';
// --- MODULES IMPORTS
import { FormsModule } from '@angular/forms';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { StubService } from '@app/testing/services/StubService.service';
import { MockAppStoreService } from '@app/testing/services/MockAppStoreService.service';
// --- COMPONENTS
import { TemplateUnderConstructionComponent } from '@app/modules/gestion-servicio/template-constructor-page/panels/template-under-construction/template-under-construction.component';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';
import { CitasTemplate } from '@models/CitasTemplate.model';

xdescribe('PANEL TemplateUnderConstructionComponent [Module: GESTION-SERVICIO]', () => {
  let component: TemplateUnderConstructionComponent;
  let fixture: ComponentFixture<TemplateUnderConstructionComponent>;
  let appStoreService: MockAppStoreService;
  let templateOnControl: CitasTemplate;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: BackendService, useClass: StubService }
      ],
      declarations: [TemplateUnderConstructionComponent, RouteMockUpComponent],
    })
      .compileComponents();
    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(TemplateUnderConstructionComponent);
    component = fixture.componentInstance;
    appStoreService = TestBed.get(AppStoreService);
    templateOnControl = component.templateUnderControl;
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> "templateUnderControl" should exist');
      expect(component.templateUnderControl).toBeDefined();
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> "savePending" should be false');
      expect(component.savePending).toBeFalsy();
      // console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> "saveCompleted" should be false');
      // expect(component.saveCompleted).toBeFalsy();
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> "show" should be true');
      let componentAny = component as any;
      expect(componentAny.show).toBeTruthy();
    });
    it('Field title can have states', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> "title" should exist');
      expect(component.templateUnderControl).toBeDefined();
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> "savePending" should be false');
      expect(component.savePending).toBeFalsy();
      // console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> "saveCompleted" should be false');
      // expect(component.saveCompleted).toBeFalsy();
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> "show" should be true');
      let componentAny = component as any;
      expect(componentAny.show).toBeTruthy();
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code Coverage Phase [onBlockDrop]', () => {
    it('onBlockDrop: dropping a null content', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: dropping a null content');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: calling function');
      component.onBlockDrop(null, '-EVENING-');
      templateOnControl = component.templateUnderControl;
      let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      expect(templateOnControl.tmhoraInicio)
        .toBeUndefined('validating tmhoraInicio to be undefined');
      let morningList = templateOnControl.getMorningList();
      expect(morningList)
        .toBeDefined('checking existence for morningList');
      expect(morningList.length)
        .toBe(0, 'size of morning list to be 0');
    });
    it('onBlockDrop: adding appointments to the Morning list', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: adding appointments to the Morning list');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      let blockBreak = new TemplateBuildingBlock({
        "nombre": "Descanso 5 minutos",
        "duracion": 5,
        "tipo": "TRANSPARENTE",
        "estado": "DESCANSO"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      expect(blockBreak).toBeDefined('checking the existence for blockBreak');

      // Inserting first appointment. Free standard.
      let event = {
        dragData: blockFree
      };
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: calling function');
      component.onBlockDrop(event, '-MORNING-');
      templateOnControl = component.templateUnderControl;
      let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> templateOnControl: '
        + JSON.stringify(templateOnControl));
      expect(templateOnControl.tmhoraInicio)
        .toBe('08:00', 'validating tmhoraInicio');
      let morningList = templateOnControl.getMorningList();
      expect(morningList)
        .toBeDefined('checking existence for morningList');
      expect(morningList.length)
        .toBe(1, 'size of morning list to be 1');
      let appointment = morningList[0];
      expect(appointment)
        .toBeDefined('checking existence for appointment');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(800, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(10, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending');

      // Inserting second appointment. Break
      event = {
        dragData: blockBreak
      };
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: calling function');
      component.onBlockDrop(event, '-MORNING-');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> templateOnControl: '
        + JSON.stringify(templateOnControl));
      expect(templateOnControl.tmhoraInicio)
        .toBe('08:00', 'validating tmhoraInicio');
      morningList = templateOnControl.getMorningList();
      expect(morningList)
        .toBeDefined('checking existence for morningList');
      expect(morningList.length)
        .toBe(2, 'size of morning list to be 2');
      appointment = morningList[1];
      expect(appointment)
        .toBeDefined('checking existence for appointment');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(810, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(5, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('DESCANSO', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending');
    });
    it('onBlockDrop: adding appointments to the Evening list', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: adding appointments to the Evening list');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      let blockBreak = new TemplateBuildingBlock({
        "nombre": "Descanso 5 minutos",
        "duracion": 5,
        "tipo": "TRANSPARENTE",
        "estado": "DESCANSO"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      expect(blockBreak).toBeDefined('checking the existence for blockBreak');

      // Inserting first appointment. Free standard.
      let event = {
        dragData: blockBreak
      };
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: calling function');
      component.onBlockDrop(event, '-EVENING-');
      templateOnControl = component.templateUnderControl;
      let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> templateOnControl: '
        + JSON.stringify(templateOnControl));
      expect(templateOnControl.tthoraInicio)
        .toBe('15:00', 'validating tmhoraInicio');
      let eveningList = templateOnControl.getEveningList();
      expect(eveningList)
        .toBeDefined('checking existence for eveningList');
      expect(eveningList.length)
        .toBe(1, 'size of evening list to be 1');
      let appointment = eveningList[0];
      expect(appointment)
        .toBeDefined('checking existence for appointment');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(1500, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(5, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('DESCANSO', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('TARDE', 'checking value for appointment.zonaHoraria');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending');

      // Inserting second appointment. Break
      event = {
        dragData: blockFree
      };
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: calling function');
      component.onBlockDrop(event, '-EVENING-');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> templateOnControl: '
        + JSON.stringify(templateOnControl));
      expect(templateOnControl.tmhoraInicio)
        .toBe('08:00', 'validating tmhoraInicio');
      eveningList = templateOnControl.getEveningList();
      expect(eveningList)
        .toBeDefined('checking existence for eveningList');
      expect(eveningList.length)
        .toBe(2, 'size of morning list to be 2');
      appointment = eveningList[1];
      expect(appointment)
        .toBeDefined('checking existence for appointment');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(1505, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(10, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('TARDE', 'checking value for appointment.zonaHoraria');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending');
    });
    it('onBlockDrop: adding name to the template', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: adding name to the template');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');

      // Inserting appointment after name. Should set the save pending to true
      let event = {
        dragData: blockFree
      };
      templateOnControl = component.templateUnderControl;
      let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      // Inserting but not setting for save.
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: calling function');
      component.onBlockDrop(event, '-EVENING-');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending to be false');

      templateOnControl.nombre = 'Nombre de Prueba';
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: calling function');
      component.onBlockDrop(event, '-EVENING-');
      expect(component.savePending)
        .toBeTruthy('checking value for component.savePending to be true');
    });
  });
  describe('Code Coverage Phase [onBlockDropOver]', () => {
    it('onBlockDropOver: dropping a null content', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: dropping a null content');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: calling function');
      // Add the appointment where to drop.
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');

      // Inserting appointment after name. Should set the save pending to true
      let event = {
        dragData: blockFree
      };
      templateOnControl = component.templateUnderControl;
      let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      // Inserting but not setting for save.
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDrop: calling function');
      component.onBlockDrop(event, '-EVENING-');
      let eveningList = templateOnControl.getEveningList();
      expect(eveningList)
        .toBeDefined('checking existence for eveningList');
      expect(eveningList.length)
        .toBe(1, 'size of evening list to be 1');
      let appointment2Drop = eveningList[0];
      expect(appointment2Drop)
        .toBeDefined('checking existence for appointment');

      // Drop another block on top of this new appointment
      component.onBlockDropOver(null, appointment2Drop, '-EVENING-');
      templateOnControl = component.templateUnderControl;
      // let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      expect(templateOnControl.tmhoraInicio)
        .toBe('08:00', 'validating tmhoraInicio to be undefined');
      expect(templateOnControl.tthoraInicio)
        .toBe('15:00', 'validating tthoraInicio to be undefined');
      eveningList = templateOnControl.getEveningList();
      expect(eveningList)
        .toBeDefined('checking existence for eveningList');
      expect(eveningList.length)
        .toBe(1, 'size of evening list to be 1');
    });
    it('onBlockDropOver: adding appointments to the Morning list', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: adding appointments to the Morning list');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      let blockBreak = new TemplateBuildingBlock({
        "nombre": "Descanso 5 minutos",
        "duracion": 5,
        "tipo": "TRANSPARENTE",
        "estado": "DESCANSO"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      expect(blockBreak).toBeDefined('checking the existence for blockBreak');

      // Inserting first appointment. Free standard.
      let event = {
        dragData: blockFree
      };
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: calling function');
      component.onBlockDrop(event, '-MORNING-');
      templateOnControl = component.templateUnderControl;
      let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> templateOnControl: '
        + JSON.stringify(templateOnControl));
      expect(templateOnControl.tmhoraInicio)
        .toBe('08:00', 'validating tmhoraInicio');
      let morningList = templateOnControl.getMorningList();
      expect(morningList)
        .toBeDefined('checking existence for morningList');
      expect(morningList.length)
        .toBe(1, 'size of morning list to be 1');
      let appointment = morningList[0];
      expect(appointment)
        .toBeDefined('checking existence for appointment');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(800, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(10, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending');

      // Inserting second appointment. Break
      event = {
        dragData: blockBreak
      };
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: calling function');
      component.onBlockDropOver(event, appointment, '-MORNING-');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> templateOnControl: '
        + JSON.stringify(templateOnControl));
      expect(templateOnControl.tmhoraInicio)
        .toBe('08:00', 'validating tmhoraInicio');
      morningList = templateOnControl.getMorningList();
      expect(morningList)
        .toBeDefined('checking existence for morningList');
      expect(morningList.length)
        .toBe(2, 'size of morning list to be 2');
      appointment = morningList[1];
      expect(appointment)
        .toBeDefined('checking existence for appointment');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(810, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(5, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('DESCANSO', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('MAÑANA', 'checking value for appointment.zonaHoraria');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending');
    });
    it('onBlockDropOver: adding appointments to the Evening list', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: adding appointments to the Evening list');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      let blockBreak = new TemplateBuildingBlock({
        "nombre": "Descanso 5 minutos",
        "duracion": 5,
        "tipo": "TRANSPARENTE",
        "estado": "DESCANSO"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      expect(blockBreak).toBeDefined('checking the existence for blockBreak');

      // Inserting first appointment. Free standard.
      let event = {
        dragData: blockBreak
      };
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: calling function');
      component.onBlockDrop(event, '-EVENING-');
      templateOnControl = component.templateUnderControl;
      let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> templateOnControl: '
        + JSON.stringify(templateOnControl));
      expect(templateOnControl.tthoraInicio)
        .toBe('15:00', 'validating tmhoraInicio');
      let eveningList = templateOnControl.getEveningList();
      expect(eveningList)
        .toBeDefined('checking existence for eveningList');
      expect(eveningList.length)
        .toBe(1, 'size of evening list to be 1');
      let appointment = eveningList[0];
      expect(appointment)
        .toBeDefined('checking existence for appointment');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(1500, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(5, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('DESCANSO', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('TARDE', 'checking value for appointment.zonaHoraria');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending');

      // Inserting second appointment. Break
      event = {
        dragData: blockFree
      };
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: calling function');
      component.onBlockDropOver(event, appointment, '-EVENING-');
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> templateOnControl: '
        + JSON.stringify(templateOnControl));
      expect(templateOnControl.tmhoraInicio)
        .toBe('08:00', 'validating tmhoraInicio');
      eveningList = templateOnControl.getEveningList();
      expect(eveningList)
        .toBeDefined('checking existence for eveningList');
      expect(eveningList.length)
        .toBe(2, 'size of morning list to be 2');
      appointment = eveningList[1];
      expect(appointment)
        .toBeDefined('checking existence for appointment');
      // Check the added appointment data.
      expect(appointment.huecoId)
        .toBe(1505, 'checking value for appointment.huecoId');
      expect(appointment.huecoDuracion)
        .toBe(10, 'checking value for appointment.huecoDuracion');
      expect(appointment.estado)
        .toBe('LIBRE', 'checking value for appointment.estado');
      expect(appointment.tipo)
        .toBe('TRANSPARENTE', 'checking value for appointment.tipo');
      expect(appointment.zonaHoraria)
        .toBe('TARDE', 'checking value for appointment.zonaHoraria');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending');
    });
    it('onBlockDropOver: adding name to the template', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: adding name to the template');
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');

      // Inserting appointment after name. Should set the save pending to true
      let event = {
        dragData: blockFree
      };
      templateOnControl = component.templateUnderControl;
      let componentAsAny = component as any;
      expect(templateOnControl)
        .toBeDefined('checking existence for templateOnControl');
      // Inserting but not setting for save.
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: calling function');
      component.onBlockDrop(event, '-EVENING-');
      let appointmentTarget = templateOnControl.getEveningList()[0];
      component.onBlockDropOver(event, appointmentTarget, '-EVENING-');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending to be false');

      templateOnControl.nombre = 'Nombre de Prueba';
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onBlockDropOver: calling function');
      component.onBlockDropOver(event, appointmentTarget, '-EVENING-');
      expect(component.savePending)
        .toBeTruthy('checking value for component.savePending to be true');
    });
  });
  describe('Code Coverage Phase [onNameBlur]', () => {
    it('onNameBlur: checking the save state', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> onNameBlur: checking the save state');
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending to be false');
      component.onNameBlur();
      expect(component.savePending)
        .toBeFalsy('checking value for component.savePending to be false');
      templateOnControl.setNombre('Nombre de Pruebas');
      component.onNameBlur();
      expect(component.savePending)
        .toBeTruthy('checking value for component.savePending to be true');
    });
  });
  describe('Code Coverage Phase [getMorningData]', () => {
    it('getMorningData: just getting the data', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> getMorningData: just getting the data');
      expect(component.getMorningData())
        .toBeDefined('checking getMorningData to exist');
      expect(component.getMorningData().length)
        .toBe(0, 'checking getMorningData to be empty');
      // Adding a new appointment to the list
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      let event = {
        dragData: blockFree
      };
      component.onBlockDrop(event, '-MORNING-');
      expect(component.getMorningData().length)
        .toBe(1, 'checking getMorningData have an element');
    });
  });
  describe('Code Coverage Phase [getEveningData]', () => {
    it('getEveningData: just getting the data', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> getEveningData: just getting the data');
      expect(component.getEveningData())
        .toBeDefined('checking getEveningData to exist');
      expect(component.getEveningData().length)
        .toBe(0, 'checking getEveningData to be empty');
      // Adding a new appointment to the list
      let blockFree = new TemplateBuildingBlock({
        "nombre": "Cita de 10 minutos",
        "duracion": 10,
        "tipo": "TRANSPARENTE",
        "estado": "LIBRE"
      });
      expect(blockFree).toBeDefined('checking the existence for blockFree');
      let event = {
        dragData: blockFree
      };
      component.onBlockDrop(event, '-EVENING-');
      expect(component.getEveningData().length)
        .toBe(1, 'checking getEveningData have an element');
    });
  });

  // - F O R M   I N T E R A C T I O N   P H A S E
  describe('Form Interaction Phase', () => {
    it('validate panel title', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> validate panel title');
      let componentAny = component as any;
      expect(componentAny.title).toBe('-PANEL TITLE-');
    });
  });

  // - B U G   I D E N T I F I C A T I O N   P H A S E.
  describe('Bug Identification Phase', () => {
    it('B01. not reordered slots when initial morning time changed', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]> B01. not reordered slots when initial time changed');
      // Read the test template with the data.
      component.templateUnderControl = appStoreService.directAccessMockBug('bugs/b01.template-on-construction') as CitasTemplate;
      templateOnControl = component.templateUnderControl;
      let componentAny = component as any;

      // Check the current time values for the appointments.
      let expectedTime = 800;
      let appointments = templateOnControl.getMorningList();
      for (let appoint of appointments) {
        expect(appoint.huecoId).toBe(expectedTime);
        expectedTime = expectedTime + appoint.getDuracion();
      }

      // Change the initial morning time.
      templateOnControl.tmhoraInicio = "09:00";
      component.onMorningTimeBlur();
      expect(component.savePending).toBeTruthy();
      // Check that time values have not changed.
      expectedTime = 900;
      appointments = templateOnControl.getMorningList();
      for (let appoint of appointments) {
        expect(appoint.huecoId).toBe(expectedTime);
        expectedTime = expectedTime + appoint.getDuracion();
      }
    });
    it('B02. save button does not activate on changes', () => {
      console.log('><[gestion-servicio/TemplateUnderConstructionComponent]>B02. save button does not activate on changes');
      // Read the test template with the data.
      component.templateUnderControl = appStoreService.directAccessMockBug('bugs/b01.template-on-construction') as CitasTemplate;
      templateOnControl = component.templateUnderControl;
      let componentAny = component as any;
    });
  });
});

