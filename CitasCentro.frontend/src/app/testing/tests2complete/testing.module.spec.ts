//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
import { TestingModule } from '@app/testing/testing.module';

describe('MODULE TestingModule', () => {
  let testingModule: TestingModule;

  beforeEach(() => {
    testingModule = new TestingModule();
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('TestingModule Construction Phase', () => {
    it('should create an instance', () => {
      expect(testingModule).toBeTruthy();
    });
  });
});
