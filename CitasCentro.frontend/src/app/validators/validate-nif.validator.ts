//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { AbstractControl } from '@angular/forms';

/**
 * Funtion to validate NIF/CIF/NIE
 * @param  value the field value at least with 8 characters.
 * @return       boolean being true that the field is value.
 */
export function ValidateNIF(control: AbstractControl) {
  let validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
  let nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
  let nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
  let str = control.value.toString().toUpperCase();

  if (!nifRexp.test(str) && !nieRexp.test(str)) return { validNIF: true };

  let nie = str
    .replace(/^[X]/, '0')
    .replace(/^[Y]/, '1')
    .replace(/^[Z]/, '2');

  let letter = str.substr(-1);
  let charIndex = parseInt(nie.substr(0, 8)) % 23;

  if (validChars.charAt(charIndex) === letter) return null;
  else
    return { validNIF: true };
}
