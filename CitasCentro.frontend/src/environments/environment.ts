//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
export const environment = {
  appName: require('../../package.json').name,
  appVersion: require('../../package.json').version + " dev",
  production: false,
  development: true,
  mockStatus: true,
  showexceptions: true,
  serverName: "https://backcitas.herokuapp.com",
  // serverName: "",
  apiVersion1: "/api/v1",
  apiVersion2: "/api/v2",
  //--- C O N S T A N T S
  TOKEN_KEY: '-TOKEN_KEY-',
  CREDENTIAL_KEY: '-CREDENTIAL-KEY-',
  PREFERRED_SERVICE: '-PREFERRED-SERVICE-',
  SERVICE_KEY: "-SERVICE_KEY-",
  TEMPLATE_KEY: "-TEMPLATE-KEY-",
  LAST_REPORT_KEY: "-LAST-REPORT-KEY-"
};
