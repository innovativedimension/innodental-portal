// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: './',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    /** * maximum number of tries a browser will attempt in the case of a disconnection */
    browserDisconnectTolerance: 6,
    /** * How long will Karma wait for a message from a browser before disconnecting from it (in ms). */
    browserNoActivityTimeout: 120000,
    customLaunchers: {
      ChromeNoSandbox: {
        base: 'Chrome',
        flags: ['--no-sandbox']
      },
      ChromeNoSandboxHeadless: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      },
      ChromeHeadlessCI: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox', '--disable-gpu']
      }
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../coverage'),
      reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true,
      thresholds: {
        statements: 30,
        lines: 30,
        branches: 10,
        functions: 30
      }
    },
    singleRun: false,
    webpack: {
      resolve: {
        extensions: ['.ts', '.js'],
        alias: {
          // same paths than tsconfig.json
          "@interfaces/*": __dirname + "/src/app/modules/shared/interfaces/",
          "@models/*": __dirname + "/src/app/modules/shared/models/",
          "@renders/*": __dirname + "/src/app/modules/shared/renders/",
          "@citamed-lib/*": __dirname + "/dist/citamed-lib/",
          "@app/*": __dirname + "/src/app/"
        }
      }
    }
  });
};