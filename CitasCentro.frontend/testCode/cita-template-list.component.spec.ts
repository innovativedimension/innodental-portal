//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- MODULES IMPORTS
import { ToastrModule } from 'ng6-toastr-notifications';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'angular-calendar';
import { StorageServiceModule } from 'angular-webstorage-service';
import { IonicModule } from 'ionic-angular';
// --- APPLICATION MODULES
import { UIModule } from '@app/modules/ui/ui.module';
import { SharedModule } from '@app/modules/shared/shared.module';
// --- CORE
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Routes } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';
// --- TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { async } from '@angular/core/testing';
import { TestAppStoreService } from '@app/testing/TestAppStoreService.service';
import { By } from '@angular/platform-browser';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// --- COMPONENTS
import { CitacionesPageComponent } from '@app/modules/citaciones/citaciones-page/citaciones-page.component';
//import { SimpleNotificationsModule } from 'angular2-notifications';
import { CalendarDefinitionPageComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/definicion-calendario-page.component';
import { TemplateListComponent } from '@app/modules/gestion-servicio/definicion-calendario-page/panels/template-list/template-list.component';

/**
 * This is an empty component to be pointed with valid soutes.
 *
 * @export
 * @class HomeComponent
 */
@Component({
  template: `Home`
})
export class RouteMockUpComponent {
}
export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: RouteMockUpComponent }
];
// --- E N D   O F   R O U T I N G   C O M P O N E N T

describe('TemplateListComponent', () => {
  let component: TemplateListComponent;
  let fixture: ComponentFixture<TemplateListComponent>;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes),
        StorageServiceModule,
        // SimpleNotificationsModule.forRoot(),
        ToastrModule.forRoot(),
        CalendarModule.forRoot(),
        HttpClientModule,
        IonicModule,
        UIModule,
        SharedModule
      ],
      declarations: [TemplateListComponent, RouteMockUpComponent],
      providers: [
        { provide: AppStoreService, useClass: TestAppStoreService },
      ]
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(TemplateListComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  // - C O M P I L A T I O N / C R E A T I O N   T E S T S
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // - U I   T A G S
  // it('OnInit phase panel "gs-header-panel" should be visible', () => {
  //   // Locate the panel on the dom.
  //   let testPanel = fixture.debugElement.query(By.css('#gs-header-panel'));
  //   expect(testPanel).toBeDefined();
  // });
  // it('OnInit phase panel "ui-menu-bar" should be visible', () => {
  //   // Locate the panel on the dom.
  //   let testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
  //   expect(testPanel).toBeDefined();
  // });
  // it('OnInit phase panel "dc-toolbar-panel" should be visible', () => {
  //   // Locate the panel on the dom.
  //   let testPanel = fixture.debugElement.query(By.css('#dc-toolbar-panel'));
  //   expect(testPanel).toBeDefined();
  // });
});
