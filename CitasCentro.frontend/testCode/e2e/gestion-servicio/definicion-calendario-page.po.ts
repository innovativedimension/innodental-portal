//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// -- PROTRACTOR
import { browser } from 'protractor';
import { element } from 'protractor';
import { by } from 'protractor';

export class DefinicionCalendarioPageE2E {
  navigateTo() {
    return browser.get('/gestionservicio/definicioncalendario');
  }
  // - P A G E   E L E M E N T   L O C A T O R S
  public locateServiceSelector() {
    console.log('E2E[page/DefinicionCalendarioPageE2E]> locate the click are to display the service selection panel');
    // Get the panel.
    let header = element(by.id('dc-toolbar-panel')); // locate the panel
    let headerLabel = element(by.css('header-label')); // check that a service is active


    // return element(by.css('#login-header-1')).getText();
  }
  public checkIfServiceActive() {
    console.log('E2E[page/DefinicionCalendarioPageE2E]> locate the label that identifies there a service active');
    // Get the panel.
    let header = element(by.id('dc-toolbar-panel')); // locate the panel
    let headerLabel = element(by.css('header-label')); // check that a service is active
    if (null != headerLabel) return headerLabel.getText();
    else return null;
  }
  // // - L O G I N   P A G E   L I T E R A L S
  // public  getApplicationName() {
  //   return element(by.css('#login-header-1')).getText();
  // }
  // public  getApplicationBrand() {
  //   return element(by.css('#login-header-2')).getText();
  // }
  // public  getApplicationVersion() {
  //   return element(by.css('#app-version')).getText();
  // }

  // // - M O D U L E S   P A N E L   L I T E R A L S
  // public  getTagText( _tagIdentifier : string ) {
  //   return element(by.css(_tagIdentifier)).getText();
  // }

  // - L O G I N   P A N E L   L I T E R A L S
  // - L O G I N   P A N E L   F O R M

}