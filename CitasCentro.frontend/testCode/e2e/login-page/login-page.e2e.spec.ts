//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// -- PROTRACTOR
import { browser } from 'protractor';
import { element } from 'protractor';
import { by } from 'protractor';
// --- E2E PAGES
import { LoginPageE2E } from './login-page.po';

describe('Login Page', () => {
  let page: LoginPageE2E;

  beforeEach(() => {
    page = new LoginPageE2E();
    page.navigateTo();
  });

  // - L O G I N   P A G E   L I T E R A L S
  // it('should display application name and version', () => {
  //   expect(page.getApplicationName()).toEqual('Citas Centro');
  //   expect(page.getApplicationBrand()).toEqual('Interface Centro');
  //   expect(page.getApplicationVersion()).toEqual('0.8.2 dev');
  // });
  // // - M O D U L E S   P A N E L   L I T E R A L S
  // it('modules panel should be titled "Lista de Módulos"', () => {
  //   expect(page.getTagText('#modules-panel-header')).toEqual('Lista de Módulos');
  // });
  // // - L O G I N   P A N E L   L I T E R A L S
  // it('login panel should be titled "Panel de Identificación"', () => {
  //   expect(page.getTagText('#login-panel-header')).toEqual('Panel de Identificación');
  // });
  // - L O G I N   P A N E L   F O R M
  it('login panel should validate NIF', () => {
    let identificador = element(by.css('#identificador'));
    let password = element(by.css('#password'));
    expect(identificador).toBeDefined();
    expect(password).toBeDefined();

    // Check the alert values while editing the NIF field.
    identificador.sendKeys("13739");
    // console.log('[LoginPageE2E]> form alerts: ' + JSON.stringify(JSON.stringify(element.all(by.css('.alert-danger div')))));
    let alertMessage1 = element.all(by.css('.alert-danger div')).get(0).getText();
    let alertMessage2 = element.all(by.css('.alert-danger div')).get(1).getText();
    // expect(alertMessage1).toEqual('NIF de autentificación debe tener al menos 8 caracteres.');
    // expect(alertMessage2).toEqual('Contraseña es un campo requerido.');

    // button.click();
    // expect(answer.getText()).toEqual("Chocolate!");

    // var answer = element(by.binding('answer'));
    // var button = element(by.className('question__button'));
    // expect(page.getTagText('#login-panel-header')).toEqual('Panel de Identificación');
  });
});