import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HTTPResultsPageComponent } from './httpresults-page.component';

describe('HTTPResultsPageComponent', () => {
  let component: HTTPResultsPageComponent;
  let fixture: ComponentFixture<HTTPResultsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HTTPResultsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HTTPResultsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
