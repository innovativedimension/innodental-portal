import { TestBed, inject } from '@angular/core/testing';

import { OpenAppointmentControllerService } from './open-appointment-controller.service';

describe('OpenAppointmentControllerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OpenAppointmentControllerService]
    });
  });

  it('should be created', inject([OpenAppointmentControllerService], (service: OpenAppointmentControllerService) => {
    expect(service).toBeTruthy();
  }));
});
