import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientAppointmentsPanelComponent } from './patient-appointments-panel.component';

describe('PatientAppointmentsPanelComponent', () => {
  let component: PatientAppointmentsPanelComponent;
  let fixture: ComponentFixture<PatientAppointmentsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientAppointmentsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAppointmentsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
