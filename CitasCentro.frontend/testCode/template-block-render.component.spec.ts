import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateBlockRenderComponent } from './template-block-render.component';
import { IonicModule } from 'ionic-angular';

describe('TemplateBlockRenderComponent', () => {
  let component: TemplateBlockRenderComponent;
  let fixture: ComponentFixture<TemplateBlockRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [IonicModule],
      declarations: [ TemplateBlockRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateBlockRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
