import { VisualTestingModule } from './visual-testing.module';

describe('VisualTestingModule', () => {
  let visualTestingModule: VisualTestingModule;

  beforeEach(() => {
    visualTestingModule = new VisualTestingModule();
  });

  it('should create an instance', () => {
    expect(visualTestingModule).toBeTruthy();
  });
});
