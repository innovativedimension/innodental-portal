import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeButtonsPageComponent } from './visualize-buttons-page.component';
import { IonicModule } from 'ionic-angular';
import { UIModule } from '@app/modules/ui/ui.module';

describe('VisualizeButtonsPageComponent', () => {
  let component: VisualizeButtonsPageComponent;
  let fixture: ComponentFixture<VisualizeButtonsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicModule,
        UIModule
      ],     
      declarations: [ VisualizeButtonsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeButtonsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
