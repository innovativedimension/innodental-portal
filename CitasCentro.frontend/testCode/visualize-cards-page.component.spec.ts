import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeCardsPageComponent } from './visualize-cards-page.component';

describe('VisualizeCardsPageComponent', () => {
  let component: VisualizeCardsPageComponent;
  let fixture: ComponentFixture<VisualizeCardsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizeCardsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeCardsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
