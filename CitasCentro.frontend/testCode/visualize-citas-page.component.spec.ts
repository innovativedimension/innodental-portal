import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeCitasPageComponent } from './visualize-citas-page.component';

describe('VisualizeCitasPageComponent', () => {
  let component: VisualizeCitasPageComponent;
  let fixture: ComponentFixture<VisualizeCitasPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizeCitasPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeCitasPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
