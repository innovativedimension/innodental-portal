import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeTestDashboardComponent } from './visualize-test-dashboard.component';
import { IonicModule } from 'ionic-angular';
import { UIModule } from '@app/modules/ui/ui.module';

describe('VisualizeTestDashboardComponent', () => {
  let component: VisualizeTestDashboardComponent;
  let fixture: ComponentFixture<VisualizeTestDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        IonicModule,
        UIModule
      ],
      declarations: [ VisualizeTestDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeTestDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
