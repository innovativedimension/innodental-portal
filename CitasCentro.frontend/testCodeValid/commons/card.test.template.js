// - C A R D   P A N E L   C O R E   T E S T S
/**
 * Tests to check for the Card properties. Cards define the Header, the Title and the common buttons and actuators that get displayed on the header at the right.
 * This source should be copied inside other 'describe' test suites to add this tests that should apply to all pages or panels that define a card.
 */
describe('CardPanelComponent PANEL', () => {
  // - C O M P I L A T I O N / C R E A T I O N   T E S T S
  it('should be created', () => {
    console.log('><[CardPanelComponent]> should be created');
    expect(component).toBeTruthy('component has not been created.');
  });
  // - R E N D E R I N G   P H A S E
  it('Rendering Phase: check all display elements', () => {
    console.log('><[CardPanelComponent]> Rendering Phase');
    // Check initial state from default configuration.
    expect(component.title).toBe('-PANEL TITLE-', 'panel title is predefined to an specific name.')
    expect(component.show).toBeTruthy('panels by default should be visible.');
    expect(component.canBeClosed).toBeFalsy('panels by default are not user closable.');
    expect(component.canBeExpanded).toBeFalsy('panels by default should be ave expand capabilities.');
    expect(component.development).toBeFalsy('panels by default should be on production mode.');
  });
});
