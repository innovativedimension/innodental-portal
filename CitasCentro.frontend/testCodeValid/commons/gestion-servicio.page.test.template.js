// - G E S T I O N   S E R V I C I O   P A G E   T E S T S
/**
 * Tests the core elements that should be present on any 'Gestion Servicio' page.
 */
describe('GestionServicioPage core PAGE', () => {
  // - C O M P I L A T I O N / C R E A T I O N   T E S T S
  it('should be created', () => {
    console.log('><[GestionServicioPage]> should be created');
    expect(component).toBeTruthy('component has not been created.');
  });
  // - R E N D E R I N G   P H A S E
  it('Rendering Phase: check all display elements', () => {
    console.log('><[GestionServicioPage]> endering Phase');
    // Locate the panel on the dom.
    let testPanel = fixture.debugElement.query(By.css('#gs-header-panel'));
    expect(testPanel).toBeDefined();
    testPanel = fixture.debugElement.query(By.css('#ui-menu-bar'));
    expect(testPanel).toBeDefined();
  });
});
