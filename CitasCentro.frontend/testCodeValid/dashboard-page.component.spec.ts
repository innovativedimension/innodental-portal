//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- MODULES IMPORTS
//import { SimpleNotificationsModule } from 'angular2-notifications';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//--- WEBSTORAGE
import { LOCAL_STORAGE, StorageServiceModule } from 'angular-webstorage-service';
// --- IONIC
import { IonicModule } from 'ionic-angular';
// --- APPLICATION MODULES
import { UIModule } from '@app/modules/ui/ui.module';
// --- CORE
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Routes } from '@angular/router';
// --- TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { async } from '@angular/core/testing';
// --- ENVIRONMENT
import { environment } from '@env/environment';
// --- SERVICES
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
// --- COMPONENTS
// import { DashboardPageComponent } from './dashboard-page.component';
// --- MODELS
import { Perfil } from '@app/models/Perfil.model';
import { DashboardPageComponent } from '@app/modules/login/pages/dashboard-page/dashboard-page.component';
// import { AppStoreServiceTestable } from '@app/services/appstore-testeable.service';
// import { Router, Routes } from '@angular/router';

// import { SESSION_STORAGE } from 'angular-webstorage-service';
// import { WebStorageService } from 'angular-webstorage-service';
//--- ROUTER
// import { Router } from '@angular/router';
/**
 * This is an empty component to be pointed with valid soutes.
 *
 * @export
 * @class HomeComponent
 */
@Component({
  template: `Home`
})

export class HomeComponent {
}
export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent }
];

class MockAppStoreService extends AppStoreService {
  public profile: Perfil = new Perfil({
    enabled: true,
    allservices: false,
    serviciosAutorizados: []
  });

  //--- C O N S T R U C T O R
  // constructor(
  //   @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
  //   @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
  //   protected router: Router) {
  //   super(storage, sessionStorage, router);
  // }

  public accessLoginProfile(): Perfil {
    return this.profile;
  }
  public storeCredential(_newcredential: Perfil): void {
    this.profile = _newcredential;
  }
};

describe('DashboardPageComponent', () => {
  let component: DashboardPageComponent;
  let appStoreService: AppStoreService;
  let fixture: ComponentFixture<DashboardPageComponent>;

  // beforeEach(async(() => {
    beforeEach(() => {
      TestBed.configureTestingModule({
      // Use the same imports that will be used on the module.
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes),
        StorageServiceModule,
        // SimpleNotificationsModule.forRoot(),
        HttpClientModule,
        IonicModule,
        UIModule
      ],
      declarations: [DashboardPageComponent, HomeComponent],
      providers: [
        { provide: AppStoreService, useClass: MockAppStoreService },
        // BackendService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPageComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
    appStoreService = TestBed.get(AppStoreService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('validate initial setup', () => {
    // const comp = new LightswitchComponent();
    expect(component.hovering).toBe(false, 'no hovering at first');
    expect(component.menuOpen).toBe(false, 'the menu is closed at first');
  });
  it('request role for uninitialized data', () => {
    // Create a dummy credential with the DEFAULT role.
    let testProfile = new Perfil({
      enabled: true,
      allservices: false,
      serviciosAutorizados: []
    });
    appStoreService.storeCredential(testProfile);
    expect(appStoreService.accessLoginProfile().getLoginRole()).toBe('DESHABILITADO', 'the dummy role should be DESHABILITADO');
    expect(component.requestsRole('ADMIN')).toBe(false, 'the dummy role is DESHABILITADO < ADMIN');
    expect(component.requestsRole('RESPONSABLE')).toBe(false, 'the dummy role is DESHABILITADO < RESPONSABLE');
    expect(component.requestsRole('CALLCENTER')).toBe(false, 'the dummy role is DESHABILITADO < CALCENTER');
  });
  it('request role for ADMIN', () => {
    // Create a dummy credential with the ADMIN role.
    let testProfile = new Perfil({
      enabled: true,
      role: "ADMIN",
      allservices: false,
      serviciosAutorizados: []
    });
    appStoreService.storeCredential(testProfile);
    expect(appStoreService.accessLoginProfile().getLoginRole()).toBe('ADMIN', 'the dummy role should be ADMIN');
    expect(component.requestsRole('ADMIN')).toBe(true, 'the dummy role is RESPONSABLE = ADMIN');
    expect(component.requestsRole('RESPONSABLE')).toBe(true, 'the dummy role is RESPONSABLE > RESPONSABLE');
    expect(component.requestsRole('CALLCENTER')).toBe(true, 'the dummy role is RESPONSABLE > CALCENTER');
  });
  it('request role for RESPONSABLE', () => {
    // Create a dummy credential with the ADMIN role.
    let testProfile = new Perfil({
      enabled: true,
      role: "RESPONSABLE",
      allservices: false,
      serviciosAutorizados: []
    });
    appStoreService.storeCredential(testProfile);
    expect(appStoreService.accessLoginProfile().getLoginRole()).toBe('RESPONSABLE', 'the dummy role should be RESPONSABLE');
    expect(component.requestsRole('ADMIN')).toBe(false, 'the dummy role is RESPONSABLE < ADMIN');
    expect(component.requestsRole('RESPONSABLE')).toBe(true, 'the dummy role is RESPONSABLE = RESPONSABLE');
    expect(component.requestsRole('CALLCENTER')).toBe(true, 'the dummy role is RESPONSABLE > CALCENTER');
  });
  it('request role for CALLCENTER', () => {
    // Create a dummy credential with the ADMIN role.
    let testProfile = new Perfil({
      enabled: true,
      role: "CALLCENTER",
      allservices: false,
      serviciosAutorizados: []
    });
    appStoreService.storeCredential(testProfile);
    expect(appStoreService.accessLoginProfile().getLoginRole()).toBe('CALLCENTER', 'the dummy role should be CALLCENTER');
    expect(component.requestsRole('ADMIN')).toBe(false, 'the dummy role is RESPONSABLE < ADMIN');
    expect(component.requestsRole('RESPONSABLE')).toBe(false, 'the dummy role is RESPONSABLE < RESPONSABLE');
    expect(component.requestsRole('CALLCENTER')).toBe(true, 'the dummy role is RESPONSABLE = CALCENTER');
  });
});
