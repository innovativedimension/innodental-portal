//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- MODULES IMPORTS
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StorageServiceModule } from 'angular-webstorage-service';
//import { SimpleNotificationsModule } from 'angular2-notifications';
import { ToastrModule } from 'ng6-toastr-notifications';
import { HttpClientModule } from '@angular/common/http';
// --- APPLICATION MODULES
import { UIModule } from '@app/modules/ui/ui.module';
import { SharedModule } from '@app/modules/shared/shared.module';
// --- TEST
import { TestBed } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
// --- CORE
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Routes } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';
//--- SERVICES
import { WebStorageService } from 'angular-webstorage-service';
//import { NotificationsService } from 'angular2-notifications';
import { AppStoreService } from '@app/services/appstore.service';
import { BackendService } from '@app/services/backend.service';
import { TestAppStoreService } from '@app/testing/TestAppStoreService.service';
import { TestBackendService } from '@app/testing/TestBackendService.service';
// --- COMPONENTS
import { ModulesPanelComponent } from '@app/modules/login/panels/modules-panel/modules-panel.component';
import { AppPanelComponent } from '@app/modules/ui/app-panel/app-panel.component';
import { MVCViewerComponent } from '@app/modules/ui/mvcviewer/mvcviewer.component';

/**
 * This is an empty component to be pointed with valid soutes.
 *
 * @export
 * @class HomeComponent
 */
@Component({
  template: `Home`
})
export class RouteMockUpComponent {
}
export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: RouteMockUpComponent }
];
// --- E N D   O F   R O U T I N G   C O M P O N E N T

describe('ModulesPanelComponent PANEL', () => {
  let component: ModulesPanelComponent;
  let fixture: ComponentFixture<ModulesPanelComponent>;

  beforeEach(() => {
    // Configure the module dependencies to be able to compile the test.
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes),
        StorageServiceModule,
        // SimpleNotificationsModule.forRoot(),
        ToastrModule.forRoot(),
        // CalendarModule.forRoot(),
        HttpClientModule,
        // IonicModule,
        // UIModule,
        SharedModule
      ],
      declarations: [ModulesPanelComponent, RouteMockUpComponent, AppPanelComponent, MVCViewerComponent],
      providers: [
        // WebStorageService,
        // NotificationsService,
        { provide: AppStoreService, useClass: TestAppStoreService },
        { provide: BackendService, useClass: TestBackendService }
      ]
    })
      .compileComponents();

    // Initialize the test instance and prepare for testing.
    fixture = TestBed.createComponent(ModulesPanelComponent);
    component = fixture.componentInstance;
  });

  // - C O M P I L A T I O N / C R E A T I O N   T E S T S
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  // - M V C   C H E C K S
  it('should have 3 modules on model list', async(() => {
    component.ngOnInit();
    fixture.whenStable().then(() => {
      expect(component.dataModelRoot).toBeDefined();
      expect(component.dataModelRoot.length).toBe(3);
    });
  }));
});
