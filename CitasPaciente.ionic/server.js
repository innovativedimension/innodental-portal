// Configure the express server for Heroku platform.
var express = require('express'),
  app = express();

app.use(express.static('www'));

// CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header('Content-Type', 'application/json; charset=utf-8')
  res.header('xApp-Platform', 'Ionic 4.x')
  res.header('xApp-Brand', 'CitasPaciente-0.8.x')
  res.header('xApp-Signature', 'S0000.0100.0000');
  next();
});

// API Routes
// app.get('/blah', routeHandler);

app.set('port', process.env.PORT || 5000);

app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});