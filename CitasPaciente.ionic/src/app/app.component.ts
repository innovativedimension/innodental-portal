//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { Inject } from '@angular/core';
import { ViewChild } from '@angular/core';
// --- ENVIRONMENT
import { EnvVariables } from '../environment-plugin/environment-plugin.token';
//--- IONIC
import { Nav } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { Storage } from '@ionic/storage';
//--- SERVICES
import { AppStoreService } from './shared/services/appstore.service';
// import { BackendService } from './shared/services/backend.service';
//--- MODELS
import { Page } from './shared/models/core/Page.model';
//--- PAGES
// import { HomePage } from '../pages/home/home.page';
// import { AceptacionCondicionesPage } from '../pages/aceptacioncondiciones/aceptacioncondiciones.page';
import { OpenAppointmentsPage } from '../pages/open-appointments/open-appointments.page';
// import { AvailableLocationsPage } from '../pages/available-locations/available-locations.page';
import { AvailableSpecialitiesPage } from '../pages/available-specialities/available-specialities.page';
// import { PatientCredential } from './shared/models/PatientCredential.model';
import { SplashPage } from './pages/splash-page/splash.page';
import { HomePage } from '../pages/home/home.page';
import { AceptacionCondicionesPage } from '../pages/aceptacioncondiciones/aceptacioncondiciones.page';
// import { OpenAppointmentsPage } from '../../../pages/open-appointments/open-appointments.page';
// import { AvailableSpecialitiesPage } from '../../../pages/available-specialities/available-specialities.page';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  public versionName: string;
  // public rootPage: any = SplashPage;
  // public pageList: Page[] = [];

  // private _pages: Page[] = [];
  public options = {
    position: ["bottom", "right"],
    showProgressBar: false,
    pauseOnHover: true,
    timeOut: 5000,
    animate: "fade",
    lastOnBottom: true,
    preventDuplicates: true,
    theClass: "rounded"
  }

  constructor(
    @Inject(EnvVariables) public environment,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    // protected storage: Storage,
    protected appStoreService: AppStoreService) {
    // protected backendService: BackendService) {
    // Show the spash screen.
    this.splashScreen.show();
    this.versionName = this.environment.appVersion;
    this.prepareMenus();
    this.initializeApp();
  }

  public initializeApp() {
    console.log('>>[MyApp.initializeApp]');
    this.platform.ready().then(() => {
      // Open the splash page that will perform all the loading and initialization.
      this.openPage(new Page().setTitle('Splash')
        .setPageComponent(SplashPage)
        .setIconName('ios-home'));
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  public prepareMenus(): void {
    // Full menu.
    // this.pageList = [];
    this.appStoreService.menuFull.push(new Page().setTitle('Inicio')
      .setPageComponent(HomePage)
      .setIconName('ios-home'));
    this.appStoreService.menuFull.push(new Page().setTitle('Datos Paciente')
      .setPageComponent(AceptacionCondicionesPage)
      .setIconName('ios-person'));
    this.appStoreService.menuFull.push(new Page().setTitle('Medicos por Especialidad')
      .setPageComponent(AvailableSpecialitiesPage)
      .setIconName('ios-contacts'));
    // this.menuFull.push(new Page().setTitle('Localizaciones')
    //   .setPageComponent(AvailableLocationsPage)
    //   .setIconName('navigate'));
    this.appStoreService.menuFull.push(new Page().setTitle('Citas Abiertas')
      .setPageComponent(OpenAppointmentsPage)
      .setIconName('md-calendar'));

    // Invalid credential.
    this.appStoreService.menuNotCredential.push(new Page().setTitle('Inicio')
      .setPageComponent(HomePage)
      .setIconName('ios-home'));
    this.appStoreService.menuNotCredential.push(new Page().setTitle('Datos Paciente')
      .setPageComponent(AceptacionCondicionesPage)
      .setIconName('ios-person'));

    // No open appointments.
    this.appStoreService.menuNotOpenAppointments.push(new Page().setTitle('Inicio')
      .setPageComponent(HomePage)
      .setIconName('ios-home'));
    this.appStoreService.menuNotOpenAppointments.push(new Page().setTitle('Datos Paciente')
      .setPageComponent(AceptacionCondicionesPage)
      .setIconName('ios-person'));
    this.appStoreService.menuNotOpenAppointments.push(new Page().setTitle('Medicos por Especialidad')
      .setPageComponent(AvailableSpecialitiesPage)
      .setIconName('ios-contacts'));

    // Set the initial menu to the small menu.
    this.appStoreService._activeMenu = this.appStoreService.menuNotCredential;
  }

  // public initializeApp1() {
  //   console.log('>>[MyApp.initializeApp]');
  //   this.platform.ready().then(() => {
  //     this.storage.get(this.environment.CREDENTIAL_KEY)
  //       .then((credentialData) => {
  //         try {
  //           if (null != credentialData) {
  //             let credential = new PatientCredential(JSON.parse(credentialData));
  //             console.log('--[MyApp.initializeApp.getCredential] Creadential received. State: ' + credential.isValid());
  //             if (credential.isValid()) {
  //               this.appStoreService.storeCredential(credential);
  //               this.updateAppPages();
  //               // Check if thre are any appointment open. If so change the root page to the list of open appointments.
  //               this.backendService.downloadPatientAppointments(credential)
  //                 .subscribe((appointments) => {
  //                   // Check for duplicated appointmes or marked as free.
  //                   let open = [];
  //                   for (let appo of appointments) {
  //                     if (appo.getCita().estado == 'RESERVADA') {
  //                       // Check if the appointment is still open and in the future.
  //                       if (appo.isOpen()) {
  //                         open.push(appo);
  //                         this.rootPage = OpenAppointmentsPage;
  //                       }
  //                     }
  //                   }
  //                   // Store the appointments for late use.
  //                   this.backendService.storeOpenAppointments(open);
  //                   // Okay, so the platform is ready and our plugins are available.
  //                   // Here you can do any higher level native things you might need.
  //                   this.statusBar.styleDefault();
  //                   this.splashScreen.hide();
  //                   this.nav.setRoot(this.rootPage);
  //                 });
  //             } else {
  //               // Okay, so the platform is ready and our plugins are available.
  //               // Here you can do any higher level native things you might need.
  //               console.log('--[MyApp.initializeApp.updateAppPages]');
  //               this.updateAppPages();
  //               this.statusBar.styleDefault();
  //               this.splashScreen.hide();
  //             }
  //           } else {
  //             console.log('--[MyApp.initializeApp.updateAppPages]');
  //             this.updateAppPages();
  //             this.statusBar.styleDefault();
  //             this.splashScreen.hide();
  //           }
  //         } catch (Exception) {
  //           console.log('--[HomePage.ngOnInit] Exception: ' + Exception.message);
  //         }
  //       });
  //   });
  //   console.log('<<[MyApp.initializeApp]');
  // }
  public getMenuPages(): Page[] {
    return this.appStoreService.accessActiveMenu();
  }
  //--- I N I T I A L I Z A T I O N
  // protected updateAppPages() {
  //   // console.log('>>[MyApp.updateAppPages]');
  //   // Add the ever available pages.
  //   this._pages = [];
  //   // this._pages.push(new Page().setTitle('Test')
  //   //   .setPageComponent(TestPage)
  //   //   .setIconName('ios-home'));
  //   this._pages.push(new Page().setTitle('Inicio')
  //     .setPageComponent(HomePage)
  //     .setIconName('ios-home'));
  //   this._pages.push(new Page().setTitle('Datos Paciente')
  //     .setPageComponent(AceptacionCondicionesPage)
  //     .setIconName('ios-person'));
  //   // Add the search pages only if the credential is valid.
  //   let credential = this.appStoreService.accessCredential();
  //   if (credential.isValid()) {
  //     // this.backendService.storeCredential(credential);
  //     this._pages.push(new Page().setTitle('Medicos por Especialidad')
  //       .setPageComponent(AvailableSpecialitiesPage)
  //       .setIconName('ios-contacts'));
  //     this._pages.push(new Page().setTitle('Localizaciones')
  //       .setPageComponent(AvailableLocationsPage)
  //       .setIconName('navigate'));
  //     // Add the Citas page after the Medicos only if the Credential is valid.
  //     this._pages.push(new Page().setTitle('Citas Abiertas')
  //       .setPageComponent(OpenAppointmentsPage)
  //       .setIconName('md-calendar'));
  //   }
  //   // console.log('<<[MyApp.updateAppPages]');
  // }
  // protected accesspages(): Page[] {
  //   this.updateAppPages();
  //   // Clear the center data to signal we can search any of the already doenlaoded data.
  //   this.backendService.storeCentros([]);
  //   return this._pages;
  // }

  public openPage(page: Page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  // public getPages(): Page[] {
  //   return this.accesspages();
  // }
  // - N O T I F I C A T I O N S
  public created() { }
  public destroyed() { }
}
