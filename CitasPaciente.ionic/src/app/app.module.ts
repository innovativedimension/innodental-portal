//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
//--- CORE MODULES
import { NgModule } from '@angular/core';
import { Injectable } from '@angular/core';
import { Injector } from '@angular/core';
import { Inject } from '@angular/core';
import { InjectionToken } from '@angular/core';
//--- BROWSER
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//--- HTTPCLIENT
import { HttpClientModule } from '@angular/common/http';
//--- TOAST NOTIFICATIONS
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
//--- IONIC
import { IonicApp } from 'ionic-angular';
import { IonicModule } from 'ionic-angular';
import { IonicErrorHandler } from 'ionic-angular';
import { CacheModule } from 'ionic-cache';
//--- IONIC PLUGINS
import { IonicStorageModule } from '@ionic/storage';
// import { Sim } from '@ionic-native/sim';
// --- IONIC NATIVE COMPONENTS
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// --- CALENDAR
import { CalendarModule } from 'angular-calendar';
// --- SERVICES
import { ErrorHandler } from '@angular/core';
import { AppStoreService } from '../app/shared/services/appstore.service';
import { BackendService } from '../app/shared/services/backend.service';
import { DoctorSearchService } from '../app/shared/services/doctorsearch.service';
import { IonicIsolationService } from './platform/isolation.service';
// --- CORE COMPONENTS
import { MyApp } from './app.component';
//--- APPLICATION MODULES
// import { ComponentsModule } from '../components/components.module';
//--- PAGES
import { BasePageComponent } from '../pages/basepage.component';
import { HomePage } from '../pages/home/home.page';
import { GDPRPage } from './pages/gdpr-page/gdpr.page';
import { GDPRRigthsPage } from './pages/gdprrights-page/gdprrights.page';

import { AceptacionCondicionesPage } from '../pages/aceptacioncondiciones/aceptacioncondiciones.page';
import { PatientRecordPage } from '../pages/patientrecord/patientrecord.page';
import { AvailableDoctorsPage } from '../pages/available-doctors/available-doctors.page';
// import { ListAppointmentsPage } from '../pages/list-appointments/list-appointments.page';
import { AppointmentReservationPage } from '../pages/appointment-reservation/appointment-reservation.page';
import { OpenAppointmentsPage } from '../pages/open-appointments/open-appointments.page';
import { AvailableLocationsPage } from '../pages/available-locations/available-locations.page';
import { AvailableSpecialitiesPage } from '../pages/available-specialities/available-specialities.page';
import { AppointmentsCalendarPage } from '../pages/appointments-calendar/appointments-calendar.page';
import { AvailableAppointmentsPage } from '../pages/available-appointments/available-appointments.page';
//--- COMPONENTS
import { CitaMonthPanelComponent } from '../components/cita-month-panel/cita-month-panel.component';
import { CitaComponent } from '../components/cita/cita.component';
import { EspecialidadComponent } from '../components/especialidad/especialidad';
import { OpenAppointmentComponent } from '../components/open-appointment/open-appointment';
import { OpenCitaComponent } from '../components/open-cita/open-cita';
// --- RENDERS
// import { RenderComponent } from '../app/shared/renders/render/render.component';
// import { ActoMedicoComponent } from '../app/shared/renders/acto-medico/acto-medico.component';
// import { AvailableMedicoComponent } from '../components/available-medico/available-medico';

//--- LOCALIZATION
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { EnvironmentsModule } from '../environment-plugin/environment-plugin.module';
// import { DownloadingPageComponent } from '../app/shared/core/downloading-page/downloading-page.component';
// import { SplashPageModule } from './pages/splash-page/splash.page.module';
// import { SplashPage } from './pages/splash-page/splash.page';
import { SplashPageModule } from './pages/splash-page/splash.page.module';
import { SharedModule } from './shared/shared.module';
// import { DownloadingPage~Com } from '../pages/downloading.component';
registerLocaleData(localeEs);

// --- ERROR INTERCEPTION
import * as Rollbar from 'rollbar';
const rollbarConfig = {
  accessToken: '8177af29044f4fbd8cf9ac35ca019668',
  captureUncaught: true,
  captureUnhandledRejections: true,
};
export const RollbarService = new InjectionToken<Rollbar>('rollbar');

@Injectable()
export class RollbarErrorHandler implements ErrorHandler {
  constructor(@Inject(RollbarService) private rollbar: Rollbar) { }

  handleError(err: any): void {
    // Use type checking to detect the different types of errors.
    if (err.constructor.name === 'TypeError') {
      // Those are syntax exceptions detected on the runtime.
      console.log('ERR[RollbarErrorHandler.handleError]> Error intercepted: ' + JSON.stringify(err));
      this.rollbar.error(err);
    } else if (err.constructor.name === 'Error') {
      // Those are syntax exceptions detected on the runtime.
      console.log('ERR[RollbarErrorHandler.handleError]> Error intercepted: ' + JSON.stringify(err.message));
      this.rollbar.error(err);
    } else {
      console.log('ERR[RollbarErrorHandler.handleError]> Error intercepted: ' + JSON.stringify(err.message));
      this.rollbar.error(err);
    }
  }
}

export function rollbarFactory() {
  return new Rollbar(rollbarConfig);
}

@NgModule({
  imports: [
    //--- BROWSER
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    //--- HTTPCLIENT
    HttpClientModule,
    //--- TOAST NOTIFICATIONS
    SimpleNotificationsModule.forRoot(),
    //--- IONIC
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      tabsPlacement: 'bottom',
      pageTransition: 'ios-transition'
    }),
    IonicStorageModule.forRoot(),
    CacheModule.forRoot(),
    //--- CALENDAR
    CalendarModule.forRoot(),
    // --- ENVIRONMENT
    EnvironmentsModule,
    // --- APPLICATION PAGE MODULES
    SharedModule,
    SplashPageModule
  ],
  declarations: [
    //--- CORE COMPONENTS
    MyApp,
    //--- APPLICATION MODULES
    // ComponentsModule,
    //--- PAGES
    // SplashPage,

    BasePageComponent,
    // DownloadingPageComponent,
    HomePage,
    GDPRPage,
    GDPRRigthsPage,
    AceptacionCondicionesPage,
    PatientRecordPage,
    AvailableDoctorsPage,
    // ListAppointmentsPage,
    AppointmentReservationPage,
    OpenAppointmentsPage,
    AvailableLocationsPage,
    AvailableSpecialitiesPage,
    AppointmentsCalendarPage,
    AvailableAppointmentsPage,
    //--- COMPONENTS
    CitaMonthPanelComponent,
    CitaComponent,
    EspecialidadComponent,
    // AvailableMedicoComponent,
    OpenAppointmentComponent,
    OpenCitaComponent,
    // RenderComponent,
    // ActoMedicoComponent
  ],
  entryComponents: [
    //--- CORE COMPONENTS
    MyApp,
    //--- APPLICATION MODULESº
    // ComponentsModule,
    //--- PAGES
    // SplashPage,

    BasePageComponent,
    // DownloadingPageComponent,
    HomePage,
    GDPRPage,
    GDPRRigthsPage,
    AceptacionCondicionesPage,
    PatientRecordPage,
    AvailableDoctorsPage,
    // ListAppointmentsPage,
    AppointmentReservationPage,
    OpenAppointmentsPage,
    AvailableLocationsPage,
    AvailableSpecialitiesPage,
    AppointmentsCalendarPage,
    AvailableAppointmentsPage,
    //--- COMPONENTS
    CitaMonthPanelComponent,
    CitaComponent,
    EspecialidadComponent,
    // AvailableMedicoComponent,
    OpenAppointmentComponent,
    OpenCitaComponent,
    // RenderComponent,
    // ActoMedicoComponent
  ],
  providers: [
    // --- IONIC
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    // --- NOTIFICATIONS
    NotificationsService,
    // --- APP SERVICES
    IonicIsolationService,
    BackendService,
    DoctorSearchService,
    AppStoreService,
    // --- ERROR INTERCEPTION
    { provide: ErrorHandler, useClass: RollbarErrorHandler },
    { provide: RollbarService, useFactory: rollbarFactory }
  ],
  bootstrap: [IonicApp]
})
export class AppModule { }
