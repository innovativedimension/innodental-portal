//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Component } from '@angular/core';
// --- IONIC
import { IonicPage } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
// --- SERVICES
import { AppStoreService } from '../../shared/services/appstore.service';
// --- PAGES
import { GDPRRigthsPage } from '../gdprrights-page/gdprrights.page';
import { HomePage } from '../../../pages/home/home.page';

@IonicPage()
@Component({
  selector: 'page-gdpr',
  templateUrl: 'gdpr.page.html',
})
export class GDPRPage {
  // - C O N S T R U C T O R
  constructor(public navCtrl: NavController
    , public navParams: NavParams
    , public appStoreService: AppStoreService) { }

  // - V I E W   I N T E R A C T I O N
  public onAccept(): void {
    // Save the accept flag on the credential and store it on the storge.
    let credential = this.appStoreService.accessCredential();
    credential.hasAcceptedGDPR = true;
    this.appStoreService.storeCredential(credential);
    this.navCtrl.push(HomePage);
  }
  public onShowRights(): void {
    this.navCtrl.push(GDPRRigthsPage);
  }
}
