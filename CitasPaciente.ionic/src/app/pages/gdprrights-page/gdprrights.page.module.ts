import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GDPRRigthsPage } from './gdprrights.page';
// import { GDPRRigthsPage } from './gdpr.page';

@NgModule({
  declarations: [
    GDPRRigthsPage,
  ],
  imports: [
    IonicPageModule.forChild(GDPRRigthsPage),
  ],
})
export class GdprPageModule {}
