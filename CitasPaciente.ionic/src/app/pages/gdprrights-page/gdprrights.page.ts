import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GdprPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'gdpr-rights-page',
  templateUrl: 'gdprrights.page.html',
})
export class GDPRRigthsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GdprPage');
  }
  public onReturnConsent(): void {
    this.navCtrl.pop();
  }
}
