//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- COMMON
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
// --- IONIC
import { IonicModule } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SplashPage } from './splash.page';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
// --- TESTING
import { TestBed } from '@angular/core/testing';
import { async } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';
// --- NOTIFICACIONES
import { NotificationsService } from 'angular2-notifications';
// --- SERVICES
import { AppStoreService } from '../../shared/services/appstore.service';
import { BackendService } from '../../shared/services/backend.service';
import { EmptyMockService } from '../../testing/services/EmptyMockService.service';
import { MockLoadingController } from '../../testing/services/MockLoadingController.service';
import { MockAppStoreService } from '../../testing/services/MockAppStoreService.service';
// --- ENVIRONMENT
import { EnvVariables } from '../../../environment-plugin/environment-plugin.token';
import { developmentenvironment } from '../../../environment-plugin/environment.dev';
import { productionenvironment } from '../../../environment-plugin/environment.prod';
export function environmentFactory() {
  if (process.env.IONIC_ENV === 'prod') return productionenvironment;
  return developmentenvironment;
}

describe('PAGE SplashPage', () => {
  let component: SplashPage;
  let fixture: ComponentFixture<SplashPage>;
  let loadingController: MockLoadingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        IonicModule.forRoot(SplashPage)
      ],
      providers: [
        {
          provide: EnvVariables,
          // useFactory instead of useValue so we can easily add more logic as needed.
          useFactory: environmentFactory
        },
        NavController,
        { provide: AppStoreService, useClass: MockAppStoreService },
        { provide: BackendService, useClass: EmptyMockService },
        { provide: LoadingController, useClass: MockLoadingController }
      ],
      declarations: [SplashPage]
    })
    fixture = TestBed.createComponent(SplashPage);
    component = fixture.componentInstance;
    loadingController = TestBed.get(LoadingController);
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should be created', () => {
      console.log('><[SplashPage]> should be created');
      expect(component).toBeDefined('component has not been created.');
    });
    it('Fields should be on initial state', () => {
      console.log('><[SplashPage]> "showError" should be false');
      expect(component.showError).toBeFalsy('"showError" should be false');
      console.log('><[SplashPage]> "appVersionName" should be v0.0.0');
      expect(component.appVersionName).toBe('v0.0.0', 'the initial version name is v0.0.0');
      console.log('><[SplashPage]> "errorMessage" should be undefined');
      expect(component.errorMessage).toBeUndefined('the initial value should be undefined');
      console.log('><[SplashPage]> "compatibleVersion" should be undefined');
      expect(component.compatibleVersion).toBe('0.8.4', 'the initial value should be 0.8.4');
    });
  });

  // - R E N D E R I N G   P H A S E
  describe('Rendering Phase', () => {
    it('Rendering Phase: some items should be hidden', () => {
      console.log('><[SplashPage]> Rendering Phase: some items should be hidden');

      let testItem = fixture.debugElement.query(By.css('#error-message'));
      console.log('><[SplashPage]> #error-message item');
      expect(testItem).toBeNull('#error-message item should not be visible');
    });
    it('Rendering Phase: error message should be visible', () => {
      console.log('><[SplashPage]> Rendering Phase: error message should be visible');
      component.showError = true;
      let testItem = fixture.debugElement.query(By.css('#error-message'));
      console.log('><[SplashPage]> #error-message item');
      expect(testItem).toBeDefined('#error-message item should be visible');
    });
  });

  // - L I F E C Y C L E   P H A S E
  describe('Lifecycle Phase', () => {
    it('Lifecycle: ionViewDidLoad -> first initialization', async(() => {
      console.log('><[SplashPage]> Lifecycle: ionViewDidLoad -> first initialization');
      component.ionViewDidLoad();
      expect(loadingController.getMessage()).toBe('<span class="loading-message">Comprobando credenciales y descargando datos. Por favor espere unos instantes...</span>', 'the loader panel should have a predefined text.')
      expect(loadingController.getVisibility()).toBeTruthy('the loader panel should be visible')
      expect(component.appVersionName).toBe('0.8.4 dev','appVersionName should be 0.8.4')
    }));
  });
});
