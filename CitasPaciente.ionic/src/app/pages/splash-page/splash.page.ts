//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Component } from '@angular/core';
// --- IONIC
import { IonicPage } from 'ionic-angular';
// --- COMPONENTS
import { DownloadingPageComponent } from '../../shared/core/downloading-page/downloading-page.component';
// --- MODELS
import { PatientCredential } from '../../shared/models/PatientCredential.model';
import { Centro } from '../../shared/models/Centro.model';
import { Especialidad } from '../../shared/models/Especialidad.model';
import { Medico } from '../../shared/models/Medico.model';
// import { Page } from '../../shared/models/core/Page.model';
//--- PAGES
import { HomePage } from '../../../pages/home/home.page';
import { GDPRPage } from '../gdpr-page/gdpr.page';
import { SimpleDownloadingPageComponent } from '../../shared/core/simple-downloading-page/simple-downloading-page.component';
// import { AceptacionCondicionesPage } from '../../../pages/aceptacioncondiciones/aceptacioncondiciones.page';
// import { OpenAppointmentsPage } from '../../../pages/open-appointments/open-appointments.page';
// import { AvailableLocationsPage } from '../../../pages/available-locations/available-locations.page';
// import { AvailableSpecialitiesPage } from '../../../pages/available-specialities/available-specialities.page';


// @IonicPage()
@Component({
  selector: 'splash-page',
  templateUrl: 'splash.page.html',
})
export class SplashPage extends SimpleDownloadingPageComponent {
  public showError: boolean = false; // Used to hide/show the initialization error message.
  public appVersionName: string = 'v0.0.0';
  public errorMessage: string;
  public compatibleVersion: string = '0.8.4';

  ionViewDidLoad() {
    console.log('>>[SplashPage.ionViewDidLoad]');
    // Show the configuration spinner.
    this.presentLoading('Comprobando credenciales y descargando datos. Por favor espere unos instantes...');
    this.appVersionName = this.environment.appVersion;
    // this.compatibleVersion = this.environment.appVersion;

    // Start getting access to the patient data.
    this.appStoreService.accessCredentialPromise()
      .then((credentialData) => {
        console.log('>>[SplashPage.accessCredentialPromise]');
        // Read and initialize a credential object. Get the state for the GDPR authorization and the version.
        let credential = this.readCredential(credentialData);
        let credentialVersion = credential.getVersion();
        console.log('--[SplashPage.accessCredentialPromise]> credentialVersion: ' + credentialVersion);
        if ((null != credentialVersion) && (credentialVersion === this.compatibleVersion)) {
          console.log('>>[SplashPage.accessCredentialPromise]> acceptedGDPR: ' + credential.hasAcceptedGDPR);
          // this.readCenterData();
          if (credential.hasAcceptedGDPR) {
            this.readOpenAppointments(this.appStoreService.accessCredential());
            this.dismissLoading();
            console.log('--[SplashPage.accessCredentialPromise]> Open Home page');
            this.appStoreService.updateActiveMenu();
            this.navController.setRoot(HomePage);
          } else {
            // Start the process to request the authorization to have patient data.
            this.delay(5).then(() => {
              this.dismissLoading();
              console.log('--[SplashPage.accessCredentialPromise]> Open GDPR page');
              this.navController.setRoot(GDPRPage);
            });
          }
        } else {
          // Need to get new credential data.
          this.errorMessage = 'Las credenciales disponibles necesitan actualización. Por favor rellene los nuevos datos en el siguiente formulario.';
          this.showError = true;
          this.dismissLoading();
          this.delay(5).then(() => {
            console.log('--[SplashPage.accessCredentialPromise]> Open Home page');
            this.appStoreService.updateActiveMenu();
            this.navController.setRoot(HomePage);
          });
        }
        console.log('<<[SplashPage.accessCredentialPromise]');
      });
    console.log('<<[SplashPage.ionViewDidLoad]');
  }
  private readCredential(credentialData: string): PatientCredential {
    console.log('>>[SplashPage.readCredential]');
    // - C R E D E N T I A L
    // Read the credential for the storage, store a copy on the main patient data.   
    let credential: PatientCredential;
    try {
      if (null != credentialData) {
        credential = new PatientCredential(JSON.parse(credentialData));
        // Store the credential and copy the validity.
        this.appStoreService.storeCredential(credential);
      } else {
        // Store and invalid credential on the store field.
        credential = new PatientCredential();
        this.appStoreService.storeCredential(credential);
      }
      console.log('--[SplashPage.readCredential]> Validating credential: ' + credential.isValid());
      console.log('--[SplashPage.readCredential]> Validating credential: ' + JSON.stringify(credential));
    } catch (Exception) {
      console.log('--[SplashPage.readCredential]> Validating credential [Exception: ' + Exception.message + ']');
      this.errorMessage = Exception.message;
      this.showError = true;
    }
    console.log('<<[SplashPage.readCredential]');
    return credential;
  }
  private readOpenAppointments(credential: PatientCredential): void {
    console.log('>>[SplashPage.readOpenAppointments]');
    // - O P E N   A P P O I N T M E N T S
    // Read the Open appointments list to know if there are any pending.
    // this.appStoreService.setHasOpenAppointments(false);
    if (credential.isValid()) {
      this.backendService.downloadPatientAppointments(this.appStoreService.accessCredential())
        .subscribe((appointments) => {
          console.log('--[SplashPage.readOpenAppointments]> Open Appointments: ' + appointments.length);
          this.backendService.processOpenAppointments(appointments)
          // if (appointments.length > 0) this.appStoreService.setHasOpenAppointments(true);
          this.appStoreService.updateActiveMenu();
        }, (error) => {
          console.log('--[SplashPage.readOpenAppointments]> error: ' + error.mesage);
          this.dismissLoading();
          // The single error that can be processed at this point is a missing center. All others must be reported and end at the login page or the connections failure page.
          if (error.status == 404) {
            this.appStoreService.errorNotification("El centro solicitado en la credencial no existe. Pongase en contacto con el servicio técnico", "¡Atención!");
            // this.router.navigate(['login']);
          } else {
            this.appStoreService.processBackendError(error);
          }
        });
      // } else {
      //   // We should go to the home page and only allow to get the patient data.
      //   // Finally remove the spinner and move to the home page.
      //   this.dismissLoading();
      //   // Set the menu set depending on the data status.
      this.appStoreService.updateActiveMenu();
      //   this.navController.setRoot(HomePage);
    }
    console.log('<<[SplashPage.readOpenAppointments]');
  }
  /**
   * This methos is launched after initialization to start the download and store of the data. This data is cached for only some time because the list of resources and centers usually will not change from minute to minute.
   * This data is loaded during initialization so the application should run faster.
   *
   * @private
   * @memberof SplashPage
   */
  private readCenterData(): void {
    console.log('>>[SplashPage.readCenterData]');
    // - C E N T E R S
    // Read the doctors and other backend data to be shown on the selection pages.
    this.backendService.getMedicalCenters()
      .subscribe((centroList: Centro[]) => {
        // Variable to collect the complete list of Medicos.
        let especialidadList = new Map<string, Especialidad>();
        let medicosList: Medico[] = [];
        // Iterate on all the centers to collect the data.
        for (let centro of centroList) {
          console.log('--[SplashPage.readCenterData]> Centro: ' + JSON.stringify(centro));
          // Get the list pf service providers.
          this.backendService.getMedicalServiceProviders(centro.getId())
            .subscribe((medicoList) => {
              // Process the medical serfice providers and set them on the hierarchy.
              for (let medico of medicoList) {
                // Skip Medicos with no free appointments.
                if (medico.getFreeAppointmentsCount() > 0) {
                  // Aggregate providers by specialty.
                  let spec = medico.getEspecialidad();
                  let hit = especialidadList.get(spec);
                  if (null == hit) {
                    hit = new Especialidad().setNombre(spec);
                    especialidadList.set(spec, hit);
                    // Add the speciality to the center.
                    centro.addEspecialidad(hit);
                  }
                  // Check if the Medico has already appointments. Then update the flag.
                  for (let appoint of this.appStoreService.accessOpenAppointments()) {
                    if (appoint.getMedico().getId() == medico.getId()) medico.markOpenAppointmentFlag(true);
                  }
                  hit.addMedico(medico);
                  medicosList.push(medico);
                }
                this.backendService.storeMedicos(medicosList)
                console.log('--[SplashPage.readCenterData]> Medico: ' + JSON.stringify(medico));
              }
            });
        }
        this.backendService.storeMedicos(medicosList)
        // Finally remove the spinner and move to the home page.
        // this.dismissLoading();
        // // Set the menu set depending on the data status.
        // this.appStoreService.updateActiveMenu();
        // this.navController.setRoot(HomePage);
      }, (error) => {
        this.dismissLoading();
        console.log('--[SplashPage.readCenterData]> Exception: [' + error.status + '] ' + error.message);
        if (error.status == 0) {
          this.errorMessage = 'Red no disponible. Espere unos minutos e intentelo de nuevo.'
        }
        if (error.status == 500) {
          this.errorMessage = 'Red no disponible. Espere unos minutos e intentelo de nuevo.'
        }
      });
    console.log('<<[SplashPage.readCenterData]');
  }
}
