//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { EnvVariables } from '../../environment-plugin/environment-plugin.token';
// --- IONIC
// import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Injectable()
export class IsolationService {
  // - C O N S T A N T S
  public CREDENTIAL_KEY: string;
  // - C O N S T R U C T O R
  constructor(
    @Inject(EnvVariables) public environment,
    protected storage: Storage) {
    this.CREDENTIAL_KEY = this.environment.CREDENTIAL_KEY;
  }

  // - E N V I R O N M E N T   A C C E S S
  public getServerName(): string {
    return this.environment.serverName;
  }
  public getApiV1(): string {
    return this.environment.apiVersion1;
  }
  public getApiV2(): string {
    return this.environment.apiVersion2;
  }
  public inDevelopment(): boolean {
    return this.environment.development;
  }
  public getAppName(): string {
    return this.environment.appName;
  }
  public getAppVersion(): string {
    return this.environment.appVersion;
  }

  // - S T O R A G E
  public getFromStorage(_key: string): Promise<any> {
    return this.storage.get(_key);
  }
  public setFromStorage(_key: string, _content: any): Promise<any> {
    return this.storage.set(_key, _content);
  }
}