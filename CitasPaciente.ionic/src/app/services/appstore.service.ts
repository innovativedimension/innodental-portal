//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
//--- ENVIRONMENT
import { environment } from '@env/environment';
//--- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
//--- ROUTER
import { Router } from '@angular/router';
//--- NOTIFICATIONS
import { ToastrManager } from 'ng6-toastr-notifications';
//--- SERVICES
import { BackendService } from '@app/services/backend.service';
// import { TemplateControllerService } from '@app/controllers/template-controller.service';
//--- MODELS
import { Credencial } from '@models/Credencial.model';
import { CitasTemplate } from '@models/CitasTemplate.model';
import { ActoMedicoCentro } from '@models/ActoMedicoCentro.model';
import { Medico } from '@models/Medico.model';
import { Cita } from '@models/Cita.model';
import { Especialidad } from '@models/Especialidad.model';
import { Limitador } from '@models/Limitador.model';
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';
import { ActiveCacheWrapper } from '@app/models/core/ActiveCacheWrapper.model';
import { AppointmentReport } from '@app/models/AppointmentReport.model';
import { Perfil } from '@app/models/Perfil.model';
import { AppModule } from '@app/models/AppModule.model';

@Injectable({
  providedIn: 'root'
})
export class AppStoreService {
  //--- S T O R E   D A T A   S E C T I O N
  private _doctorsActiveCache: ActiveCacheWrapper<Medico>;
  private _specialtylitiesActiveCache: ActiveCacheWrapper<Especialidad>;
  private _medicalActsActiveCache: ActiveCacheWrapper<ActoMedicoCentro>;
  private _preferredServiceActiveCache: ActiveCacheWrapper<Medico>;
  private _appointmentsSource = new BehaviorSubject(new Array<Cita>());
  private appointments = this._appointmentsSource.asObservable();

  //--- C O N S T R U C T O R
  constructor(
    // @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    // @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
    protected router: Router,
    // protected notifier: ToastrManager,
    protected backendService: BackendService) {
    //--- S T O R E   D A T A   S E C T I O N
    this._doctorsActiveCache = new ActiveCacheWrapper<Medico>()
      .setTimedCache(false)
      .setReturnObsoletes(true)
      .setDownloader((): Observable<Medico[]> => {
        return this.downloadDoctors(this.accessCredential().getCentro().id);
      });
    this._specialtylitiesActiveCache = new ActiveCacheWrapper<Especialidad>()
      .setTimedCache(false)
      .setReturnObsoletes(true)
      .setDownloader((): Observable<Especialidad[]> => {
        return this.downloadSpecialities(this.accessCredential().getCentro().id);
      });
    this._medicalActsActiveCache = new ActiveCacheWrapper<ActoMedicoCentro>()
      .setTimedCache(true)
      .setCachingTime(15 * 60)
      .setReturnObsoletes(false)
      .setDownloader((): Observable<ActoMedicoCentro[]> => {
        return this.downloadMedicalActs(this.accessCredential().getCentro().id);
      });
    this._preferredServiceActiveCache = new ActiveCacheWrapper<Medico>()
      .setTimedCache(false)
      // .setCachingTime(15 * 60)
      .setReturnObsoletes(true)
      .setDownloader((): Observable<Medico[]> => {
        let serviceData = this.backendService.getFromStorage(environment.PREFERRED_SERVICE);
        if (null != serviceData) {
          return Observable.create((observer) => {
            try {
              let service = new Medico(JSON.parse(serviceData));
              observer.next([service]);
              observer.complete();
            } catch (ParsingException) {
              console.log("EX[_preferredServiceActiveCache.setDownloader]> Exception: " + ParsingException.message);
              observer.next(null);
              // observer.complete();
            }
          });
        } else
          return Observable.create((observer) => {
            observer.next(null);
            // observer.complete();
          });
      });
  }

  //--- S T O R E   S U B J E C T S  &   V A R I A B L E S
  /** Cache for the credential profiled record. This is onli valid after a login. The GUARD should take care that this field ever stores a valid value. */
  // private _credential: Perfil;
  private _activeService: Medico;
  // public _activeTemplate: CitasTemplate = new CitasTemplate();

  //--- S T O R E   D A T A   D O W N L O A D E R S
  private downloadDoctors(_centroIdentifier: number): Observable<Medico[]> {
    return this.backendService.backendMedicalServiceProviders(_centroIdentifier)
      .pipe(map((medicos) => {
        // try {
        let especialidadList = new Map<string, Especialidad>();
        let medicoList = []; // Subproduct of reading the data.
        // Process the medical service providers and set them on the hierarchy.
        for (let medico of medicos) {
          // Skip Medicos with no free appointments.
          medicoList.push(medico);
          if (medico.getFreeAppointmentsCount() > 0) {
            // If it is not a service then process the speciality.
            if (medico.tratamiento != 'Servicio') {
              // Aggregate providers by specialty.
              let spec = medico.getEspecialidad();
              let hit = especialidadList.get(spec);
              if (null == hit) {
                hit = new Especialidad().setNombre(spec);
                especialidadList.set(spec, hit);
              }
              hit.addMedico(medico);
            }
          }
        }
        // The processing is complete. Store the results into the cache store.
        let specialityList = [];
        especialidadList.forEach((value, key) => {
          specialityList.push(value);
        })
        return medicoList;
        // } catch (exception) {
        //   this.backendService.registerException('downloadDoctors',
        //     'Exception processing the downloaded Centro data. ' + exception.message);
        //   return [];
        // }
      }));
  }
  public downloadSpecialities(_centroIdentifier: number): Observable<Especialidad[]> {
    return this.backendService.backendMedicalServiceProviders(_centroIdentifier)
      .pipe(map((medicos) => {
        // try {
        let especialidadList = new Map<string, Especialidad>();
        let medicoList = []; // Subproduct of reading the data.
        // Process the medical service providers and set them on the hierarchy.
        for (let medico of medicos) {
          // Skip Medicos with no free appointments.
          medicoList.push(medico);
          if (medico.getFreeAppointmentsCount() > 0) {
            // If it is not a service then process the speciality.
            if (medico.tratamiento != 'Servicio') {
              // Aggregate providers by specialty.
              let spec = medico.getEspecialidad();
              let hit = especialidadList.get(spec);
              if (null == hit) {
                hit = new Especialidad().setNombre(spec);
                especialidadList.set(spec, hit);
              }
              hit.addMedico(medico);
            }
          }
        }
        // The processing is complete. Store the results into the cache store.
        let specialityList = [];
        especialidadList.forEach((value, key) => {
          specialityList.push(value);
        })
        return specialityList;
        // } catch (exception) {
        //   this.backendService.registerException('fireCentroDataDownload',
        //     'Exception processing the downloaded Centro data. ' + exception.message);
        //   return [];
        // }
      }));
  }
  public downloadMedicalActs(_centroIdentifier: number): Observable<ActoMedicoCentro[]> {
    return this.backendService.backendActosMedicos4Centro(_centroIdentifier)
      .pipe(map((actos) => {
        return actos;
      }));
  }
  // private _specialtylitiesSource = new BehaviorSubject(new Array<Especialidad>());
  // private specialities = this._specialtylitiesSource.asObservable();
  // private _doctorsSource = new BehaviorSubject(new Array<Medico>());
  // private doctors = this._doctorsSource.asObservable();
  // private _medicalActsSource = new BehaviorSubject([]); // The subject support for the list.
  // private medicalActs = this._medicalActsSource.asObservable();
  private _appointmentReportSource = new BehaviorSubject([]);
  public appointmentReport = this._appointmentReportSource.asObservable();

  //--- S T O R E   A C C E S S   S E C T I O N
  /**
   * Resets and clears the cached stored contents so on next login we should reload all data.
   */
  public clearStore(): void {
    this.backendService._credential = undefined;
    this._activeService = undefined;
    // Clear dynamic caches.
    this._appointmentsSource.next([]);
    this._specialtylitiesActiveCache.clear();
    this._doctorsActiveCache.clear();
    this._medicalActsActiveCache.clear();
    // Remove data from the local session
    this.backendService.removeFromStorage(environment.CREDENTIAL_KEY);
    this.backendService.removeFromStorage(environment.SERVICE_KEY);
  }
  // - C R E D E N T I A L
  public accessLoginProfile(): Perfil {
    return this.accessCredential();
  }
  /**
  * Get the currrent credential. This should be called when the credential is already validaded so in case it is empty we should return a new empty instance.
  * @return [description]
  */
  public accessCredential(): Perfil {
    return this.backendService.accessCredential();
    // // Search the credential on the app store or the session storage.
    // if (null == this.backendService._credential) {
    //   let cred = this.sessionbackendService.getFromStorage(environment.CREDENTIAL_KEY);
    //   if (null == cred) {
    //     // If the credential is not found then we should go back to the login page.
    //     this.router.navigate(['login']);
    //   }
    //   else {
    //     this.backendService._credential = new Perfil(JSON.parse(cred));
    //     return this.backendService._credential;
    //   }
    // } else return this.backendService._credential;
  }
  /**
  * Store the credential in the app store and also on the session storage. On the session we avoid to request the credentials on every page reload. Maybe we can remove that storage when going to production.
  * @param _credential the credential to be stored.
  */
  public storeCredential(_credential: Perfil): void {
    if (null != _credential) {
      this.backendService._credential = undefined;
      this.backendService.setToStorage(environment.CREDENTIAL_KEY, JSON.stringify(_credential));
    }
  }
  // - A C T I V E   S E R V I C E
  public accessActiveService(): Medico {
    if (null == this._activeService) {
      // Add credential information to allow use of the same navigator by more that a single user.
      let identifier: string = this.accessCredential().getId();
      let serviceData = this.backendService.getFromSession(environment.SERVICE_KEY + ':' + identifier)
      if (null == serviceData) return null;
      else this._activeService = new Medico(JSON.parse(serviceData));
    }
    return this._activeService;
  }
  public storeActiveService(_service: Medico): void {
    if (null != _service) {
      this._activeService = _service;
      // Add credential information to allow use of the same navigator by more that a single user.
      let identifier: string = this.accessCredential().getId();
      this.backendService.setToSession(environment.SERVICE_KEY + ':' + identifier, JSON.stringify(_service));
    }
  }
  public clearActiveService(): void {
    this._activeService = null;
    // Add credential information to allow use of the same navigator by more that a single user.
    let identifier: string = this.accessCredential().getId();
    this.backendService.setToSession(environment.SERVICE_KEY + ':' + identifier, JSON.stringify(this._activeService));
  }
  // - A P P O I N T M E N T S
  public accessAppointments(): Observable<Cita[]> {
    return this.appointments;
  }
  public storeAppointments(_newappointments: Cita[]): void {
    this._appointmentsSource.next(_newappointments);
  }
  // - S P E C I A L I T I E S
  public accessSpecialities(): Observable<Especialidad[]> {
    return this._specialtylitiesActiveCache.accessData();
  }
  public setSpecialitities(_newdata: Especialidad[]): void {
    this._specialtylitiesActiveCache.storeData(_newdata);
  }
  //- D O C T O R S
  public accessDoctors(): Observable<Medico[]> {
    return this._doctorsActiveCache.accessData();
  }
  public clearDoctors(): void {
    this._doctorsActiveCache.clear();
  }
  // - M E D I C A L   A C T S
  public accessMedicalActs(): Observable<ActoMedicoCentro[]> {
    return this._medicalActsActiveCache.accessData();
  }
  public storeMedicalActs(_newdata: ActoMedicoCentro[]): void {
    this._medicalActsActiveCache.storeData(_newdata);
  }
  // - A C T I V E   T E M P L A T E
  /**
   * INstead having the variable we should use the template controller to manage this instances.
   *
   * @returns {CitasTemplate}
   * @memberof AppStoreService
   */
  // public accessActiveTemplate(): CitasTemplate {
  //   return this.templateController.accessTemplate();
  // }
  // - P R E F E R R E D   S E R V I C E
  // public accessPreferredService(): Observable<Medico[]> {
  //   return this._preferredServiceActiveCache.accessData();
  // }
  // public accessLastPreferredService(): Medico[] {
  //   return this._preferredServiceActiveCache.accessLastData();
  // }
  // public storePreferredService(_newdata: Medico): void {
  //   // Save the data on the storage and on the cache.
  //   this.sessionStorage.set(environment.SERVICE_KEY, JSON.stringify(_newdata));
  //   this._preferredServiceActiveCache.storeData([_newdata]);
  // }

  //--- A P P O I N T M E N T   R E P O R T
  public accessAppointmentReport(_centroId: number): Observable<AppointmentReport[]> {
    return this.appointmentReport;
  }
  public storeAppointmentReport(report: AppointmentReport[]): void {
    this.backendService.setToStorage(environment.LAST_REPORT_KEY, JSON.stringify(report));
    this._appointmentReportSource.next(report);
  }
  //--- L A S T   R E P O R T
  public readLastReport(notNull?: boolean): AppointmentReport[] {
    let reportData = this.backendService.getFromStorage(environment.LAST_REPORT_KEY);
    if (null == reportData) {
      if (null != notNull)
        if (notNull)
          return [];
    } else return JSON.parse(reportData);
    return null;
  }

  // - E N V I R O N M E N T    C A L L S
  public getApplicationName(): string {
    return this.backendService.getApplicationName()
  }
  public getApplicationVersion(): string {
    return this.backendService.getApplicationVersion()
  }
  public inDevelopment(): boolean {
    return this.backendService.inDevelopment()
  }
  public showExceptions(): boolean {
    return this.backendService.showExceptions()
  }

  // - N O T I F I C A T I O N S
  public successNotification(_message: string, _title?: string, _options?: any): void {
    this.backendService.isolation.successNotification(_message, _title, _options);
  }
  public errorNotification(_message: string, _title?: string, _options?: any): void {
    this.backendService.isolation.errorNotification(_message, _title, _options);
  }
  public warningNotification(_message: string, _title?: string, _options?: any): void {
    this.backendService.isolation.warningNotification(_message, _title, _options);
  }
  public infoNotification(_message: string, _title?: string, _options?: any): void {
    this.backendService.isolation.infoNotification(_message, _title, _options);
  }
  // private notifierConfiguration: any = {
  //   toastTimeout: 5000,
  //   newestOnTop: true,
  //   position: 'bottom-right',
  //   messageClass: 'notifier-message',
  //   titleClass: 'notifier-title',
  //   animate: 'slideFromLeft'
  // };
  // public successNotification(_message: string, _title?: string, _options?: any): void {
  //   // Join options configuration.
  //   let notConf;
  //   if (null != _options) notConf = { ...this.notifierConfiguration, ..._options };
  //   else notConf = this.notifierConfiguration;
  //   this.notifier.successToastr(_message, _title, notConf);
  // }
  // public errorNotification(_message: string, _title?: string, _options?: any): void {
  //   // Join options configuration.
  //   let notConf;
  //   if (null != _options) notConf = { ...this.notifierConfiguration, ..._options };
  //   else notConf = this.notifierConfiguration;
  //   this.notifier.errorToastr(_message, _title, notConf);
  // }
  // public warningNotification(_message: string, _title?: string, _options?: any): void {
  //   // Join options configuration.
  //   let notConf;
  //   if (null != _options) notConf = { ...this.notifierConfiguration, ..._options };
  //   else notConf = this.notifierConfiguration;
  //   this.notifier.warningToastr(_message, _title, notConf);
  // }
  // public infoNotification(_message: string, _title?: string, _options?: any): void {
  //   // Join options configuration.
  //   let notConf;
  //   if (null != _options) notConf = { ...this.notifierConfiguration, ..._options };
  //   else notConf = this.notifierConfiguration;
  //   this.notifier.infoToastr(_message, _title, notConf);
  // }

  //--- P R O P E R T I E S   S E R V I C E
  public propertiesEspecialidades(): Observable<string[]> {
    console.log("><[AppStoreServiceDefinitions.getSpecialityList]");
    // Construct the request to call the backend.
    let request = '/assets/properties/especialidades.json';
    return this.backendService.wrapHttpRESOURCECall(request)
      .pipe(map(data => {
        return data as string[];
      }));
  }
  public propertiesLimitadores(): Observable<Limitador[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesLimitadores]");
    // Construct the request to call the backend.
    let request = '/assets/properties/limitadores.json';
    return this.backendService.wrapHttpRESOURCECall(request)
      .pipe(map(data => {
        return this.backendService.transformRequestOutput(data) as Limitador[];
      }));
  }
  public propertiesAseguradoras(): Observable<string[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesAseguradoras]");
    // Construct the request to call the backend.
    let request = '/assets/properties/aseguradoras.json';
    return this.backendService.wrapHttpRESOURCECall(request)
      .pipe(map(data => {
        return data as string[];
      }));
  }
  public propertiesTemplateBuildingBlocks(): Observable<TemplateBuildingBlock[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesTemplateBuildingBlocks]");
    // Construct the request to call the backend.
    let request = '/assets/properties/template-building-blocks.json';
    return this.backendService.wrapHttpRESOURCECall(request)
      .pipe(map(data => {
        return this.backendService.transformRequestOutput(data) as TemplateBuildingBlock[];
      }));
  }
  public propertiesModulos(): Observable<AppModule[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesModulos]");
    // Construct the request to call the backend.
    let request = '/assets/properties/modulos.json';
    return this.backendService.wrapHttpRESOURCECall(request)
      .pipe(map(data => {
        return this.backendService.transformRequestOutput(data) as AppModule[];
      }));
  }
  public propertiesTratamientos(): Observable<string[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesTreatments]");
    // Construct the request to call the backend.
    let request = '/assets/properties/tratamientos.json';
    return this.backendService.wrapHttpRESOURCECall(request)
      .pipe(map(data => {
        return data as string[];
      }));
  }
  public propertiesActosMedicos(): Observable<ActoMedicoCentro[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesActosMedicos]");
    // Construct the request to call the backend.
    let request = '/assets/properties/actosmedicos.json';
    return this.backendService.wrapHttpRESOURCECall(request)
      .pipe(map(data => {
        return this.backendService.transformRequestOutput(data) as ActoMedicoCentro[];
      }));
  }

  //--- G L O B A L   A C C E S S   M E T H O D S
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }
  public isNonEmptyArray(data: any[]): boolean {
    if (null == data) return false;
    if (data.length < 1) return false;
    return true;
  }
  public isEmptyString(str: string): boolean {
    let empty = str && str.length > 0; // Or any other logic, removing whitespace, etc.
    return !empty;
  }
  public isEmptyArray(data: any[]): boolean {
    if (null == data) return true;
    if (data.length < 1) return true;
    return false;
  }
  /**
   * Adds time to a date. Modelled after MySQL DATE_ADD function.
   * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
   * https://stackoverflow.com/a/1214753/18511
   *
   * @param date  Date to start with
   * @param interval  One of: year, quarter, month, week, day, hour, minute, second
   * @param units  Number of units of the given interval to add.
   */
  public dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }
  public date2BasicISO(_date: Date): string {
    var local = new Date(_date);
    local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }
  /**
   * Funtion to validate NIF/CIF/NIE
   * @param  value the field value at least with 8 characters.
   * @return       boolean being true that the field is value.
   */
  public validateNIF(value: string): boolean {
    let validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
    let nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    let nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    let str = value.toString().toUpperCase();

    if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

    let nie = str
      .replace(/^[X]/, '0')
      .replace(/^[Y]/, '1')
      .replace(/^[Z]/, '2');

    let letter = str.substr(-1);
    let charIndex = parseInt(nie.substr(0, 8)) % 23;

    if (validChars.charAt(charIndex) === letter) return true;

    return false;
  }

  // - E R R O R   P R O C E S S I N G
  public processBackendError(_error: any): void {
    // Process any 401 exception that means the session is no longer valid.
    if (_error.status == 401) {
      this.errorNotification("La credencial ha expirado o no se encuentra.", "¡Atención!");
      this.router.navigate(['login']);
    }
    if (_error.status == 0) {
      this.errorNotification("La credencial ya no es válida. Es necesario logarse de nuevo.", "¡Atención!");
      this.router.navigate(['login']);
    }
    if (_error.name === "HttpErrorResponse") {
      this.errorNotification("Error inesperado durante las comunicaciones. Es necesario logarse de nuevo.", "¡Atención!");
      this.router.navigate(['login']);
    }
  }
}
