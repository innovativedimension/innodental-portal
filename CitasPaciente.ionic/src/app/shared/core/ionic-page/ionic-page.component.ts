//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { EnvVariables } from '../../../../environment-plugin/environment-plugin.token';
// --- IONIC
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
// --- NOTIFICACIONES
import { NotificationsService } from 'angular2-notifications';
// --- SERVICES
import { AppStoreService } from '../../services/appstore.service';
import { BackendService } from '../../services/backend.service';
// --- COMPONENTS
import { MVCViewerComponent } from '../../core/mvcviewer/mvcviewer.component';

@Component({
  templateUrl: './notused.html',
  selector: 'notused-ionic-page'
})
export class IonicPageComponent extends MVCViewerComponent {
  @Input() title: string = '-PANEL TITLE-'; // The title to setup on the panel header.
  @Input() show: boolean = true; // By default all panels are visible. Other logic should hide them.

  public canBeClosed: boolean = false; // If TRUE then we can show the close icon and activate the closing action.
  public development: boolean = false;

  // - C O N S T R U C T O R
  constructor(
    @Inject(EnvVariables) public environment,
    protected navController: NavController,
    protected navParams: NavParams,
    protected viewController: ViewController,
    protected toasterService: NotificationsService,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) {
    super();
    this.development = this.environment.development;
  }

  // - G E T T E R S   &   S E T T E R S
  public getTitle(): string {
    return this.title;
  }

  // - V I E W   I N T E R A C T I O N
  public closePanel(): void {
    this.show = false;
  }
}
