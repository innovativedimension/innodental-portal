//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- INTERFACES
import { IContainerController } from '../../interfaces/core/IContainerController.interface';
import { INode } from '../../interfaces/core/INode.interface';

@Component({
  templateUrl: './notused.html',
  selector: 'notused-mvcviewer'
})
export class MVCViewerComponent implements IContainerController {
  @Input() container: IContainerController;
  @Input() variant: string = '-DEFAULT-';

  public self: IContainerController = this; // Auto pointer to the container.

  /** Node activated by hovering over it with the mouse cursor. May be null. */
  protected selectedNode: Node = null;
  /** This exportable property will be used by the UI to know when to draw/hide the spinner. */
  public downloading: boolean = false;
	/** This is the single pointer to the model data that is contained on this page. This is the first element than when processed with the collaborate2View process will generate the complete list of nodes to render and received by the factory from the getBodyComponents().
	This variable is accessed directly (never been null) and it if shared with all descendans during the generation process. */
  protected dataModelRoot: INode[] = [];
  /** The real time updated list of nodes to render. */
  public renderNodeList: INode[] = [];
  /** This defines the rendering variant that can be used when collaborating nodes. */
  // private _variant: string = '-DEFAULT-';

  //--- C O N S T R U C T O R
  // constructor(
  //   // @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
  //   protected router: Router,
  //   protected toasterService: NotificationsService,
  //   protected appStoreService: AppStoreService) {
  // }

  // --- G E T T E R S   &   S E T T E R S
  public getVariant(): string {
    return this.variant;
  }
  public setVariant(variant: string): void {
    this.variant = variant;
  }

  //--- I V I E W E R   I N T E R F A C E
  /**
	* Return the reference to the component that knows how to locate the Page to transmit the refresh events when any user action needs to update the UI.
	*/
  public getContainer(): IContainerController {
    return this;
  }
  /**
    Reconstructs the list of nodes to be rendered from the current DataRoot and their collaborations to the view.
    */
  public notifyDataChanged(): void {
    console.log(">>[BasePageComponent.notifyDataChanged]");
    // Clear the current list while reprocessing the new nodes.
    let copyList = [];
    // Get the initial list by applying the policies defined at the page to the initial root node contents. Policies may be sorting or filtering actions.
    let initialList = this.applyPolicies(this.dataModelRoot);
    // Generate the contents by collaborating to the view all the nodes.
    for (let node of initialList) {
      let nodes = node.collaborate2View(this.getVariant());
      console.log("--[BasePageComponent.notifyDataChanged]> Collaborating " + nodes.length + " nodes.");
      // Add the collaborated nodes to the list of nodes to return.
      for (let childNode of nodes) {
        copyList.push(childNode);
      }
    }
    this.renderNodeList = copyList;
    console.log("<<[BasePageComponent.notifyDataChanged]");
  }
  public applyPolicies(_entries: INode[]): INode[] {
    return _entries;
  }

  //--- G L O B A L   F U N C T I O N A L I T Y
  /**
   * Adds time to a date. Modelled after MySQL DATE_ADD function.
   * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
   * https://stackoverflow.com/a/1214753/18511
   *
   * @param date  Date to start with
   * @param interval  One of: year, quarter, month, week, day, hour, minute, second
   * @param units  Number of units of the given interval to add.
   */
  public dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }
  public date2BasicISO(_date: Date): string {
    var local = new Date(_date);
    local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }
  public async delay(s: number) {
    return new Promise(resolve => setTimeout(resolve, s * 1000));
  }
}
