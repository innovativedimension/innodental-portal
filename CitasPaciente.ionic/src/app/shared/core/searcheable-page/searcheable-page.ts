//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Component } from '@angular/core';
import { Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
// --- ENVIRONMENT
import { EnvVariables } from '../../../../environment-plugin/environment-plugin.token';
// --- IONIC
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
// --- NOTIFICACIONES
import { NotificationsService } from 'angular2-notifications';
// --- SERVICES
import { AppStoreService } from '../../services/appstore.service';
import { BackendService } from '../../services/backend.service';
import { DoctorSearchService } from '../../services/doctorsearch.service';
// --- COMPONENTS
import { DownloadingPageComponent } from '../../core/downloading-page/downloading-page.component';
import { INode } from '../../interfaces/core/INode.interface';

// @IonicPage()
@Component({
  selector: 'notused-searcheable-page',
  templateUrl: './notused.html',
})
export class SearcheablePageComponent extends DownloadingPageComponent {
  public searchTerm: string = '';
  public searchControl: FormControl;
  public hierarchicalList: INode[] = [];

  //--- C O N S T R U C T O R
  constructor(
    @Inject(EnvVariables) public environment,
    protected navController: NavController,
    protected navParams: NavParams,
    protected viewController: ViewController,
    protected toasterService: NotificationsService,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    protected loadingController: LoadingController,
    protected dataService: DoctorSearchService) {
    super(environment, navController, navParams, viewController, toasterService, appStoreService, backendService, loadingController);
    this.searchControl = new FormControl();
  }

  //--- L I F E C Y C L E
  ionViewDidLoad() {
    this.setFilteredItems();
    this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
      this.setFilteredItems();
    });
  }
  public setFilteredItems() {
    // If the search term is not empty then we clear the current list and show the new list.
    if (this.backendService.isNonEmptyString(this.searchTerm)) {
      this.dataModelRoot = this.dataService.filterItems(this.searchTerm);
      this.notifyDataChanged();
    } else {
      this.dataModelRoot = this.hierarchicalList;
      this.notifyDataChanged();
    }
  }
}
