//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { EnvVariables } from '../../../../environment-plugin/environment-plugin.token';
// --- IONIC
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
// --- NOTIFICACIONES
import { NotificationsService } from 'angular2-notifications';
// --- SERVICES
import { AppStoreService } from '../../services/appstore.service';
import { BackendService } from '../../services/backend.service';
// --- COMPONENTS
import { IonicPageComponent } from '../../core/ionic-page/ionic-page.component';
import { MVCViewerComponent } from '../mvcviewer/mvcviewer.component';

@Component({
  templateUrl: './notused.html',
  selector: 'notused-downloading-page'
})
export class SimpleDownloadingPageComponent extends MVCViewerComponent {
  @Input() title: string = '-PANEL TITLE-'; // The title to setup on the panel header.
  @Input() show: boolean = true; // By default all panels are visible. Other logic should hide them.
  public canBeClosed: boolean = false; // If TRUE then we can show the close icon and activate the closing action.
  public development: boolean = false;
  public _loader; // Variable to store the page loader view.

  //--- C O N S T R U C T O R
  constructor(
    @Inject(EnvVariables) public environment,
    protected navController: NavController,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    protected loadingController: LoadingController) {
    super();
    this.development = this.environment.development;
  }

  // - G E T T E R S   &   S E T T E R S
  public getTitle(): string {
    return this.title;
  }

  // - V I E W   I N T E R A C T I O N
  public closePanel(): void {
    this.show = false;
  }

  //--- L O A D   N O T I F I CA T I O N   I N T E R F A C E
  protected presentLoading(_message?: string): void {
    let showMessage = 'Descargando datos. Por favor espere...';
    if (null != _message) showMessage = _message;
    this._loader = this.loadingController.create({
      content: this.styleMessage(showMessage),
      spinner: 'ios'
    });
    this._loader.present();
  }
  protected dismissLoading(): void {
    if (null != this._loader) this._loader.dismiss();
  }
  protected styleMessage(_message: string): string {
    let result = '<span class="loading-message">' + _message + '</span>';
    return result;
  }
}