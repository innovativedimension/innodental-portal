//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
// export enum ETaskState {
//   RUNNING = "RUNNING",
//   COMPLETED_OK = "COMPLETED_OK",
//   COMPLETED_FAIL = "COMPLETED_FAIL"
// }
// export enum EColorTheme {
//   RED = "RED",
//   ORANGE = "ORANGE",
//   YELLOW = "YELLOW",
//   GREEN = "GREEN",
//   BLUE = "BLUE",
//   WHITE = "WHITE",
//   BLACK = "BLACK"
// }
// export enum ETratamiento {
//   DTR = "Dr.",
//   DTRA = "Dra."
// }
// export enum ETipoCita {
//   NORMAL = "-NORMAL-",
//   EXTENDIDA = "-EXTENDIDA-",
//   LIMITADA = "-LIMITADA-"
// }
// export enum EVariant {
//   DEFAULT = "-DEFAULT-",
//   LIMITS_AVAILABLE = "-LIMITS_AVAILABLE-",
//   LIMITS_SELECTED = "-LIMITS_AVAILABLE-"
// }
