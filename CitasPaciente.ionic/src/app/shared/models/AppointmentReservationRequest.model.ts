//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
// --- MODELS
import { Node } from './core/Node.model';
import { PatientCredential } from './PatientCredential.model';
// import { Medico } from './Medico.model';

export class AppointmentReservationRequest extends Node {
  public patientData: PatientCredential;
  public identifiers: number[] = [];

  // - G E T T E R S   &   S E T T E R S
  public setPatientData(_patientData: PatientCredential): AppointmentReservationRequest {
    this.patientData = _patientData;
    return this;
  }
  public setIdentifiers(_identifiers: number[]): AppointmentReservationRequest {
    this.identifiers = _identifiers;
    return this;
  }
}
