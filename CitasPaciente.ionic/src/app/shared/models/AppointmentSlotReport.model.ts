//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- MODELS
import { Node } from '../models/core/Node.model';
import { Cita } from '../models/Cita.model';

export class AppointmentSlotReport extends Node {
  public freeAppointmentCount: number = 0;
  public firstAppointment: Cita;

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "AppointmentSlotReport";

    // Transform dependency objects
    if (null != this.firstAppointment) {
      let cita = new Cita(this.firstAppointment);
      this.firstAppointment = cita;
    }
  }

  //--- G E T T E R S   &   S E T T E R S
}
