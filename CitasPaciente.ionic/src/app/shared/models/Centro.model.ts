//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- SERVICES
// import { AppStoreService } from '../services/appstore.service';
//--- INTERFACES
// import { INode } from '../interfaces/core/INode.interface';
//--- MODELS
import { Node } from './core/Node.model';
import { Especialidad } from './Especialidad.model';
import { Medico } from './Medico.model';

export class Centro extends Node {
  public id: number = -1;
  public nombre: string = "-Seleccione un Centro-";
  public logotipo: string = "/assets/images/notfound.jpg";
  public localidad: string;
  public direccion: string;
  private _especialidades: Especialidad[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Centro";
    // Do not show empty Centros.
    // this.renderWhenEmpty = false;
  }

  //--- I C O L L A B O R A T I O N   I N T E R F A C E
  // public collaborate2View(/*backendService: AppStoreService*/): INode[] {
  //   let collab: INode[] = [];
  //   // Collaborate the node only if not empty.
  //   if (this.isEmpty()) {
  //     if (this.renderWhenEmpty) {
  //       collab.push(this);
  //     }
  //   } else collab.push(this);
  //   for (let node of this._especialidades) {
  //     let partialcollab = node.collaborate2View(/*backendService*/);
  //     for (let partialnode of partialcollab) {
  //       collab.push(partialnode);
  //     }
  //   }
  //   return collab;
  // }

  //--- I E X P A N D A B L E   I N T E R F A C E
  public isEmpty(): boolean {
    if (this._especialidades.length > 0) return false;
    return true;
  }
  public getContentSize(): number {
    return this._especialidades.length;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getId(): number {
    return this.id;
  }
  public getNombre(): string {
    return this.nombre;
  }
  public addEspecialidad(esp: Especialidad): number {
    this._especialidades.push(esp);
    return this._especialidades.length;
  }
  public getEspecialidades(): Especialidad[] {
    return this._especialidades;
  }
  public addMedico(_medico: Medico): void {
    // Check if the speciality already is on the list.
    let especialidad = _medico.getEspecialidad();
    let found: boolean = false;
    for (let esp of this._especialidades) {
      if (esp.nombre === especialidad) {
        esp.addMedico(_medico);
        found = true;
      }
    }
    if (!found) {
      let esp = new Especialidad()
        .setNombre(especialidad);
      esp.addMedico(_medico);
    }
  }
}
