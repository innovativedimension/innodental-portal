//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
export class ChangeDateForm {
  public newDate: Date = new Date();

  public toDateInputValue() {
    var local = new Date(this.newDate);
    local.setMinutes(this.newDate.getMinutes() - this.newDate.getTimezoneOffset());
    return local.getDate() + "/" + local.getMonth() + 1 + "/" + local.getFullYear();
  }
  public toDateRequest() {
    var local = new Date(this.newDate);
    local.setMinutes(this.newDate.getMinutes() - this.newDate.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }
}
