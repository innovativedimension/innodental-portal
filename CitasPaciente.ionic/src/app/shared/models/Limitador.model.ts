//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- INTERFACES
import { INode } from '../interfaces/core/INode.interface';
//--- MODELS
import { Node } from './core/Node.model';
// import { Cita } from './Cita.model';
// import { MinuteInterval } from './MinuteInterval.model';

export class Limitador extends Node implements INode {
  // public id: number = -1; // Unique identifier for this database persistent instance.
  public tipo: string = "-BLOCKING-"; // Sets the tipo of limit to apply. BLOCKING or PERCENT.
  // public clase: string = "-BELOW-BLOCKING-" // Sets the under or over side of the limit as the blocking.
  public icon: string; // The reference to the icon for this limit.
  public titulo: string = '-LIMITE-'; // The title to show for the limit.
  public descripcion: string; // The explanation text that describes the limit.
  public valorLimite: number = 10; // The numeric value limit that applies to the limit.
  public valorReferencia: string; // Field for the reference parameter affected by this limit.
  public periodo: string = "-DAY-";
  // public regla: string; // Value of the rule. The assurance corporation or the rule script.
  public activeState: boolean = false; // The boolean state. If true the limit is active.

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Limitador";
  }

  //--- G E T T E R S   &   S E T T E R S
  public hasEqualValues(_target: Limitador): boolean {
    let valme = this.tipo + this.titulo + this.valorLimite + this.valorReferencia + this.periodo + this.activeState;
    let valTarget = "-";
    if (null != _target) valTarget = _target.tipo + _target.titulo + _target.valorLimite + _target.valorReferencia + _target.periodo + _target.activeState;
    return valme === valTarget;
  }
}
