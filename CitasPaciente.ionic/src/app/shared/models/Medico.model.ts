//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- INTERFACES
import { INode } from '../interfaces/core/INode.interface';
import { IAppointmentEnabled } from '../interfaces/IAppointmentEnabled.interface';
// import { IExpandable } from '../interfaces/core/IExpandable.interface';
//--- MODELS
import { Node } from './core/Node.model';
import { Cita } from './Cita.model';
import { ActoMedico } from './ActoMedico.model';
import { Centro } from './Centro.model';

export class Medico extends Node implements IAppointmentEnabled {
  public id: string = '';
  public referencia: string;
  public tratamiento: string;
  public nombre: string = "";
  public apellidos: string = "";
  public especialidad: string;
  public activo: boolean = true;
  public localidad: string;
  public direccion: string;
  public horario: string;
  public telefono: string;
  public appointmentsCount: number = 0;
  public freeAppointmentsCount: number = 0;
  public firstAppointment: Cita = new Cita();
  public hasOpenAppointmentFlag: boolean = false;

  private actosMedicos: ActoMedico[] = [];

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Medico";

    // Transform dependency objects
    if (null != this.firstAppointment) {
      let firstCita = new Cita(this.firstAppointment);
      this.firstAppointment = firstCita;
    }
    if (null != this.actosMedicos) {
      let newactos = [];
      for (let acto of this.actosMedicos) {
        newactos.push(new ActoMedico(acto));
      }
      this.actosMedicos = newactos;
    }
  }

  //--- I C O L L A B O R A T I O N   I N T E R F A C E
  public collaborate2View(): INode[] {
    let collab: INode[] = [];
    collab.push(this);
    return collab;
  }

  //--- I A P P O I N T M E N T E N A B L E D   I N T E R F A C E
  /**
   * Return the unique identifier to locate the appointments for this provider. This should be tied to the current Consulta concept and not to the broad Medico concept because searches are local to Centros.
   * @return the unique identifier to be used to locate this instace appointments.
   */
  public getServiceIdentifiers(): string[] {
    let result: string[] = [];
    result.push(this.id);
    return result;
  }
  public getEspecialidad(): string {
    if (this.isNonEmptyString(this.especialidad)) return this.especialidad;
    else return "Médico Familia";
  }

  //--- I E X P A N D A B L E   I N T E R F A C E
  // public isEmpty(): boolean {
  //   // if (this._citas.length > 0) return false;
  //   return true;
  // }
  // public getContentSize(): number {
  //   // return this._citas.length;~
  //   return 0;
  // }
  public generateUniqueIdentifier(_associatedCenter: Centro): string {
    return 'MID:' + _associatedCenter.getId() + ':' + this.referencia;
  }
  //--- G E T T E R S   &   S E T T E R S
  public getId(): string {
    return this.id;
  }
  public getReferencia(): string {
    return this.referencia;
  }
  public getNombre(): string {
    return this.nombre;
  }
  public setId(newuniqueid: string): Medico {
    this.id = newuniqueid;
    return this;
  }
  public setReferencia(newreferencia: string): Medico {
    this.referencia = newreferencia;
    return this;
  }
  public setTratamiento(newtratamiento: string): Medico {
    this.tratamiento = newtratamiento;
    return this;
  }
  public setNombre(newnombre: string): Medico {
    this.nombre = newnombre;
    return this;
  }
  public setApellidos(newapellidos: string): Medico {
    this.apellidos = newapellidos;
    return this;
  }
  public setEspecialidad(newespecialidad: string): Medico {
    this.especialidad = newespecialidad;
    return this;
  }
  public setLocalidad(newlocalidad: string): Medico {
    this.localidad = newlocalidad;
    return this;
  }
  public setDireccion(newdireccion: string): Medico {
    this.direccion = newdireccion;
    return this;
  }
  public setHorario(newhorario: string): Medico {
    this.horario = newhorario;
    return this;
  }
  public setTelefono(newtelefono: string): Medico {
    this.telefono = newtelefono;
    return this;
  }
  public getActosMedicos(): ActoMedico[] {
    if (null == this.actosMedicos) return [];
    else return this.actosMedicos;
  }
  public addActoMedico(_newacto: ActoMedico): number {
    this.actosMedicos.push(_newacto);
    return this.actosMedicos.length;
  }
  public removeActoMedico(_target2Remove: ActoMedico): ActoMedico {
    let found: boolean = false;
    let newList: ActoMedico[] = [];
    if (null != _target2Remove)
      if (null != this.actosMedicos){
        for (let acto of this.actosMedicos) {
          if (_target2Remove.equals(acto))
            found = true;
          else
            newList.push(acto);
        }
        // Copy the result to the list of actos medicos.
        this.actosMedicos = newList;
      }
    if (found) return _target2Remove;
    else return null;
  }
  public getFreeAppointmentsCount(): number {
    return this.freeAppointmentsCount;
  }
  public getFirstAppointment(): Cita {
    if (null != this.firstAppointment) {
      return this.firstAppointment;
    } else return new Cita();
  }
  public markOpenAppointmentFlag(state: boolean): boolean {
    this.hasOpenAppointmentFlag = state;
    return this.hasOpenAppointmentFlag;
  }
  public isService(): boolean {
    if (this.isNonEmptyString(this.tratamiento))
      if (this.tratamiento == 'Servicio') return true;
    return false;
  }
}
