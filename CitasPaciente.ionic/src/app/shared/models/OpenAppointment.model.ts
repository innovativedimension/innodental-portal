//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
// --- FUNCTIONS
import { isAfter } from 'date-fns';
import { differenceInCalendarDays } from 'date-fns';
// --- MODELS
import { Node } from '../models/core/Node.model';
import { Medico } from '../models/Medico.model';
import { Cita } from '../models/Cita.model';

export class OpenAppointment extends Node {
  public cita: Cita;
  public medico: Medico;
  public consulta: Medico;

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "OpenAppointment";

    // Transform dependency objects
    if (null != this.cita) {
      let cita = new Cita(this.cita);
      this.cita = cita;
    }
    if (null != this.medico) {
      let medico = new Medico(this.medico);
      this.medico = medico;
    }
    this.consulta = this.medico;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getId(): number {
    if (null != this.cita) return this.cita.getId();
    else return -1;
  }
  public getEstado(): string {
    if (null != this.cita) return this.cita.estado;
    else return 'INDEFINIDO';
  }
  public getCita(): Cita {
    if (null != this.cita) return this.cita;
    else return new Cita();
  }
  public getMedico(): Medico {
    if (null != this.medico) return this.medico;
    else return new Medico();
  }
  public getConsulta(): Medico {
    if (null != this.medico) return this.medico;
    else return new Medico();
  }
  public setCita(newcita: Cita): OpenAppointment {
    this.cita = newcita;
    return this;
  }
  public setMedico(newmedico: Medico): OpenAppointment {
    this.medico = newmedico;
    return this;
  }
  public isOpen(): boolean {
    if (null != this.cita)
      if (this.cita.estado === 'RESERVADA') {
        return isAfter(this.cita.getFecha(), new Date())
      }
    return false;
  }
  public isToday(): boolean {
    if (this.isOpen()) {
      let days = differenceInCalendarDays(this.cita.getFecha(), new Date());
      if (days === 0) return true
    }
    return false;
  }
}
