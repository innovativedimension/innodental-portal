//  PROJECT:     Citas.library(CIT.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  SITE:        citascentro.com
//  DESCRIPTION: Citas. Componente libreria. Este proyecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitasCentro/CitasPaciente).
//--- MODELS
// import { Node } from './core/Node.model';
import { Cita } from './Cita.model';
// import { AppointmentSlotReport } from './AppointmentSlotReport.model';

export class Payload {
  public open: number = 0;
  public reserved: number = 0;
  public citas: Cita[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    // super(values);
    Object.assign(this, values);
    // this.jsonClass = "Payload";

    // Transform dependency objects
    if (null != this.citas) {
      let listCitas = [];
      for (let ct of this.citas) {
        listCitas.push(new Cita(ct));
      }
      this.citas = listCitas;
    }
  }

  //--- G E T T E R S   &   S E T T E R S
  public addOpen(): number {
    return this.open++;
  }
  public addReserved(): number {
    this.open++;
    return this.reserved++;
  }
  public getOpen(): number {
    return this.open;
  }
  public getReserved(): number {
    return this.reserved;
  }
  public addCita(_newcita: Cita): void {
    this.citas.push(_newcita);
  }
  public getFreeAppointmentsCount(): number {
    return this.citas.length;
  }
}
