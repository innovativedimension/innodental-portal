//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.

export class Page {
  public title: string;
  public iconName: string = "ion-ios-contact";
  public component: any;

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    Object.assign(this, values);
    // this.jsonClass = "Node";
  }
  //--- G E T T E R S   &   S E T T E R S
  public getTitle(): string {
    return this.title;
  }
  public getIconName(): string {
    return this.iconName;
  }
  public getPageComponent(): any {
    return this.component;
  }
  public setTitle(_title: string): Page {
    this.title = _title;
    return this;
  }
  public setIconName(_name: string): Page {
    this.iconName = _name;
    return this;
  }
  public setPageComponent(_page: any): Page {
    this.component = _page;
    return this;
  }
}
