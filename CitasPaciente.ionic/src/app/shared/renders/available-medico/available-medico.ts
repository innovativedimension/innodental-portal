//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
// --- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
// --- COMPONENTS
import { RenderIonicComponent } from '../render-ionic/render-ionic.component';
// --- MODELS
import { ActoMedico } from '../../models/ActoMedico.model';
import { Medico } from '../../models/Medico.model';
//--- PAGES
import { AppointmentsCalendarPage } from '../../../../pages/appointments-calendar/appointments-calendar.page';

@Component({
  selector: 'available-medico',
  templateUrl: 'available-medico.html'
})
export class AvailableMedicoComponent extends RenderIonicComponent {
  @Input() medico: Medico;
  @Input() variant: string = '-DEFAULT-';

  public development: boolean = false;

  //--- C O N S T R U C T O R
  // constructor(
  //   @Inject(EnvVariables) public environment,
  //   protected navCtrl: NavController,
  //   protected appStoreService: AppStoreService,
  //   protected backendService: BackendService) {
  //   this.development = this.environment.development;
  // }

  // - R E N D E R   I N T E R F A C E
  public getVariant(): string {
    if (null == this.variant) return '-DEFAULT-';
    else return this.variant;
  }
  public setVariant(_newvariant: string): AvailableMedicoComponent {
    this.variant = _newvariant;
    return this;
  }

  // - V I E W   I N T E R A C T I O N
  /**
   * There are two options for this function. If has no parameters then we should check if the click is processed. To process it the Medico should not have any medical act. If the Medico has Actos Medicos then we should select one of them.
   *
   * @param {ActoMedico} [_target]
   * @memberof AvailableMedicoComponent
   */
  public medicClick(_target?: ActoMedico): void {
    if (null == _target) {
      // Check the number of medical acts.
      let acts = this.getMedicalActs();
      if (acts.length > 0) return;
      else {
        // process the click as the defaul for the Medico.
        if (!this.medico.hasOpenAppointmentFlag) {
          // Set the selected Medico to be used on the backend interchange.
          this.appStoreService.storeSelectedMedico(this.medico);
          // this.presentLoading();
          this.navCtrl.push(AppointmentsCalendarPage, { 'providerIdentifier': this.medico.id });
        }
      }
    } else {
      // User should have selected a medical act.
      if (!this.medico.hasOpenAppointmentFlag) {
        // Set the selected Medico to be used on the backend interchange.
        this.appStoreService.storeSelectedMedico(this.medico);
        this.appStoreService.storeSelectedActoMedico(_target);
        // this.presentLoading();
        this.navCtrl.push(AppointmentsCalendarPage, { 'providerIdentifier': this.medico.id });
      }
    }
  }
  public hasMedicalActs(): boolean {
    if (this.getMedicalActs().length > 0) return true;
    else return false;
  }

  // - G E T T E R S   &   S E T T E R S
  public getMedicalActs(): ActoMedico[] {
    if (null != this.medico) return this.medico.getActosMedicos();
    else return [];
  }

  //--- T R A N S F O R M A T I O N S
  public getAppointmentDateDisplay(): string {
    // Get the date for the first appointment.
    if (null != this.medico)
      if (null != this.medico.getFirstAppointment()) {
        let cita = this.medico.getFirstAppointment();
        let display = "";
        display = display + this.environment.WEEKDAYS[cita.getFecha().getDay()] + ', ';
        display = display + cita.getFecha().getDate() + ' ' + this.environment.MONTHNAMES[cita.getFecha().getMonth()] + ' ';
        display = display + cita.getFecha().getFullYear();
        return display;
      }
    return '';
  }
  public hasFreeAppointments(): boolean {
    if (this.medico.freeAppointmentsCount > 0) return true;
    else return false;
  }
}
