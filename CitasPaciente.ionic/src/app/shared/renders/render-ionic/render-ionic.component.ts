//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE
import { Component } from '@angular/core';
import { Inject } from '@angular/core';
// --- IONIC
import { NavController } from 'ionic-angular';
// --- ENVIRONMENT
import { EnvVariables } from '../../../../environment-plugin/environment-plugin.token';
// --- SERVICES
import { AppStoreService } from '../../services/appstore.service';
import { BackendService } from '../../services/backend.service';
// --- COMPONENTS
import { RenderComponent } from '../../renders/render/render.component';

@Component({
  selector: 'notused-render-ionic',
  templateUrl: './notused.html'
})
export class RenderIonicComponent extends RenderComponent {
  //--- C O N S T R U C T O R
  constructor(
    @Inject(EnvVariables) public environment,
    protected navCtrl: NavController,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) {
    super();
    this.development = this.environment.development;
  }
}
