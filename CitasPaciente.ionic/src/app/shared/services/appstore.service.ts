//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { EnvVariables } from '../../../environment-plugin/environment-plugin.token';
// --- NOTIFICATIONS
import { NotificationsService } from 'angular2-notifications';
// --- IONIC
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// -- HTTP PACKAGE
// import { HttpClient } from '@angular/common/http';
// --- MODELS
import { Medico } from '../models/Medico.model';
import { ActoMedico } from '../models/ActoMedico.model';
import { PatientCredential } from '../models/PatientCredential.model';
import { Centro } from '../models/Centro.model';
import { Especialidad } from '../models/Especialidad.model';
import { Cita } from '../models/Cita.model';
import { OpenAppointment } from '../models/OpenAppointment.model';
import { Page } from '../models/core/Page.model';
import { HomePage } from '../../../pages/home/home.page';
import { BackendService } from './backend.service';
//--- PAGES
// import { HomePage } from '../../../pages/home/home.page';
// import { AceptacionCondicionesPage } from '../../../pages/aceptacioncondiciones/aceptacioncondiciones.page';
// import { OpenAppointmentsPage } from '../../../pages/open-appointments/open-appointments.page';
// import { AvailableSpecialitiesPage } from '../../../pages/available-specialities/available-specialities.page';

@Injectable()
export class AppStoreService {
  // protected _credential: PatientCredential; // Field to store the Patient data. This is a copy of the storage data.

  // - P R O G R E S S   D A T A   S T O R E
  protected _selectedCentro: Centro;
  protected _selectedEspecialidad: Especialidad;
  protected _selectedMedico: Medico;
  protected _selectedActoMedico: ActoMedico;
  protected _selectedCita: Cita;
  // protected _openAppointments: OpenAppointment[] = [];
  // protected _hasOpenAppointments: boolean = false;

  // - M E N U   S E T S
  public _activeMenu: Page[] = [];
  public menuFull: Page[] = [];
  public menuNotCredential: Page[] = [];
  public menuNotOpenAppointments: Page[] = [];

  // - C O N S T R U C T O R
  constructor(
    // @Inject(EnvVariables) public environment,
    // protected storage: Storage,
    // ,protected navController: NavController
    protected notifier: NotificationsService,
    protected backendService: BackendService) { }

  // - C R E D E N T I A L
  public accessCredential(): PatientCredential {
    return this.backendService.accessCredential();
    // if (null != this._credential) return this._credential;
    // else {
    //   // Fire the credential load from the storage data for next calls.
    //   this.storage.get(this.environment.CREDENTIAL_KEY)
    //     .then((data) => {
    //       console.log('--[AppStoreService.accessCredential]> Credential data: ', JSON.stringify(data));
    //       if (null == data) {
    //         this._credential = null;
    //         console.log('<<[AppStoreService.accessCredential]> Credential is empty');
    //       } else {
    //         this._credential = new PatientCredential(JSON.parse(data));
    //         console.log('<<[AppStoreService.accessCredential]> Credential found');
    //       }
    //     })
    //     .catch((error) => {
    //       console.log('<<[AppStoreService.accessCredential]> Credential not found: ', JSON.stringify(error));
    //     });
    //   return new PatientCredential();
    // }
  }
  public accessCredentialPromise(): Promise<string> {
    return this.backendService.accessCredentialPromise();
    // Fire the credential load from the storage data for next calls.
    // return this.storage.get(this.environment.CREDENTIAL_KEY);
  }
  public clearCredential(): void {
    this.backendService.clearCredential();
    // this._credential = null;
  }
  public storeCredential(_newcredential: PatientCredential): PatientCredential {
    return this.backendService.storeCredential(_newcredential);
    // this._credential = _newcredential;
    // // Store it also on the session storage.
    // this.storage.set(this.environment.CREDENTIAL_KEY, JSON.stringify(_newcredential));
    // return this._credential;
  }
  // - C E N T R O
  public accessSelectedCentro(): Centro {
    return this._selectedCentro;
  }
  public storeSelectedCentro(_centro: Centro): void {
    this._selectedCentro = _centro;
  }
  // - E S P E C I A L I D A D
  public accessSelectedEspecialidad(): Especialidad {
    return this._selectedEspecialidad;
  }
  public storeSelectedEspecialidad(_especialidad: Especialidad): void {
    this._selectedEspecialidad = _especialidad;
  }
  // - M E D I C O
  public accessSelectedMedico(): Medico {
    return this._selectedMedico;
  }
  public storeSelectedMedico(_medico: Medico): void {
    this._selectedMedico = _medico;
  }
  // - A C T O M E D I C O
  public accessSelectedActoMedico(): ActoMedico {
    return this._selectedActoMedico;
  }
  public storeSelectedActoMedico(_actomedico: ActoMedico): void {
    this._selectedActoMedico = _actomedico;
  }
  // - C I T A
  public accessSelectedCita(): Cita {
    return this._selectedCita;
  }
  public storeSelectedCita(_cita: Cita): void {
    this._selectedCita = _cita;
  }
  // - O P E N   A P P O I N T M E N T S
  public getHasOpenAppointments(): boolean {
    return this.backendService.getHasOpenAppointments();
  }
  // public setHasOpenAppointments(_newstate: boolean): void {
  //   this._hasOpenAppointments = _newstate;
  // }
  public accessOpenAppointments(): OpenAppointment[] {
    return this.backendService.accessOpenAppointments();
  }
  public storeOpenAppointments(_newlist: OpenAppointment[]): void {
    this.backendService.storeOpenAppointments(_newlist);
  }

  // - M E N U   M A N A G E M E N T
  public prepareMenus(): void {
    // Full menu.
    // this.menuFull=[];
    // this.menuFull.push(new Page().setTitle('Inicio')
    //   .setPageComponent(HomePage)
    //   .setIconName('ios-home'));
    // this.menuFull.push(new Page().setTitle('Datos Paciente')
    //   .setPageComponent(AceptacionCondicionesPage)
    //   .setIconName('ios-person'));
    // this.menuFull.push(new Page().setTitle('Medicos por Especialidad')
    //   .setPageComponent(AvailableSpecialitiesPage)
    //   .setIconName('ios-contacts'));
    // // this.menuFull.push(new Page().setTitle('Localizaciones')
    // //   .setPageComponent(AvailableLocationsPage)
    // //   .setIconName('navigate'));
    // this.menuFull.push(new Page().setTitle('Citas Abiertas')
    //   .setPageComponent(OpenAppointmentsPage)
    //   .setIconName('md-calendar'));

    // // Invalid credential.
    // this.menuNotCredential.push(new Page().setTitle('Inicio')
    //   .setPageComponent(HomePage)
    //   .setIconName('ios-home'));
    // this.menuNotCredential.push(new Page().setTitle('Datos Paciente')
    //   .setPageComponent(AceptacionCondicionesPage)
    //   .setIconName('ios-person'));

    // // No open appointments.
    // this.menuNotOpenAppointments.push(new Page().setTitle('Inicio')
    //   .setPageComponent(HomePage)
    //   .setIconName('ios-home'));
    // this.menuNotOpenAppointments.push(new Page().setTitle('Datos Paciente')
    //   .setPageComponent(AceptacionCondicionesPage)
    //   .setIconName('ios-person'));
    // this.menuNotOpenAppointments.push(new Page().setTitle('Medicos por Especialidad')
    //   .setPageComponent(AvailableSpecialitiesPage)
    //   .setIconName('ios-contacts'));

    // // Set the initial menu to the small menu.
    // this._activeMenu = this.menuNotCredential;
  }
  public updateActiveMenu() {
    this._activeMenu = this.menuNotCredential;
    let validCredential = this.accessCredential().isValid();
    if (validCredential) {
      this._activeMenu = this.menuNotOpenAppointments;
      if (this.backendService.getHasOpenAppointments())
        this._activeMenu = this.menuFull;
    }
  }
  public accessActiveMenu(): Page[] {
    return this._activeMenu;
  }

  // - N O T I F I C A T I O N S
  public successNotification(_message: string, _title?: string, _options?: any): void {
    this.notifier.success(_title, _message, _options);
  }
  public errorNotification(_message: string, _title?: string, _options?: any): void {
    this.notifier.error(_title, _message, _options);
  }
  public warningNotification(_message: string, _title?: string, _options?: any): void {
    this.notifier.warn(_title, _message, _options);
  }
  public infoNotification(_message: string, _title?: string, _options?: any): void {
    this.notifier.info(_title, _message, _options);
  }

  //--- G L O B A L   A C C E S S   M E T H O D S
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }
  public isNonEmptyArray(data: any[]): boolean {
    if (null == data) return false;
    if (data.length < 1) return false;
    return true;
  }
  public isEmptyString(str: string): boolean {
    let empty = str && str.length > 0; // Or any other logic, removing whitespace, etc.
    return !empty;
  }
  public isEmptyArray(data: any[]): boolean {
    if (null == data) return true;
    if (data.length < 1) return true;
    return false;
  }
  /**
   * Adds time to a date. Modelled after MySQL DATE_ADD function.
   * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
   * https://stackoverflow.com/a/1214753/18511
   *
   * @param date  Date to start with
   * @param interval  One of: year, quarter, month, week, day, hour, minute, second
   * @param units  Number of units of the given interval to add.
   */
  public dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }
  public date2BasicISO(_date: Date): string {
    var local = new Date(_date);
    local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }
  /**
   * Funtion to validate NIF/CIF/NIE
   * @param  value the field value at least with 8 characters.
   * @return       boolean being true that the field is value.
   */
  public validateNIF(value: string): boolean {
    let validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
    let nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    let nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    let str = value.toString().toUpperCase();

    if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

    let nie = str
      .replace(/^[X]/, '0')
      .replace(/^[Y]/, '1')
      .replace(/^[Z]/, '2');

    let letter = str.substr(-1);
    let charIndex = parseInt(nie.substr(0, 8)) % 23;

    if (validChars.charAt(charIndex) === letter) return true;

    return false;
  }

  // - E R R O R   P R O C E S S I N G
  public processBackendError(_error: any): void {
    // Process any 401 exception that means the session is no longer valid.
    if (_error.status == 401) {
      this.errorNotification("Problemas durante la autenticación del aplicativo. No es posible continuar.", "¡Atención!");
      // this.navController.setRoot(HomePage);
    }
    if (_error.name === "HttpErrorResponse") {
      this.errorNotification("Error inesperado durante las comunicaciones. Es necesario logarse de nuevo.", "¡Atención!");
      // this.navController.setRoot(HomePage);
    }
  }
}
