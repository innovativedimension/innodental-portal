//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
//--- IONIC
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CacheService } from 'ionic-cache';
//--- HTTP PACKAGE
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
// --- SERVICES
// import { IonicIsolationService } from '../../platform/ionic.isolation.service';
//--- MODEL
import { Node } from '../models/core/Node.model';
import { Centro } from '../models/Centro.model';
import { Medico } from '../models/Medico.model';
import { Cita } from '../models/Cita.model';
import { OpenAppointment } from '../models/OpenAppointment.model';
import { PatientCredential } from '../models/PatientCredential.model';
import { ChangeDateForm } from '../models/core/ChangeDateForm.model';
import { Especialidad } from '../models/Especialidad.model';
import { INode } from '..//interfaces/core/INode.interface';
import { AppointmentReservationRequest } from '../models/AppointmentReservationRequest.model';
import { EnvVariables } from '../../../environment-plugin/environment-plugin.token';
import { AppStoreService } from './appstore.service';
import { IsolationService } from '../../platform/isolation.service';

/**
This service will store persistent application data and has the knowledge to get to the backend to retrieve any data it is requested to render on the view.
*/
@Injectable()
export class BackendService {
  //--- C A C H E   S T O R E
  protected _credential: PatientCredential; // Field to store the Patient data. This is a copy of the storage data.
  protected _openAppointments: OpenAppointment[] = [];
  // private _pages: Page[] = [];
  // public credential: PatientCredential;
  // public openAppointments: OpenAppointment[] = [];
  public centros: Centro[] = [];
  public especialidades: Especialidad[] = [];
  public medicos: Medico[] = [];
  // public medico: Medico = null;
  public appointments: Cita[] = [];

  //--- C O N S T R U C T O R
  constructor(
    protected isolation: IsolationService,
    // @Inject(EnvVariables) public environment,
    // public platform: Platform,
    // public statusBar: StatusBar,
    // public splashScreen: SplashScreen,
    protected http: HttpClient,
    // protected cache: CacheService,
    // protected storage: Storage,
    // protected appStoreService: AppStoreService
  ) {
    // cache.setDefaultTTL(60 * 2); // set to 2 minutes.
  }

  //--- M O C K   S E C T I O N
  public getMockStatus(): boolean {
    return false;
  }
  // Define mock data references to input data on files.
  protected responseTable = {
    // CENTROS
    // '/api/v1/centros':
    //   '/assets/mockData/centros.json',
    // '/api/v1/centros/1000052/medicos':
    //   '/assets/mockData/proveedores-1000052.json'
  }
  public inDevelopment(): boolean {
    return this.isolation.inDevelopment();
  }
  //--- G L O B A L   F U C T I O N A L I T I E S
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }

  // - S T O R E   A C C E S S   S E C T I O N
  // - C R E D E N T I A L
  public accessCredential(): PatientCredential {
    if (null != this._credential) return this._credential;
    else {
      // Fire the credential load from the storage data for next calls.
      this.isolation.getFromStorage(this.isolation.CREDENTIAL_KEY)
        .then((data) => {
          console.log('--[AppStoreService.accessCredential]> Credential data: ', JSON.stringify(data));
          if (null == data) {
            this._credential = null;
            console.log('<<[AppStoreService.accessCredential]> Credential is empty');
          } else {
            this._credential = new PatientCredential(JSON.parse(data));
            console.log('<<[AppStoreService.accessCredential]> Credential found');
          }
        })
        .catch((error) => {
          console.log('<<[AppStoreService.accessCredential]> Credential not found: ', JSON.stringify(error));
        });
      return new PatientCredential();
    }
  }
  public accessCredentialPromise(): Promise<string> {
    // Fire the credential load from the storage data for next calls.
    return this.isolation.getFromStorage(this.isolation.CREDENTIAL_KEY);
  }
  public clearCredential(): void {
    this._credential = null;
  }
  public storeCredential(_newcredential: PatientCredential): PatientCredential {
    this._credential = _newcredential;
    // Store it also on the session storage.
    this.isolation.setFromStorage(this.isolation.CREDENTIAL_KEY, JSON.stringify(_newcredential));
    return this._credential;
  }

  // - O P E N   A P P O I N T M E N T S
  public getHasOpenAppointments(): boolean {
    if (this._openAppointments.length > 0) return true;
    else return false;
  }
  // public setHasOpenAppointments(_newstate: boolean): void {
  //   this._hasOpenAppointments = _newstate;
  // }
  public accessOpenAppointments(): OpenAppointment[] {
    return this._openAppointments;
  }
  public storeOpenAppointments(_newlist: OpenAppointment[]): void {
    this._openAppointments = _newlist;
  }
  public processOpenAppointments(_appointments: OpenAppointment[]): OpenAppointment[] {
    // this.downloadPatientAppointments(this.appStoreService.accessCredential())
    //   .subscribe((appointments) => {
    // Check for duplicated appointmes or marked as free.
    let appointmentList: OpenAppointment[] = [];
    let activeAppointment: Cita;
    for (let appo of _appointments) {
      console.log("--[BackendService.processOpenAppointments]> Appointment: " + appo.getId() +
        ' state: ' + appo.getEstado() + ' medico: ' + appo.getCita().medicoIdentifier);
      // Check if the current appointment is active.
      if (null == activeAppointment) {
        activeAppointment = appo.getCita();
        if (appo.getCita().estado == 'RESERVADA')
          if (appo.isOpen()) appointmentList.push(appo);
      } else {
        // Check for consecutive appointments.
        if (appo.getCita().medicoIdentifier == activeAppointment.medicoIdentifier) {
          // I am almost sure I can pack it. Check the date.
          if (appo.getCita().fecha === activeAppointment.fecha) {
            // Pack it.
            activeAppointment.packNewAppointment(appo.getCita());
            continue;
          }
        }
        if (appo.getCita().estado == 'RESERVADA')
          if (appo.isOpen()) appointmentList.push(appo);
      }
    }
    this.storeOpenAppointments(appointmentList as OpenAppointment[]);
    return appointmentList;
    // });
  }

  // public getCredential(): Observable<PatientCredential> {
  //   console.log('>>[BackendService.getCredential]');
  //   return Observable.create((observer) => {
  //     this.storage.get(this.environment.CREDENTIAL_KEY)
  //       .then((data) => {
  //         console.log('--[BackendService.getCredential]> Credential data: ', JSON.stringify(data));
  //         if (null == data) {
  //           console.log('<<[BackendService.getCredential]> Credential is empty');
  //           observer.next(new PatientCredential());
  //           observer.complete();
  //         } else {
  //           console.log('<<[BackendService.getCredential]> Credential found');
  //           this.credential = new PatientCredential(JSON.parse(data));
  //           observer.next(this.credential);
  //           observer.complete();
  //         }
  //       })
  //       .catch((error) => {
  //         console.log('--[BackendService.getCredential]> Credential not found: ', JSON.stringify(error));
  //         observer.next(new PatientCredential());
  //         observer.complete();
  //       });
  //   });
  // }
  // public accessCredential(): PatientCredential {
  //   if (null != this.credential) return this.credential;
  //   else return new PatientCredential();
  // }
  // public storeCredential(_newcredential: PatientCredential): void {
  //   if (null != _newcredential) this.credential = _newcredential;
  // }
  public saveAppointment(newappointment: OpenAppointment): Observable<OpenAppointment> {
    // Get the current appointment list and then add this new appointment.
    return Observable.create((observer) => {
      let appointments = this.accessOpenAppointments();
      appointments.push(newappointment);
      this.storeOpenAppointments(appointments);
      observer.next(newappointment);
      observer.complete();
    });
  }
  // public accessOpenAppointments(): OpenAppointment[] {
  //   return this.openAppointments;
  // }
  // public storeOpenAppointments(_appointments: OpenAppointment[]): void {
  //   this.openAppointments = _appointments;
  // }
  public storeCentros(_centros: Centro[]): void {
    this.centros = _centros;
  }
  public accessCentros(): Centro[] {
    return this.centros;
  }
  public storeEspecialidades(_especialidades: Especialidad[]): void {
    this.especialidades = _especialidades;
  }
  public accessEspecialidades(): Especialidad[] {
    return this.especialidades;
  }
  public storeMedicos(medicos: Medico[]): void {
    this.medicos = medicos;
  }
  public accessMedicos(): Medico[] {
    return this.medicos;
  }
  // public accessCentro(): Centro {
  //   return this.currentCentro;
  // }
  // public storeSelectedCentro(_centro: Centro): void {
  //   this.currentCentro = _centro;
  // }
  // public accessEspecialidad(): Especialidad {
  //   return this.currentEspecialidad;
  // }
  // public storeSelectedEspecialidad(_especialidad: Especialidad): void {
  //   this.currentEspecialidad = _especialidad;
  // }
  // public accessMedico(): Medico {
  //   return this.currentMedico;
  // }
  // public storeSelectedMedico(_medico: Medico): void {
  //   this.currentMedico = _medico;
  // }
  public storeSelectedAppointments(_citas: Cita[]): void {
    this.appointments = _citas;
  }
  public accessAppointments(): Cita[] {
    return this.appointments;
  }

  //--- P R O P E R T I E S   S E R V I C E
  public propertiesAseguradoras(): Observable<string[]> {
    console.log("><[AppStoreServiceDefinitions.propertiesAseguradoras]");
    // Construct the request to call the backend.
    let request = '/assets/properties/aseguradoras.json';
    return this.wrapHttpGETCall(request)
      .pipe(map(data => {
        return data as string[];
      }));
  }

  // //--- I N I T I A L I Z A T I O N
  // public updateAppPages() {
  //   // Add the ever available pages.
  //   this._pages = [];
  //   this._pages.push(new Page().setTitle('Inicio')
  //     .setPageComponent(HomePage)
  //     .setIconName('ios-home'));
  //   this._pages.push(new Page().setTitle('Datos Paciente')
  //     .setPageComponent(AceptacionCondicionesPage)
  //     .setIconName('ios-person'));
  //   // Add the search pages only if the credential is valid.
  //   this.getCredential()
  //     .subscribe((credential) => {
  //       if (credential.isValid()) {
  //         this.storeCredential(credential);
  //         this._pages.push(new Page().setTitle('Medicos por Especialidad')
  //           .setPageComponent(AvailableSpecialitiesPage)
  //           .setIconName('ios-contacts'));
  //         this._pages.push(new Page().setTitle('Localizaciones')
  //           .setPageComponent(AvailableLocationsPage)
  //           .setIconName('navigate'));
  //         // Add the Citas page after the Medicos only if the Credential is valid.
  //         this._pages.push(new Page().setTitle('Citas Abiertas')
  //           .setPageComponent(OpenAppointmentsPage)
  //           .setIconName('md-calendar'));
  //       }
  //     });
  // }
  // public accesspages(): Page[] {
  //   this.updateAppPages();
  //   return this._pages;
  // }
  //--- B A C K E N D   C A L L S   A C C E S S O R S
  public getMedicalCenters(): Observable<Centro[]> {
    console.log("><[BackendService.getMedicalCenters]");
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/centros";
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let centroList = this.transformRequestOutput(data.content) as Centro[];
        console.log("--[BackendService.getMedicalCenters]> Centro.count: " + centroList.length);
        return centroList;
      }));
  }
  /**
   * Gets the complete list of medical service providers from each of the available centers. This is the global list that will cached and shown to the caller services. The list first downloads the list of centers, then for each of them the list of providers and then we coalesce tll this information into a list that is then rendered to the view.
   * @return List of Centers with all the Specialties and then the Doctors.
   */
  public getMedicalServiceProviders(centerIdentifier: number): Observable<Medico[]> {
    console.log("><[BackendService.getMedicalServiceProviders]> centerIdentifier: " + centerIdentifier);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/centros/" + centerIdentifier + "/medicos";
    // let getCall = this.wrapHttpGETCall(request);
    // let delayType = 'none';
    // return this.cache.loadFromDelayedObservable(request, getCall, 'SERVICE-providers', 60 * 2, delayType)
    return this.wrapHttpGETCall(request)
      .pipe(map((data) => {
        // Transform received data and return the node list to the caller for aggregation.
        let medicoList = this.transformRequestOutput(data) as Medico[];
        console.log("--[BackendService.getMedicalCenters]> Medico.count: " + medicoList.length);
        return medicoList;
      }));
  }

  // - C I T A S
  public backendCitas4MedicoByReferencia(identifier: string) {
    console.log("><[BackendService.backendCitas4MedicoByReferencia]> medicoIdentifier: " + identifier);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/medicos/" + identifier + "/citas/byreferencia";
    return this.wrapHttpGETCall(request)
      .pipe(map((data) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data) as Cita[];
        console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
        return citaList;
      }));
  }

  public getAppoinmentList(providerIdentifier: string, searchDate: ChangeDateForm): Observable<Cita[]> {
    console.log("><[BackendService.getAppoinmentList]> providerIdentifier: " + providerIdentifier);
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/medicos/" + providerIdentifier + "/citas/nextfree/" + searchDate.toDateRequest();
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data.content) as Cita[];
        console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
        return citaList;
      }));
  }
  // public blockAppointmentById(patient: PatientCredential, citaIdentifier: number): Observable<Cita> {
  //   console.log("><[BackendService.blockAppointment]> cita: " + citaIdentifier);
  //   // Construct the request to call the backend.
  //   let request = BackendService.RESOURCE_SERVICE_URL + "/citas/" + citaIdentifier + "/block/" + patient.getIdentificador();
  //   return this.wrapHttpGETCall(request)
  //     .pipe(map((data: any) => {
  //       // Transform received data and return the node list to the caller for aggregation.
  //       let citaList = this.transformRequestOutput(data) as Cita;
  //       // console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
  //       return citaList;
  //     }));
  // }
  public blockAppointment(patient: PatientCredential, cita: Cita): Observable<Cita[]> {
    console.log("><[BackendService.blockAppointment]> cita: " + JSON.stringify(cita));
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV2() + "/citas/block/"
      + patient.getIdentificador();
    return this.wrapHttpPOSTCall(request, JSON.stringify(cita.getServiceIdentifierss()))
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as Cita[];
        // console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
        // return citaList;
      }));
  }
  public cancelAppointment(patient: PatientCredential, cita: Cita): Observable<Cita[]> {
    console.log("><[BackendService.cancelAppointment]> cita: " + JSON.stringify(cita));
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV2() + "/citas/cancel/"
      + patient.getIdentificador();
    return this.wrapHttpPOSTCall(request, JSON.stringify(cita.getServiceIdentifierss()))
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as Cita[];
        // console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
        // return citaList;
      }));
  }
  /**
   * Changes the state for an appointment specified by its ID from any state to RESERVADA.
   * @param  patient the patient identifier. The method should port also the rest of the patient data.
   * @param  cita    the selected appointment date and time.
   * @return         a reserved OpenAppointment where we have the Cita and the Medico stored and ready.
   */
  public reserveAppointment(patient: PatientCredential, cita: Cita): Observable<OpenAppointment> {
    console.log("><[BackendService.reserveAppointment]> cita: " + JSON.stringify(cita));
    // Construct the request to call the backend.
    let request = this.isolation.getServerName() + this.isolation.getApiV2() + "/citas/reserve/"
      + patient.getIdentificador();
    // console.log("--[BackendService.reserveAppointment]> request = " + request);
    // console.log("--[BackendService.reserveAppointment]> body = " + JSON.stringify(patient));
    let appointmentRequest = new AppointmentReservationRequest()
      .setPatientData(patient)
      .setIdentifiers(cita.getServiceIdentifierss());
    return this.wrapHttpPOSTCall(request, JSON.stringify(appointmentRequest))
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let appointments = this.transformRequestOutput(data) as OpenAppointment[];
        console.log("--[BackendService.reserveAppointment]> Cita.count: " + appointments.length);
        return appointments[0];
      }));
  }
  public downloadPatientAppointments(credential: PatientCredential): Observable<OpenAppointment[]> {
    console.log("><[BackendService.downloadPatientAppointments]> credential: " + credential.getIdentificador());
    // Construct the request to call the backend.
    try {
      let request = this.isolation.getServerName() + this.isolation.getApiV1() + "/paciente/" + credential.getIdentificador() + "/citas";
      return this.wrapHttpGETCall(request)
        .pipe(map((data: any) => {
          try {
            // Transform received data and return the node list to the caller for aggregation.
            return this.transformRequestOutput(data) as OpenAppointment[];
            // console.log("--[BackendService.downloadPatientAppointments]> Cita.count: " + appointments.length);
            // Store a copy of the appointment list on the appointments observable.
            // this.openAppointments = appointments;
            // return appointments;
          } catch (error) {
            return [];
          }
        }));
    } catch (error) {
      return Observable.create((observer) => {
        observer.next([]);
        observer.complete();
      });
    }
  }



  //---  H T T P   W R A P P E R S
  public wrapHttpRESOURCECall(request: string): Observable<any> {
    console.log("><[AppStoreService.wrapHttpGETCall]> request: " + request);
    return this.http.get(request);
  }
  /**
    * This method wrapts the HTTP access to the backend. It should add any predefined headers, any request specific headers and will also deal with mock data.
    * Mock data comes now into two flavours. he first one will search for the request on the list of defined requests (if the mock is active). If found then it will check if the request should be sent to the file system ot it should be resolved by accessing the LocalStorage.
    * @param  request [description]
    * @return         [description]
    */
  public wrapHttpGETCall(_request: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpGETCall]> request: " + adjustedRequest);
      let newheaders = this.wrapHttpSecureHeaders(_requestHeaders);
      return this.http.get(adjustedRequest, { headers: newheaders });
    } else {
      // The requst is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpGETCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  public wrapHttpPUTCall(_request: string, _body: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[AppStoreService.wrapHttpPUTCall]> request: " + adjustedRequest);
      let newheaders = this.wrapHttpSecureHeaders(_requestHeaders);
      return this.http.put(adjustedRequest, _body, { headers: newheaders });
    } else {
      // The requst is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpPUTCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  public wrapHttpPOSTCall(_request: string, _body: string, _requestHeaders?: HttpHeaders): Observable<any> {
    let adjustedRequest = this.wrapHttpSecureRequest(_request);
    if (typeof adjustedRequest === 'string') {
      // The request should continue with processing.
      console.log("><[BackendService.wrapHttpPOSTCall]> request: " + adjustedRequest);
      let newheaders = this.wrapHttpSecureHeaders(_requestHeaders);
      return this.http.post(adjustedRequest, _body, { headers: newheaders });
    } else {
      // The request is a LOCALSTORAGE mockup and shoudl return with no more processing.
      console.log("><[AppStoreService.wrapHttpPOSTCall]> request: " + _request);
      return adjustedRequest;
    }
  }
  protected wrapHttpSecureRequest(request: string): string | Observable<any> {
    // Check if we should use mock data.
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) {
        // Check if the resolution should be from the LocalStorage. URL should start with LOCALSTORAGE::.
        if (hit.search('LOCALSTORAGE::') > -1) {
          return Observable.create((observer) => {
            try {
              this.isolation.getFromStorage(hit + ':' + this.accessCredential().getIdentificador())
                .then((targetData) => {
                  if (null != targetData) {
                    console.log('--[BackendService.wrapHttpPOSTCall]> Mockup data: ', targetData);
                    // Process and convert the data string to the class instances.
                    let results = this.transformRequestOutput(JSON.parse(targetData));
                    observer.next(results);
                    observer.complete();
                  } else {
                    observer.next([]);
                    observer.complete();
                  }
                });
            } catch (mockException) {
              observer.next([]);
              observer.complete();
            }
          });
        } else request = hit;
      }
    }
    return request;
  }
  /**
  * This is the common code to all secure calls. It will check if the call can use the mockup system and if that system has a mockup destionation for the request.
  * This call also should create a new set of headers to be used on the next call and should put inside the current authentication data.
  *
  * @protected
  * @param {string} request
  * @param {string} _body
  * @param {HttpHeaders} [_requestHeaders]
  * @returns {HttpHeaders}
  * @memberof BackendService
  */
  protected wrapHttpSecureHeaders(_requestHeaders?: HttpHeaders): HttpHeaders {
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Origin', '*')
      .set('xApp-Name', this.isolation.getAppName())
      .set('xApp-version', this.isolation.getAppVersion())
      .set('xApp-Platform', 'Ionic 4.x')
      .set('xApp-Brand', 'CitasPaciente-Demo')
      .set('xApp-Signature', 'S0000.0100.0000');

    // Add authentication token but only for authorization required requests.
    // let cred = this.accessCredential();
    // if (null != cred) {
    //   let auth = this.accessCredential().getAuthorization();
    //   if (null != auth) headers = headers.set('xApp-Authentication', auth);
    //   console.log("><[BackendService.wrapHttpSecureHeaders]> xApp-Authentication: " + auth);
    // }
    if (null != _requestHeaders) {
      for (let key of _requestHeaders.keys()) {
        headers = headers.set(key, _requestHeaders.get(key));
      }
    }
    return headers;
  }

  //--- R E S P O N S E   T R A N S F O R M A T I O N
  public transformRequestOutput(entrydata: any): INode[] | INode {
    let results: INode[] = [];
    // Check if the entry data is a single object. If so process it because can be an exception.
    if (entrydata instanceof Array) {
      for (let key in entrydata) {
        // Access the object into the spot.
        let node = entrydata[key] as INode;
        // Convert and add the node.
        results.push(this.convertNode(node));
      }
    } else {
      // Process a single element.
      let jclass = entrydata["jsonClass"];
      if (null == jclass) return [];
      return this.convertNode(entrydata);
    }
    return results;
  }
  protected convertNode(node): Node {
    switch (node.jsonClass) {
      case "Centro":
        let convertedCentro = new Centro(node);
        console.log("--[BackendService.convertNode]> Centro node: " + convertedCentro.getId());
        return convertedCentro;
      case "Medico":
        let convertedMedico = new Medico(node);
        console.log("--[BackendService.convertNode]> Medico node: " + convertedMedico.getId());
        return convertedMedico;
      case "Cita":
        let convertedCita = new Cita(node);
        console.log("--[BackendService.convertNode]> Cita node: " + convertedCita.getId());
        return convertedCita;
      case "OpenAppointment":
        let convertedAppointment = new OpenAppointment(node);
        console.log("--[BackendService.convertNode]> OpenAppointment node: " + convertedAppointment.getId());
        return convertedAppointment;
    }
  }
  // private date2BasicISO(_date: Date): string {
  //   var local = new Date(_date);
  //   local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
  //   let requestDateString: string = local.getFullYear() + "";
  //   let month = local.getMonth() + 1;
  //   if (month < 10) requestDateString = requestDateString + "0" + month;
  //   else requestDateString = requestDateString + month;
  //   let day = local.getDate();
  //   if (day < 10) requestDateString = requestDateString + "0" + day;
  //   else requestDateString = requestDateString + day;
  //   return requestDateString;
  // }
}
