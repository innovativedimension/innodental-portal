//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Injectable } from '@angular/core';
//--- MODEL
import { Medico } from '../models/Medico.model';

/**
This service will store persistent application data and has the knowledge to get to the backend to retrieve any data it is requested to render on the view.
*/
@Injectable()
export class DoctorSearchService {
  public doctors: Medico[] = [];

  public filterItems(term: string): any[] {
    let searchHits = [];
    for (let doc of this.doctors) {
      console.log('>>[DoctorSearchService.filterItems]> name: ' + doc.nombre.toLowerCase() + " " + doc.apellidos.toLowerCase());
      if (doc.nombre.toLowerCase().includes(term.toLowerCase()))
        searchHits.push(doc);
      if (doc.apellidos.toLowerCase().includes(term.toLowerCase()))
        searchHits.push(doc);
    }
    return searchHits;
  }
  public setDoctorList(_list: Medico[]): void {
    this.doctors = _list;
  }
}
