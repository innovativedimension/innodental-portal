//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
//--- CORE MODULES
import { NgModule } from '@angular/core';
// import { Commons } from '@angular/core';
//--- IONIC
// import { IonicApp } from 'ionic-angular';
import { IonicModule } from 'ionic-angular';
// import { IonicPageModule } from 'ionic-angular';

// --- CORE
import { MVCViewerComponent } from './core/mvcviewer/mvcviewer.component';
import { IonicPageComponent } from './core/ionic-page/ionic-page.component';
import { SearcheablePageComponent } from './core/searcheable-page/searcheable-page';
import { DownloadingPageComponent } from './core/downloading-page/downloading-page.component';
// --- RENDERS
import { RenderComponent } from './renders/render/render.component';
import { RenderIonicComponent } from './renders/render-ionic/render-ionic.component';
import { ActoMedicoComponent } from './renders/acto-medico/acto-medico.component';
import { AvailableMedicoComponent } from './renders/available-medico/available-medico';

@NgModule({
  imports: [
    // IonicPageModule.forChild(SplashPage),
    IonicModule
  ],
  declarations: [
    // --- CORE
    MVCViewerComponent ,
    IonicPageComponent,
    SearcheablePageComponent,
    DownloadingPageComponent,
    
    RenderComponent,
    RenderIonicComponent,
    ActoMedicoComponent,
    AvailableMedicoComponent
  ],
  exports: [
    RenderComponent,
    RenderIonicComponent,
    ActoMedicoComponent,
    AvailableMedicoComponent
  ]
})
export class SharedModule {}
