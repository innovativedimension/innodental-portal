//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
import { Injectable } from '@angular/core';
// --- MODELS
import { PatientCredential } from '../../shared/models/PatientCredential.model';

@Injectable()
export class MockAppStoreService {
  // - M O C K   S T O R E   F I E L D S
  protected _credential: PatientCredential; // Field to store the Patient data. This is a copy of the storage data.

  // - C R E D E N T I A L
  public accessCredential(): PatientCredential {
    if (null != this._credential) return this._credential;
    else return new PatientCredential();
  }
  public accessCredentialPromise(): Promise<PatientCredential> {
    console.log('><[MockAppStoreService.accessCredentialPromise]');
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        console.log("Async Work Complete");
        resolve(this._credential);
      }, 1000);
    });
  }
  public clearCredential(): void {
    this._credential = null;
  }
  public storeCredential(_newcredential: PatientCredential): PatientCredential {
    this._credential = _newcredential;
    return this._credential;
  }
}