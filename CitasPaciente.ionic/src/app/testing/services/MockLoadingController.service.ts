//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
import { Injectable } from '@angular/core';

@Injectable()
export class MockLoadingController {
  // public visible: boolean = false; // To store the supposed visual state for the loading panel.
  public panel: LoadingPanel;

  public create(_data: any): any {
    this.panel = new LoadingPanel();
    this.panel.content = _data.content;
    this.panel.spinner = _data.spinner;
    return this.panel;
  }
  public getMessage(): string {
    if (null != this.panel) return this.panel.content;
    else return null;
  }
  public getVisibility(): boolean {
    if (null != this.panel) return this.panel.visible;
    else return false;
  }
}
export class LoadingPanel {
  public content: string;
  public spinner: string;
  public visible: boolean = false;

  public present(): void {
    this.visible = true;
  }
  public dismiss(): void {
    this.visible = false;
  }
}