//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Subject } from 'rxjs';
//--- IONIC
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
//--- CALENDAR
import { isBefore } from 'date-fns';
import { startOfDay } from 'date-fns';
import { endOfDay } from 'date-fns';
import { CalendarEvent } from 'angular-calendar';
// import { CalendarEventAction } from 'angular-calendar';
// import { CalendarEventTimesChangedEvent } from 'angular-calendar';
// import { CalendarDateFormatter } from 'angular-calendar';
// import { DateFormatterParams } from 'angular-calendar';
//--- SERVICES
import { AppStoreService } from '../../app/shared/services/appstore.service';
import { BackendService } from '../../app/shared/services/backend.service';
//--- MODELS
import { Cita } from '../../app/shared/models/Cita.model';
import { Medico } from '../../app/shared/models/Medico.model';
import { Payload } from '../../app/shared/models/Payload.model';
//--- PAGES
import { AvailableAppointmentsPage } from '../../pages/available-appointments/available-appointments.page';
import { ActoMedico } from '../../app/shared/models/ActoMedico.model';

@Component({
  selector: 'cita-month-panel',
  templateUrl: 'cita-month-panel.component.html'
})
export class CitaMonthPanelComponent implements OnInit {
  // @ViewChild('modalContent') modalContent: TemplateRef<any>;
  @Input() medico: Medico;
  private _filters: any = [];
  private _loader: any;

  //--- ANGULAR-CALENDAR VARIABLES
  public viewDate: Date = new Date();
  public refresh: Subject<any> = new Subject();
  public events: CalendarEvent[] = [];
  // public activeDayIsOpen: boolean = false;

  //--- COMPONENT VARIABLES
  public citaList: Cita[] = [];
  public eventList = new Map<string, Payload>();
  public downloading: boolean = true;
  public daySelected: any; // Stores the current selected date.
  public freeAppointmentsList: Cita[] = [];

  //--- OTHER STRUCTURES
  // private intervalHolder: any;
  view: string = 'month';

  //--- C O N S T R U C T O R
  constructor(
    protected navCtrl: NavController,
    protected storage: Storage,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    protected loadingCtrl: LoadingController) {
  }

  //--- L I F E C Y C L E
  ngOnInit() {
    console.log(">>[CitaMonthPanelComponent.ngOnInit]");
    if (null == this.medico) this.medico = this.appStoreService.accessSelectedMedico();
    this.downloadAppointmentData();
    console.log("<<[CitaMonthPanelComponent.ngOnInit]");
  }

  //--- D A T A   M A N A G E M E N T
  protected downloadAppointmentData(): void {
    console.log("><[CitaMonthPanelComponent.downloadAppointmentData]");
    this.downloading = true;
    this.presentLoading();

    // Define the filters.
    this._filters.push({
      name: 'appointment < now',
      description: 'Filter out appointments in the past',
      filter: (_target: Cita): boolean => {
        return isBefore(_target.getFecha(), new Date());
      }
    });
    this._filters.push({
      name: 'appointment = today',
      description: 'Filter out appointments at today',
      filter: (_target: Cita): boolean => {
        return isBefore(_target.getFecha(), this.dateAdd(new Date(), 'day', 1));
      }
    });
    this._filters.push({
      name: 'appointment = LIBRE',
      description: 'Filter out not LIBRE appointments',
      filter: (_target: Cita): boolean => {
        return !_target.isFree();
      }
    });
    let actoMedico: ActoMedico = this.appStoreService.accessSelectedActoMedico();
    let packSlots: boolean = false;
    if (null != actoMedico) packSlots = true;
    // Download the data about the appointments already reserved.
    this.backendService.backendCitas4MedicoByReferencia(this.medico.referencia)
      .subscribe((citas: Cita[]) => {
        console.log("--[CitaMonthPanelComponent.downloadAppointmentData]> count: " + citas.length);
        this.eventList = new Map<string, Payload>();
        this.citaList = [];
        // let startDate = new Date();
        // startDate = this.dateAdd(startDate, 'day', 1);
        // startDate = startOfDay(startDate);
        let endIndex: number = citas.length - 1;
        let currentPosition: number = 0;
        while (currentPosition <= endIndex) {
          let cita = citas[currentPosition];
          // for (let cita of citas) {
          // Apply the list of filters to drop appointments that should not be accesible.
          let filtered = this.applyFilters(cita);
          if (filtered) {
            currentPosition++;
            continue;
          }
          // Pack appointments depending on the selected slot size. This is defined on the Medical Act.
          let packedAppointment = new Cita(cita); // Stores the resulting packed appointment to be used.
          if (packSlots) {
            let requiredSlotSize = actoMedico.tiempoRequerido;
            let slotSize = cita.getDuracion();
            let notPack: boolean = false; // Used to skip packing when there is no contiguous appointment.
            while (requiredSlotSize > slotSize) {
              // if (requiredSlotSize > slotSize) {
              // Try to join with next appointment if this is free. If not we should skip it.
              if (currentPosition < endIndex) {
                let nextSlot = citas[currentPosition + 1];
                // Check the slot is free.
                if (nextSlot.isFree()) {
                  // Check also that the slot is next to the current appointment.
                  if (packedAppointment.zonaHoraria == nextSlot.zonaHoraria) {
                    slotSize += nextSlot.getDuracion();
                    packedAppointment.packNewAppointment(nextSlot);
                  } else {
                    // Next appointment not LIBRE. Skip the current one. Not able to fit the request.
                    notPack = true;
                    break;
                  }
                } else {
                  // Next appointment not LIBRE. Skip the current one. Not able to fit the request.
                  notPack = true;
                  break;
                }
              } else {
                // End of the list. Skip the current one. Not able to fit the request.
                notPack = true;
                break;
              }
            }
            if (notPack) {
              currentPosition++;
              continue;
            }
          }
          // Insert the packed slot into the results.
          this.citaList.push(packedAppointment);
          currentPosition++;

          // Do the accounting to generate the events.
          let hit: Payload = this.eventList.get(this.date2BasicISO(packedAppointment.getFecha()));
          if (null == hit) {
            hit = new Payload();
            this.eventList.set(this.date2BasicISO(packedAppointment.getFecha()), hit);
          }
          hit.addOpen();
          hit.addCita(packedAppointment);
        }
        this.processEvents();
        this.downloading = false;
        this.dismissLoading();
      }, (error) => {
        console.log('--[CitaMonthPanelComponent.downloadAppointmentData]> Error: ' + JSON.stringify(error));
        this.downloading = false;
        this.dismissLoading();
      });
  }

  //--- C A L E N D A R   E V E N T   M A N A G E M E N T
  protected processEvents(): void {
    this.eventList.forEach((value: Payload, key: string) => {
      console.log("--[CitaMonthPanelComponent.processEvents]> Day payload: " +
        value.getOpen() + " : " + value.getFreeAppointmentsCount());
      // if (value.getReserved() > 0) {
      let newevent = {
        start: startOfDay(key),
        end: endOfDay(key),
        title: 'Citas',
        payload: value,
        selected: false
      };
      this.events.push(newevent);
    });
    // Send a message to update the calendar.
    this.refresh.next();
  }
  //--- L O A D   N O T I F I CA T I O N   I N T E R F A C E
  protected presentLoading(): void {
    this._loader = this.loadingCtrl.create({
      content: "Descargando datos. Por favor espere...",
      duration: 15000
    });
    this._loader.present();
  }
  protected dismissLoading(): void {
    if (null != this._loader) this._loader.dismiss();
  }

  //--- C A L E N D A R   E V E N T   M A N A G E M E N T
  public dayClicked(_event: any): void {
    console.log("><[CitaMonthPanelComponent.dayClicked]> event: " + JSON.stringify(_event));
    let day = _event.day;
    // Set the day clicked.
    if (null != this.daySelected)
      this.daySelected['selected'] = false;
    this.daySelected = day;
    this.daySelected['selected'] = true;

    let payload: Payload = null;
    // Search for the list and set it to the render list of free appointments.
    for (let event of day.events) {
      if (null != event['payload']) payload = event['payload']
    }
    if (null != payload) this.freeAppointmentsList = payload.citas

    // Store the appointments on the store to interchange to next page.
    this.backendService.storeSelectedAppointments(payload.citas);
    this.navCtrl.push(AvailableAppointmentsPage);
  }
  public getAppointmentsList(): Cita[] {
    return this.freeAppointmentsList;
  }
  public getDateFreeAppointments(_event: any): Cita[] {
    if (null != _event)
      if (null != _event['payload']) return _event['payload'].citas;
    return [];
  }
  //--- P R I V A T E   M E T H O D S
  /**
   * Adds time to a date. Modelled after MySQL DATE_ADD function.
   * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
   * https://stackoverflow.com/a/1214753/18511
   *
   * @param date  Date to start with
   * @param interval  One of: year, quarter, month, week, day, hour, minute, second
   * @param units  Number of units of the given interval to add.
   */
  public dateAdd(date, interval, units) {
    var ret = new Date(date); //don't change original date
    var checkRollover = function () { if (ret.getDate() != date.getDate()) ret.setDate(0); };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }
  protected date2BasicISO(_date: Date): string {
    var local = new Date(_date);
    local.setMinutes(_date.getMinutes() - _date.getTimezoneOffset());
    let requestDateString: string = local.getFullYear() + "";
    let month = local.getMonth() + 1;
    if (month < 10) requestDateString = requestDateString + "0" + month;
    else requestDateString = requestDateString + month;
    let day = local.getDate();
    if (day < 10) requestDateString = requestDateString + "0" + day;
    else requestDateString = requestDateString + day;
    return requestDateString;
  }

  //--- C O N T E N T   A D A P T E R S
  public getFreeAppointmentsCount(_day: any): number {
    if (null != _day)
      if (null != _day.events[0].payload) {
        let load = _day.events[0].payload as Payload;
        return load.getFreeAppointmentsCount();
      }
    return 0;
  }
  public isCellSelected(_day: any): boolean {
    if (null == _day['selected']) return false;
    else return _day['selected'];
  }
  public getCurrentSelectedDate(): Date {
    return this.viewDate;
  }

  // public handleEvent(action: string, event: CalendarEvent): void {
  //   this.modalData = { event, action };
  //   // this.modal.open(this.modalContent, { size: 'lg' });
  // }
  // public eventTimesChanged({
  //   event,
  //   newStart,
  //   newEnd
  // }: CalendarEventTimesChangedEvent): void {
  //   event.start = newStart;
  //   event.end = newEnd;
  //   // this.handleEvent('Dropped or resized', event);
  //   this.refresh.next();
  // }
  // public activateRefresh(): void {
  //   console.log("><[AppointmentDateSelectorComponent.activateRefresh]");
  //   this.downloadAppointmentData();
  // }
  // public activateMenu(selectedDay: any) {
  //   console.log("><[AppointmentDateSelectorComponent.activateMenu]>Target: " + JSON.stringify(selectedDay));
  //   this.activeDayIsOpen = true;
  // }
  // public mouseLeave() {
  //   this.activeDayIsOpen = false;
  // }
  //-- LOCAL METHODS
  //--- F I L T E R I N G
  /**
   * This will apply all the filters defined in sequence. If any of the fiters drops the appoitment then we do not continue and we terminate the process with a true.
   * If we reach the end of the filters list and the appointment still is present then we return a false.
   * @param  _target the appointment to be checked.
   * @return         <b>true</b> if the appointment has failed the filters and should be discarded. <b>false</b> if the appointmet has to be kept.
   */
  public applyFilters(_target: Cita): boolean {
    for (let filter of this._filters) {
      let filterResult = filter.filter(_target);
      if (filterResult) return true;
    }
    return false;
  }
}
