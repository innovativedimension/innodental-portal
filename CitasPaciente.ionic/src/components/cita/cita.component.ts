//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
// import { trigger } from '@angular/animations';
// import { state } from '@angular/animations';
// import { style } from '@angular/animations';
// import { transition } from '@angular/animations';
// import { animate } from '@angular/animations';
// import { keyframes } from '@angular/animations';
// import { query } from '@angular/animations';
// import { stagger } from '@angular/animations';
//--- SERVICES
// import { AppStoreService } from '../../services/appstore.service';
//--- COMPONENTS
// import { NodeComponent } from '../node.component';
// import { ExpandableNodeComponent } from '../expandablenode.component';
//--- INTERFACES
// import { INode } from '../../interfaces/core/INode.interface';
// //--- MODELS
import { Cita } from '../../app/shared/models/Cita.model';

@Component({
  selector: 'patient-cita',
  templateUrl: 'cita.component.html'
})
export class CitaComponent {
  @Input() node: Cita;

  public inDevelopment(): boolean {
    return true;
  }
}
