//  PROJECT:     CitaMed.frontend(CITM.A6F)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.
//  DESCRIPTION: CitaMed. Sistema S2. Aplicación Angular adaptada para que los médicos puedan cargar sus
//               planillas de huecos de citas en el repositorio indicado de forma que los pacientes puedan
//               reservarlos. Tiene como backend el S1 de CitaMed.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- SERVICES
// import { BackendService } from '../../app/shared/services/backend.service';
//--- INTERFACES
// import { IViewer } from '../../interfaces/core/IViewer.interface';
import { IDataSource } from '../../app/shared/interfaces/core/IDataSource.interface';
//--- MODELS
import { OpenAppointment } from '../../app/shared/models/OpenAppointment.model';
// import { RenderComponent } from '../../app/shared/renders/render/render.component';
import { RenderIonicComponent } from '../../app/shared/renders/render-ionic/render-ionic.component';

@Component({
  selector: 'open-appointment',
  templateUrl: 'open-appointment.html'
})
export class OpenAppointmentComponent extends RenderIonicComponent {
  @Input() dataSource: IDataSource;
  @Input() open: OpenAppointment;
  public development: boolean = true;

  //--- C O N S T R U C T O R
  // constructor(protected backendService: BackendService) {
  //   this.development = true;
  // }

  //--- E V E N T S
  public cancelOpenAppointment(): void {
    console.log('>>[OpenAppointmentComponent.cancelAppointment]');
    // Send the cancelation to the backend.
    let credential = this.appStoreService.accessCredential()
    // .subscribe((credential) => {
    if (credential.isValid()) {
      this.backendService.cancelAppointment(credential, this.open.getCita())
        .subscribe((reservedCita) => {
          console.log('>>[OpenAppointmentComponent.cancelAppointment]> blockedCita: ' + reservedCita);
          // Notify the content page to refresh.
          this.dataSource.removeNode(this.open);
        });
    }
    // });
    console.log('<<[OpenAppointmentComponent.cancelAppointment]');
  }
  //--- T R A N S F O R M A T I O N S
}
