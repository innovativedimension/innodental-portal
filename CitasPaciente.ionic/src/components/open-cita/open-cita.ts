//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- CORE
import { Component } from '@angular/core';
import { Inject } from '@angular/core';
import { Input } from '@angular/core';
//--- ENVIRONMENT
import { EnvVariables } from '../../environment-plugin/environment-plugin.token';
//--- MODELS
// import { OpenAppointment } from '../../models/OpenAppointment.model';
import { Cita } from '../../app/shared/models/Cita.model';

@Component({
  selector: 'open-cita',
  templateUrl: 'open-cita.html'
})
export class OpenCitaComponent {
  @Input() cita: Cita;
  public development: boolean = false;

  //--- C O N S T R U C T O R
  constructor(
    @Inject(EnvVariables) public environment) {
    this.development = environment.development;
  }
  //--- T R A N S F O R M A T I O N S
  public getAppointmentDateDisplay(): string {
    let display = "";
    display = display + this.environment.WEEKDAYS[this.cita.getFecha().getDay()] + ', ';
    display = display + this.cita.getFecha().getDate() + ' ';
    display = display + this.environment.MONTHNAMES[this.cita.getFecha().getMonth()] + ' ';
    display = display + this.cita.getFecha().getFullYear();
    return display;
  }
  public getMinutes2Expire(): string {
    var appointmentDateTime = this.cita.fechaDate;
    appointmentDateTime.setHours(this.cita.getHour());
    appointmentDateTime.setMinutes(this.cita.getMinutes());
    return this.time2Expire(this.cita.fechaDate);
  }
  protected time2Expire(appointmentTime: Date): string {
    let now = new Date();
    let timeDiff = appointmentTime.getTime() - now.getTime();
    timeDiff /= 1000;
    // get seconds (Original had 'round' which incorrectly counts 0:28, 0:29, 1:30 ... 1:59, 1:0)
    // var seconds = Math.round(timeDiff % 60);
    // remove seconds from the date
    timeDiff = Math.floor(timeDiff / 60);
    // get minutes
    var minutes = Math.round(timeDiff % 60);
    // remove minutes from the date
    timeDiff = Math.floor(timeDiff / 60);
    // get hours
    var hours = Math.round(timeDiff % 24);
    // remove hours from the date
    timeDiff = Math.floor(timeDiff / 24);
    // the rest of timeDiff is number of days
    var days = timeDiff;

    let x = days + " dias " + hours + " horas y " + minutes + " minutos";
    return x;
  }
}
