//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
import { NgModule } from '@angular/core';
import { EnvVariables } from './environment-plugin.token';
import { developmentenvironment } from './environment.dev';
import { productionenvironment } from './environment.prod';
// import { globalConstants } from './environment.global';

declare const process: any; // Typescript compiler will complain without this

export function environmentFactory() {
  if (process.env.IONIC_ENV === 'prod') return productionenvironment;
  return developmentenvironment;
}
// export function globalFactory() {
//   return globalConstants;
// }

@NgModule({
  providers: [
    {
      provide: EnvVariables,
      // useFactory instead of useValue so we can easily add more logic as needed.
      useFactory: environmentFactory
    }
    // {
    //   provide: globalConstants,
    //   // useFactory instead of useValue so we can easily add more logic as needed.
    //   useFactory: globalFactory
    // }
  ]
})
export class EnvironmentsModule { }