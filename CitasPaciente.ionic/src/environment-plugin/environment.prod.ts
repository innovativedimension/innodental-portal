//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
export const productionenvironment = {
  appName: "CitasPaciente",
  appVersion: " v0.8.2",
  production: true,
  development: false,
  mockStatus: true,
  showexceptions: true,
  serverName: "https://backcitas.herokuapp.com",
  apiVersion1: "/api/v1",
  apiVersion2: "/api/v2",
  //--- C O N S T A N T S
  ACEPTACIONCONDICIONES_KEY: "-ACEPTACIONCONDICIONES-KEY-",
  CREDENTIAL_KEY: "-CREDENTIAL-KEY-",
  TEMPLATE_KEY: "-TEMPLATE-KEY-",
  LAST_REPORT_KEY: "-LAST-REPORT-KEY-"
};
