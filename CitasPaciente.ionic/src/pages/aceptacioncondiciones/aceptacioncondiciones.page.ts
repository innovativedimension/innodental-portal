//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Inject } from '@angular/core';
//--- FORMS
// import { FormGroup } from '@angular/forms';
// import { FormControl } from '@angular/forms';
// import { Validators } from '@angular/forms';
//--- IONIC
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// import { Nav } from 'ionic-angular';
import { Platform } from 'ionic-angular';
// import { environment } from '@env'
//--- SERVICES
import { BackendService } from '../../app/shared/services/backend.service';
//--- MODELS
// import { PatientCredential } from '../../models/PatientCredential.model';
//--- PAGES
import { HomePage } from '../../pages/home/home.page';
import { PatientRecordPage } from '../../pages/patientrecord/patientrecord.page';
import { EnvVariables } from '../../environment-plugin/environment-plugin.token';

/**
 * This component is only visible when the user has not already accepted the RGPD conditions. Those conditions report the Patient what data and where is will be stored and the use for that data following the RGPD rules to manage and store user data.
 */
@Component({
  selector: 'aceptacioncondiciones-page',
  templateUrl: 'aceptacioncondiciones.page.html'
})
export class AceptacionCondicionesPage implements OnInit {
  public showAceptacionPage: boolean = false;

  constructor(
    @Inject(EnvVariables) public environment,
    protected ionic: Platform,
    protected navCtrl: NavController,
    protected storage: Storage,
    protected backendService: BackendService) {
    if (ionic.is('ios')) this.showAceptacionPage = false;
  }

  ngOnInit() {
    console.log('>>[AceptacionCondicionesPage.ngOnInit]');
    // Check if the patient has already accepted the conditions to get its data.
    this.storage.get(this.environment.ACEPTACIONCONDICIONES_KEY)
      .then((data) => {
        let aceptacion: string = "NO-ACEPTADA";
        if (null != data)
          aceptacion = data;
        if (aceptacion === "ACEPTADA") this.navCtrl.push(PatientRecordPage);
        else { this.showAceptacionPage = true; }
      });
    console.log('<<[AceptacionCondicionesPage.ngOnInit]');
  }
  public acceptAceptacion(): void {
    this.storage.set(this.environment.ACEPTACIONCONDICIONES_KEY, "ACEPTADA")
      .then((data) => {
        this.navCtrl.push(PatientRecordPage);
      });
  }
  public cancelAceptacion(): void {
    this.storage.set(this.environment.ACEPTACIONCONDICIONES_KEY, "NO-ACEPTADA")
      .then((data) => {
        this.navCtrl.push(HomePage);
      });
  }
}
