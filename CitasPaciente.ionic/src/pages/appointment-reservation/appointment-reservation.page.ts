//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { EnvVariables } from '../../environment-plugin/environment-plugin.token';
//--- IONIC
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
//--- SERVICES
import { AppStoreService } from '../../app/shared/services/appstore.service';
import { BackendService } from '../../app/shared/services/backend.service';
//--- MODELS
import { Medico } from '../../app/shared/models/Medico.model';
import { Cita } from '../../app/shared/models/Cita.model';
import { PatientCredential } from '../../app/shared/models/PatientCredential.model';
//--- PAGES
import { HomePage } from '../../pages/home/home.page';
import { OpenAppointmentsPage } from '../../pages/open-appointments/open-appointments.page';
import { ActoMedico } from '../../app/shared/models/ActoMedico.model';

@Component({
  selector: 'appointment-reservation-page',
  templateUrl: 'appointment-reservation.page.html',
})
export class AppointmentReservationPage implements OnInit {
  public currentMedico: Medico;
  public currentActoMedico: ActoMedico;
  public currentPaciente: PatientCredential;
  public currentCita: Cita;

  //--- C O N S T R U C T O R
  constructor(
    @Inject(EnvVariables) public environment,
    protected navCtrl: NavController,
    protected navParams: NavParams,
    protected storage: Storage,
    protected loadingCtrl: LoadingController,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) {
    // super(navCtrl, navParams, storage, loadingCtrl, backendService)
    console.log('><[AppointmentReservationPage.constructor]');
    this.currentMedico = this.appStoreService.accessSelectedMedico();
    this.currentActoMedico = this.appStoreService.accessSelectedActoMedico();
    this.currentCita = this.appStoreService.accessSelectedCita();
    this.currentPaciente = this.appStoreService.accessCredential();
  }

  ngOnInit() {
    console.log('>>[AppointmentReservationPage.ngOnInit]');
    this.currentMedico = this.appStoreService.accessSelectedMedico();
    this.currentActoMedico = this.appStoreService.accessSelectedActoMedico();
    this.currentCita = this.appStoreService.accessSelectedCita();
    this.currentPaciente = this.appStoreService.accessCredential();
  }
  public dataReady(): boolean {
    if (null == this.currentMedico) return false;
    if (null == this.currentPaciente) return false;
    if (null == this.currentCita) return false;
    return true;
  }
  public getWeekDay(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return this.environment.WEEKDAYS[theDate.getDay()];
  }
  public getDateNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getDate();
  }
  public getYearNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getFullYear();
  }
  public getMonthName(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return this.environment.MONTHNAMES[theDate.getMonth()];
  }
  public getDateDate(cita: Cita): Date {
    let theDate = new Date(cita.huecoId * 60 * 1000);
    return theDate;
  }
  /**
   * This event is generated when clicking the 'Confirmar Cita' button. Should post it to the backend server for reservation and then save the appointment on the local list.
   */
  public confirmAppointment(): void {
    console.log('>>[AppointmentReservationPage.confirmAppointment]');
    this.backendService.reserveAppointment(this.currentPaciente, this.currentCita)
      .subscribe((reservedOpenAppointment) => {
        console.log('--[AppointmentReservationPage.confirmAppointment]> reservedOpenAppointment: ' + reservedOpenAppointment.getCita().getId());
        // Store the appointment on the list of reservations.
        this.backendService.saveAppointment(reservedOpenAppointment)
          .subscribe((cita) => {
            if (null == cita) this.navCtrl.push(HomePage);
            // else this.navCtrl.push(OpenAppointmentsPage);
            this.navCtrl.push(OpenAppointmentsPage);
          });
      }, (error) => {
        console.log('--[AppointmentReservationPage.confirmAppointment]> Exception: ' + error.getMessage());
        // TODO - This is the right point to insert a Toast.
      });
    console.log('<<[AppointmentReservationPage.confirmAppointment]');
  }
  public cancelAppointment(): void {
    console.log('>>[AppointmentReservationPage.cancelAppointment]');
    this.backendService.cancelAppointment(this.currentPaciente, this.currentCita)
      .subscribe((reservedCita) => {
        console.log('>>[ListAppointmentsPage.citaClick.cancelAppointment]> blockedCita: ' + reservedCita);
        this.navCtrl.push(HomePage);
      });
    console.log('<<[AppointmentReservationPage.cancelAppointment]');
  }
}
