//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
// --- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// --- MODELS
import { Medico } from '../../app/shared/models/Medico.model';
import { Cita } from '../../app/shared/models/Cita.model';
// import { PatientCredential } from '../../app/shared/models/PatientCredential.model';
import { ChangeDateForm } from '../../app/shared/models/core/ChangeDateForm.model';
//--- PAGES
// import { DownloadingPage } from '../../pages/downloading.component';
import { AppointmentReservationPage } from '../../pages/appointment-reservation/appointment-reservation.page';
import { Especialidad } from '../../app/shared/models/Especialidad.model';
import { ActoMedico } from '../../app/shared/models/ActoMedico.model';
import { DownloadingPageComponent } from '../../app/shared/core/downloading-page/downloading-page.component';


@Component({
  selector: 'appointments-calendar-page',
  templateUrl: 'appointments-calendar.html',
})
export class AppointmentsCalendarPage extends DownloadingPageComponent implements OnInit {
  public especialidadSeleccionada: Especialidad;
  public medicoSeleccionado: Medico;
  public actoMedicoSeleccionado: ActoMedico;
  public changeDateForm: ChangeDateForm = new ChangeDateForm();

  // - L I F E C Y C L E
  ngOnInit() {
    this.especialidadSeleccionada = this.appStoreService.accessSelectedEspecialidad();
    this.medicoSeleccionado = this.appStoreService.accessSelectedMedico();
    this.actoMedicoSeleccionado = this.appStoreService.accessSelectedActoMedico();
  }
  ionViewDidLoad() {
    console.log('>>[AppointmentsCalendarPage.ionViewDidLoad]');
    this.viewController.setBackButtonText('Medicos');
    this.especialidadSeleccionada = this.appStoreService.accessSelectedEspecialidad();
    this.medicoSeleccionado = this.appStoreService.accessSelectedMedico();
    this.actoMedicoSeleccionado = this.appStoreService.accessSelectedActoMedico();
    console.log('<<[AppointmentsCalendarPage.ionViewDidLoad]');
  }
  /**
   * Start the process to block and reserve the appointment. At this point we have the Medico and the Cita ready so this is the right place to block the appointment and them move to the confirmation page.
   * @param cita the Cita clicked and selected for blocking.
   */
  public citaClick(cita: Cita): void {
    console.log('>>[ListAppointmentsPage.citaClick]');
    // Block the appointment. If the block is confirmed then move to the information page.
    // this.storage.get(environment.CREDENTIAL_KEY)
    //   .then((data) => {
    //     if (null != data) {
    //       console.log('--[PatientRecordPage.ngOnInit]> Patient data: ' + data);
    //       let patientRecord = new PatientCredential(JSON.parse(data));
          this.backendService.blockAppointment(this.appStoreService.accessCredential(), cita)
            .subscribe((blockedCita) => {
              console.log('>>[ListAppointmentsPage.citaClick.blockAppointment]> blockedCita: ' + blockedCita);
              // Start the reservation process. Show the reservation page.
              // Store the data in the store for other page access.
              this.appStoreService.storeSelectedMedico(this.medicoSeleccionado);
              // this.appStoreService.storeSelectedPaciente(patientRecord);
              this.appStoreService.storeSelectedCita(cita);
              this.navController.push(AppointmentReservationPage, { 'citaIdentifier': cita.id });
            });
        // }
      // });
    console.log('<<[ListAppointmentsPage.citaClick]');
  }

  // public getWeekDay(cita: Cita): string {
  //   let theDate = new Date(cita.fecha);
  //   return environment.WEEKDAYS[theDate.getDay()];
  // }
  // public getDateNumber(cita: Cita): number {
  //   let theDate = new Date(cita.fecha);
  //   return theDate.getDate();
  // }
  // public getYearNumber(cita: Cita): number {
  //   let theDate = new Date(cita.fecha);
  //   return theDate.getFullYear();
  // }
  // public getMonthName(cita: Cita): string {
  //   let theDate = new Date(cita.fecha);
  //   return environment.MONTHNAMES[theDate.getMonth()];
  // }
  // public getDateDate(cita: Cita): Date {
  //   let theDate = new Date(cita.huecoId * 60 * 1000);
  //   return theDate;
  // }

  //--- F O R M   I N T E R A C T I O N
  public onSubmit() {
    console.log(">>[ListAppointmentsPage.onSubmit]");
    // Get the new date and refresh the list of appointments with the new date data.
    // let searchDate = this.changeDateForm.newDate;
    // this.downloading = true;
    // Call the backend to retrieve the list of Doctors to be presented to the Patient
    this.backendService.getAppoinmentList(this.navParams.get('providerIdentifier'), this.changeDateForm)
      .subscribe((citaList: Cita[]) => {
        // Iterate on all the centers to collect the data.
        this.dataModelRoot = [];
        for (let cita of citaList) {
          console.log('--[ListAppointmentsPage.ngOnInit]> Cita: ' + JSON.stringify(cita));
          this.dataModelRoot.push(cita);
        }
        this.notifyDataChanged();
        // this.downloading = false;
      });
    console.log("<<[ListAppointmentsPage.onSubmit]");
  }
}
