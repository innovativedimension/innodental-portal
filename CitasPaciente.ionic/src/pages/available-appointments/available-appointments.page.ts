//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 4.0.
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- MODELS
import { Node } from '../../app/shared/models/core/Node.model';
import { Medico } from '../../app/shared/models/Medico.model';
import { Cita } from '../../app/shared/models/Cita.model';
// import { PatientCredential } from '../../models/PatientCredential.model';
import { ChangeDateForm } from '../../app/shared/models/core/ChangeDateForm.model';
//--- PAGES
// import { DownloadingPage } from '../../pages/downloading.component';
import { AppointmentReservationPage } from '../../pages/appointment-reservation/appointment-reservation.page';
import { Especialidad } from '../../app/shared/models/Especialidad.model';
import { ActoMedico } from '../../app/shared/models/ActoMedico.model';
import { DownloadingPageComponent } from '../../app/shared/core/downloading-page/downloading-page.component';
// import { Observable } from 'rxjs';

@Component({
  selector: 'available-appointments-page',
  templateUrl: 'available-appointments.page.html',
})
export class AvailableAppointmentsPage extends DownloadingPageComponent implements OnInit {
  public especialidadSeleccionada: Especialidad;
  public medicoSeleccionado: Medico;
  public actoMedicoSeleccionado: ActoMedico;
  public freeAppointments: Cita[] = [];

  /** This is the single pointer to the model data that is contained on this page. This is the first element than when processed with the collaborate2View process will generate the complete list of nodes to render and received by the factory from the getBodyComponents().
	This variable is accessed directly (never been null) and it if shared with all descendans during the generation process. */
  public dataModelRoot: Node[] = [];
  /** The real time updated list of nodes to render. */
  public renderNodeList: Node[] = [];
  // public providerIdentifier: string = '';
  public downloading: boolean = true;
  // private medicos: Medico[] = [];
  public changeDateForm: ChangeDateForm = new ChangeDateForm();
  // public development: boolean = false;

  ngOnInit() {
    console.log('>>[ListAppointmentsPage.ngOnInit]');
    // Read the transferred data from the store.
    this.freeAppointments = this.backendService.accessAppointments();
    this.especialidadSeleccionada = this.appStoreService.accessSelectedEspecialidad();
    this.medicoSeleccionado = this.appStoreService.accessSelectedMedico();
    this.actoMedicoSeleccionado = this.appStoreService.accessSelectedActoMedico();
  }
  /**
   * Start the process to block and reserve the appointment. At this point we have the Medico and the Cita ready so this is the right place to block the appointment and them move to the confirmation page.
   * @param cita the Cita clicked and selected for blocking.
   */
  public citaClick(cita: Cita): void {
    console.log('>>[ListAppointmentsPage.citaClick]');
    // Block the appointment. If the block is confirmed then move to the information page.
    // this.storage.get(environment.CREDENTIAL_KEY)
    //   .then((data) => {
    //     if (null != data) {
    //       console.log('--[PatientRecordPage.ngOnInit]> Patient data: ' + data);
    //       let patientRecord = new PatientCredential(JSON.parse(data));
    let patientRecord = this.appStoreService.accessCredential();
    // Block the complete list of appintment identifiers.
    // this.processAllAppointments(cita.getServiceIdentifierss());
    this.backendService.blockAppointment(patientRecord, cita)
      .subscribe((blockedCita) => {
        console.log('>>[ListAppointmentsPage.citaClick.blockAppointment]> blockedCita: ' + blockedCita);
        // Start the reservation process. Show the reservation page.
        // Store the data in the store for other page access.
        // this.appStoreService.storeSelectedPaciente(patientRecord);
        this.appStoreService.storeSelectedMedico(this.medicoSeleccionado);
        this.appStoreService.storeSelectedCita(cita);
        this.navController.push(AppointmentReservationPage, { 'citaIdentifier': cita.id });
      });
    // }
    // });
    console.log('<<[ListAppointmentsPage.citaClick]');
  }
  // private processAllAppointments(patientRecord: PatientCredential, _appointmentIds: number[]): Observable<Cita> {
  //   let processObservable: Observable<Cita>[] = [];
  //   for (let id in _appointmentIds) {
  //     processObservable.push(this.backendService.blockAppointmentById(patientRecord, Number(id)));
  //   }
  //   return Observable.merge(processObservable);
  // }

  public getWeekDay(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return this.environment.WEEKDAYS[theDate.getDay()];
  }
  public getDateNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getDate();
  }
  public getYearNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getFullYear();
  }
  public getMonthName(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return this.environment.MONTHNAMES[theDate.getMonth()];
  }
  public getDateDate(cita: Cita): Date {
    let theDate = new Date(cita.huecoId * 60 * 1000);
    return theDate;
  }

  //--- F O R M   I N T E R A C T I O N
  public onSubmit() {
    console.log(">>[ListAppointmentsPage.onSubmit]");
    // Get the new date and refresh the list of appointments with the new date data.
    // let searchDate = this.changeDateForm.newDate;
    this.downloading = true;
    // Call the backend to retrieve the list of Doctors to be presented to the Patient
    this.backendService.getAppoinmentList(this.navParams.get('providerIdentifier'), this.changeDateForm)
      .subscribe((citaList: Cita[]) => {
        // Iterate on all the centers to collect the data.
        this.dataModelRoot = [];
        for (let cita of citaList) {
          console.log('--[ListAppointmentsPage.ngOnInit]> Cita: ' + JSON.stringify(cita));
          this.dataModelRoot.push(cita);
        }
        this.notifyDataChanged();
        this.downloading = false;
      });
    console.log("<<[ListAppointmentsPage.onSubmit]");
  }

  //--- C O M P O N E N T   I N T E R A C T I O N   M A N A G E M E N T
  public getAppointmentsList(): Cita[] {
    // Read the list of appointments from the date selector component.
    return this.freeAppointments;
  }
  // public todayInputValue(): string {
  //   return this.changeDateForm.toDateInputValue();
  // }
  // public blurNewDate() {
  //   console.log(">>[ListAppointmentsPage.blurNewDate]");
  //   // Get the new date and refresh the list of appointments with the new date data.
  //   // let searchDate = this.changeDateForm.newDate;
  //   this.downloading = true;
  //   // Call the backend to retrieve the list of Doctors to be presented to the Patient
  //   this.backendService.getAppoinmentList(this.navParams.get('providerIdentifier'), this.changeDateForm)
  //     .subscribe((citaList: Cita[]) => {
  //       // Iterate on all the centers to collect the data.
  //       this.dataModelRoot = [];
  //       for (let cita of citaList) {
  //         console.log('--[ListAppointmentsPage.blurNewDate]> Cita: ' + JSON.stringify(cita));
  //         this.dataModelRoot.push(cita);
  //       }
  //       this.notifyDataChanged();
  //       this.downloading = false;
  //     });
  //   console.log("<<[ListAppointmentsPage.blurNewDate]");
  // }
}
