//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import 'rxjs/add/operator/debounceTime';
//--- MODELS
import { Node } from '../../app/shared/models/core/Node.model';
import { Especialidad } from '../../app/shared/models/Especialidad.model';
import { Medico } from '../../app/shared/models/Medico.model';
import { Cita } from '../../app/shared/models/Cita.model';
import { OpenAppointment } from '../../app/shared/models/OpenAppointment.model';
//--- PAGES
import { SearcheablePageComponent } from '../../app/shared/core/searcheable-page/searcheable-page';

@Component({
  selector: 'available-doctors-page',
  templateUrl: 'available-doctors.html'
})
export class AvailableDoctorsPage extends SearcheablePageComponent implements OnInit {
  public especialidad: Especialidad;
  public openAppointments: OpenAppointment[] = [];
  // public searchTerm: string = '';
  // public searchControl: FormControl;
  public doctors: Medico[] = [];
  // public hierarchicalList: Node[] = [];

  //--- C O N S T R U C T O R
  // constructor(
  //   @Inject(EnvVariables) public environment,
  //   protected navCtrl: NavController,
  //   protected navParams: NavParams,
  //   protected storage: Storage,
  //   protected viewCtrl: ViewController,
  //   protected toasterService: NotificationsService,
  //   protected appStoreService: AppStoreService,
  //   protected backendService: BackendService,
  //   protected loadingCtrl: LoadingController,
  //   protected dataService: DoctorSearchService) {
  //   super(environment, navCtrl, navParams, storage, viewCtrl, toasterService, appStoreService, backendService, loadingCtrl);
  //   this.searchControl = new FormControl();
  // }

  // //--- L I F E C Y C L E
  // ionViewDidLoad() {
  //   this.viewCtrl.setBackButtonText('Especialidades');
  //   this.setFilteredItems();
  //   this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
  //     this.setFilteredItems();
  //   });
  // }
  // public setFilteredItems() {
  //   // If the search term is not empty then we clear the current list and show the new list.
  //   if (this.backendService.isNonEmptyString(this.searchTerm)) {
  //     this.dataModelRoot = this.dataService.filterItems(this.searchTerm);
  //     this.notifyDataChanged();
  //   } else {
  //     this.dataModelRoot = this.hierarchicalList;
  //     this.notifyDataChanged();
  //   }
  // }
  ngOnInit() {
    console.log('>>[AvailableDoctorsPage.ngOnInit]');
    this.especialidad = this.appStoreService.accessSelectedEspecialidad();

    if (null != this.especialidad)
      for (let medico of this.especialidad.getMedicos()) {
        this.dataModelRoot.push(medico);
      }
    this.hierarchicalList = this.dataModelRoot;
    this.dataService.setDoctorList(this.especialidad.getMedicos());
    this.notifyDataChanged();
    // this.dismissLoading();
    console.log('<<[AvailableDoctorsPage.ngOnInit]');
  }
  // ngOnInit2() {
  //   console.log('>>[AvailableDoctorsPage.ngOnInit]');
  //   // Call the backend to retrieve the list of Doctors to be presented to the Patient
  //   // this.presentLoading();
  //   this.backendService.getCredential()
  //     .subscribe((credential) => {
  //       if (credential.isValid()) {
  //         // Go to the backend and download the list of this patient appointments.
  //         this.backendService.downloadPatientAppointments(credential)
  //           .subscribe((appointments) => {
  //             this.openAppointments = appointments;
  //             this.backendService.getMedicalCenters()
  //               .subscribe((centroList: Centro[]) => {
  //                 // Variable to collect the complete list of Medicos.
  //                 let especialidadList = new Map<string, Especialidad>();
  //                 let medicosList: Medico[] = [];
  //                 // Iterate on all the centers to collect the data.
  //                 for (let centro of centroList) {
  //                   console.log('--[AvailableDoctorsPage.ngOnInit]> Centro: ' + JSON.stringify(centro));
  //                   // Get the list pf service providers.
  //                   this.backendService.getMedicalServiceProviders(centro.getId())
  //                     .subscribe((medicoList) => {
  //                       // Process the medical serfice providers and set them on the hierarchy.
  //                       for (let medico of medicoList) {
  //                         // Skip Medicos with no free appointments.
  //                         if (medico.getFreeAppointmentsCount() > 0) {
  //                           // Aggregate providers by specialty.
  //                           let spec = medico.getEspecialidad();
  //                           let hit = especialidadList.get(spec);
  //                           if (null == hit) {
  //                             hit = new Especialidad().setNombre(spec);
  //                             especialidadList.set(spec, hit);
  //                             this.dataModelRoot.push(hit);
  //                           }
  //                           // Check if the Medico has already appointment. Then update the flag.
  //                           for (let appoint of this.openAppointments) {
  //                             if (appoint.getMedico().getId() == medico.getId()) medico.markOpenAppointmentFlag(true);
  //                           }
  //                           hit.addMedico(medico);
  //                           medicosList.push(medico);
  //                         }
  //                         console.log('--[AvailableDoctorsPage.ngOnInit]> Medico: ' + JSON.stringify(medico));
  //                       }
  //                       // Cache the list of providers into the service.
  //                       this.backendService.storeMedicos(medicosList)

  //                       // When all the providers are processed we can add the center to the list.
  //                       // this.dataModelRoot.push(centro);
  //                       this.notifyDataChanged();
  //                     });
  //                 }
  //                 this.notifyDataChanged();
  //                 this.hierarchicalList = this.dataModelRoot;
  //                 this.dataService.setDoctorList(medicosList);
  //                 // this.dismissLoading();
  //               }, (error) => {
  //                 console.log('--[AvailableDoctorsPage.ngOnInit]> Error: ' + JSON.stringify(error));
  //                 // this.dismissLoading();
  //               });
  //           });
  //       }
  //     });
  // }
  // public presentLoading(): void {
  //   this.loader = this.loadingCtrl.create({
  //     content: "Por favor espere..."
  //   });
  //   this.loader.present();
  // }
  // public dismissLoading(): void {
  //   this.loader.dismiss();
  // }
  public itemSelected(item: any) {
    console.log('>>[AvailableDoctorsPage.itemSelected]> Item: ' + item);
  }
  // public medicClick(target: Medico): void {
  //   // Move to the list of appointments with the selected provider.
  //   if (null != target)
  //     if (!target.hasOpenAppointmentFlag) {
  //       // Set the selected Medico to be used on the backend interchange.
  //       this.backendService.storeSelectedMedico(target);
  //       // this.presentLoading();
  //       this.navCtrl.push(AppointmentsCalendarPage, { 'providerIdentifier': target.id });
  //     }
  // }
  public inDevelopment(): boolean {
    return this.backendService.inDevelopment();
  }
  //--- I V I E W E R   I N T E R F A C E
  public applyPolicies(inputs: Especialidad[]): Especialidad[] {
    console.log("><[AvailableDoctorsPage.applyPolicies]> Sort Especialidades alphabetically");
    let sortedContents: Especialidad[] = inputs.sort((n1, n2) => {
      return n1.nombre.localeCompare(n2.nombre);
    });
    return sortedContents;

  }

  //--- T R A N S F O R M A T I O N S
  public getAppointmentDateDisplay(_node: Medico): string {
    // Get the date for the first appointment.
    if (null != _node)
      if (null != _node.getFirstAppointment()) {
        let cita = _node.getFirstAppointment();
        let display = "";
        display = display + this.environment.WEEKDAYS[cita.getFecha().getDay()] + ', ';
        display = display + cita.getFecha().getDate() + ' ' + this.environment.MONTHNAMES[cita.getFecha().getMonth()] + ' ';
        display = display + cita.getFecha().getFullYear();
        return display;
      }
    return '';
  }

  //--- V I E W E R   N O D E   A D A P T E R S
  /**
   * The node iterator over the 'renderNodeList' knows about Nodes. But the methods that belong to other classes are not accesible. With this method I will convert a node to ita right class to get access to the methods.
   * @param  node the abstract Node instance
   * @return      a class converted instance to the Medico class name.
   */
  public getMedico(node: Node): Medico {
    return node as Medico;
  }

  public getWeekDay(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return this.environment.WEEKDAYS[theDate.getDay()];
  }
  public getDateNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getDate();
  }
  public getYearNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getFullYear();
  }
  public getMonthName(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return this.environment.MONTHNAMES[theDate.getMonth()];
  }
  public getDateDate(cita: Cita): Date {
    let theDate = new Date(cita.huecoId * 60 * 1000);
    return theDate;
  }
  // public hasOpenAppointments(node: Medico): boolean {
  //   if (null != node)
  //     return node.ifHasOpenAppointments();
  //   else return false;
  // }
}
