//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import 'rxjs/add/operator/debounceTime';
//--- MODELS
import { Node } from '../../app/shared/models/core/Node.model';
import { Centro } from '../../app/shared/models/Centro.model';
import { Especialidad } from '../../app/shared/models/Especialidad.model';
import { Medico } from '../../app/shared/models/Medico.model';
import { Cita } from '../../app/shared/models/Cita.model';
import { OpenAppointment } from '../../app/shared/models/OpenAppointment.model';
import { PatientCredential } from '../../app/shared/models/PatientCredential.model';
//--- PAGES
import { AvailableSpecialitiesPage } from '../../pages/available-specialities/available-specialities.page';
import { SearcheablePageComponent } from '../../app/shared/core/searcheable-page/searcheable-page';

@Component({
  selector: 'available-locations-page',
  templateUrl: 'available-locations.html'
})
export class AvailableLocationsPage extends SearcheablePageComponent implements OnInit {
  // public development: boolean = false;
  public credential: PatientCredential;
  public openAppointments: OpenAppointment[] = [];
  // public searchTerm: string = '';
  // public searchControl: FormControl;
  public doctors: Medico[] = [];
  // public hierarchicalList: Node[] = [];

  // //--- L I F E C Y C L E
  // ionViewDidLoad() {
  //   this.setFilteredItems();
  //   this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
  //     this.setFilteredItems();
  //   });
  // }
  // public setFilteredItems() {
  //   // If the search term is not empty then we clear the current list and show the new list.
  //   if (this.backendService.isNonEmptyString(this.searchTerm)) {
  //     this.dataModelRoot = this.dataService.filterItems(this.searchTerm);
  //     this.notifyDataChanged();
  //   } else {
  //     this.dataModelRoot = this.hierarchicalList;
  //     this.notifyDataChanged();
  //   }
  // }

  ngOnInit() {
    console.log('>>[AvailableDoctorsPage.ngOnInit]');
    // Call the backend to retrieve the list of Doctors to be presented to the Patient
    this.presentLoading();
    this.credential = this.appStoreService.accessCredential();
    // .subscribe((credential) => {
    //   if (credential.isValid()) {
    // Go to the backend and download the list of this patient appointments.
    this.backendService.downloadPatientAppointments(this.credential)
      .subscribe((appointments) => {
        this.openAppointments = appointments;
        this.backendService.getMedicalCenters()
          .subscribe((centroList: Centro[]) => {
            // Variable to collect the complete list of Medicos.
            let especialidadList = new Map<string, Especialidad>();
            let medicosList: Medico[] = [];
            // Iterate on all the centers to collect the data.
            for (let centro of centroList) {
              console.log('--[AvailableDoctorsPage.ngOnInit]> Centro: ' + JSON.stringify(centro));
              this.dataModelRoot.push(centro);
              this.notifyDataChanged();

              // Get the list pf service providers.
              this.backendService.getMedicalServiceProviders(centro.getId())
                .subscribe((medicoList) => {
                  // Process the medical serfice providers and set them on the hierarchy.
                  for (let medico of medicoList) {
                    // Skip Medicos with no free appointments.
                    if (medico.getFreeAppointmentsCount() > 0) {
                      // Aggregate providers by specialty.
                      let spec = medico.getEspecialidad();
                      let hit = especialidadList.get(spec);
                      if (null == hit) {
                        hit = new Especialidad().setNombre(spec);
                        especialidadList.set(spec, hit);
                        // Add the speciality to the center.
                        centro.addEspecialidad(hit);
                        // this.dataModelRoot.push(hit);
                      }
                      // Check if the Medico has already appointment. Then update the flag.
                      for (let appoint of this.openAppointments) {
                        if (appoint.getMedico().getId() == medico.getId()) medico.markOpenAppointmentFlag(true);
                      }
                      hit.addMedico(medico);
                      medicosList.push(medico);
                    }
                    console.log('--[AvailableDoctorsPage.ngOnInit]> Medico: ' + JSON.stringify(medico));
                  }
                  // Cache the list of providers into the service.
                  // this.backendService.setMedicos(medicosList)

                  // When all the providers are processed we can add the center to the list.
                  // this.dataModelRoot.push(centro);
                  // this.notifyDataChanged();
                });
            }
            this.notifyDataChanged();
            this.hierarchicalList = this.dataModelRoot;
            this.backendService.storeMedicos(medicosList)
            this.dataService.setDoctorList(medicosList);
            this.dismissLoading();
          }, (error) => {
            console.log('--[AvailableDoctorsPage.ngOnInit]> Error: ' + JSON.stringify(error));
            this.dismissLoading();
          });
      });
    // }
    // });
  }
  public itemSelected(item: any) {
    console.log('>>[AvailableDoctorsPage.itemSelected]> Item: ' + item);
  }
  public medicClick(target: Medico): void {
    // Move to the list of appointments with the selected provider.
    // let provider = target as Medico;
    if (null != target)
      if (!target.hasOpenAppointmentFlag)
        this.navController.push(AvailableSpecialitiesPage, { 'providerIdentifier': target.id });
  }
  public inDevelopment(): boolean {
    return this.backendService.inDevelopment();
  }
  //--- I V I E W E R   I N T E R F A C E
  public applyPolicies(inputs: Especialidad[]): Especialidad[] {
    console.log("><[AvailableDoctorsPage.applyPolicies]> Sort Especialidades alphabetically");
    let sortedContents: Especialidad[] = inputs.sort((n1, n2) => {
      return n1.nombre.localeCompare(n2.nombre);
    });
    return sortedContents;

  }

  //--- V I E W E R   N O D E   A D A P T E R S
  /**
   * The node iterator over the 'renderNodeList' knows about Nodes. But the methods that belong to other classes are not accesible. With this method I will convert a node to ita right class to get access to the methods.
   * @param  node the abstract Node instance
   * @return      a class converted instance to the Medico class name.
   */
  public getMedico(node: Node): Medico {
    return node as Medico;
  }

  public getWeekDay(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return this.environment.WEEKDAYS[theDate.getDay()];
  }
  public getDateNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getDate();
  }
  public getYearNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getFullYear();
  }
  public getMonthName(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return this.environment.MONTHNAMES[theDate.getMonth()];
  }
  public getDateDate(cita: Cita): Date {
    let theDate = new Date(cita.huecoId * 60 * 1000);
    return theDate;
  }
}
