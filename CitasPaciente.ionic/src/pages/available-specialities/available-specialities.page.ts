//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import 'rxjs/add/operator/debounceTime';
//--- MODELS
import { Node } from '../../app/shared/models/core/Node.model';
import { Centro } from '../../app/shared/models/Centro.model';
import { Especialidad } from '../../app/shared/models/Especialidad.model';
import { Medico } from '../../app/shared/models/Medico.model';
//--- PAGES
import { AvailableDoctorsPage } from '../../pages/available-doctors/available-doctors.page';
import { AppointmentsCalendarPage } from '../../pages/appointments-calendar/appointments-calendar.page';
import { SearcheablePageComponent } from '../../app/shared/core/searcheable-page/searcheable-page';

// @IonicPage()
@Component({
  selector: 'available-specialities',
  templateUrl: 'available-specialities.html',
})
export class AvailableSpecialitiesPage extends SearcheablePageComponent {
  private _especialidadList: Map<string, Especialidad> = new Map<string, Especialidad>();
  public development: boolean = false;
  //--- SEARCH VARIABLES
  // public searchTerm: string = '';
  // public searchControl: FormControl;
  // public hierarchicalList: Node[] = [];

  //--- C O N S T R U C T O R
  // constructor(
  //   @Inject(EnvVariables) public environment,
  //   protected navCtrl: NavController,
  //   protected navParams: NavParams,
  //   protected storage: Storage,
  //   protected viewCtrl: ViewController,
  //   protected toasterService: NotificationsService,
  //   protected appStoreService: AppStoreService,
  //   protected backendService: BackendService,
  //   protected loadingCtrl: LoadingController,
  //   protected dataService: DoctorSearchService) {
  //   super(environment, navCtrl, navParams, storage, viewCtrl, toasterService, appStoreService, backendService, loadingCtrl);
  //   this.searchControl = new FormControl();
  //   this.development = this.environment.development;
  // }

  //--- L I F E C Y C L E
  // public setFilteredItems() {
  //   // If the search term is not empty then we clear the current list and show the new list.
  //   if (this.backendService.isNonEmptyString(this.searchTerm)) {
  //     this.dataModelRoot = this.dataService.filterItems(this.searchTerm);
  //     this.notifyDataChanged();
  //   } else {
  //     this.dataModelRoot = this.hierarchicalList;
  //     this.notifyDataChanged();
  //   }
  // }
  ionViewDidLoad() {
    super.ionViewDidLoad();
    console.log('>>[AvailableSpecialitiesPage.ionViewDidLoad]');
    this.viewController.setBackButtonText('Inicio');

    // this.setFilteredItems();
    // this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
    //   this.setFilteredItems();
    // });

    // Get the full list of Medicos that are available on the platform. This is done by scanning the Centros available.
    this.presentLoading();
    // Store the credential into the selected Patient.
    // this.storage.get(this.environment.CREDENTIAL_KEY)
    //   .then((data) => {
    //     if (null != data) {
    //       let patientRecord = new PatientCredential(JSON.parse(data));
    // this.appStoreService.accessCredential();
      // .then((patientRecord) => {
      //   this.appStoreService.storeSelectedPaciente(patientRecord);
      // });
    //   }
    // });

    this.backendService.getMedicalCenters()
      .subscribe((centroList: Centro[]) => {
        // Variable to collect the complete list of Medicos.
        this._especialidadList = new Map<string, Especialidad>();
        let medicosList: Medico[] = [];
        // Iterate on all the centers to collect the data.
        for (let centro of centroList) {
          console.log('--[AvailableSpecialitiesPage.ionViewDidLoad]> Centro: ' + centro.getNombre());
          // Get the list pf service providers.
          this.backendService.getMedicalServiceProviders(centro.getId())
            .subscribe((medicoList) => {
              // Process the medical serfice providers and set them on the hierarchy.
              for (let medico of medicoList) {
                // Skip Medicos with no free appointments.
                if (medico.getFreeAppointmentsCount() > 0) {
                  // Aggregate providers by specialty.
                  let spec = medico.getEspecialidad();
                  let hit = this._especialidadList.get(spec);
                  if (null == hit) {
                    hit = new Especialidad().setNombre(spec);
                    this._especialidadList.set(spec, hit);
                    this.dataModelRoot.push(hit);
                    this.notifyDataChanged();
                  }
                  // Check if the Medico has already appointment. Then update the flag.
                  for (let appoint of this.appStoreService.accessOpenAppointments()) {
                    if (appoint.getMedico().getId() == medico.getId()) medico.markOpenAppointmentFlag(true);
                  }
                  hit.addMedico(medico);
                  medicosList.push(medico);
                }
                // console.log('--[AvailableDoctorsPage.ngOnInit]> Medico: ' + JSON.stringify(medico));
              }
              this.hierarchicalList = this.dataModelRoot;
              this.dataService.setDoctorList(medicosList);
            });
        }
        this.dismissLoading();
      }, (error) => {
        console.log('--[AvailableSpecialitiesPage.ionViewDidLoad]> Error: ' + JSON.stringify(error));
        this.dismissLoading();
      });
  }
  public hasOpenAppointments(node: Medico): boolean {
    if (null != node)
      return node.hasOpenAppointmentFlag;
    else return false;
  }

  //--- V I E W E R   N O D E   A D A P T E R S
  /**
   * The node iterator over the 'renderNodeList' knows about Nodes. But the methods that belong to other classes are not accesible. With this method I will convert a node to ita right class to get access to the methods.
   * @param  node the abstract Node instance
   * @return      a class converted instance to the Medico class name.
   */
  public getMedico(node: Node): Medico {
    return node as Medico;
  }
  public medicClick(target: Medico): void {
    // Move to the list of appointments with the selected provider.
    // let provider = target as Medico;
    if (null != target)
      if (!target.hasOpenAppointmentFlag) {
        // Set the selected Medico to be used on the backend interchange.
        this.appStoreService.storeSelectedMedico(target);
        // this.presentLoading();
        this.navController.push(AppointmentsCalendarPage, { 'providerIdentifier': target.id });
      }
  }

  //--- T R A N S F O R M A T I O N S
  public getAppointmentDateDisplay(_node: Medico): string {
    // Get the date for the first appointment.
    if (null != _node)
      if (null != _node.getFirstAppointment()) {
        let cita = _node.getFirstAppointment();
        let display = "";
        display = display + this.environment.WEEKDAYS[cita.getFecha().getDay()] + ', ';
        display = display + cita.getFecha().getDate() + ' ' + this.environment.MONTHNAMES[cita.getFecha().getMonth()] + ' ';
        display = display + cita.getFecha().getFullYear();
        return display;
      }
    return '';
  }

  //--- P A G E   E V E N T S
  public selectSpeciality(_node: Especialidad): void {
    // Jump to the list of Medicos for this Especialidad. Do the interchange on the backend service.
    this.appStoreService.storeSelectedEspecialidad(_node);
    // this.presentLoading();
    this.navController.push(AvailableDoctorsPage, { 'especialidad': _node.nombre });
  }

  //--- I V I E W E R   I N T E R F A C E
  public applyPolicies(inputs: Especialidad[]): Especialidad[] {
    console.log("><[AvailableDoctorsPage.applyPolicies]> Sort Especialidades alphabetically");
    let sortedContents: Especialidad[] = inputs.sort((n1, n2) => {
      return n1.nombre.localeCompare(n2.nombre);
    });
    return sortedContents;
  }
}
