//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- CORE
import { Component } from '@angular/core';
// import { Inject } from '@angular/core';
// // --- ENVIRONMENT
// import { EnvVariables } from '../environment-plugin/environment-plugin.token';
// //--- IONIC
// import { NavController } from 'ionic-angular';
// import { NavParams } from 'ionic-angular';
// import { Storage } from '@ionic/storage';
// // import { LoadingController } from 'ionic-angular';
// import { ViewController } from 'ionic-angular';
// //--- NOTIFICATIONS
// import { NotificationsService } from 'angular2-notifications';
// //--- SERVICES
// import { BackendService } from '../app/shared/services/backend.service';
// //--- INTERFACES
// // import { IViewer } from '../../app/shared/interfaces/core/IViewer.interface';
// //--- MODELS
// import { Node } from '../app/shared/models/core/Node.model';
import { MVCViewerComponent } from '../app/shared/core/mvcviewer/mvcviewer.component';


/**
This is the core code that is shared by all pages. Implements most of the code to deal with the model list data and the external request for data to be rendered on the UI.
*/
@Component({
  selector: 'notused-base-page',
  templateUrl: './notused.html',
})
export class BasePageComponent extends MVCViewerComponent {
  // /** This is the single pointer to the model data that is contained on this page. This is the first element than when processed with the collaborate2View process will generate the complete list of nodes to render and received by the factory from the getBodyComponents().
	// This variable is accessed directly (never been null) and it if shared with all descendans during the generation process. */
  // public dataModelRoot: Node[] = [];
  // /** The real time updated list of nodes to render. */
  // public renderNodeList: Node[] = [];

  // //--- C O N S T R U C T O R
  // constructor(
  //   @Inject(EnvVariables) public environment,
  //   protected navCtrl: NavController,
  //   protected navParams: NavParams,
  //   protected storage: Storage,
  //   protected viewCtrl: ViewController,
  //   protected toasterService: NotificationsService,
  //   protected backendService: BackendService) {
  // }

  // //--- I V I E W E R   I N T E R F A C E
  // /**
  // Return the reference to the component that knows how to locate the Page to transmit the refresh events when any user action needs to update the UI.
  // */
  // public getViewer(): IViewer {
  //   return this;
  // }
  // /**
  //   Reconstructs the list of nodes to be rendered from the current DataRoot and their collaborations to the view.
  //   */
  // public notifyDataChanged(): void {
  //   console.log(">>[BasePageComponent.notifyDataChanged]");
  //   let copyList = [];
  //   // Get the initial list by applying the policies defined at the page to the initial root node contents. Policies may be sorting or filtering actions.
  //   let initialList = this.applyPolicies(this.dataModelRoot);
  //   // Generate the contents by collaborating to the view all the nodes.
  //   for (let node of initialList) {
  //     let nodes = node.collaborate2View();
  //     console.log("--[BasePageComponent.notifyDataChanged]> Collaborating " + nodes.length + " nodes.");
  //     // Add the collaborated nodes to the list of nodes to return.
  //     for (let childNode of nodes) {
  //       copyList.push(childNode);
  //     }
  //   }
  //   this.renderNodeList = copyList;
  //   console.log("<<[BasePageComponent.notifyDataChanged]");
  // }
  // public applyPolicies(inputs: Node[]): Node[] {
  //   return inputs;
  // }
}
