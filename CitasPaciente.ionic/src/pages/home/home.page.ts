//  PROJECT:     CitasPaciente.ionic(CP.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Ionic 3.2.0
//  DESCRIPTION: CitasPaciente. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proveedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { EnvVariables } from '../../environment-plugin/environment-plugin.token';
// --- IONIC
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// --- NOTIFICACIONES
import { NotificationsService } from 'angular2-notifications';
// --- SERVICES
import { AppStoreService } from '../../app/shared/services/appstore.service';
import { BackendService } from '../../app/shared/services/backend.service';
//--- MODELS
import { PatientCredential } from '../../app/shared/models/PatientCredential.model';
//--- PAGES
import { AceptacionCondicionesPage } from '../../pages/aceptacioncondiciones/aceptacioncondiciones.page';
import { AvailableLocationsPage } from '../../pages/available-locations/available-locations.page';
import { AvailableSpecialitiesPage } from '../../pages/available-specialities/available-specialities.page';
import { IonicPageComponent } from '../../app/shared/core/ionic-page/ionic-page.component';

@Component({
  selector: 'home-page',
  templateUrl: 'home.page.html'
})
export class HomePage extends IonicPageComponent implements OnInit {
  // public processing: boolean = true; // This flag is true while the page content and destination is being calculated
  // public credentialValid: boolean = false;
  public credential: PatientCredential = new PatientCredential();

  // - C O N S T R U C T O R
  constructor(
    @Inject(EnvVariables) public environment,
    protected navController: NavController,
    protected navParams: NavParams,
    protected viewController: ViewController,
    protected toasterService: NotificationsService,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService,
    protected storage: Storage,
    protected loadingController: LoadingController) {
    super(environment, navController, navParams, viewController, toasterService, appStoreService, backendService);
  }

  // - L I F E C Y C L E
  ngOnInit() {
    console.log('>>[HomePage.ngOnInit]');
    // Update the menus than can have changed at other pages.
    this.appStoreService.updateActiveMenu();
    //   // this.processing = true;
    //   // this.presentLoading();
    //   // this.credentialValid = false;
    //   // Read the credential for the storage, store a copy on the main patient data and check it is valid.
    this.credential = this.appStoreService.accessCredential();
    //   // this.storage.get(this.environment.CREDENTIAL_KEY)
    //   //   .then((credentialData) => {
    //   // try {
    //   //   if (null != credentialData) {
    //   //     this.credential = new PatientCredential(JSON.parse(credentialData));
    //   //     // Store the credential and copy the validity.
    //   //     this.appStoreService.storeCredential(this.credential);
    //   // this.credentialValid = this.credential.isValid();
    //   // }
    //   // this.processing = false;
    //   // this.dismissLoading();
    //   // } catch(Exception) {
    //   //   console.log('--[HomePage.ngOnInit] Exception: ' + Exception.message);
    //   // }
    //   // });
  }
  public isCredentialValid(): boolean {
    return this.credential.isValid();
  }
  // ngOnInit2() {
  //   console.log('>>[HomePage.ngOnInit]');
  //   // Show an spinner telling we are processing credential data.
  //   this.processing = true;
  //   this.presentLoading();
  //   // Check if there is Patient data available. If not hide the card and show the creation button.
  //   // this.appStoreService.accessFutureCredential()
  //   //   .subscribe((credential) => {
  //   //     console.log('--[HomePage.ngOnInit.accessFutureCredential] Credential received. State: ' + credential.isValid());
  //   //     this.credential = credential;
  //   //     this.credentialValid = this.credential.isValid();
  //   //     console.log('--[HomePage.ngOnInit.accessFutureCredential]> Credential state: ', this.credentialValid);
  //   //     // Signal the termination of the processing and then show the result.
  //   //     this.processing = false;
  //   //     this.dismissLoading();
  //   //   }, (error) => {
  //   //     console.log('--[HomePage.ngOnInit.accessFutureCredential]> Error: ' + JSON.stringify(error));
  //   //     this.dismissLoading();
  //   //   });
  //   console.log('<<[HomePage.ngOnInit]');
  // }

  //--- I N T E R F A C E   A C T I O N S
  public openCredentialForm(): void {
    console.log('>>[HomePage.openCredentialForm]');
    this.navController.push(AceptacionCondicionesPage);
    console.log('<<[HomePage.openCredentialForm]');
  }
  public newAppointment(): void {
    console.log('>>[HomePage.newAppointment]');
    this.navController.push(AvailableSpecialitiesPage);
    console.log('<<[HomePage.newAppointment]');
  }
  public newAppointmentByLocation(): void {
    console.log('>>[HomePage.newAppointmentByLocation]');
    this.navController.push(AvailableLocationsPage);
    console.log('<<[HomePage.newAppointmentByLocation]');
  }
  public clearCredential(): void {
    console.log('>>[HomePage.clearCredential]');
    let credential = new PatientCredential();
    this.storage.set(this.environment.CREDENTIAL_KEY, JSON.stringify(credential));
    // this.storage.set(this.environment.ACEPTACIONCONDICIONES_KEY, "NO-ACEPTADA");
    // this.credential = new PatientCredential();
    // this.credentialValid = false;
    this.appStoreService.storeCredential(credential);
    console.log('<<[HomePage.clearCredential]');
  }

  //--- L O A D   N O T I F I CA T I O N   I N T E R F A C E
  // protected presentLoading(): void {
  //   this._loader = this.loadingController.create({
  //     content: 'Verificando credenciales del paciente. Por favor espere...'
  //   });
  //   this._loader.present();
  // }
  // protected dismissLoading(): void {
  //   if (null != this._loader) this._loader.dismiss();
  // }
}
