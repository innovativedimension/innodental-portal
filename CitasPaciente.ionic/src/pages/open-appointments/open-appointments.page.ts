//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- INTERFACES
import { IDataSource } from '../../app/shared/interfaces/core/IDataSource.interface';
import { INode } from '../../app/shared/interfaces/core/INode.interface';
//--- MODELS
import { OpenAppointment } from '../../app/shared/models/OpenAppointment.model';
// import { PatientCredential } from '../../app/shared/models/PatientCredential.model';
//--- PAGES
import { IonicPageComponent } from '../../app/shared/core/ionic-page/ionic-page.component';
import { Cita } from '../../app/shared/models/Cita.model';

@Component({
  selector: 'open-appointments-page',
  templateUrl: 'open-appointments.page.html'
})
export class OpenAppointmentsPage extends IonicPageComponent implements OnInit, IDataSource {
  // public credential: PatientCredential;

  //--- L I F E C Y C L E
  ngOnInit() {
    console.log('>>[OpenAppointmentsPage.ngOnInit]');
    // Read and load the list of open appointments.
    // this.backendService.getCredential()
    //   .subscribe((credential) => {
    //     if (credential.isValid()) {
    //       this.credential = credential;
    // Go to the backend and download the list of this patient appointments.
    this.downloadOpenAppointments();
    //   }
    // });
  }

  //--- I V I E W E R   I N T E R F A C E
  public applyPolicies(inputs: OpenAppointment[]): OpenAppointment[] {
    console.log("><[OpenAppointmentsPage.applyPolicies]> Sort OpenAppointment by next action time");
    let sortedContents: OpenAppointment[] = inputs.sort((n1, n2) => {
      let timeDiff = n1.getCita().getFecha().getTime() - n2.getCita().getFecha().getTime();
      if (timeDiff > 0) return 1;
      if (timeDiff < 0) return -1;
      return 0;
    });
    return sortedContents;
  }

  //--- V I E W E R   N O D E   A D A P T E R S
  public getOpenAppointmentsCount(): number {
    return this.dataModelRoot.length;
  }
  public getDataSource(): IDataSource {
    return this;
  }

  //--- I D A T A S O U R C E   I N T E R F A C E
  public removeNode(_node: INode): boolean {
    // Reload again the list of open appointments after the database appointment is canceled.
    let appointment = _node as OpenAppointment;
    let especialidad = appointment.getMedico().getEspecialidad();
    this.toasterService.success("CITA CANCELADA", "La cita de " + especialidad + " ha sido cancelada satisfactoriamente.");
    // Reaload the data from the backend to update all the structures.
    this.downloadOpenAppointments();
    return true;
  }
  public updateNode(_node: INode): boolean {
    return true;
  }

  //--- P R O T E C T E D   S E C T I O N
  /**
   * Open appoinments are ordered by date ascending. It is supposed that there is no possibility to get appointments at the same time from different doctors so consecutive appointments should be one next to the other.
   * We keep the last appointment referenced so if we get another fror the same doctor and same date we are sure this is a pack.
   *
   * @protected
   * @memberof OpenAppointmentsPage
   */
  protected downloadOpenAppointments(): void {
    this.backendService.downloadPatientAppointments(this.appStoreService.accessCredential())
      .subscribe((appointments) => {
        // Check for duplicated appointmes or marked as free.
        this.dataModelRoot = [];
        let activeAppointment: Cita;
        for (let appo of appointments) {
          console.log("--[OpenAppointmentsPage.downloadOpenAppointments]> Appointment: " + appo.getId() +
            ' state: ' + appo.getEstado()+' medico: '+appo.getCita().medicoIdentifier);
          // Check if the current appointment is active.
          if (null == activeAppointment) {
            activeAppointment = appo.getCita();
            if (appo.getCita().estado == 'RESERVADA')
              if (appo.isOpen()) this.dataModelRoot.push(appo);
          } else {
            // Check for consecutive appointments.
            if (appo.getCita().medicoIdentifier == activeAppointment.medicoIdentifier) {
              // I am almost sure I can pack it. Check the date.
              if (appo.getCita().fecha === activeAppointment.fecha) {
                // Pack it.
                activeAppointment.packNewAppointment(appo.getCita());
                continue;
              }
            }
            if (appo.getCita().estado == 'RESERVADA')
            if (appo.isOpen()) this.dataModelRoot.push(appo);
          }
        }
        this.appStoreService.storeOpenAppointments(this.dataModelRoot as OpenAppointment[]);
        this.notifyDataChanged();
      });
  }
}
