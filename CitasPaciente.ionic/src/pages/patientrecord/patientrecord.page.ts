//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- FORMS
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
//--- IONIC
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// import { Sim } from '@ionic-native/sim';
//--- SERVICES
import { AppStoreService } from '../../app/shared/services/appstore.service';
import { BackendService } from '../../app/shared/services/backend.service';
//--- MODELS
import { PatientCredential } from '../../app/shared/models/PatientCredential.model';
//--- PAGES
// import { CommonPage } from '../../pages/common/common';
import { HomePage } from '../../pages/home/home.page';

@Component({
  selector: 'patient-record-page',
  templateUrl: 'patientrecord.page.html'
})
export class PatientRecordPage implements OnInit {
  public patientRecord: PatientCredential = new PatientCredential();
  public nifvalido: boolean = false; // Stores the nif validation value.
  public credentialForm: FormGroup;
  public aseguradoras: string[] = [];

  constructor(
    protected navCtrl: NavController,
    protected storage: Storage,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) {
  }

  ngOnInit() {
    console.log('>>[PatientRecordPage.ngOnInit]');
    // Initialize form controls and validation structures.
    this.credentialForm = new FormGroup({
      tipoIdentificador: new FormControl(''),
      identificador: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(30)]),
      tefContacto: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(14)]),
      nombre: new FormControl('', [Validators.minLength(3)]),
      apellidos: new FormControl(''),
      email: new FormControl(''),
      aseguradora: new FormControl('')
    });
    // Read the list of insirance corporations.
    this.backendService.propertiesAseguradoras()
      .subscribe((_aseguradoras) => {
        this.aseguradoras = _aseguradoras;
      })
    // Read the current state of the patient record.
    this.patientRecord = this.appStoreService.accessCredential();
  }
  //--- F O R M   V A L I D A T I O N
  public blurField(): void {
    // Save the current data on the storage and check if the record is valid.
    this.appStoreService.storeCredential(this.patientRecord);
  }
  public blurIdentificador(): void {
    console.log('>>[PatientRecordPage.blurIdentificador]');
    // Validate that this field if a valif NIF NIE.
    if (this.patientRecord.identificador.length > 8) {
      // Convert the NOF to uppercase.
      this.patientRecord.identificador = this.patientRecord.identificador.toUpperCase();
      // Validate the current NIF.
      this.nifvalido = this.appStoreService.validateNIF(this.patientRecord.identificador);
    } else this.nifvalido = false;
    if (this.nifvalido)
      this.appStoreService.storeCredential(this.patientRecord);
    console.log('<<[PatientRecordPage.blurIdentificador]');
  }
  public blurTefContacto(): void {
    console.log('>>[PatientRecordPage.blurTefContacto]');
    // Save the current data on the storage and check if the record is valid.
    console.log('--[PatientRecordPage.blurTefContacto]> Patient data: ' + JSON.stringify(this.patientRecord));
    this.appStoreService.storeCredential(this.patientRecord);
    console.log('<<>[PatientRecordPage.blurTefContacto]');
  }
  public blurNombre(): void {
    console.log('>>[PatientRecordPage.blurNombre]');
    // Save the current data on the storage and check if the record is valid.
    console.log('--[PatientRecordPage.blurNombre]> Patient data: ' + JSON.stringify(this.patientRecord));
    this.appStoreService.storeCredential(this.patientRecord);
    console.log('<<[PatientRecordPage.blurNombre]');
  }
  public blurEmail(): void {
    console.log('>>[PatientRecordPage.blurEmail]');
    // Save the current data on the storage and check if the record is valid.
    console.log('--[PatientRecordPage.blurEmail]> Patient data: ' + JSON.stringify(this.patientRecord));
    this.appStoreService.storeCredential(this.patientRecord);
    console.log('<<[PatientRecordPage.blurEmail]');
  }

  public savePatientData(): void {
    this.appStoreService.storeCredential(this.patientRecord);
    this.navCtrl.setRoot(HomePage);
  }
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }
  public isValid(): boolean {
    if (null != this.patientRecord) return this.patientRecord.isValid();
    else return false;
  }
  public test4Valid(field: string): boolean {
    if (!this.isNonEmptyString(field)) return false;
    else return true;
  }
}
