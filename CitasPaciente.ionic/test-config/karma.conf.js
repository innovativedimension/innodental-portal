var webpackConfig = require('./webpack.test.js');

module.exports = function (config) {
  var _config = {
    basePath: '../',
    frameworks: ['jasmine'],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    reporters: config.coverage ? ['kjhtml', 'dots', 'coverage-istanbul'] : ['kjhtml', 'dots'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    /** * maximum number of tries a browser will attempt in the case of a disconnection */
    browserDisconnectTolerance: 6,
    /** * How long will Karma wait for a message from a browser before disconnecting from it (in ms). */
    browserNoActivityTimeout: 120000,
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../coverage'),
      reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true,
      thresholds: {
        statements: 30,
        lines: 30,
        branches: 30,
        functions: 30
      }
    },
    singleRun: false,
    webpack: webpackConfig,
    files: [
      {
        pattern: './test-config/karma-test-shim.js',
        watched: true
      },
      {
        pattern: './src/assets/**/*',
        watched: false,
        included: false,
        served: true,
        nocache: false
      }
    ],
    proxies: {
      '/assets/': '/base/src/assets/'
    },
    preprocessors: {
      './test-config/karma-test-shim.js': ['webpack', 'sourcemap']
    },
    webpackMiddleware: {
      stats: 'errors-only'
    },
    webpackServer: {
      noInfo: true
    },
    browserConsoleLogOptions: {
      level: 'log',
      format: '%b %T: %m',
      terminal: true
    }
  };

  config.set(_config);
};
