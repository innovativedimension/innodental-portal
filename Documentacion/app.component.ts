//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { EnvVariables } from '../environment-plugin/environment-plugin.token';
//--- IONIC
import { Nav } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
//--- SERVICES
import { BackendService } from '../services/backend.service';
//--- MODELS
import { Page } from '../models/core/Page.model';
//--- PAGES
import { TestPage } from './pages/test.page';
import { HomePage } from '../pages/home/home.page';
import { AceptacionCondicionesPage } from '../pages/aceptacioncondiciones/aceptacioncondiciones.page';
import { OpenAppointmentsPage } from '../pages/open-appointments/open-appointments.page';
import { AvailableLocationsPage } from '../pages/available-locations/available-locations.page';
import { AvailableSpecialitiesPage } from '../pages/available-specialities/available-specialities.page';
import { AppStoreService } from '../services/appstore.service';
import { PatientCredential } from '../models/PatientCredential.model';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  public rootPage: any = HomePage;
  private _pages: Page[] = [];
  public options = {
    position: ["bottom", "right"],
    showProgressBar: false,
    pauseOnHover: true,
    timeOut: 5000,
    animate: "fade",
    lastOnBottom: true,
    preventDuplicates: true,
    theClass: "rounded"
  }

  constructor(
    @Inject(EnvVariables) public environment,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    protected storage: Storage,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) {
    // Show the spash screen.
    this.splashScreen.show();
    this.preInitialization();
  }

  protected preInitialization(): void {
    console.log('>>[MyApp.preInitialization]');
    this.platform.ready().then(() => {
      this.storage.get(this.environment.CREDENTIAL_KEY)
        .then((credentialData) => {
          // If the data is valid save a copy on the cache.
          if (null != credentialData) {
            try {
              let cred = new PatientCredential(JSON.parse(credentialData));
              this.appStoreService.storeCredential(cred);
              this.initializeApp();
            } catch (CredentialError) {
              // Error parsing the credential. Maybe changes from version. Remove from the storage.
              console.log('>>[MyApp.preInitialization]> Exception: ' + CredentialError.message);
              this.storage.remove(this.environment.CREDENTIAL_KEY);
              this.initializeApp();
            }
          } else this.initializeApp();
        });
    });
  }
  public initializeApp() {
    console.log('>>[MyApp.initializeApp]');
    // Fire the initialization and load of the credential if available on the storage.
    // this.backendService.getCredential()
    //   .subscribe((credential) => {
    //       this.backendService.storeCredential(credential);
    let credential = this.appStoreService.accessCredential();
    if (credential.isValid()) {
      this.updateAppPages();
      // Check if thre are any appointment open. If so change the root page to the list of open appointments.
      this.backendService.downloadPatientAppointments(credential)
        .subscribe((appointments) => {
          // Check for duplicated appointmes or marked as free.
          let open = [];
          for (let appo of appointments) {
            if (appo.getCita().estado == 'RESERVADA') {
              // Check if the appointment is still open and in the future.
              if (appo.isOpen()) {
                open.push(appo);
                this.rootPage = OpenAppointmentsPage;
              }
            }
          }
          // Store the appointments for late use.
          this.backendService.storeOpenAppointments(open);
          // Okay, so the platform is ready and our plugins are available.
          // Here you can do any higher level native things you might need.
          this.statusBar.styleDefault();
          this.splashScreen.hide();
          this.nav.setRoot(this.rootPage);
        });
    } else {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.updateAppPages();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    }
    // });
    console.log('<<[MyApp.initializeApp]');
  }
  //--- I N I T I A L I Z A T I O N
  protected updateAppPages() {
    // console.log('>>[MyApp.updateAppPages]');
    // Add the ever available pages.
    this._pages = [];
    this._pages.push(new Page().setTitle('Test')
      .setPageComponent(TestPage)
      .setIconName('ios-home'));
    this._pages.push(new Page().setTitle('Inicio')
      .setPageComponent(HomePage)
      .setIconName('ios-home'));
    this._pages.push(new Page().setTitle('Datos Paciente')
      .setPageComponent(AceptacionCondicionesPage)
      .setIconName('ios-person'));
    // Add the search pages only if the credential is valid.
    let credential = this.appStoreService.accessCredential();
    if (credential.isValid()) {
      this.backendService.storeCredential(credential);
      this._pages.push(new Page().setTitle('Medicos por Especialidad')
        .setPageComponent(AvailableSpecialitiesPage)
        .setIconName('ios-contacts'));
      this._pages.push(new Page().setTitle('Localizaciones')
        .setPageComponent(AvailableLocationsPage)
        .setIconName('navigate'));
      // Add the Citas page after the Medicos only if the Credential is valid.
      this._pages.push(new Page().setTitle('Citas Abiertas')
        .setPageComponent(OpenAppointmentsPage)
        .setIconName('md-calendar'));
    }
    // console.log('<<[MyApp.updateAppPages]');
  }
  protected accesspages(): Page[] {
    this.updateAppPages();
    // Clear the center data to signal we can search any of the already doenlaoded data.
    this.backendService.storeCentros([]);
    return this._pages;
  }

  public openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  public getPages(): Page[] {
    return this.accesspages();
  }
  public created() { }
  public destroyed() { }
}
