﻿
DROP TABLE citamed.citas CASCADE;
DROP TABLE citamed.medicos;
DROP TABLE citamed.centros;

DROP SEQUENCE citamed.centros_sequence;

DROP SCHEMA citamed CASCADE;

CREATE SCHEMA citamed
  AUTHORIZATION postgres;

GRANT ALL ON SCHEMA citamed TO postgres;
GRANT ALL ON SCHEMA citamed TO public;
COMMENT ON SCHEMA citamed
  IS 'Schema para el S1 del aplicativo CitaMed';

  