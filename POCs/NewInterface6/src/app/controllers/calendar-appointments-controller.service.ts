//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - CORE
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Observable } from "rxjs/internal/Observable";
// - SERVICES
import { AppStoreService } from "@app/services/appstore.service";
// import { BackendService } from "@app/services/backend.service";
// - INTERFACES
import { IAppointmentEnabled } from "@interfaces/IAppointmentEnabled.interface";
// - MODELS
import { Cita } from "@models/Cita.model";
import { Subject } from "rxjs/Subject";

@Injectable({
  providedIn: 'root'
})
export class CalendarAppointmentsControllerService {
  private _appointmentsSource = new Subject();
  private appointments = this._appointmentsSource.asObservable() as Observable<Cita[]>;

  // private appointments: Cita[] = [];
  // - C O N S T R U C T O R
  constructor(
    protected appStoreService: AppStoreService) { }

  // - B A C K E N D   A C C E S S
  public downloadAppointments4Target(_target: IAppointmentEnabled): void {
    console.log(">>[CalendarAppointmentsControllerService.downloadAppointments4Target]");
    this._appointmentsSource.next([]);
  }
  // - A P P O I N T M E N T S
  public accessAppointments(): Observable<Cita[]> {
    return this.appointments;
  }
}