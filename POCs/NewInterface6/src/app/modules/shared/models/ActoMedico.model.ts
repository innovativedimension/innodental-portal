//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- MODELS
import { Node } from './core/Node.model';
// import { Medico } from './Medico.model';

export class ActoMedico extends Node {
  public etiqueta: string = "M";
  public nombre: string = "Primera Cita";
  public tiempoRequerido: number = 30;

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "ActoMedico";
  }

  //--- I A P P O I N T M E N T E N A B L E D   I N T E R F A C E
  //--- G E T T E R S   &   S E T T E R S
  // - I C O M P A R A B L E   I N T E R F A C E
  public equals(_target: ActoMedico): boolean {
    let localContent = this.etiqueta + ":" + this.nombre + ":" + this.tiempoRequerido;
    let targetContent = _target.etiqueta + ":" + _target.nombre + ":" + _target.tiempoRequerido;
    if (localContent.toLowerCase() === targetContent.toLowerCase()) return true;
    else return false;
  }
}
