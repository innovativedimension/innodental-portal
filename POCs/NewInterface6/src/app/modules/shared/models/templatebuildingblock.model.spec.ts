//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - MODELS
import { TemplateBuildingBlock } from '@models/TemplateBuildingBlock.model';

describe('MODEL TemplateBuildingBlock [Module: MODELS]', () => {
  let model: TemplateBuildingBlock;

  beforeEach(() => {
    model = new TemplateBuildingBlock();
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('model/TemplateBuildingBlock Construction Phase', () => {
    it('should create an instance', () => {
      console.log('><[model/TemplateBuildingBlock]> should create an instance');
      expect(model).toBeDefined('instance has not been created.');
    });
    it('Constructor transformations', () => {
      console.log('><[model/TemplateBuildingBlock]> Constructor transformations');
      let alterModel = new TemplateBuildingBlock({
        nombre: 'Nombre Test',
        duracion: 30,
        tipo: '-PRUEBAS-',
        estado: 'PRUEBAS'
      });
      expect(alterModel.nombre).toBe('Nombre Test');
      expect(alterModel.duracion).toBe(30);
      expect(alterModel.tipo).toBe('-PRUEBAS-');
      expect(alterModel.estado).toBe('PRUEBAS');
    });
    it('Fields should be on initial state', () => {
      console.log('><[model/TemplateBuildingBlock]> "nombre" should be "Template"');
      expect(model.nombre).toBe('Template');
      console.log('><[model/TemplateBuildingBlock]> "duracion" should be 15');
      expect(model.duracion).toBe(15);
      console.log('><[model/TemplateBuildingBlock]> "tipo" should be "-NORMAL-"');
      expect(model.tipo).toBe('-NORMAL-');
      console.log('><[model/TemplateBuildingBlock]> "estado" should be "LIBRE"');
      expect(model.estado).toBe('LIBRE');
    });
  });

  // - G E T T E R S / S E T T E R S   P H A S E
  describe('model/TemplateBuildingBlock Getters/Setters Phase', () => {
    it('Getters/Setters: duracion', () => {
      console.log('><[model/TemplateBuildingBlock]> Getters/Setters: duracion');
      expect(model.duracion).toBe(15);
      let alterModel = model.setDuracion(30);
      expect(alterModel).toBeDefined();
      expect(model.duracion).toBe(30);
      expect(model.getDuracion()).toBe(30);
    });
  });
});