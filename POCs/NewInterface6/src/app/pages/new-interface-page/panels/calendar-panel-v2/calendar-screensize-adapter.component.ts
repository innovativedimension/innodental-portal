//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - CORE
import { Component } from '@angular/core';
import { HostListener } from "@angular/core";
// - ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CalendarCorev2Component } from './calendar-corev2.component';
// import { CalendarCorev2Component } from '@app/pages/citaciones-page/panels/calendar-panel-v2/calendar-corev2.component';
// import { CalendarCorev2Component } from '../calendar-corev2/calendar-corev2.component';
// - SERVICES
// import { AppStoreService } from '@app/services/appstore.service';
// import { BackendService } from '@app/services/backend.service';
// - COMPONENTS
// import { CalendarCoreComponent } from '@app/modules/ui/calendar-core/calendar-core.component';
// import { CalendarCorev2Component } from '../calendar-core/calendar-core.component';

const MAX_MONTH_ROWS: number = 6;
const CALENDAR_HEADER_SIZE: number = 210;

@Component({
  selector: 'x-calendar-screen-size-adapter',
  templateUrl: './notused.html'
})

export class CalendarScreenSizeAdapter extends CalendarCorev2Component {
  public screenHeight: any;
  public screenWidth: any;
  public cellWidth: number = 74;
  public cellHeight: number = 78;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    console.log(this.screenHeight, this.screenWidth);
    // Calculate the cell height.
    this.cellHeight = (this.screenHeight - CALENDAR_HEADER_SIZE) / MAX_MONTH_ROWS;
  }
}