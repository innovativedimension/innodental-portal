// - CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { HostListener } from '@angular/core';
import { ElementRef } from '@angular/core';
// - ANIMATIONS
import { style } from '@angular/animations';
import { state } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { trigger } from '@angular/animations';
// - SERVICES
import { AvailableServicesController } from '@app/controllers/available-services-controller.service';
// - MODELS
import { Medico } from '@models/Medico.model';
// import { MVCViewerComponent } from '@app/modules/ui/mvcviewer/mvcviewer.component';

@Component({
  selector: 'gs-service-selection-panel',
  templateUrl: './service-selection-panel.component.html',
  styleUrls: ['./service-selection-panel.component.scss'],
  animations: [
    trigger('expandCollapse', [
      state('collapsed', style({
        color: 'black'
      })),
      state('expanded', style({
        color: 'black'
      })),
      transition('collapsed => expanded', [
        animate('1.0s 100ms ease-in')
      ]),
      transition('expanded => collapsed', [
        animate('0.5s 100ms ease-in')
      ]),
    ])
  ]
})
export class ServiceSelectionPanelComponent implements OnInit {
  @Input() activeService: Medico = new Medico();
  @Output() serviceSelected = new EventEmitter<Medico>();
  @Input() dup: boolean = true;
  public openSelection: boolean = false; // A 'true' means that the service selection is open  for selection.
  public serviceList: Medico[] = [];

  // - C O N S T R U C T O R
  constructor(protected serviceController: AvailableServicesController) { }

  // - L I F E C Y C L E
  ngOnInit() {
    console.log(">>[ServiceSelectionPanelComponent.ngOnInit]");
    this.serviceController.accessAvailableServices(this.dup)
      .subscribe((services) => {
        // Put the service list into the rendering list.
        this.serviceList = services;
        console.log("<<[ServiceSelectionPanelComponent.ngOnInit]> Services: " + this.serviceList.length);
      })
    console.log("<<[ServiceSelectionPanelComponent.ngOnInit]");
  }

  // - G E T T E R S   &   S E T T E R S
  public getSelectedService(): Medico {
    return this.activeService;
  }

  // - V I E W   I N T E R A C T I O N
  /**
   * Activates the service/doctor selection by expanding the current service to a list of the authorised
   * services for this user.
   *
   * @memberof ServiceSelectionPanelComponent
   */
  public activateSelection() {
    this.openSelection = true;
  }
  public doSelection(_service: Medico) {
    if (null != _service) {
      this.openSelection = false;
      // Emit the selected service.
      this.serviceSelected.emit(_service);
    }
  }
}
