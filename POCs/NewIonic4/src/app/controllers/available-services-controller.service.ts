import { Observable } from "rxjs/internal/Observable";
import { Medico } from "@models/Medico.model";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AvailableServicesController {
  public accessAvailableServices(_dup: boolean): Observable<Medico[]> {
    return Observable.create((observer) => {
      let medlist: Medico[] = [];
      for (let i = 1; i < 6; i++) {
        medlist.push(new Medico().setNombre('Nombre ' + i));
      }
      if (_dup) {
        medlist = medlist.concat(medlist);
        medlist = medlist.concat(medlist);
        medlist = medlist.concat(medlist);
      }
      observer.next(medlist);
      observer.complete();
    });

  }
}