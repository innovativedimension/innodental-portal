//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - MODELS
import { Cita } from './Cita.model';

export class Payload {
  public free: number = 0; // Stores the number of appointments free
  public reserved: number = 0; // This accounts for the already reserved appointments
  public vacancy: number = 0; // This stores the appointments in the VACACIONES state
  public confirmed: number = 0; // This field stores the appointments already confirmed
  public citas: Cita[] = [];

  // - C O N S T R U C T O R
  constructor(values: Object = {}) {
    Object.assign(this, values);

    // Transform dependency objects
    if (null != this.citas) {
      let listCitas = [];
      for (let ct of this.citas) {
        listCitas.push(new Cita(ct));
      }
      this.citas = listCitas;
    }
  }

  // - G E T T E R S   &   S E T T E R S
  public getOpen(): number {
    return this.citas.length;
  }
  public getReserved(): number {
    return this.reserved;
  }
  public getVacancy(): number {
    return this.vacancy;
  }
  public getConfirmed(): number {
    return this.confirmed;
  }
  public getFree(): number {
    return this.free;
  }
  public getTotal(): number {
    return this.getOpen();
  }
  public addCita(_newcita: Cita): void {
    this.citas.push(_newcita);
    // Do the appointment type accounting.
    if (_newcita.getEstado() === 'LIBRE')
      this.free++;
    else if (_newcita.getEstado() === 'VACACIONES')
      this.vacancy++;
    else if (_newcita.getEstado() === 'CONFIRMADA')
      this.confirmed++;
    else if (_newcita.patientIsValid())
      this.reserved++;
  }
}
