import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewInterfacePageComponent } from './pages/new-interface-page/new-interface-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  { path: 'dashboard', component: NewInterfacePageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
