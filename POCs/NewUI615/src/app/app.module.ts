import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewInterfacePageComponent } from './pages/new-interface-page/new-interface-page.component';
import { CalendarModule } from 'angular-calendar';
// import { CalendarScreenSizeAdapter } from './pages/new-interface-page/panels/calendar-panel-v2/calendar-screensize-adapter.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { CalendarScreenSizeAdapter } from './components/calendar/calendar-screensize-adapter.component';

import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { AppPanelComponent } from './components/calendar/app-panel.component';
import { CalendarCorev2Component } from './components/calendar/calendar-corev2.component';
registerLocaleData(localeEs);

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    CalendarModule.forRoot()
  ],
  declarations: [
    AppComponent,
    NewInterfacePageComponent,
    AppPanelComponent,
    CalendarCorev2Component,
    CalendarScreenSizeAdapter,
    CalendarComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
