// - CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
// import { ISubscription } from 'rxjs/Subscription';
import { isBefore } from 'date-fns';
// - COMPONENTS
// import { CalendarCoreComponent } from '@app/modules/ui/calendar-core/calendar-core.component';
// import { IAppointmentEnabled } from '@interfaces/IAppointmentEnabled.interface';
// - MODELS
// import { Cita } from '@models/Cita.model';
// import { Payload } from '@models/Payload.model';
import { CalendarScreenSizeAdapter } from './calendar-screensize-adapter.component';
// import { Subject } from 'rxjs/Subject';
// import { Cita } from '@models/Cita.model';
// import { AppStoreService } from '@app/services/appstore.service';
// import { CalendarAppointmentsControllerService } from '@app/controllers/calendar-appointments-controller.service';
import { Subject } from 'rxjs/internal/Subject';
import { CalendarAppointmentsControllerService } from 'src/app/controllers/calendar-appointments-controller.service';
import { AppStoreService } from 'src/app/services/appstore.service';
import { Cita } from 'src/app/modules/shared/models/Cita.model';
import { SubscriptionLike } from 'rxjs/internal/types';
/**
 * The Calendar Panl will show in a month calendar style panel all the appointments that belong to the time period to 
 * be shown. The calendar cell contents depends on the template configured to the cell.
 *
 * @input
 * @export
 * @class CalendarPanelV2Component
 * @implements {OnInit}
 */
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent extends CalendarScreenSizeAdapter implements OnInit, OnDestroy {
  @Input() container: any;
  @Input() title: string;

  @Input() source: any; // This is the source of appointments to be used on this calendar
  @Output() daySelectedOutput = new EventEmitter<any>(); // Output the dfate selected by the user.
  private _appointmentsSubscription: SubscriptionLike; // Subscription observable to the list of appointments from the source identifiers.
  private filters: any = []; // This stores the list of appointment filters to apply to the downloaded list.

  public refresh: Subject<any> = new Subject();

  // - C O N S T R U C T O R
  constructor(
    // protected router: Router,
    // protected activeRoute: ActivatedRoute,
    protected appStoreService: AppStoreService,
    protected appointmentsController: CalendarAppointmentsControllerService) {
    super(appStoreService, appointmentsController);
    this.getScreenSize();
  }

  // - L I F E C Y C L E
  ngOnInit() {
    console.log(">>[CalendarPanelV2Component.ngOnInit]");
    // Define the filters.
    this.filters.push({
      name: 'appointment < now',
      description: 'Filter appointment in the past',
      filter: (_target: Cita): boolean => {
        return isBefore(_target.getFecha(), new Date());
      }
    });
    this.downloading = true;
    // Get the current list of appointments from the subject. Order by date/time and then filter.
    this._appointmentsSubscription = this.appointmentsController.accessAppointments()
      .subscribe((appointmentList: Cita[]) => {
        let citas = this.processAppointments(this.filters, appointmentList);
        this.processEvents();
        this.appStoreService.infoNotification("Descarga de citas disponibles completado.", "Completado");
        this.downloading = false;
      });
    // Fire the download for the appointments
    this.appointmentsController.downloadAppointments4Target(this.source);
    console.log("<<[CalendarPanelV2Component.ngOnInit]");
  }
  ngOnDestroy() {
    if (null != this._appointmentsSubscription) this._appointmentsSubscription.unsubscribe();
  }

  // - I C I T A S C A L E N D A R P A N E L   I N T E R F A C E
  /**
   * This method processes the click on a calendar date cell.
   * There are to actions. If the Control key is down then the click arrives then we go to the parent 
   * (CalendarCoreComponent) functionality that should add or remove the cell from the selection. If the Control
   * is not down then check if the cell has appointments. If it has then output the <day> selected.
   * A <day> is an classified structure that is associated to each of the calendar cells. In the Citas
   * implementation the day, apart from the predefined fields will include a <Payload> instance that has the
   * date we need for the appointment management.
   *
   * @param _event the angular event that is received by the calendar cell. This event containt a <day>
   */
  public dayClicked(_event: any): void {
    console.log("><[AppointmentDateSelectorComponent.dayClicked]> event: " + JSON.stringify(_event));
    // Check the Control key.
    // if (_event.ctrlKey) return super.dayClicked(_event);
    // Extract the payload.
    let payload = this.accessPayload(_event);
    if (null != payload) {
      // Check the content to see if it has appointments
      // this.clearSelection();
      this.daySelectedOutput.emit(_event.day);
    }
  }
  public getSource(): any {
    return this.source;
  }

  // - P`R I V A T E   F U N C T I O N A L I T Y
  protected accessPayload(_event: any): any {
    // if (this.isNotEmpty(_event))
    //   if (this.isNotEmpty(_event.day))
    return _event.day['payload'];
  }
}
