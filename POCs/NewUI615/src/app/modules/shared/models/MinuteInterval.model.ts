//  PROJECT:     CitaMed.lib(CITM.LIB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Endless Dimensions, all rights reserved.
//  ENVIRONMENT: Angular 6.1 Library
//  DESCRIPTION: CitaMed. Componente libreria. Este projecto contiene gran parte del código Typescript que puede
//               ser reutilizado en otros aplicativos del mismo sistema (CitaMed) o inclusive en otros
//               desarrollos por ser parte de la plataforma MVC de despliegue de nodos extensibles y
//               interacciones con elementos seleccionables.
//--- MODELS
import { Node } from './core/Node.model';
import { Cita } from './Cita.model';

export class MinuteInterval extends Node {
  public startMinute: number = 0;
  public endMinute: number = 0;
  public payload: Cita = null;

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Node";

    // Transform non native fields.
    if (null != this.payload) {
      let appointment = new Cita(this.payload);
      this.payload = appointment;
    }
  }
  public getState(): string {
    if (null != this.payload) return this.payload.estado;
    else "-INDEFINIDO-";
  }
  public getCentroName(): string {
    if (null != this.payload) return this.payload.centroName;
    else "-INDEFINIDO-";
  }
}
