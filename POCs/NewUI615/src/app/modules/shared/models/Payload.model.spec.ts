//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 3.9
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// - MODELS
import { Payload } from "./Payload.model";
import { Cita } from "./Cita.model";

describe('MODEL Payload [Module: MODELS]', () => {
  let model: Payload;

  beforeEach(() => {
    model = new Payload();
  });

  // - C O N S T R U C T I O N   P H A S E
  describe('Construction Phase', () => {
    it('should create an instance', () => {
      console.log('><[model/Payload]> should create an instance');
      expect(model).toBeDefined('instance has not been created.');
    });
    it('Constructor transformations', () => {
      console.log('><[model/Payload]> Constructor transformations');
      let alterModel = new Payload({
        reserved: 10,
        vacancy: 3,
        confirmed: 0,
        citas: []
      });
      expect(alterModel.reserved).toBe(10);
      expect(alterModel.vacancy).toBe(3);
      expect(alterModel.citas).toBeDefined();
    });
  });

  // - G E T T E R S / S E T T E R S   P H A S E
  describe('Getters/Setters Phase', () => {
    it('Getters: free', () => {
      console.log('><[model/Payload]> Getters: free');
      model = new Payload({
        free: 20,
        reserved: 10,
        vacancy: 3,
        confirmed: 0,
        citas: []
      });
      expect(model.getFree()).toBe(20, 'the value should be 10');
    });
    it('Getters: reserved', () => {
      console.log('><[model/Payload]> Getters: reserved');
      model = new Payload({
        reserved: 10,
        vacancy: 3,
        confirmed: 0,
        citas: []
      });
      expect(model.getReserved()).toBe(10, 'the value should be 10');
    });
    it('Getters: vacancy', () => {
      console.log('><[model/Payload]> Getters: vacancy');
      model = new Payload({
        reserved: 10,
        vacancy: 3,
        confirmed: 0,
        citas: []
      });
      expect(model.getVacancy()).toBe(3, 'the value should be 3');
    });
    it('Getters: confirmed', () => {
      console.log('><[model/Payload]> Getters: confirmed');
      model = new Payload({
        reserved: 10,
        vacancy: 3,
        confirmed: 2,
        citas: []
      });
      expect(model.getConfirmed()).toBe(2, 'the value should be 2');
    });
    it('Getters: open', () => {
      console.log('><[model/Payload]> Getters: open');
      model = new Payload({
        reserved: 10,
        vacancy: 3,
        confirmed: 2,
        citas: [{
          jsonClass: 'Cita'
        }, {
          jsonClass: 'Cita'
        }]
      });
      expect(model.getOpen()).toBe(2, 'the value should be 2');
      expect(model.getTotal()).toBe(2, 'the value should be 2');
    });
  });

  // - C O D E   C O V E R A G E   P H A S E
  describe('Code coverage Phase [addCita]', () => {
    it('addCita: adding appointment to the payload', () => {
      console.log('><[model/Payload]> addCita: adding appointment to the payload');
      model = new Payload();
      model.addCita(new Cita().setEstado('LIBRE'));
      model.addCita(new Cita().setEstado('VACACIONES'));
      let cita = new Cita().setEstado('RESERVDA');
      cita.pacienteLocalizador = '12345678'
      model.addCita(cita);
      model.addCita(new Cita().setEstado('CONFIRMADA'));
      expect(model.getTotal()).toBe(4, 'the total appointments should be 4');
      expect(model.getFree()).toBe(1, 'the free appointments should be 1');
      expect(model.getReserved()).toBe(1, 'the reserved appointments should be 1');
      expect(model.getVacancy()).toBe(1, 'the vacancy appointments should be 1');
      expect(model.getConfirmed()).toBe(1, 'the confirmed appointmenes should be 1');
    });
  });
});
