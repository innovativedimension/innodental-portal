import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewInterfacePageComponent } from './new-interface-page.component';

describe('NewInterfacePageComponent', () => {
  let component: NewInterfacePageComponent;
  let fixture: ComponentFixture<NewInterfacePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewInterfacePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewInterfacePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
