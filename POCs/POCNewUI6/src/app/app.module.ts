//  PROJECT:     CitasMedico.frontend(CMF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citasmedico.com
//  DESCRIPTION: CitasMedico. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
//--- CORE MODULES
import { NgModule } from '@angular/core';
//--- BROWSER & ANIMATIONS
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BootstrapModalModule } from 'ng6-bootstrap-modal';
//--- HTTP CLIENT
import { HttpClientModule } from '@angular/common/http';
// --- NOTIFICATIONS
import { ToastrModule } from 'ng6-toastr-notifications';
//--- DRAG & DROP
import { NgDragDropModule } from 'ng-drag-drop';
//--- WEBSTORAGE
import { StorageServiceModule } from 'angular-webstorage-service';
//--- CALENDAR
// import { CalendarModule } from 'angular-calendar';
//--- IONIC
// import { IonicModule } from 'ionic-angular';
//--- ROUTING
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
//--- SERVICES
// import { AppStoreService } from '@app/services/appstore.service';
// import { BackendService } from './services/backend.service';
// import { ModalService } from '@app/services/modal.service';
//--- COMPONENTS-CORE
import { AppComponent } from './app.component';

//--- APPLICATION MODULES
// import { AuthorizationModule } from '@app/modules/authorization/authorization.module';
// import { UIModule } from './modules/ui/ui.module';
// import { CenterAdminModule } from '@app/modules/center-admin/center-admin.module';
// import { CitacionesModule } from './modules/citaciones/citaciones.module';
// import { GestionServicioModule } from '@app/modules/gestion-servicio/gestion-servicio.module';
// import { LoginModule } from '@app/modules/login/login.module';
// //--- PAGES
// import { NotFoundPage } from './pages/not-found-page/not-found-page.component';

// import { MedicCredentialFormPageComponent } from './pages/medic-credential-form-page/medic-credential-form-page.component';
// import { CitaCreationTemplatePageComponent } from './pages/cita-creation-template-page/cita-creation-template-page.component';
// import { CitaCreationReportPage } from './pages/cita-creation-report-page/cita-creation-report-page.component';
//--- COMPONENTS
// import { AppointmentDateReportComponent } from './components/appointment-date-report/appointment-date-report.component';
// import { TimeSlotComponent } from './components/time-slot/time-slot.component';

import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common'
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.compone;
import { NewInterfacePageComponent } from './pages/new-interface-page/new-interface-page.component'nt';
registerLocaleData(localeEs);

// --- ERROR INTERCEPTION
// import * as Rollbar from 'rollbar';
// import { rollbarConfig } from '@app/rollbar-errorhandler.service';
// import { RollbarService } from '@app/rollbar-errorhandler.service';
// import { ErrorHandler } from '@angular/core';
// import { RollbarErrorHandler } from '@app/rollbar-errorhandler.service';
// import { IsolationService } from '@app/platform/isolation.service';
// import { Angular6BackendService } from '@app/platform/angular6-backend.service';
// export function rollbarFactory() {
//   return new Rollbar(rollbarConfig);
// }

@NgModule({
  imports: [
    //--- BROWSER & ANIMATIONS
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    BootstrapModalModule,
    //--- HTTP CLIENT
    HttpClientModule,
    //--- NOTIFICATIONS
    ToastrModule.forRoot(),
    //--- DRAG & DROP
    NgDragDropModule.forRoot(),
    //--- WEBSTORAGE
    StorageServiceModule,
    //--- CALENDAR
    // CalendarModule.forRoot(),
    //--- MATERIAL
    // MatDatepickerModule,
    //--- TABS
    // TabsModule.forRoot(),
    //--- IONIC
    // IonicModule.forRoot(AppModule, {
    //   backButtonText: 'Retroceder',
    //   iconMode: 'ios',
    //   modalEnter: 'modal-slide-in',
    //   modalLeave: 'modal-slide-out',
    //   tabsPlacement: 'bottom',
    //   pageTransition: 'ios-transition'
    // }),
    // CacheModule.forRoot(),
    //--- APPLICATION LIBRARIES
    // CitaMedLibModule,
    // CitaMedLibModule.forRoot(environment),
    //--- APPLICATION MODULES
    // AuthorizationModule,
    // LoginModule,
    // UIModule,
    // SharedModule,
    // CenterAdminModule,
    // CitasModule,
    // LibModule,
    // CitacionesModule,
    // GestionServicioModule,
    //--- ROUTING
    RouterModule,
    AppRoutingModule
  ],
  declarations: [
    //--- COMPONENTS-CORE
    AppComponent,
    DashboardPageCompon,
    NewInterfacePageComponentent
    //--- PAGES
    // NotFoundPage,
    //--- PANELS
    // MedicCredentialFormPageComponent,
    // CitaCreationTemplatePageComponent,
    // CitaCreationReportPage,
    // //--- COMPONENTS
    // AppointmentDateReportComponent,
    // TimeSlotComponent
  ],
  providers: [
    //--- SERVICES
    // IsolationService,
    // AppStoreService,
    // BackendService,
    // // { provide: BackendService, useClass: Angular6BackendService },
    // // --- ERROR INTERCEPTION
    // // { provide: ErrorHandler, useClass: RollbarErrorHandler },
    // // { provide: RollbarService, useFactory: rollbarFactory },
    // ModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
