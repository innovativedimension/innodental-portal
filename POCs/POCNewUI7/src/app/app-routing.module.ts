import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CitacionesPageComponent } from './pages/citaciones-page/citaciones-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/new',
    pathMatch: 'full'
  },
  { path: 'new', component: CitacionesPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
