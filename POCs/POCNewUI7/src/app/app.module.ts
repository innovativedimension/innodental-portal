import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CitacionesPageComponent } from './pages/citaciones-page/citaciones-page.component';
import { IonicModule } from '@ionic/angular';
// import { SharedModule } from './modules/shared/shared.module';
import { CalendarModule } from 'angular-calendar';
import { CalendarScreenSizeAdapter } from './pages/citaciones-page/panels/calendar-panel-v2/calendar-screensize-adapter.component';
import { CalendarPanelV2Component } from './pages/citaciones-page/panels/calendar-panel-v2/calendar-panel-v2.component';
import { AppStoreService } from './services/appstore.service';

import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeEs);

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    IonicModule.forRoot(),
    CalendarModule.forRoot()
    // SharedModule
  ],
  declarations: [
    AppComponent,
    CitacionesPageComponent,
    CalendarPanelV2Component
  ],
  providers: [
    AppStoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
