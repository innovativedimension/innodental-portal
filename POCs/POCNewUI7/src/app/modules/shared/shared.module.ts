//  PROJECT:     CitasCentro.frontend(CCF.A6+I)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018, 2019 by Endless Dimensions Ltd., all rights reserved.
//  ENVIRONMENT: Angular 6.0 + Ionic 4.0
//  SITE:        citascentro.com
//  DESCRIPTION: CitasCentro. Sistema S2. Aplicación Angular modular para acceder a las funcionalidades de
//               administracion de calendarios de servicios, gestion de citaciones, gestión de recursos
//               y administración del sistema en general.
//               Este sistema utiliza como backend el sistema S1 para almacenar los datos de las citaciones.
// --- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// --- IONIC
import { IonicModule } from '@ionic/angular';

// --- RENDERS
// import { RenderComponent } from '@renders/render/render.component';
// import { ActoMedicoRenderComponent } from '@renders/acto-medico/acto-medico.component';
// import { Cita4TemplateRenderComponent } from '@renders/cita4-template/cita4-template.component';
// import { EspecialidadRenderComponent } from '@renders/especialidad/especialidad.component';
// import { MedicoRenderComponent } from '@renders/medico/medico.component';
// import { CitaRenderV1Component } from '@renders/citav1/cita.component';
// import { CitaRenderV2Component } from '@renders/citav2/cita.component';
// import { DatosPacienteRenderComponent } from '@renders/datos-paciente/datos-paciente.component';
// import { TemplateBlockRenderComponent } from '@renders/template-block-render/template-block-render.component';
// import { TemplateRenderComponent } from '@renders/template-render/template-render.component';
// import { Appointment4ViewComponent } from '@renders/appointment4-view/appointment4-view.component';
// import { Appointment4CitationsRenderComponent } from '@renders/appointment4-citations/appointment4-citations.component';
// import { ActoMedicoCentroRenderComponent } from '@renders/acto-medico-centro/acto-medico-centro.component';
// import { ServiceSelectedRenderComponent } from './renders/service-selected-render/service-selected-render.component';

@NgModule({
  imports: [
    CommonModule,
    IonicModule
  ],
  declarations: [
    // --- RENDERS
    // RenderComponent,
    // ActoMedicoRenderComponent,
    // ActoMedicoCentroRenderComponent,
    // CitaRenderV1Component,
    // CitaRenderV2Component,
    // Cita4TemplateRenderComponent,
    // DatosPacienteRenderComponent,
    // EspecialidadRenderComponent,
    // MedicoRenderComponent,
    // TemplateBlockRenderComponent,
    // TemplateRenderComponent,
    // Appointment4ViewComponent,
    // Appointment4CitationsRenderComponent,
    // ServiceSelectedRenderComponent
  ],
  exports: [
    // --- RENDERS
    // RenderComponent,
    // ActoMedicoRenderComponent,
    // ActoMedicoCentroRenderComponent,
    // CitaRenderV1Component,
    // CitaRenderV2Component,
    // Cita4TemplateRenderComponent,
    // DatosPacienteRenderComponent,
    // EspecialidadRenderComponent,
    // MedicoRenderComponent,
    // TemplateBlockRenderComponent,
    // TemplateRenderComponent,
    // Appointment4ViewComponent,
    // Appointment4CitationsRenderComponent,
    // ServiceSelectedRenderComponent
  ]

})
export class SharedModule { }
