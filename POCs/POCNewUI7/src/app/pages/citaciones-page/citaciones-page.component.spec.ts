import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitacionesPageComponent } from './citaciones-page.component';

describe('CitacionesPageComponent', () => {
  let component: CitacionesPageComponent;
  let fixture: ComponentFixture<CitacionesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitacionesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitacionesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
