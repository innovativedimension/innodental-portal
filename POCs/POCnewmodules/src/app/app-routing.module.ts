import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationsTestPageComponent } from '@app/pages/notifications-test-page/notifications-test-page.component';

const routes: Routes = [
  { path: 'login', component: NotificationsTestPageComponent },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
