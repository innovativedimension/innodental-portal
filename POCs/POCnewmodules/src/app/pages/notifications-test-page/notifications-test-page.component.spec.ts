import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsTestPageComponent } from './notifications-test-page.component';

describe('NotificationsTestPageComponent', () => {
  let component: NotificationsTestPageComponent;
  let fixture: ComponentFixture<NotificationsTestPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationsTestPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationsTestPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
