import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng6-toastr';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'notifications-test-page',
  templateUrl: './notifications-test-page.component.html',
  styleUrls: ['./notifications-test-page.component.scss']
})
export class NotificationsTestPageComponent implements OnInit {

  constructor(
    protected notifier: ToastsManager,
    protected toaster: ToastrManager,
    protected vcr: ViewContainerRef) {
    this.notifier.setRootViewContainerRef(vcr);
  }

  ngOnInit() {

  }
  onFireNotifications(): void {
    console.log(">>[NotificationsTestPageComponent.onFireNotifications]");
    console.log("--[NotificationsTestPageComponent.onFireNotifications]> vcr: " + this.vcr);
    this.showSuccess();
    this.showError();
    this.showWarning();
    this.showInfo();
    this.showCustom();
    console.log("<<[NotificationsTestPageComponent.onFireNotifications]");
  }
  showSuccess() {
    this.toaster.successToastr('You are awesome!', 'Success!');
  }

  showError() {
    this.toaster.errorToastr('This is not good!', 'Oops!', { dismiss: 'click' });
  }

  showWarning() {
    this.toaster.warningToastr('You are being warned.', 'Alert!');
  }

  showInfo() {
    this.toaster.infoToastr('Just some information for you.');
  }

  showCustom() {
    this.toaster.customToastr('<span style="color: red">Message in red.</span>', null, { enableHTML: true });
  }
}
